<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Levels;
use App\Models\SubLevels;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class LevelsController extends Controller
{
    public function list(Request $request){
        $loginUser = \Auth::user();
    	if(@$request->type){
    		Levels::destroy($request->id);
    		return redirect()->back()->with('status','Successfully Deleted.');
    	}
    	if($request->isMethod('GET')){
            $role = $loginUser->getRole()->get();
    		return view("admin_pages.levels.list",compact('data','role'));
    	}
        if($request->isMethod('POST')){
            $roles = $request->all();
            unset($roles['_token']);
            foreach ($roles as $key => $value) {
                $per = $value["percentage"];
                $price = $value["price"];
                DB::update("update roles set percentage = $per, level_price = $price where slug = '$key'");
                DB::update("update levels set percentage_amount = $per where level_type = '$key'");
            }
            return redirect()->back()->with('status','Successfully Updated.');
        }
    }

    public function save(Request $request,SubLevels $sublevels){
    	$users = \Auth::user();
    	if($request->isMethod('GET')) {
    		$sub_level = $data = NULL;
    		if($request->id){
    			$data = Levels::where('id','=',$request->id)->first();
    			$sub_level = $data->hasManylevels()->get()->toArray();
    		}
            $roles = $users->getRole()->get();
    		$states = $users->getStates();
    		return view("admin_pages.levels.save",compact('data','states','sub_level','roles'));
    	}

    	if($request->isMethod('POST')){
    		$this->validate($request, $this->validationRules($request));
            $type = explode("@", $request->levels);
    		$levels = new Levels;
    		$levels->name = trim($request->name);
    		$levels->state_id = $request->states;
    		$levels->level_type = $type[0];
    		$levels->percentage_amount = $type[1];
    		$levels->bdm_percentage_amount = $request->bdm_percentage_amount;
    		$levels->save(); 
    		$this->subLevelEntries($request,$levels->id);

    	}
    	if($request->isMethod('PUT')){
    		$levels = Levels::find($request->id);
    		$levels->name = trim($request->name);
    		//$levels->percentage_amount = $request->percentage_amount;
    		$levels->bdm_percentage_amount = $request->bdm_percentage_amount;
    		$levels->save();
    	}
    	return redirect('admin/levels')->with('status','Successfully Saved.');
    }

    function subLevelEntries($request, $last_id){
    	$typeArr = ['sector', 'metro', 'region'];
        $type = explode("@", $request->levels);
    	if(in_array($type[0], $typeArr)){
    		$loop = $request->district;
    		foreach($loop as $key => $val){
	    		$sublevel = new SubLevels;
	    		$sublevel->level_id = $last_id;
	    		//$sublevel->state_id = $request->states;
	    		$sublevel->district_id = $val;
	    		$sublevel->save();
	    	}
    	}else if($type[0] == 'district' || $type[0] == 'taluka' || $type[0] == 'counter'){

            $levelAllocate = new \App\Models\LevelAllocated;
            $levelAllocate->id = \Uuid::generate()->string;
            $levelAllocate->type = $type[0];
            if($type[0] == 'district')
                $levelAllocate->type_id = $request->district[0];
            else if($type[0] == 'taluka')
                $levelAllocate->type_id = $request->taluka[0];
            else
                $levelAllocate->type_id = $last_id;

            $levelAllocate->level_id = $last_id;
            $levelAllocate->status = 0;
            $levelAllocate->save();

    		$loop = $request->taluka;
    		foreach($loop as $key => $val){
	    		$sublevel = new SubLevels;
	    		$sublevel->level_id = $last_id;
	    		//$sublevel->state_id = $request->states;
	    		$sublevel->district_id = $request->district[0];
	    		$sublevel->taluka_id = $val;
	    		$sublevel->save();
	    	}
    	}
    }

    public function search(Request $request){
        $users = \Auth::user();
        $states = $users->getStates();
        $data = [];

        if($request->isMethod('GET')) {
            return view("admin_pages.levels.search",compact('data','states'));
        }
    }

    public function json(Request $request){

        if(@$request->states){
            $records = Levels::getAllData();
            $where_states = (@$request->states)?$request->states:NULL;
            $records = $records->whereIn('l.level_type',[$request->levels])
                      ->whereIn('l.state_id',[$where_states])
                      ->get();
        }else{
           //dd($request->state());
           $records = Levels::getData()->get();
    	   //$records = Levels::with('userBdm')->get();
            
        }
    	return Datatables::of($records)->make(true);
    }

    public function validationRules($request){
    	$validation['name'] = "required";
    	$validation['levels'] = "required";
    	$validation['states'] = "required";
    	//$validation['percentage_amount'] = "required|numeric";
    	$validation['bdm_percentage_amount'] = "required|numeric";

    	if($request->district)
    		$validation['district'] = "required";

    	if($request->taluka)
    		$validation['taluka'] = "required";

    	return $validation;
    }

    public function validationMessages($module = NULL){
        return [
            'module_name.required'     	    =>      "The Module Name field is required",
            'parent_product_id.unique'      =>      "The ".$module->name." already created with this product.",
            'product.required'              =>      "Atleast add one product",
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Models\User;
use App\Notifications\NewUser;
use App\Events\Statement;

class UsersController extends Controller
{
    public $levelUserRole = FALSE;
    public function list(Request $request){
    	if(@$request->type){
            $deleteuser = User::find($request->id);
    		$deleteuser->status = 'suspend';
            $deleteuser->save();
            $deleteuser->delete();
    		return redirect()->back()->with('status','Successfully Deleted.');
    	}
    	if($request->isMethod('GET')){
    		return view("admin_pages.users.list",compact('data'));
    	}
    }

    public function save(Request $request){
    	$users = \Auth::user();
        if($request->isMethod('GET')) {
    	    $roles = $users->getRole();
            $sub_level = $data = NULL;
            $states = $users->getStates();
            $rechargeData = null;
            $bank = \App\Models\BankDetails::where('status','=',1)->get();
            if($request->id){
                $levels = \App\Models\Levels::getAllData();

                $levelsFirst = $levels->where('l.assigned_user_id','=', $request->id);

                $levels2 = 0;
                if($levelsFirst->count() == 0){
                    $levels2 = \App\Models\Levels::getAllData()->whereNull('l.archived_at')->get();
                }else{
                    $levels = $levelsFirst->first();
                }

                $data = User::whereNull('deleted_at')->find($request->id);
                if(!$data)
                    return redirect()->back()->with('status','This user was deleted.');
                $rechargeData = $data->hasManyRecharge()->orderBy('created_at','ASC')->first();
    			return view("admin_pages.users.edit",compact('data','states','roles','bank','rechargeData','levels','levels2'));
    		}else{
    			return view("admin_pages.users.create",compact('data','states','roles','bank','rechargeData'));
    		}
    	}

        $this->levelUserRole =  $users->getRole()->where('display_in_levels','=',1)->where('id','=',$request->roles_id)->get()->count();
    	if($request->isMethod('POST')){

    		$this->validate($request, $this->validationRules($request));
            //dd(date("Y-m-d h:i:s",strtotime($request->dob)));
            $user = new User;

            // 9 = BDM ROLE
            if($this->levelUserRole || $request->roles_id == 9){
                $files = userDocuments($request);
                $user->passbook_copy = $files["passbook_copy"];
                $user->passport_photo = $files["passport_photo"];
                $user->address_proof = $files["address_proof"];
                $user->id_proof = $files["id_proof"];

                $user->nominee_name = trim($request->nominee_name);
                $user->relationship = trim($request->relationship);

                $user->bank_ac_no = trim($request->bank_ac_no);
                $user->bank_ac_holder = trim($request->bank_ac_holder);
                $user->bank_name = trim($request->bank_name);
                $user->ifsc_code = trim($request->ifsc_code);
            }
            
    		$user->first_name = trim($request->first_name);
    		$user->middle_name = trim($request->middle_name);
    		$user->last_name = trim($request->last_name);
    		$user->name = $request->first_name." ".$request->last_name;
    		$user->father_husband_name = trim($request->father_husband_name);
    		$user->email = trim($request->email);
    		$user->present_address = trim($request->present_address);
    		$user->city = $request->city;
    		$user->pincode = $request->pincode;
    		$user->alternate_mobile = (@clean($request->alternate_mobile))?clean($request->alternate_mobile):NULL;
    		$user->primary_mobile = clean($request->primary_mobile);
    		$user->sex = clean($request->sex);
    		$user->marital_status = clean($request->marital_status);
    		$user->dob = date("Y-m-d h:i:s",strtotime($request->dob));
    		$user->password = bcrypt(clean($request->primary_mobile));
    		$user->roles_id = $request->roles_id;
    		$user->status = $request->user_status;
    		$user->state_id = $request->state_id;

            if(!$this->levelUserRole || $request->roles_id == 9)
                $user->available_balance = $request->available_balance;
    		
            $user->created_by = $users->id;
    		$user->save();

    		$user = User::find($user->id);
    		$user->employee_code = "ETAX".$user->id;
    		$user->save();

            if($this->levelUserRole)
                $this->makeRecharge($request,$user);

            $statements["user_id"]=$users->id;
            $statements["to_user_id"]=$user->id;
            $statements["descriptions"]="Initial Recharge";
            $statements["price"]= $request->available_balance;
            $statements["type"]="credited";
            event(new Statement($statements));

            $body['type'] = "account-created";
            $body['id'] = $users->id;
            $body['message'] = 'Your account as been created by admin.';
            $user->notify(new NewUser($user,$body));
    	}

    	if($request->isMethod('PUT')){
            $this->validate($request, $this->validationRules($request));
    		$user = User::find($request->id);
            $files = userDocuments($request);

            // 9 = BDM Role
            if($this->levelUserRole || $request->roles_id == 9){
                if(@$files["passbook_copy"])
                $user->passbook_copy = @$files["passbook_copy"];

                if(@$files["passport_photo"])
                    $user->passport_photo = @$files["passport_photo"];
                
                if(@$files["address_proof"])
                    $user->address_proof = @$files["address_proof"];
                
                if(@$files["id_proof"])
                    $user->id_proof = @$files["id_proof"];

                $user->nominee_name = trim($request->nominee_name);
                $user->relationship = trim($request->relationship);

                $user->bank_ac_no = trim($request->bank_ac_no);
                $user->bank_ac_holder = trim($request->bank_ac_holder);
                $user->bank_name = trim($request->bank_name);
                $user->ifsc_code = trim($request->ifsc_code);
            }

            /*$user->email = trim($request->email);
            $user->primary_mobile = clean($request->primary_mobile);*/

            $user->first_name = trim($request->first_name);
            $user->middle_name = trim($request->middle_name);
            $user->last_name = trim($request->last_name);
            $user->name = $request->first_name." ".$request->last_name;
            $user->father_husband_name = trim($request->father_husband_name);
            $user->present_address = trim($request->present_address);
            $user->city = $request->city;
            $user->pincode = $request->pincode;
            $user->alternate_mobile = clean($request->alternate_mobile);
            $user->sex = clean($request->sex);
            $user->marital_status = clean($request->marital_status);
            $user->dob = date("Y-m-d h:i:s",strtotime($request->dob));

            if($request->roles_id == 9)
                $user->available_balance = $request->available_balance;

            //$user->roles_id = $request->roles_id;
            $user->status = $request->user_status;
            $user->state_id = $request->state_id;
            $user->reject_reason = $request->reject_reason;
            $user->save();

            if(@$request->level_id && $user->status == "active"){
                $assignedLevel = \App\Models\Levels::where('id','=',$request->level_id)->first();
                $assignedLevel->assigned_user_id = $user->id;
                $assignedLevel->allocated_status = 1;
                $assignedLevel->archived_at = date("Y-m-d h:i:s");
                $assignedLevel->save();

                $sublevels = $assignedLevel->hasManyLevel()->select('district_id', 'taluka_id')->get()->toArray();

                $typeArr = ['taluka', 'counter'];
                if(in_array($assignedLevel->level_type, $typeArr)){
                    $user->taluka_id = end($sublevels)['taluka_id'];
                }

                $typeArr = ['district', 'taluka', 'counter'];
                if(in_array($assignedLevel->level_type, $typeArr)){
                    $user->district_id = end($sublevels)['district_id'];
                }else{
                    $dist = implode(", ",array_column($sublevels, 'district_id'));
                    $user->district_id = $dist;
                }

                $user->save();

                if(in_array($assignedLevel->level_type, $typeArr)){
                    $levelAllocate = \App\Models\LevelAllocated::where('level_id','=',$request->level_id)->first();
                    $levelAllocate->status = 1;
                    $levelAllocate->save();
                }
            }
            // commission to bdm
            $bdm_user = User::find($user->created_by);
            $bdm_role = \Auth::user()->getRole()->where('id','=',$bdm_user->roles_id)->first();
            $roles = levels();
            $roles['bdm'] = "BDM";
            if(array_key_exists($bdm_role->slug, $roles)){
                $user_role = \Auth::user()->getRole()->where('id','=',$user->roles_id)->first();
                $commission = \App\Models\BdmCommissions::where('bdm_id','=',$user->created_by)->where('user_id','=',$user->id)->count();

                if($user->status == "active" && $commission == 0 && array_key_exists($user_role->slug, $roles)){
                    $user_level = \App\Models\Levels::where('assigned_user_id', '=', $user->id)->first();
                    $per = ($user_role->level_price * $user_level->bdm_percentage_amount) / 100;
                    
                    $bdm_user->available_balance = $bdm_user->available_balance + $per;
                    $bdm_user->save();

                    $commission = new \App\Models\BdmCommissions;
                    $commission->id = \Uuid::generate()->string;
                    $commission->bdm_id = $user->created_by;
                    $commission->user_id = $user->id;
                    $commission->price = $per;
                    $commission->save();

                    $statements["user_id"]=$users->id;
                    $statements["to_user_id"]=$bdm_user->id;
                    $statements["descriptions"]="You get the commission for ". $user->employee_code ." user, Rs: ".$per;
                    $statements["price"]= $per;
                    $statements["type"]="credited";
                    event(new Statement($statements));

                }
            }

            if($this->levelUserRole)
                $this->makeRecharge($request,$user);
            
            // notify to normal user
            $body['type'] = "account-update";
            $body['id'] = $users->id;
            $body['message'] = 'Your account as been Updated.';
            $user->notify(new NewUser($user,$body));
    	}
    	return redirect('admin/users')->with('status','Successfully Saved.');
    }

    public function makeRecharge($request,$user){
        if(@$request->recharge_id){
            $recharge = \App\Models\Recharges::find($request->recharge_id);
            $recharge->remark = (@$request->remark)?$request->remark:$recharge->remark;
            $recharge->status = (@$request->status)?$request->status:$recharge->status;
            $recharge->save();
        }else{

            $recharge = new \App\Models\Recharges;
            if($request->hasFile('receipt')){
                $img["key"]= "receipt";
                $img["file"]= $request->receipt;
                $img["dir"]= "user_documents";
                $img["old_file"]= null;
                $recharge->receipt = file_upload($request, $img);
            }

            $recharge->id = \Uuid::generate()->string;
            $recharge->price = $request->price;
            $recharge->payment_method = $request->payment_method;
            $recharge->cheque_dd = $request->cheque_dd;
            $recharge->bank_acc = $request->bank_acc;
            $recharge->descriptions = $request->descriptions;
            $recharge->e_date = date("Y-m-d h:i:s",strtotime($request->e_date));
            $recharge->status = "accepted";
            $recharge->user_id = $user->id; // $user->id
            $recharge->remark = @$request->remark; // $user->id
            $recharge->save();
        }
    }

    public function json(){
    	$data = User::getData()->whereNull('deleted_at')->get();
    	return Datatables::of($data)->make(true);
    }

    public function validationRules($request){
    	$validation['first_name'] = "required";
    	$validation['middle_name'] = "required";
    	$validation['last_name'] = "required";
        $validation['present_address'] = ['required'];
        $validation['state_id'] = ['required'];
        $validation['pincode'] = ['required'];

        if($request->isMethod('POST')){
            $validation['primary_mobile'] = 'required|string|min:10|unique:users,primary_mobile';
            $validation['email'] = 'required|string|max:255|unique:users,email';

            if($this->levelUserRole){
                $validation['bank_ac_no'] = ['required'];
                $validation['bank_ac_holder'] = ['required'];
                $validation['bank_name'] = ['required'];
                $validation['ifsc_code'] = ['required'];
                
                $validation['id_proof'] = ['required'];
                $validation['address_proof'] = ['required'];
                $validation['passport_photo'] = ['required'];
                $validation['passbook_copy'] = ['required'];
            }
        }

    	return $validation;
    }

    public function validationMessages($module = NULL){
        return [
            'module_name.required'     	    =>      "The Module Name field is required",
            'parent_product_id.unique'      =>      "The ".$module->name." already created with this product.",
            'product.required'              =>      "Atleast add one product",
        ];
    }
}

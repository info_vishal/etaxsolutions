<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LatestNews;
use App\Models\User;
use App\Models\Levels;
use Mail;

class FrontController extends Controller
{
    public function index(){
        $latestNews = LatestNews::orderBy('created_at', 'DESC')->get();
    	return view('front_page.home',compact('latestNews'));
    }

    public function about(){
    	return view('front_page.about');
    }

    public function downloads(Request $request){
        $downloads = \App\Models\ClientDownloads::all();
        return view("front_page.downloads",compact('downloads'));
    }

    public function services(){
    	return view('front_page.services');
    }

    public function ourClients(){
    	return view('front_page.ourClients');
    }

    public function contactUs(){
    	return view('front_page.contactUs');
    }

    public function testimonial(){
        return view('front_page.testimonial');
    }

    public function email(Request $request){
        if($request->isMethod('post')){
            $this->validate($request, $this->validationRules($request));
            $data = $request->all();
            $emails = [];
            $admin = User::whereIn('roles_id',[8])->get();
            foreach ($admin as $key => $value) {
                $emails[] = $value->email;
            }
            Mail::send("email.contactus", $data, function($message) use ($data, $emails) {
                $message->to($emails)->subject($data['subject']);
                $message->from($data['email'],$data["name"]);
            });
            return redirect()->back()->with('status','Successfully Submitted.');
        }
    }

    public function becomeApartner(){
        return view('front_page.becomePartner');
    }

    public function faq(){
    	return view('front_page.faq');
    }

    public function why(){
    	return view('front_page.why');
    }

    public function gst(){
        return view('front_page.gst');
    }

    public function incomeTax(){
        return view('front_page.incomeTax');
    }

    public function anyOther(){
        return view('front_page.anyOther');
    }

    public function validationRules($request){
        $validation['name'] = "required";
        $validation['email'] = "required";
        $validation['subject'] = "required";
        $validation['type'] = "required";
        $validation['message_body'] = "required";
        $validation['phone'] = "required|numeric";
        return $validation;
    }

    public function validationMessages($module = NULL){
        return [
            'module_name.required'          =>      "The Module Name field is required",
            'parent_product_id.unique'      =>      "The ".$module->name." already created with this product.",
            'product.required'              =>      "Atleast add one product",
        ];
    }

    public function searchService(Request $request,User $user){
        
            $role = $user->getRole()->get()->toArray();
            $role = array_column($role, "id", "slug");
            unset($role["admin"], $role["sub_admin"]);
            $users = User::whereIn('roles_id', $role)->where('status','=','active')->where('pincode','=',$request->pincode)->get();
            //dd($users);
            return view('front_page.service_search',compact('users'));

    }

    public function cron(Request $request){
        if($request->type=="CronJob"){
            $data = Levels::where('allocated_status',0)->whereNotNull("archived_at")->whereNotNull("archive_by_user_id")->get();
            
            foreach ($data as $key => $value) {
                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->archived_at);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', date("Y-m-d H:i:s"));
                $diff_in_hours = $to->diffInHours($from);  
                if($diff_in_hours >= 4){
                    $value->archive_by_user_id = NULL;
                    $value->archived_at = NULL;
                    $value->save();
                }
            }
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RequestedUsers;
use App\Models\User;
use App\Models\Levels;
use App\Notifications\NewUser;
use App\Notifications\Suggesation;
use Yajra\DataTables\DataTables;
use Mail;

class SuggestController extends Controller
{

    public function userIndex(Request $request){
    	if(@$request->type == "delete"){
            RequestedUsers::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }

    	if($request->isMethod('GET')){    		
    		return view('user_pages.suggest.list');
    	}
    }


    public function userSave(Request $request){
    	$login_user = \Auth::user();
    	if($request->isMethod('GET')){ 

    		$suggestData = NULL;
    		if($request->id){
    			$suggestData = RequestedUsers::find($request->id);

    		}

    		return view('user_pages.suggest.save',compact('suggestData'));
    	}

    	if($request->isMethod('POST')){
    		$this->validate($request, $this->validationRules($request));
    		$new = new RequestedUsers;
    		$new->id = \Uuid::generate()->string;
    		$new->referred_by = $login_user->id;
    		$new->referral_code = $login_user->employee_code;
    		$new->requested_email = $request->email;
    		$new->description = $request->description;
    		$new->status = 'requested';
    		$new->tracking_no = str_random(10);
    		$new->save();

    		$body['type'] = "new-suggesation";
            $body['id'] = $new->id;
            $body['message'] = 'The '.$login_user->employee_code." requested for friend ".$request->email;

            $admin = User::whereIn('roles_id',[8,7])->get();

            // notify to admin
            foreach ($admin as $key => $value) {
                $value->notify(new Suggesation($login_user,$body));
            }
            return redirect('user/suggest')->with('status','Successfully Saved.');
    	}
    }

    public function adminIndex(Request $request){
    	if(@$request->type == "delete"){
            RequestedUsers::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }

    	if($request->isMethod('GET')){    		
    		return view('admin_pages.suggest.list');
    	}
    }


    public function adminSave(Request $request){
    	$login_user = \Auth::user();
    	if($request->isMethod('GET')){ 
    		
    		$suggestData = NULL;
    		if($request->id){
    			$suggestData = RequestedUsers::find($request->id);
    		}
    		return view('admin_pages.suggest.save',compact('suggestData'));
    	}

    	if($request->isMethod('PUT')){
    		$this->validate($request, $this->validationRules($request));
    		$new = RequestedUsers::find($request->suggest_id);
    		$new->accepted_by = $login_user->id;
    		$new->remark = $request->remark;
    		$new->token = \Uuid::generate()->string;
    		$new->status = $request->status;
    		$new->save();

    		if($request->status == "accepted"){
    			$data = $new->toArray(); 

				Mail::send("email.suggest_invitation", $data, function($message) use ($data) {
	                $message->to($data["requested_email"])->subject("Invitation");
	                $message->from(config('app.adminEmail'),config('app.name'));
	            });
    		}

    		$receiver = User::find($new->referred_by);

    		$body['type'] = "update-suggesation";
            $body['id'] = $new->id;
            $body['message'] = 'Suggesation Tracking no:'.$new->tracking_no;
            $receiver->notify(new Suggesation($login_user,$body));

            return redirect('admin/suggest')->with('status','Request Successfully Send.');
    	}
    }

    public function json(Request $request){
        $login_user = \Auth::user();
        if(@$request->from == 'user')
            $data = $login_user->hasManySuggest()->orderBy('updated_at','desc')->get();
        else
            $data = RequestedUsers::get();

        return Datatables::of($data)->make(true);
    }

    public function validationRules($request){
        if($request->isMethod('POST')){
            $validation['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];
            $validation['description'] = ['required'];
        }

        if($request->isMethod('PUT')){
            $validation['status'] = ['required'];
            $validation['remark'] = ['required'];
        }

    	return $validation;
    }

    public function emailInvitation(Request $request){
        $users = new User;
        $find = RequestedUsers::where('token','=',$request->token)->first();
        if($find && $request->isMethod('GET')){

            $sub_level = $data = NULL;
            $states = $users->getStates();
            $levels = Levels::getAllData();
            $roles = $users->getRole()->get();
            $bank = \App\Models\BankDetails::where('status','=',1)->get();
            $rechargeData = null;
    
            return view('front_page.suggest.create',compact('data','states','roles','levels','bank','rechargeData'));
        }else if($request->isMethod('GET'))
        {
            return view('front_page.suggest.thankyou');
        }

        if($request->isMethod('POST')){
            $request->merge(['primary_mobile' => clean($request->primary_mobile)]);
            $this->validate($request, $this->validationUser($request));
            //dd($request->all());
            $users = User::find($find->referred_by);

            $user = new User;

            $files = userDocuments($request);
            $user->passbook_copy = $files["passbook_copy"];
            $user->passport_photo = $files["passport_photo"];
            $user->address_proof = $files["address_proof"];
            $user->id_proof = $files["id_proof"];

            $user->nominee_name = trim($request->nominee_name);
            $user->relationship = trim($request->relationship);

            $user->bank_ac_no = trim($request->bank_ac_no);
            $user->bank_ac_holder = trim($request->bank_ac_holder);
            $user->bank_name = trim($request->bank_name);
            $user->ifsc_code = trim($request->ifsc_code);

            $user->first_name = trim($request->first_name);
            $user->middle_name = trim($request->middle_name);
            $user->last_name = trim($request->last_name);
            $user->name = $request->first_name." ".$request->last_name;
            $user->father_husband_name = trim($request->father_husband_name);
            $user->email = trim($request->email);
            $user->present_address = trim($request->present_address);
            $user->city = $request->city;
            $user->pincode = $request->pincode;
            $user->alternate_mobile = (@clean($request->alternate_mobile))?clean($request->alternate_mobile):NULL;
            $user->primary_mobile = clean($request->primary_mobile);
            $user->sex = clean($request->sex);
            $user->marital_status = clean($request->marital_status);
            $user->dob =  date("Y-m-d h:i:s",strtotime($request->dob));
            $user->password = bcrypt(clean($request->primary_mobile));
            $user->roles_id = $request->roles_id;
            $user->status = $request->status;
            //$user->available_balance = $request->price;
            $user->state_id = $request->state_id;
            $user->created_by = $users->id;
            $user->save();

            $user = User::find($user->id);
            $user->employee_code = "ETAX".$user->id;
            $user->save();


            $recharge = new \App\Models\Recharges;

            if($request->hasFile('receipt')){
                $img["key"]= "receipt";
                $img["file"]= $request->receipt;
                $img["dir"]= "user_documents";
                $img["old_file"]= null;
                $recharge->receipt = file_upload($request, $img);
            }

            $recharge->id = \Uuid::generate()->string;
            $recharge->price = $request->price;
            $recharge->payment_method = $request->payment_method;
            $recharge->cheque_dd = $request->cheque_dd;
            $recharge->bank_acc = $request->bank_acc;
            $recharge->descriptions = $request->descriptions;
            $recharge->e_date = date("Y-m-d h:i:s",strtotime($request->e_date));
            $recharge->status = "requested";
            $recharge->user_id = $user->id;
            //$recharge->remark = "Initial Recharge";
            $recharge->save();

            $find->token = NULL;
            $find->save();

            /*$statements["user_id"]=5;
            $statements["to_user_id"]=$user->id;
            $statements["descriptions"]="Initial Recharge";
            $statements["price"]= $request->price;
            $statements["type"]="credited";
            event(new Statement($statements));*/
            
            $body['type'] = "new-user";
            $body['id'] = $user->id;
            $body['message'] = 'The new user : '.$user->employee_code." referred by ".$users->employee_code." send request.";
            $admin = User::whereIn('roles_id',[8,7])->get();

            // notify to admin
            foreach ($admin as $key => $value) {
                $value->notify(new NewUser($user,$body));
            }

            // notify to normal user
            $body['type'] = "account-created";
            $body['id'] = $users->id;
            $body['message'] = 'Your account successfully registered.';
            $user->notify(new NewUser($users,$body));
            redirect()->back()->with('status','Successfully Saved.');
            return view('front_page.suggest.thankyou');
        }
    }

    public function validationUser($request){
        $validation['first_name'] = "required";
        $validation['middle_name'] = "required";
        $validation['last_name'] = "required";
        $validation['present_address'] = ['required'];
        $validation['state_id'] = ['required'];
        $validation['pincode'] = ['required'];

        $validation['bank_ac_no'] = ['required'];
        $validation['bank_ac_holder'] = ['required'];
        $validation['bank_name'] = ['required'];
        $validation['ifsc_code'] = ['required'];

        if($request->isMethod('POST')){
            $validation['primary_mobile'] = 'required|string|min:10|unique:users,primary_mobile';
            $validation['email'] = 'required|string|max:255|unique:users,email';
            $validation['id_proof'] = ['required'];
            $validation['address_proof'] = ['required'];
            $validation['passport_photo'] = ['required'];
            $validation['passbook_copy'] = ['required'];
            //$validation['assign_level'] = ['required'];
        }

        return $validation;
    }
}

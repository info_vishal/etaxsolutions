<?php

namespace App\Http\Controllers\Bdm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Models\User;
use App\Models\Levels;
use App\Notifications\NewUser;

class UsersController extends Controller
{
    public function list(Request $request){
    	return view("bdm_pages.users.list");
    }

    public function save(Request $request){
    	$users = \Auth::user();
    	if($request->isMethod('GET')) {
    		$sub_level = $data = NULL;
    		$states = $users->getStates();
            $levels = Levels::getAllData();
            $roles = $users->getRole()->get();
    		if($request->id){
                $levels = $levels->whereNotNull('l.archived_at')
                      ->where('l.archive_by_user_id','=',$users->id)->get();
    			$data = User::where("id",'=',$request->id)->where('created_by','=',$users->id)->first();
    			return view("bdm_pages.users.edit",compact('data','states','roles','levels'));
    		}else{
                $levels = $levels->whereNotNull('l.archived_at')
                        ->where('l.allocated_status','=',0)
                      ->where('l.archive_by_user_id','=',$users->id)->get();
    			return view("bdm_pages.users.create",compact('data','states','roles','levels'));
    		}
    	}

    	if($request->isMethod('POST')){
    		$this->validate($request, $this->validationRules($request));
    		$user = new User;
    		$user->first_name = trim($request->first_name);
    		$user->middle_name = trim($request->middle_name);
    		$user->last_name = trim($request->last_name);
    		$user->name = $request->first_name." ".$request->last_name;
    		$user->father_husband_name = trim($request->father_husband_name);
    		$user->email = trim($request->email);
    		$user->present_address = trim($request->present_address);
    		$user->city = $request->city;
    		$user->pincode = $request->pincode;
    		$user->alternate_mobile = (@clean($request->alternate_mobile))?clean($request->alternate_mobile):NULL;
    		$user->primary_mobile = clean($request->primary_mobile);
    		$user->sex = clean($request->sex);
    		$user->marital_status = clean($request->marital_status);
    		$user->dob = date("Y-m-d",strtotime($request->dob));
    		$user->password = bcrypt(clean($request->primary_mobile));
    		$user->roles_id = $request->roles_id;
    		$user->status = $request->status;
    		$user->state_id = $request->state_id;
    		$user->created_by = $users->id;
    		$user->save();

            $user = User::find($user->id);
    		$user->employee_code = "ETAX".$user->id;
    		$user->save();

            $level = Levels::find($request->assign_level);
            $level->assigned_user_id = $user->id;
            $level->allocated_status = 1;
            $level->save();
            
            $body['type'] = "new-user";
            $body['id'] = $user->id;
            $body['message'] = 'The new user : '.$user->email." created by BDM";
            $admin = User::whereIn('roles_id',[8,7])->get();

            // notify to admin
            foreach ($admin as $key => $value) {
                $value->notify(new NewUser($user,$body));
            }

            // notify to normal user
            $body['type'] = "account-created";
            $body['id'] = $users->id;
            $body['message'] = 'Your account successfully registered.';
            $user->notify(new NewUser($users,$body));
    	}

    	if($request->isMethod('PUT')){
    		$user = User::find($request->id);
    		$user->first_name = trim($request->first_name);
    		$user->middle_name = trim($request->middle_name);
    		$user->last_name = trim($request->last_name);
    		$user->name = $request->first_name." ".$request->last_name;
    		$user->father_husband_name = trim($request->father_husband_name);
    		$user->present_address = trim($request->present_address);
    		$user->city = $request->city;
    		$user->pincode = $request->pincode;
    		$user->alternate_mobile = clean($request->alternate_mobile);
    		$user->sex = clean($request->sex);
    		$user->marital_status = clean($request->marital_status);
    		$user->dob = date("Y-m-d",strtotime($request->dob));
    		//$user->roles_id = $request->roles_id;
    		//$user->status = $request->status;
    		$user->state_id = $request->state_id;
    		$user->save();
    	}
    	return redirect('bdm/users')->with('status','Successfully Saved.');
    }

    public function json(){
        $user = \Auth::user();
    	$data = User::getData()->where('u.created_by','=',$user->id)->get();
    	return Datatables::of($data)->make(true);
    }

    public function validationRules($request){
    	$validation['first_name'] = "required";
    	$validation['middle_name'] = "required";
    	$validation['last_name'] = "required";
    	$validation['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];
    	$validation['present_address'] = ['required'];
    	$validation['state_id'] = ['required'];
    	$validation['pincode'] = ['required'];
    	$validation['primary_mobile'] = 'required|unique:users';

    	return $validation;
    }

    public function validationMessages($module = NULL){
        return [
            'module_name.required'     	    =>      "The Module Name field is required",
            'parent_product_id.unique'      =>      "The ".$module->name." already created with this product.",
            'product.required'              =>      "Atleast add one product",
        ];
    }
}

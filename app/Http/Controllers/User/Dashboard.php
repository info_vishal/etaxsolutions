<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\EtaxNotifications;
use App\Models\Services;

class Dashboard extends Controller
{
    public function index(Request $request){
    	$users = \Auth::user();
    	$notification = EtaxNotifications::getData()->orderBy('created_at','desc')->first();
    	$services = Services::all();
    	return view("user_pages.dashboard.home",compact('users','notification','services'));
    }
}

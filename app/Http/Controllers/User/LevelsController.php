<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Levels;
use App\Models\SubLevels;
use Yajra\DataTables\DataTables;

class LevelsController extends Controller
{

    public function index(Request $request){
        $users = \Auth::user();
        $data = Levels::where('assigned_user_id','=',$users->id)->first();
        $state = $users->getStates($data->state_id);

        if($data->level_type == "taluka"){
            $sub_levels = $data->hasManylevels()->first();
            $sub_levels = levels::getCounter($sub_levels);
        }else{
            $sub_levels = $data->hasManylevels()->get();
        }
        $allocatedLevels = \App\Models\LevelAllocated::get()->toArray();
        $allocatedLevels = arrayToGroupBy($allocatedLevels,'type');
        //dd($allocatedLevels);
        return view("user_pages.levels.view",compact('data','state','sub_levels','allocatedLevels'));
    }

    public function view(Request $request){
        $users = \Auth::user();
        $data = Levels::where('id','=',$request->id)->where('archive_by_user_id','=',$users->id)->first();
        $state = $users->getStates($data->state_id);
        $sub_levels = $data->hasManylevels()->get();

        return view("bdm_pages.levels.view",compact('data','state','sub_levels'));
    }

    public function json(Request $request){
        $records = Levels::getAllData();
        $where_states = (@$request->states)?$request->states:NULL;
        $records = $records->whereIn('l.level_type',[$request->levels])
                      ->whereIn('l.state_id',[$where_states])
                      ->where('l.archived_at','=',NULL)
                      ->get();
        return Datatables::of($records)->make(true);
    }
}
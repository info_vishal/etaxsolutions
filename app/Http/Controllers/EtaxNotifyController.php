<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Models\EtaxNotifications;
use App\Models\User;

class EtaxNotifyController extends Controller
{
    public function index(Request $request){
        if(@$request->type){
            EtaxNotifications::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }
    	if($request->isMethod('GET')){
    		return view("admin_pages.etax_notify.list");
    	}
    }

    public function save(Request $request){
    	if($request->isMethod('post')){
    		$notify = new EtaxNotifications;
    		$notify->id = \Uuid::generate()->string;
    		$notify->title = $request->title;
    		$notify->descriptions = $request->descriptions;
    		$notify->save();
            return redirect()->back()->with('status','Successfully Saved.');
    	}
    }

    public function json(Request $request){
        $data = EtaxNotifications::getData()->orderBy('created_at','desc')->get();
        return Datatables::of($data)->make(true);
    }

    public function viewAll(Request $request){
        if(@$request->type == 'json'){
            $data = EtaxNotifications::getData()->orderBy('created_at','desc')->get();
            return Datatables::of($data)->make(true);
        }else{
            return view("user_pages.etax_notify.list");
        }
    }
}

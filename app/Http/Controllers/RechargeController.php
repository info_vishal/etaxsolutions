<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Models\Recharges;
use App\Models\User;
use App\Notifications\Recharges as RechargeNotify;
use App\Events\Statement;

class RechargeController extends Controller
{
    public function index(Request $request){
        if(@$request->type == "delete"){
            Recharges::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }
    	if($request->isMethod('GET')){
            if(@$request->type == "view")
            {
                $bank = \App\Models\BankDetails::where('status','=',1)->get();
                $rechargeData = Recharges::find($request->id);
                return view("admin_pages.recharges.save",compact('rechargeData','bank'));
            }
    		return view("admin_pages.recharges.list");
    	}
    }

    public function userList(Request $request){
    	if(@$request->type){
    		Recharges::destroy($request->id);
    		return redirect()->back()->with('status','Successfully Deleted.');
    	}
    	if($request->isMethod('GET')){
    		$login_user = \Auth::user();
    		return view("user_pages.recharges.list");
    	}
    }

    public function save(Request $request){
        $login_user = \Auth::user();
        if($request->isMethod('GET')){
            $rechargeData = null;
            if(@$request->id)
            {
                $rechargeData = Recharges::find($request->id);
            }
            $bank = \App\Models\BankDetails::where('status','=',1)->get();
            return view("user_pages.recharges.save",compact('rechargeData','bank'));
        }
    	if($request->isMethod('post')){


            $recharge = new \App\Models\Recharges;
            if($request->hasFile('receipt')){
                $img["key"]= "receipt";
                $img["file"]= $request->receipt;
                $img["dir"]= "user_documents";
                $img["old_file"]= null;
                $recharge->receipt = file_upload($request, $img);
            }
            $recharge->id = \Uuid::generate()->string;
            $recharge->price = $request->price;
            $recharge->payment_method = $request->payment_method;
            $recharge->cheque_dd = $request->cheque_dd;
            $recharge->bank_acc = $request->bank_acc;
            $recharge->descriptions = $request->descriptions;
            $recharge->e_date = date("Y-m-d h:i:s",strtotime($request->e_date));
            $recharge->status = "requested";
            $recharge->user_id = $login_user->id;
            $recharge->remark = @$request->remark; // $user->id
            $recharge->save();
    		// notify to normal user
    			
			$body['type'] = "recharge-request";
            $body['id'] =$recharge->id;
            $body['message'] = 'The '.$login_user->employee_code." requested for recharge Rs. ".$request->price;

            $admin = User::whereIn('roles_id',[8,7])->get();

            // notify to admin
            foreach ($admin as $key => $value) {
                $value->notify(new RechargeNotify($login_user,$body));
            }
	            
    		return redirect('user/recharge')->with('status','Successfully Saved.');
    	}


    	if($request->isMethod('put')){
    		$login_user = \Auth::user();
    		$recharge = Recharges::find($request->recharge_id);
    		$recharge->remark = @$request->remark;
    		$recharge->status = @$request->status;
    		$recharge->save();
    		// notify to normal user
    		if($request->status=='accepted' || $request->status=='rejected'){
    			$receiver = \App\Models\User::find($recharge->user_id);
    			if($request->status=='accepted'){
    				$receiver->available_balance = $receiver->available_balance + $recharge->price;
    				$receiver->save();
                    $statements["user_id"]=$login_user->id;
                    $statements["to_user_id"]=$receiver->id;
                    $statements["descriptions"]="Wallet recharge of Rs: ".$recharge->price ." accepted by admin";
                    $statements["price"]=$recharge->price;
                    $statements["type"]="credited";
                    event(new Statement($statements));
    			}
    			$body['type'] = "wallet-update"; 
	            $body['id'] =$recharge->id;
	            $body['message'] = 'Your wallet of Rs. '.$recharge->price.' is '.$recharge->status;
	            $receiver->notify(new RechargeNotify($login_user,$body));
    		}
    		return redirect('admin/recharge')->with('status','Successfully Saved.');
    	}
    }
}

<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Et113 as EtModel;
use App\Events\ET113 as EventListner;
use Yajra\DataTables\DataTables;

class ET113Controller extends Controller
{
	public $viewPath = "et113";

    public function index(Request $request){

    	$login_user = \Auth::user();
    	if($request->isMethod('GET')){
            //$states = $login_user->getStates();
            if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                return view('admin_pages.services.'.$this->viewPath.'.list');
            else if($login_user->role()->slug == 'bdm')
                return view('bdm_pages.services.'.$this->viewPath.'.list');
            else
                return view('user_pages.services.'.$this->viewPath.'.list');
        }
    }

    public function save(Request $request){

        $login_user = \Auth::user();

        $directors = $district = $serviceLogs = $message = $data = NULL;
        if(@$request->id){
            $data = EtModel::find($request->id);
            $message = \App\Models\Messages::where('service_type_id','=',$request->id)->orderBy('created_at','desc')->get();
            $serviceLogs = \App\Models\ServiceLogs::where('service_type_id','=',$request->id)->orderBy('created_at','desc')->get();
            $district = $login_user->getdistricts($data->res_add_state);
            $directors = \App\Models\Et113Childs::where('service_type_id','=',$data->id)->get();
        }

        if($request->isMethod('GET')){
            $states = $login_user->getStates();
            
            if(@$request->type=='re-create'){
                $viewPage = ".recreate";
            }else{
                $viewPage = ".save";
            }

            $currentRole = $login_user->role()->slug;
            if($currentRole == 'admin' || $currentRole == 'sub_admin')
                return view('admin_pages.services.'.$this->viewPath.$viewPage,compact('district','states','data','message', 'serviceLogs','currentRole','directors'));
            else if($currentRole == 'bdm')
                return view('bdm_pages.services.'.$this->viewPath.$viewPage,compact('district','states','data','message', 'serviceLogs','currentRole','directors'));
            else
                return view('user_pages.services.'.$this->viewPath.$viewPage,compact('district','states','data','message', 'serviceLogs','currentRole','directors'));
        }

        if($request->isMethod('post')){
            
            $this->validate($request, $this->validationUser($request));
            $model = event(new EventListner($request, $login_user));
            $back = $request->getPathInfo()."?id=".$model[0]->id;
            return redirect($back)->with('status','Successfully Submitted.');
        }

        if($request->isMethod('put')){

            $this->validate($request, $this->validationUser($request));
            event(new EventListner($request, $login_user));
            return redirect()->back()->with('status','Successfully Updated.');
        }
    }

    public function validationUser($request){

        $validation = [];
        if($request->isMethod('POST')){
            $validation['firm_name'] = "required";
            $validation['address_proof_firm'] = ['required'];
        }

        return $validation;
    }

    public function json(Request $request){
        $login_user = \Auth::user();
        if(@$login_user->role()->slug == 'admin' || @$login_user->role()->slug == 'sub_admin'){
            $data = EtModel::with('user')->get();
        }else{
            $data = $login_user->hasManyET113()->get();
        }
        return Datatables::of($data)->make(true);
    }

    public function print(Request $request){

        $login_user = \Auth::user();
        $data = NULL;
        if(@$request->id){
            $data = EtModel::find($request->id);
            $name = $data->firm_name; 
        }

        $service = \App\Models\Services::find($data->service_id);
        if($request->isMethod('GET')){
            if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                return view('include_service.receipt',compact('data','service','name'));
            else if($login_user->role()->slug == 'bdm')
                return view('include_service.receipt',compact('data','service','name'));
            else
                return view('include_service.receipt',compact('data','service','name'));
        }
    }

    public function printout(Request $request){
         $login_user = \Auth::user();
         $data = NULL;
         if(@$request->id){
             $data = EtModel::find($request->id);
             $service = \App\Models\Services::find($data->service_id);
             $res_state = $login_user->getStates($data->res_add_state);
             $res_district = $login_user->getdistricts($data->res_add_district,1);

             $childData = \App\Models\Et113Childs::where('service_type_id','=',$data->id)->get();

             if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                 return view('include_service.'.$this->viewPath.'.printout',compact('data','service', 'res_state', 'res_district', 'childData'));
             else if($login_user->role()->slug == 'bdm')
                 return view('include_service.'.$this->viewPath.'.printout',compact('data','service', 'res_state', 'res_district', 'childData'));
             else
                 return view('include_service.'.$this->viewPath.'.printout',compact('data','service', 'res_state', 'res_district', 'childData'));
         }
     }
}

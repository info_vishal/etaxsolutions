<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Messages;
use App\Notifications\Messages as MessageNotify;
use App\Models\User;
use App\Models\Services;

class MessageLogs extends Controller
{
    public function index(Request $request){
    }

    public function msgSave(Request $request){
    	$login_user = \Auth::user();
    	if($request->isMethod('POST')){
            $this->validate($request, $this->validationUser($request));
            $newMessage = new Messages;

            $service = Services::find($request->message_service_id);

            if($request->hasFile('file')){
	            $img["key"]= "file";
	            $img["file"]= $request->file;
	            $img["dir"]= "messages/".$service->code;
	            $img["old_file"]= null;
	            $newMessage->file = file_upload($request, $img);
	        }

            $newMessage->id = \Uuid::generate()->string;
            $newMessage->service_id = $request->message_service_id;
            $newMessage->created_by = $login_user->id;
            $newMessage->service_type_id = $request->service_type_id;
            $newMessage->descriptions = $request->descriptions;
            $newMessage->save();

            $body['type'] = "service-new-message";
	        $body['id'] = $newMessage->id;
	        //$body['route'] = "user.pancard.save";
	        $body['message'] = $login_user->employee_code." commented on ".$service->name."(". $service->code ."), Service Tracking No: ".$request->tracking_no;

	        $role = $login_user->role();
	        if($role->slug == "admin" || $role->slug == "sub_admin"){
	        	$receiver = User::find($request->service_created_by);
	        	$receiver->notify(new MessageNotify($login_user,$body));
	        }else{
	        	$admin = User::whereIn('roles_id',[8,7])->get();
		        foreach ($admin as $key => $value) {
		            $value->notify(new MessageNotify($login_user,$body));
		        }
	        }
	        

            return redirect()->back()->with('status','Message successfully send');
        }
    }

    public function validationUser($request){
        $validation['descriptions'] = "required";
        return $validation;
    }
}

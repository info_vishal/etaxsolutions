<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PanCards;
use App\Events\PanCard;
use Yajra\DataTables\DataTables;

class PanCardController extends Controller
{
    public function index(Request $request){
    	$login_user = \Auth::user();
    	if($request->isMethod('GET')){
            //$states = $login_user->getStates();
            if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                return view('admin_pages.services.pancard.list');
            else if($login_user->role()->slug == 'bdm')
                return view('bdm_pages.services.pancard.list');
            else
                return view('user_pages.services.pancard.list');
        }
    }

    public function save(Request $request){
    	$login_user = \Auth::user();

        $serviceLogs = $message = $data = NULL;
        if(@$request->id){
            $data = PanCards::find($request->id);
            $message = \App\Models\Messages::where('service_type_id','=',$request->id)->orderBy('created_at','desc')->get();
            $serviceLogs = \App\Models\ServiceLogs::where('service_type_id','=',$request->id)->orderBy('created_at','desc')->get();
        }

    	if($request->isMethod('GET')){
            $states = $login_user->getStates();
            $currentRole = $login_user->role()->slug;
            if($currentRole == 'admin' || $currentRole == 'sub_admin')
                return view('admin_pages.services.pancard.save',compact('states','data','message', 'serviceLogs','currentRole'));
            else if($login_user->role()->slug == 'bdm')
                return view('bdm_pages.services.pancard.save',compact('states','data','message', 'serviceLogs','currentRole'));
            else
                return view('user_pages.services.pancard.save',compact('states','data','message', 'serviceLogs','currentRole'));
        }

        if($request->isMethod('post')){
        	$request->merge(['mobile_number' => clean($request->mobile_number)]);
            $this->validate($request, $this->validationUser($request));
            $model = event(new PanCard($request, $login_user));
            $back = $request->getPathInfo()."?id=".$model[0]->id;
            return redirect($back)->with('status','Successfully Submitted.');
        }

        if($request->isMethod('put')){
            $request->merge(['mobile_number' => clean($request->mobile_number)]);
            $this->validate($request, $this->validationUser($request));
            event(new PanCard($request, $login_user));
            return redirect()->back()->with('status','Successfully Updated.');
        }
    }

    public function validationUser($request){

        $validation = [];
        if($request->isMethod('POST')){
            $validation['cli_last_name'] = "required";
            $validation['cli_first_name'] = "required";
            $validation['cli_middle_name'] = "required";
            $validation['cli_name_on_card'] = ['required'];
            $validation['cli_gender'] = ['required'];
            $validation['cli_dob'] = ['required'];

            $validation['fat_last_name'] = ['required'];
            $validation['fat_first_name'] = ['required'];
            $validation['fat_middle_name'] = ['required'];

            $validation['parent_name_on_card'] = ['required'];

            $validation['address_of_com'] = ['required'];

            $validation['mobile_number'] = ['required'];
            $validation['email_id'] = ['required'];

            $validation['status_of_applicant'] = ['required'];

            $validation['aadhar_name'] = ['required'];

            $validation['source_of_income'] = ['required'];

        
            $validation['id_proof_file'] = ['required'];
            $validation['address_proof_file'] = ['required'];
            $validation['dob_file'] = ['required'];
            $validation['signature_file'] = ['required'];
            $validation['passport_size_file'] = ['required'];
        }
        return $validation;

    }

    public function json(Request $request){
        $login_user = \Auth::user();
        if(@$login_user->role()->slug == 'admin' || @$login_user->role()->slug == 'sub_admin'){
            $data = PanCards::with('user')->get();
        }else{
            $data = $login_user->hasManyPanCard()->get();
        }
        return Datatables::of($data)->make(true);
    }

    public function print(Request $request){

        $login_user = \Auth::user();
        $data = NULL;
        if(@$request->id){
            $data = PanCards::find($request->id);
            $name = $data->cli_last_name." ".$data->cli_first_name." ".$data->cli_middle_name; 
        }

        $service = \App\Models\Services::find($data->service_id);
        if($request->isMethod('GET')){
            if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                return view('include_service.receipt',compact('data','service','name'));
            else if($login_user->role()->slug == 'bdm')
                return view('include_service.receipt',compact('data','service','name'));
            else
                return view('include_service.receipt',compact('data','service','name'));
        }
    }

    public function printout(Request $request){

        $login_user = \Auth::user();
        $data = NULL;
        if(@$request->id){
            $data = PanCards::find($request->id);
            $service = \App\Models\Services::find($data->service_id);
            $ofc_state = $login_user->getStates($data->ofc_add_state);
            $res_state = $login_user->getStates($data->res_add_state);

            if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                return view('include_service.pancard.printout',compact('data','service','ofc_state', 'res_state'));
            else if($login_user->role()->slug == 'bdm')
                return view('include_service.receipt',compact('data','service','ofc_state', 'res_state'));
            else
                return view('include_service.receipt',compact('data','service','ofc_state', 'res_state'));
        }
    }
}

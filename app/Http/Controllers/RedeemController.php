<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RedeemBalance;
use App\Models\User;

class RedeemController extends Controller
{
    public function index(Request $request){
    	if($request->isMethod('GET')){
            $login_user = \Auth::user();
            return view("admin_pages.dashboard.redeem");
        }
    }

    public function userList(Request $request){
    	if($request->isMethod('GET')){
            $login_user = \Auth::user();
    		$data = $login_user->with("hasManyRedeempion");
            $data = $data->whereHas("hasManyRedeempion", function($query){
                $query->where('status','pending');
            })->count();
    		return view("user_pages.dashboard.redeem", compact('data'));
    	}
    }

    public function userSave(Request $request){
        $login_user = \Auth::user();
        $entity = new RedeemBalance;
        $entity->id = \Uuid::generate()->string;
        $entity->user_id = $login_user->id;
        $entity->amount = $login_user->reward_balance;
        $entity->description = "-----";
        $entity->tracking_no = date("Ymdhis");
        $entity->save();
        return redirect()->back()->with('status','Successfully Saved.');
    }


    public function save(Request $request){
        $login_user = \Auth::user();
        if($request->isMethod('get')){
            $data = RedeemBalance::find($request->id);
            return view("admin_pages.dashboard.redeem_save",compact('data'));
        }
    	if($request->isMethod('post')){
            $entity = RedeemBalance::find($request->id);
            $entity->description = $request->description;
            $entity->status = $request->redeem_status;
            $entity->save();
            
            if($request->redeem_status == "completed"){
                $entity = User::find($entity->user_id);
                $entity->reward_balance = 0;
                $entity->save();
            }
            return redirect()->back()->with('status','Successfully Saved.');
    	}
    }
}

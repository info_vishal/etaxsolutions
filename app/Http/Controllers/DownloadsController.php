<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Downloads;
use App\Models\DownloadFiles;
use Yajra\DataTables\DataTables;

class DownloadsController extends Controller
{
    public function index(Request $request){
    	if($request->isMethod('GET')){
    		$downloads = Downloads::getData();
    		return view("admin_pages.downloads.list",compact('downloads'));
    	}
    }
    
    public function listDownload(Request $request,Downloads $downloads){
        if($request->isMethod('GET')){
            $downloads = $downloads->all();
            //hasManyFiles()->get()
            return view("user_pages.downloads.list",compact('downloads'));
        }
    }

    public function centerSave(Request $request){
        if(@$request->type){
            Downloads::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }
    	if($request->isMethod('POST')){
    		$id = ($request->center_id)?$request->center_id:\Uuid::generate()->string;
    		Downloads::updateOrCreate(
    			["id" => $id],
    			["title" => $request->title]
    		);
    		return redirect()->back()->with('status','Successfully Saved.');
    	}
    }

    public function fileSave(Request $request){
        if(@$request->type){
            DownloadFiles::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }
        if($request->isMethod('POST')){
            $id = ($request->file_id)?$request->file_id:\Uuid::generate()->string;
            $center = $request->center;
            if($request->hasFile('file')){
                $img["key"]= "file";
                $img["file"]= $request->file;
                $img["dir"]= "download";
                $img["old_file"]= "";
                $file = file_upload($request, $img);
                DownloadFiles::updateOrCreate(
                    ["id" => $id],
                    ["downloads_id" => $center,"descriptions" => $request->title,'file_path' => $file]
                );
            }else{
                DownloadFiles::updateOrCreate(
                    ["id" => $id],
                    ["downloads_id" => $center,"descriptions" => $request->title]
                );
            }
            return redirect()->back()->with('status','Successfully Saved.');
        }
    }

    public function fileJson(Request $request){
        $data = DownloadFiles::getData()->get();
        return Datatables::of($data)->make(true);
    }

    public function client(Request $request){
        if(@$request->type){
            \App\Models\ClientDownloads::destroy($request->id);
            return redirect()->back()->with('status','Successfully Deleted.');
        }

        if($request->isMethod('GET')){
            $downloads = \App\Models\ClientDownloads::all();
            return view("admin_pages.downloads.client",compact('downloads'));
        }

        if($request->isMethod('POST')){

            if($request->hasFile('file')){
                $img["key"]= "file";
                $img["file"]= $request->file;
                $img["dir"]= "download";
                $img["old_file"]= "";
                $file = file_upload($request, $img);

                $id = \Uuid::generate()->string;

                $download = new \App\Models\ClientDownloads;
                $download->id= $id;
                $download->title= $request->title;
                $download->file_path = $file;
                $download->save();
                return redirect()->back()->with('status','Successfully Saved.');
            }
        }
    }
}

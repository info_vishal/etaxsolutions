<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Levels;
use App\Models\Recharges;
use Yajra\DataTables\DataTables;

class CommonController extends Controller
{
    public function districtAjax(Request $request){
    	if($request->isMethod('POST')){
            $lvl = ['taluka','counter'];

    		$user = \Auth::user();
            $district = $user->getdistricts($request->state_id);
            if(@$request->level){
                $old = levels::getUnique($request->level, $request->state_id);
                //dd($old);
                $assignedDistrict  = array_column($old, 'district_id');

                $html = '<option value=""> Select District </option>';
                foreach ($district as $key => $value) {
                    if(in_array($value->id, $assignedDistrict) && !in_array($request->level, $lvl))
                        continue;

                    $html .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
            }else{
                $html = '<option value=""> Select District </option>';
                foreach ($district as $key => $value) {
                    $html .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
            }
    		
    		return response()->json(['data' => $html],200);
    	}
    }

    public function talukaAjax(Request $request){
        if($request->isMethod('POST')){
            $user = \Auth::user();
            $taluka = $user->getTaluka($request->district_id);
            if(@$request->level){
                $old = levels::getUnique($request->level, $request->state_id);
                $assignedtaluka  = array_column($old, 'taluka_id');

                $html = '<option value=""> Select Taluka </option>';
                foreach ($taluka as $key => $value) {

                    if(in_array($value->id, $assignedtaluka))
                        continue;

                    $html .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
            }else{
                $html = '<option value=""> Select Taluka </option>';
                foreach ($taluka as $key => $value) {
                    $html .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
            }
            return response()->json(['data' => $html],200);
        }
    }
    
    public function markNotificationAsRead(Request $request){
    	if($request->isMethod('POST')){
    		\Auth::user()->unreadNotifications->where('id', $request->id)->markAsRead();
    		return response()->json([],200);
    	}
        if($request->isMethod('GET')){
            \Auth::user()->unreadNotifications->markAsRead();
            return redirect()->back();
        }
    }

    public function myProfile(Request $request){
        $data = $user = \Auth::user();
        if($request->isMethod('GET')){
            $states = $user->getStates();
            $levels = Levels::getAllData();
            $roles = $user->getRole()->get();
            if($user->role()->slug == 'admin' || $user->role()->slug == 'sub_admin')
                return view('admin_pages.users.my_profile',compact('data','states','levels','roles'));
            else if($user->role()->slug == 'bdm')
                return view('bdm_pages.users.my_profile',compact('data','states','levels','roles'));
            else
                return view('user_pages.users.my_profile',compact('data','states','levels','roles'));
        }

        if($request->isMethod('PUT')){

            if($request->hasFile('profile_image')){
                $img["key"]= "profile_image";
                $img["file"]= $request->profile_image;
                $img["dir"]= "profile";
                $img["old_file"]= $user->profile_image;
                $user->profile_image = file_upload($request, $img);
            }

            $user->first_name = trim($request->first_name);
            $user->middle_name = trim($request->middle_name);
            $user->last_name = trim($request->last_name);
            $user->name = $request->first_name." ".$request->last_name;
            $user->father_husband_name = trim($request->father_husband_name);
            $user->present_address = trim($request->present_address);
            $user->city = $request->city;
            $user->pincode = $request->pincode;
            $user->alternate_mobile = clean($request->alternate_mobile);
            $user->sex = clean($request->sex);
            $user->marital_status = clean($request->marital_status);
            $user->dob = dbDate($request->dob, 1);
            $user->state_id = $request->state_id;
            $user->save();
        }
        return redirect()->back()->with('status','Successfully Saved.');
    }

    public function rechargeJson(Request $request){
        $login_user = \Auth::user();
        $recharge = new Recharges;
        if(@$request->from == 'user')
            $data = \App\Models\Recharges::getData()->where("r.user_id",'=',$login_user->id)->orderBy('r.updated_at','desc')->get();
        else{
           // $data = \App\Models\Recharges::getData()->orderBy('r.updated_at','desc')->get();
            $data = $recharge->with('user')->whereHas('user')->orderBy('updated_at','desc')->get();
        }
        return Datatables::of($data)->make(true);
    }

    public function redeemJson(Request $request){
        $login_user = \Auth::user();
        $entity = new \App\Models\RedeemBalance;
        if(@$request->from == 'user')
            $data = $entity->where("user_id",'=',$login_user->id)->orderBy('created_at','desc')->get();
        else{
            $data = $entity->with('user')->whereHas('user')->orderBy('created_at','desc')->get();
        }
        return Datatables::of($data)->make(true);
    }

    public function statements(Request $request){
        $login_user = \Auth::user();
        if(@$request->type == 'json'){
            $data = $login_user->hasManyStatements()->get();
            return Datatables::of($data)->make(true);
        }
        
        if($login_user->role()->slug == 'bdm')
            return view('bdm_pages.statements.list');
        else
            return view('user_pages.statements.list');
    }

    public function changePassword(Request $request){
        $login_user = \Auth::user();
        if(@$request->isMethod('GET')){
            if($login_user->role()->slug == 'admin' || $login_user->role()->slug == 'sub_admin')
                return view('admin_pages.users.change_password');
            else if($login_user->role()->slug == 'bdm')
                return view('bdm_pages.users.change_password');
            else
                return view('user_pages.users.change_password');
        }

        if(@$request->isMethod('PUT')){
            $this->validate($request, $this->validationRules($request));
            $login_user->password = bcrypt(clean($request->password));
            $login_user->save();
            return redirect()->back()->with('status','Password successfully changed.');
        }
    }

    public function validationRules(){
         $user = \Auth::user();
        return [
            'password' => 'nullable|required_with:password_confirmation|string|confirmed',
            'current_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                    if (!\Hash::check($value, $user->password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }],
        ];
    }

    public function redeem(){
        $user = \Auth::user();
        $user->available_balance += $user->reward_balance;
        $user->reward_balance = 0;
        $user->save();
        return redirect()->back();
    }

}

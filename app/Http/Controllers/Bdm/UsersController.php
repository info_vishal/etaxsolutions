<?php

namespace App\Http\Controllers\Bdm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Models\User;
use App\Models\Levels;
use App\Notifications\NewUser;
use App\Events\Statement;

class UsersController extends Controller
{
    public function list(Request $request){
    	return view("bdm_pages.users.list");
    }

    public function save(Request $request){
    	$users = \Auth::user();
    	if($request->isMethod('GET')) {
    		$sub_level = $data = NULL;
    		$states = $users->getStates();
            $levels = Levels::getAllData();
            $roles = $users->getRole()->get();
            $bank = \App\Models\BankDetails::where('status','=',1)->get();
            $rechargeData = null;
    		if($request->id){
    			$data = User::where("id",'=',$request->id)->where('created_by','=',$users->id)->whereNull('deleted_at')->first();
                $rechargeData = $data->hasManyRecharge()->orderBy('created_at','ASC')->first();
                $levels = $levels->whereNotNull('l.archived_at')
                      ->where('l.archive_by_user_id','=',$users->id)->get();
    			return view("bdm_pages.users.edit",compact('data','states','roles','levels','bank','rechargeData'));
    		}else{
                $levels = $levels->whereNotNull('l.archived_at')
                        ->where('l.allocated_status','=',0)
                      ->where('l.archive_by_user_id','=',$users->id)->get();
    			return view("bdm_pages.users.create",compact('data','states','roles','levels','bank','rechargeData'));
    		}
    	}

    	if($request->isMethod('POST')){


            $request->merge(['primary_mobile' => clean($request->primary_mobile)]);
    		$this->validate($request, $this->validationRules($request));
    		$user = new User;

            $files = userDocuments($request);
            $user->passbook_copy = $files["passbook_copy"];
            $user->passport_photo = $files["passport_photo"];
            $user->address_proof = $files["address_proof"];
            $user->id_proof = $files["id_proof"];

            $user->nominee_name = trim($request->nominee_name);
            $user->relationship = trim($request->relationship);

            $user->bank_ac_no = trim($request->bank_ac_no);
            $user->bank_ac_holder = trim($request->bank_ac_holder);
            $user->bank_name = trim($request->bank_name);
            $user->ifsc_code = trim($request->ifsc_code);


    		$user->first_name = trim($request->first_name);
    		$user->middle_name = trim($request->middle_name);
    		$user->last_name = trim($request->last_name);
    		$user->name = $request->first_name." ".$request->last_name;
    		$user->father_husband_name = trim($request->father_husband_name);
    		$user->email = trim($request->email);
    		$user->present_address = trim($request->present_address);
    		$user->city = $request->city;
    		$user->pincode = $request->pincode;
    		$user->alternate_mobile = (@clean($request->alternate_mobile))?clean($request->alternate_mobile):NULL;
    		$user->primary_mobile = clean($request->primary_mobile);
    		$user->sex = clean($request->sex);
    		$user->marital_status = clean($request->marital_status);
    		$user->dob =  date("Y-m-d h:i:s",strtotime($request->dob));
    		$user->password = bcrypt(clean($request->primary_mobile));
    		$user->roles_id = $request->roles_id;
            $user->status = $request->status;
    		//$user->available_balance = $request->price;
    		$user->state_id = $request->state_id;
    		$user->created_by = $users->id;
    		$user->save();

            $user = User::find($user->id);
    		$user->employee_code = "ETAX".$user->id;
    		$user->save();

            $recharge = new \App\Models\Recharges;

            if($request->hasFile('receipt')){
                $img["key"]= "receipt";
                $img["file"]= $request->receipt;
                $img["dir"]= "user_documents";
                $img["old_file"]= null;
                $recharge->receipt = file_upload($request, $img);
            }

            $recharge->id = \Uuid::generate()->string;
            $recharge->price = $request->price;
            $recharge->payment_method = $request->payment_method;
            $recharge->cheque_dd = $request->cheque_dd;
            $recharge->bank_acc = $request->bank_acc;
            $recharge->descriptions = $request->descriptions;
            $recharge->e_date = date("Y-m-d h:i:s",strtotime($request->e_date));
            $recharge->status = "requested";
            $recharge->user_id = $user->id;
            //$recharge->remark = "Initial Recharge";
            $recharge->save();

            /*$statements["user_id"]=5;
            $statements["to_user_id"]=$user->id;
            $statements["descriptions"]="Initial Recharge";
            $statements["price"]= $request->price;
            $statements["type"]="credited";
            event(new Statement($statements));*/

            $level = Levels::find($request->assign_level);
            $level->assigned_user_id = $user->id;
            $level->allocated_status = 1;
            $level->save();
            
            $body['type'] = "new-user";
            $body['id'] = $user->id;
            $body['message'] = 'The new user : '.$user->employee_code." created by ".$users->employee_code." (BDM)";
            $admin = User::whereIn('roles_id',[8,7])->get();

            // notify to admin
            foreach ($admin as $key => $value) {
                $value->notify(new NewUser($user,$body));
            }

            // notify to normal user
            $body['type'] = "account-created";
            $body['id'] = $users->id;
            $body['message'] = 'Your account successfully registered.';
            $user->notify(new NewUser($users,$body));
    	}

    	if($request->isMethod('PUT')){
            $this->validate($request, $this->validationRules($request));
    		$user = User::find($request->id);

             $files = userDocuments($request);

            if(@$files["passbook_copy"])
                $user->passbook_copy = @$files["passbook_copy"];

            if(@$files["passport_photo"])
                $user->passport_photo = @$files["passport_photo"];
            
            if(@$files["address_proof"])
                $user->address_proof = @$files["address_proof"];
            
            if(@$files["id_proof"])
                $user->id_proof = @$files["id_proof"];

            $user->nominee_name = trim($request->nominee_name);
            $user->relationship = trim($request->relationship);

            $user->bank_ac_no = trim($request->bank_ac_no);
            $user->bank_ac_holder = trim($request->bank_ac_holder);
            $user->bank_name = trim($request->bank_name);
            $user->ifsc_code = trim($request->ifsc_code);

    		$user->first_name = trim($request->first_name);
    		$user->middle_name = trim($request->middle_name);
    		$user->last_name = trim($request->last_name);
    		$user->name = $request->first_name." ".$request->last_name;
    		$user->father_husband_name = trim($request->father_husband_name);
    		$user->present_address = trim($request->present_address);
    		$user->city = $request->city;
    		$user->pincode = $request->pincode;
    		$user->alternate_mobile = clean($request->alternate_mobile);
    		$user->sex = clean($request->sex);
    		$user->marital_status = clean($request->marital_status);
    		$user->dob =  date("Y-m-d h:i:s",strtotime($request->dob));
            //$user->available_balance = $request->available_balance;
    		//$user->roles_id = $request->roles_id;
    		//$user->status = $request->status;
    		$user->state_id = $request->state_id;
    		$user->save();
    	}
    	return redirect('bdm/users')->with('status','Successfully Saved.');
    }

    public function json(){
        $user = \Auth::user();
    	$data = User::getData()->where('u.created_by','=',$user->id)->get();
    	return Datatables::of($data)->make(true);
    }

    public function validationRules($request){
    	$validation['first_name'] = "required";
    	$validation['middle_name'] = "required";
    	$validation['last_name'] = "required";
        $validation['present_address'] = ['required'];
        $validation['state_id'] = ['required'];
        $validation['pincode'] = ['required'];

        $validation['bank_ac_no'] = ['required'];
        $validation['bank_ac_holder'] = ['required'];
        $validation['bank_name'] = ['required'];
        $validation['ifsc_code'] = ['required'];


        $validation['ifsc_code'] = ['required'];
        if($request->isMethod('POST')){
            $validation['primary_mobile'] = 'required|string|min:10|unique:users';
            $validation['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];
            $validation['id_proof'] = ['required'];
            $validation['address_proof'] = ['required'];
            $validation['passport_photo'] = ['required'];
            $validation['passbook_copy'] = ['required'];
            $validation['assign_level'] = ['required'];
        }

    	return $validation;
    }

    public function validationMessages($module = NULL){
        return [
            'module_name.required'     	    =>      "The Module Name field is required",
            'parent_product_id.unique'      =>      "The ".$module->name." already created with this product.",
            'product.required'              =>      "Atleast add one product",
        ];
    }
}

<?php

namespace App\Http\Controllers\Bdm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Levels;
use App\Models\SubLevels;
use Yajra\DataTables\DataTables;

class LevelsController extends Controller
{

    public function index(Request $request){

        $users = \Auth::user();
        $states = $users->getStates();
        $sub_level = $data = [];
        if(@$request->type == "archive") {
            $find = Levels::find($request->id);
            //$find->allocated_status = 1;
            $find->archived_at = date("Y-m-d h:i:s");
            $find->archive_by_user_id = $users->id;
            $find->save();
            return redirect()->back()->with('status','Successfully Archived');
        }

        if($request->isMethod('GET')) {
            return view("bdm_pages.levels.save",compact('data','states'));
        }
    }

    public function myArchived(Request $request){
        $users = \Auth::user();
        $records = Levels::getAllData();
        $records = $records->whereNotNull('l.archived_at')
                      ->where('l.archive_by_user_id','=',$users->id)->get();
        return view("bdm_pages.levels.myArchived",compact('records'));
    }

    public function view(Request $request){
        $users = \Auth::user();

        $lastUrl = str_replace(url('/'), '', url()->previous());
        if($lastUrl != "/bdm/levels")
            $data = Levels::where('id','=',$request->id)->where('archive_by_user_id','=',$users->id)->first();
        else
            $data = Levels::where('id','=',$request->id)->first();
        
        $state = $users->getStates($data->state_id);
        //$sub_levels = $data->hasManylevels()->get();
        if($data->level_type == "taluka"){
            $sub_levels = $data->hasManylevels()->first();
            $sub_levels = levels::getCounter($sub_levels);
        }else{
            $sub_levels = $data->hasManylevels()->get();
        }

        
        $allocatedLevels = \App\Models\LevelAllocated::get()->toArray();
        $allocatedLevels = arrayToGroupBy($allocatedLevels,'type');
        return view("bdm_pages.levels.view",compact('data','state','sub_levels','allocatedLevels'));
    }

    public function json(Request $request){
        $records = Levels::getAllData();
        $where_states = (@$request->states)?$request->states:NULL;
        $records = $records->whereIn('l.level_type',[$request->levels])
                      ->whereIn('l.state_id',[$where_states])
                      ->where('l.archived_at','=',NULL)
                      ->get();
        return Datatables::of($records)->make(true);
    }
}

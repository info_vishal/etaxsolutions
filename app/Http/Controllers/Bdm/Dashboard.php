<?php

namespace App\Http\Controllers\Bdm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NewUser;
use App\Models\User;
use App\Models\Services;

class Dashboard extends Controller
{
    public function index(Request $request){
    	$users = \Auth::user();
    	
    	$services = Services::all();
    	return view("bdm_pages.dashboard.home",compact('users','services'));
    }
}

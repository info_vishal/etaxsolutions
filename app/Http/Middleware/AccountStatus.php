<?php

namespace App\Http\Middleware;

use Closure;

class AccountStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $is_admin = \Auth::User()->getRole()->where('id','=',\Auth::User()->roles_id)->first();
        $prefix = $request->route()->getPrefix();
        if(\Auth::User()->status=='active'){
            return $next($request);
        }else{
            return redirect($prefix);
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $is_admin = \Auth::User()->getRole()->where('id','=',\Auth::User()->roles_id)->first();
        $prefix = $request->route()->getPrefix();
        //dd($is_admin->slug);
        if($prefix == '/admin' && ($is_admin->slug == "admin" || $is_admin->slug == "sub_admin") ){
            return $next($request);
        }else if($prefix == '/bdm' && $is_admin->slug == "bdm"){
            return $next($request);
        }else if($prefix == '/user' && array_key_exists($is_admin->slug, levels())){
            return $next($request);
        }
        return redirect()->back();
    }
}

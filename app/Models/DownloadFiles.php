<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DownloadFiles extends Model
{
    protected $fillable = ["id","downloads_id", "descriptions","file_path"];

    public static function getData(){
        return DB::table('download_files as df')
                ->select('df.id as df_id','df.descriptions','df.file_path', 'df.downloads_id as center_id', 'df.file_path', 'd.title')
                ->leftJoin('downloads as d', 'd.id', '=', 'df.downloads_id');
    }
}

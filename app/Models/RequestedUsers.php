<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RequestedUsers extends Model
{
    //
    use Notifiable;
    public $incrementing = false;

}

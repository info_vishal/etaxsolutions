<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Districts extends Model
{
	protected $fillable = ['id','name','state_id'];
    public static function getList(){
    	return DB::table('districts as d')
				->select('d.*', 's.id as state_id','s.name as state_name')
				->leftJoin('states as s', 's.id', '=', 'd.state_id');
    }
}

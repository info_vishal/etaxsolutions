<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ServiceLogs extends Model
{
    use Notifiable;
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class ,'created_by', 'id');
   	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EtaxNotifications extends Model
{
    protected $fillable = ["id","title", "descriptions"];
    public static function getData(){
        return DB::table('etax_notifications')->select('*');
    }
}

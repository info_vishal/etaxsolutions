<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

class Recharges extends Model
{
	use Notifiable;
    public $incrementing = false;

	protected function setUUID()
    {
        $this->id = preg_replace('/\./', '', uniqid('bpm', true));
    }
    
    public static function getData(){
        return DB::table('recharges as r')
                ->select('r.id as recharge_id','r.e_date','r.remark as recharge_remark','r.cheque_dd', 'r.payment_method','r.bank_acc','r.receipt','r.status as recharge_status','r.descriptions', 'r.created_at', 'r.price as recharge_price', 'u.employee_code', 'u.name as user_name')
                ->leftJoin('users as u', 'u.id', '=', 'r.user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}

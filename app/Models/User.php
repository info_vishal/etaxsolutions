<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('*');

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->getRole()->where('id','=',\Auth::User()->roles_id)->first();
    }

    public function getStates($id = 0){
        if($id)
            return DB::table('states')->select('*')->where('id','=',$id)->first();
        else
            return DB::table('states')->select('*')->get();
    }
    
    public function getdistricts($id, $multiple = 0){
        if($multiple)
            return DB::table('districts')->select('*')->where('id','=',$id)->first();
        else
            return DB::table('districts')->select('*')->where('state_id','=',$id)->get();
    }

    public function getTaluka($id, $multiple = 0){
        if($multiple)
            return DB::table('talukas')->select('*')->where('id','=',$id)->first();
        else
            return DB::table('talukas')->select('*')->where('district_id','=',$id)->get();
    }

    public function getRole($id = 0, $multiple = 0){
        /*if($id)
            return DB::table('roles')->select('*')->where('id','=',$id)->first();
        else if($multiple)
            return DB::table('roles')->select('*')->where('id','=',$id)->get();
        else*/
        return DB::table('roles')->select('*');
    }

    public static function getData(){
        return DB::table('users as u')
                ->select('u.id as user_id', 'u.name as user_name', 'u.profile_image', 'u.employee_code', 'u.primary_mobile','u.available_balance', 'u.status','r.name as role_name')
                ->leftJoin('roles as r', 'u.roles_id', '=', 'r.id');
    }

    public function hasManyRecharge()
    {
        return $this->hasMany(Recharges::class);
    }

    public function hasManyStatements()
    {
        return $this->hasMany(Statements::class,'to_user_id');
    }

    public function hasManySuggest(){
        return $this->hasMany(RequestedUsers::class,'referred_by');
    }

    public function hasManyPanCard(){
        return $this->hasMany(PanCards::class,'created_by');
    }

    public function hasManyET102(){
        return $this->hasMany(Et102::class,'created_by');
    }

    public function hasManyET103(){
        return $this->hasMany(Et103::class,'created_by');
    }

    public function hasManyET104(){
        return $this->hasMany(Et104::class,'created_by');
    }

    public function hasManyET105(){
        return $this->hasMany(Et105::class,'created_by');
    }

    public function hasManyET106(){
        return $this->hasMany(Et106::class,'created_by');
    }

    public function hasManyET107(){
        return $this->hasMany(Et107::class,'created_by');
    }

    public function hasManyET108(){
        return $this->hasMany(Et108::class,'created_by');
    }

    public function hasManyET109(){
        return $this->hasMany(Et109::class,'created_by');
    }

    public function hasManyET110(){
        return $this->hasMany(Et110::class,'created_by');
    }

    public function hasManyET111(){
        return $this->hasMany(Et111::class,'created_by');
    }

    public function hasManyET112(){
        return $this->hasMany(Et112::class,'created_by');
    }

    public function hasManyET113(){
        return $this->hasMany(Et113::class,'created_by');
    }

    public function hasManyET114(){
        return $this->hasMany(Et114::class,'created_by');
    }

    public function hasManyRedeempion(){
        return $this->hasMany(RedeemBalance::class,'user_id');
    }
}

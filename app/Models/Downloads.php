<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Downloads extends Model
{
    protected $fillable = ["id","title"];

    public static function getData(){
        return DB::select('select * from downloads');
    }

    public function hasManyFiles(){
    	return $this->hasMany(DownloadFiles::class,'downloads_id');
    }
}

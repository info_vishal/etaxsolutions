<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Levels extends Model
{
    //

    public static function getData(){
    	return DB::table('levels as l')
    			->select('l.id as level_id', 'l.name as level_name', 'l.level_type', 'l.allocated_status', 's.name as state_name','us.name as user_name', 'l.archived_at')
    			->join('states as s', 's.id', '=', 'l.state_id')
    			->leftJoin('users as us', 'us.id', '=', 'l.archive_by_user_id');
    }

   	public function hasManylevels(){
    	return $this->hasMany(SubLevels::class,'level_id');
    }

    public static function getAllData(){
        return DB::table('levels as l')
                ->select('l.id as level_id', 'l.name as level_name', 'l.level_type', 'l.allocated_status', 's.name as state_name','l.archived_at','l.assigned_user_id')
                ->join('states as s', 's.id', '=', 'l.state_id');
    }

    public static function getCounter($sub_levels){
        return DB::select('select l.id, l.name from levels as l, sub_levels as s where l.id = s.level_id and l.level_type = "counter"  and s.district_id = '.$sub_levels->district_id.' and s.taluka_id = '.$sub_levels->taluka_id);

        /*return DB::select('select l.id, l.name from levels as l, sub_levels as s where l.id = s.level_id and l.level_type = "counter" and l.assigned_user_id IS NOT NULL and s.district_id = '.$sub_levels->district_id.' and s.taluka_id = '.$sub_levels->taluka_id);*/
    }

    public static function getUnique($level_type, $state_id){
        return DB::select('select sl.* from sub_levels as sl, levels as l where sl.level_id = l.id and l.level_type="'.$level_type.'" and l.state_id = '.$state_id);
    }

    public function hasManyLevel(){
        return $this->hasMany(SubLevels::class,'level_id');
    }

    public static function commission($user,$role, $district_id = 0){
        $arr = ['sector', 'metro','region'];
        if($district_id){
            return DB::select("select us.id as user_id,us.name,l.*  from users as us, `levels` as l, sub_levels as s where us.id = l.assigned_user_id AND us.status = 'active' AND s.level_id = l.id AND l.level_type = '".$role."' AND s.district_id = ".$district_id);
        }else if(in_array($role,$arr)){
            return DB::select("select us.id as user_id,us.name,l.*  from users as us, `levels` as l, sub_levels as s where us.id = l.assigned_user_id AND us.status = 'active' AND s.level_id = l.id AND l.level_type = '".$role."' AND s.district_id = ".$user->district_id);
        }else{
            return DB::select("select us.id as user_id,us.name,l.*  from users as us, `levels` as l, sub_levels as s where us.id = l.assigned_user_id AND us.status = 'active' AND s.level_id = l.id AND l.level_type = '".$role."' AND s.taluka_id = ".$user->taluka_id." AND s.district_id = ".$user->district_id);
        }
    }

    public function userBdm()
    {
        return $this->belongsTo(User::class ,'archive_by_user_id', 'id');
    }
}

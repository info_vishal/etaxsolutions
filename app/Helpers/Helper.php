<?php

if (!function_exists('p')) {
    function p($data = [])
    {
        echo "<pre>";
        print_r($data);
        echo "<pre>";
    }
}


if (!function_exists('showError')) {
    function showError($msg){
        $html = '<ul class="parsley-errors-list filled"><li class="parsley-required">'.$msg.'</li></ul>';
        return $html;
    }
}

if (!function_exists('successAlert')) {
    function successAlert($msg){
        $html='<div class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>'.$msg.'</strong>
            </div>';
        return $html;
    }
}

if (!function_exists('errorAlert')) {
    function errorAlert($msg){
        $html='<div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>'.$msg.'</strong>
            </div>';
        return $html;
    }
}

if (!function_exists('filterDate')) {
    function filterDate($dateTime){
        return date("d-m-Y", strtotime($dateTime));
    }
}

if (!function_exists('clean')) {
    function clean($string) {
       $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
       $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
       $string = preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
       return (@trim($string))?trim($string):NULL;
    }
}

if (!function_exists('noImg')) {
    function noImg() {
        return asset('images/noimg.png');
    }
}

if (!function_exists('dbDate')) {
    function dbDate($date,$all = 0) {
        $date = str_replace("/", "-", $date);
        if($all)
            return date('Y-m-d h:i:s',strtotime($date));
        else
            return date('Y-m-d',strtotime($date));
    }
}

if (!function_exists('ddmmyy')) {
    function ddmmyy($date) {
        return date('d-m-Y',strtotime($date));
    }
}

if (!function_exists('userStatus')) {
    function userStatus() {
        $status['active'] = 'Active';
        $status['review'] = 'In Review';
        $status['reject'] = 'Reject';
        $status['suspend'] = 'Suspend';
        return $status;
    }
}

if (!function_exists('rechargeStatus')) {
    function rechargeStatus() {
        $status['accepted'] = 'Accepted';
        $status['requested'] = 'Requested';
        $status['rejected'] = 'Rejected';
        return $status;
    }
}

if (!function_exists('levels')) {
    function levels() {
        $status['sector'] = 'Sector';
        $status['metro'] = 'Metro';
        $status['region'] = 'Region';
        $status['district'] = 'District';
        $status['taluka'] = 'Taluka';
        $status['counter'] = 'Counter';
        return $status;
    }
}

if (!function_exists('relationship')) {
    function relationship() {
        $relationship['mother'] = 'Mother';
        $relationship['father'] = 'Father';
        $relationship['brother'] = 'Brother';
        $relationship['sister'] = 'Sister';
        $relationship['son'] = 'Son';
        $relationship['daughter'] = 'Daughter';
        $relationship['uncle'] = 'Uncle';
        $relationship['auntie'] = 'Auntie';
        $relationship['other'] = 'Other';
        return $relationship;
    }
}

if (!function_exists('file_upload')) {
    function file_upload($request, $data, $loop = 0){
        $dir = $data["dir"];
        $image = $data["file"];
        $originalName = $image->getClientOriginalName();
        if($loop){
            $file = $data["key"];
        }else{
            $file = $request->file($data["key"]);
        }

        $filename = $originalName."_". date('ymdhis') . '.' . $image->getClientOriginalExtension();
        if (!\Storage::disk('public')->exists($dir)) {
            \Storage::disk('public')->makeDirectory($dir);
        }
        if(@$data["old_file"]){
            $usersImage = public_path($data["old_file"]); // get previous image from 
            if (File::exists($usersImage)) { // unlink or remove previous image from folder
                unlink($usersImage);
            }
        }
        $storage = storage_path('app/public/'.$dir);
        if($file->move($storage, $filename))
            return $dir . '/' . $filename;
        else    
            return NULL;
    }
}

if (!function_exists('image_url')) {
    function image_url($data = NULL)
    {
        if(@$data == "NULL")
            return asset('storage/');
        else if(@$data)
            return asset('storage/' . $data);
        else 
            return asset('images/noimg.png');
    }
}

function userDocuments($request,$users = NULL){
    $data = [];
    if($request->hasFile('id_proof')){
        $img["key"]= "id_proof";
        $img["file"]= $request->id_proof;
        $img["dir"]= "user_documents";
        $img["old_file"]= @$users->id_proof;
        $data["id_proof"] = file_upload($request, $img);
    }

    if($request->hasFile('address_proof')){
        $img["key"]= "address_proof";
        $img["file"]= $request->address_proof;
        $img["dir"]= "user_documents";
        $img["old_file"]= @$users->address_proof;
        $data["address_proof"] = file_upload($request, $img);
    }

    if($request->hasFile('passport_photo')){
        $img["key"]= "passport_photo";
        $img["file"]= $request->passport_photo;
        $img["dir"]= "user_documents";
        $img["old_file"]= @$users->passport_photo;
        $data["passport_photo"] = file_upload($request, $img);
    }

    if($request->hasFile('passbook_copy')){
        $img["key"]= "passbook_copy";
        $img["file"]= $request->passbook_copy;
        $img["dir"]= "user_documents";
        $img["old_file"]= @$users->passbook_copy;
        $data["passbook_copy"] = file_upload($request, $img);
    }
    return $data;
}


function paymentMethod(){
    $method['cheque'] = 'Cheque';
    $method['cash'] = 'Cash';
    $method['neft'] = 'IMPS / NEFT / RTGS / ONLINE TRANSATION';
    $method['payu'] = 'Online Payment Gateway';
    $method['paytm'] = 'Paytm PG Wallet';
    return $method;
}

function arrayToGroupBy($array = [],$groupKey){
    $new = [];
    foreach ($array as $key => $value) {
        $new[$value[$groupKey]][] = $value;
    }
    return $new;
}

function customerGender(){
    $data["male"] = "Male";
    $data["female"] = "Female";
    $data["transgender"] = "Transgender";
    return $data;
}


function parentName(){
    $data["father"] = "Father";
    $data["mother"] = "Mother";
    return $data;
}

function addOfComunication(){
    $data["residence"] = "Residence";
    $data["office"] = "Office";
    return $data;
}

function status_of_applicant(){
    $data["government"] = "Government";
    $data["individual"] = "Residence";
    $data["hindu undivided family"] = "Hindu Undivided Family";
    $data["company"] = "Company";
    $data["partnership firm"] = "Partnership Firm";
    $data["association of persons"] = "Partnership Firm";
    $data["trusts"] = "Trusts";
    $data["body of individual"] = "Body of Individual";
    $data["local authority"] = "Local Authority";
    $data["limited liability partnership"] = "Limited Liability Partnership";
    return $data;
}

function source_of_income(){
    $data["salary"] = "Salary";
    $data["income from business / profession"] = "Income From Business / Profession";
    $data["income from house property"] = "Income From House Property";
    $data["business/profession code"] = "Business/Profession Code";
    $data["capital gains"] = "Capital Gains";
    $data["income from other source"] = "Income From Other Source";
    $data["no income"] = "No Income";
    return $data;
}

function pan_proof(){
    $data["aadhar card"] = "Aadhar Card";
    $data["voter id"] = "Voter ID";
    return $data;
}

function limiteServicePrice(){
    return 0;
}

function serviceStatus(){
    $data["pending"] = "Pending";
    $data["review"] = "Review";
    $data["rejected"] = "Rejected";
    $data["completed"] = "Completed";
    return $data;  
}

function cutCommissionfromLevel(){
    $data["counter"] = "Counter";
    $data["taluka"] = "Taluka";
    $data["district"] = "District";
    $data["metro"] = "Metro";
    $data["region"] = "Region";
    return $data;
}

function bankAccountType(){
    $data["saving"] = "Saving";
    $data["current"] = "Current";
    return $data;
}

function et102ContactType(){
    $data["self"] = "Self";
    $data["ca"] = "CA";
    $data["relative if any"] = "Relative if any";
    return $data;
}

function et103ContactType(){
    $data["Partner or director"] = "Partner or director";
    $data["ca"] = "CA";
    $data["Operation officer if any"] = "Operation officer if any";
    return $data;
}

function et103FirmOrCom(){
    $data["Firm"] = "Firm";
    $data["Company"] = "Company";
    return $data;
}


function et107ServiceType(){
    $data["Individual"] = "Individual";
    $data["Corporate"] = "Corporate";
    return $data;
}

function et110compType(){
    $data["Private"] = "Private";
    $data["Public"] = "Public";
    return $data;
}


function et110DirectorType(){
    $data["MD"] = "MD";
    $data["DIRECTOR"] = "DIRECTOR";
    $data["CEO"] = "CEO";
    return $data;
}

function redmeeStatus(){
    $data["pending"] = "Pending";
    $data["rejected"] = "Rejected";
    $data["completed"] = "Completed";
    return $data;  
}
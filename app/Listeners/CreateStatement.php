<?php

namespace App\Listeners;

use App\Events\Statement;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Statements;

class CreateStatement
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Statement  $event
     * @return void
     */
    public function handle(Statement $event)
    {
        $data = $event->data;
        $statements = new  Statements;
        $statements->id = \Uuid::generate()->string;
        $statements->user_id = $data["user_id"];
        $statements->to_user_id = $data["to_user_id"];
        $statements->descriptions = $data["descriptions"];
        $statements->price = $data["price"];
        $statements->type = $data["type"];
        $statements->save();
    }
}

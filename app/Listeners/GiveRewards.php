<?php

namespace App\Listeners;

use App\Events\Rewards;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Services;
use App\Events\Statement;

class GiveRewards
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = NULL;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Rewards  $event
     * @return void
     */
    public function handle(Rewards $event)
    {
        $receiver = $event->data;
        $serviceObject = $event->serviceObject;
        $this->service_id = $serviceObject->service_id;

        $getRoles = $receiver->getRole()->where('id','=',$receiver->roles_id)->first();

        $role = $receiver->getRole()->get()->toArray();
        $role = array_column($role, 'percentage','slug');

        $service = Services::find($this->service_id);
        $receiver->available_balance = $receiver->available_balance - $service->price;
        $receiver->save();


        $calc = 0;
        switch ($getRoles->slug) {
            case 'bdm':
                $percentage = $role['bdm'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->available_balance = $receiver->available_balance + $calc;
                $receiver->save();
                break;

            case 'counter':
                $percentage = $role['counter'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->reward_balance = $receiver->reward_balance + $calc;
                $receiver->save();

                $talukaUser = \App\Models\Levels::commission($receiver, 'taluka');
                if($talukaUser){
                    $talukaUser = $talukaUser[0];
                    $user = User::find($talukaUser->user_id);
                    $percentage = $role['taluka'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $districtUser = \App\Models\Levels::commission($receiver, 'district');
                if($districtUser){
                    $districtUser = $districtUser[0];
                    $user = User::find($districtUser->user_id);
                    $percentage = $role['district'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $regionUser = \App\Models\Levels::commission($receiver, 'region');
                if($regionUser){
                    $regionUser = $regionUser[0];
                    $user = User::find($regionUser->user_id);
                    $percentage = $role['region'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $metroUser = \App\Models\Levels::commission($receiver, 'metro');
                if($metroUser){
                    $metroUser = $metroUser[0];
                    $user = User::find($metroUser->user_id);
                    $percentage = $role['metro'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $sectorUser = \App\Models\Levels::commission($receiver, 'sector');
                if($sectorUser){
                    $sectorUser = $sectorUser[0];
                    $user = User::find($sectorUser->user_id);
                    $percentage = $role['sector'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }
                break;
            case 'taluka':
                $percentage = $role['counter'] + $role['taluka'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->reward_balance = $receiver->reward_balance + $calc;
                $receiver->save();

                $districtUser = \App\Models\Levels::commission($receiver, 'district');
                if($districtUser){
                    $districtUser = $districtUser[0];
                    $user = User::find($districtUser->user_id);
                    $percentage = $role['district'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $regionUser = \App\Models\Levels::commission($receiver, 'region');
                if($regionUser){
                    $regionUser = $regionUser[0];
                    $user = User::find($regionUser->user_id);
                    $percentage = $role['region'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $metroUser = \App\Models\Levels::commission($receiver, 'metro');
                if($metroUser){
                    $metroUser = $metroUser[0];
                    $user = User::find($metroUser->user_id);
                    $percentage = $role['metro'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $sectorUser = \App\Models\Levels::commission($receiver, 'sector');
                if($sectorUser){
                    $sectorUser = $sectorUser[0];
                    $user = User::find($sectorUser->user_id);
                    $percentage = $role['sector'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }
                break;
            case 'district':
                $percentage = $role['counter'] + $role['taluka'] + $role['district'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->reward_balance = $receiver->reward_balance + $calc;
                $receiver->save();

                $regionUser = \App\Models\Levels::commission($receiver, 'region');
                if($regionUser){
                    $regionUser = $regionUser[0];
                    $user = User::find($regionUser->user_id);
                    $percentage = $role['region'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $metroUser = \App\Models\Levels::commission($receiver, 'metro');
                if($metroUser){
                    $metroUser = $metroUser[0];
                    $user = User::find($metroUser->user_id);
                    $percentage = $role['metro'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }

                $sectorUser = \App\Models\Levels::commission($receiver, 'sector');
                if($sectorUser){
                    $sectorUser = $sectorUser[0];
                    $user = User::find($sectorUser->user_id);
                    $percentage = $role['sector'];
                    $calc = ($service->price * $percentage)/100;
                    $user->reward_balance = $user->reward_balance + $calc;
                    $user->save();
                }
                break;

            case 'region':
                $percentage = $role['counter'] + $role['taluka'] + $role['district'] + $role['region'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->reward_balance = $receiver->reward_balance + $calc;
                $receiver->save();

                $uniqueUser = [];
                $allDistricts = explode(", ",$receiver->district_id);
                foreach($allDistricts as $key => $value) {
                    $metroUser = \App\Models\Levels::commission($receiver, 'metro', $value);
                    if($metroUser){
                        $metroUser = $metroUser[0];
                        if(in_array($metroUser->user_id, $uniqueUser)){
                            continue;
                        }
                        $uniqueUser[$metroUser->user_id] = $metroUser->user_id;
                        
                        $user = User::find($metroUser->user_id);
                        $percentage = $role['metro'];
                        $calc = ($service->price * $percentage)/100;
                        $user->reward_balance = $user->reward_balance + $calc;
                        $user->save();
                    }
                }
                
                $uniqueUser = [];
                foreach($allDistricts as $key => $value) {
                    $sectorUser = \App\Models\Levels::commission($receiver, 'sector', $value);
                    if($sectorUser){
                        $sectorUser = $sectorUser[0];
                        if(in_array($sectorUser->user_id, $uniqueUser)){
                            continue;
                        }
                        $uniqueUser[$sectorUser->user_id] = $sectorUser->user_id;
                       
                        $user = User::find($sectorUser->user_id);
                        $percentage = $role['sector'];
                        $calc = ($service->price * $percentage)/100;
                        $user->reward_balance = $user->reward_balance + $calc;
                        $user->save();
                    }
                }

                break;

            case 'metro':
                $percentage = $role['counter'] + $role['taluka'] + $role['district'] + $role['region'] + $role['metro'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->reward_balance = $receiver->reward_balance + $calc;
                $receiver->save();

                $uniqueUser = [];
                $allDistricts = explode(", ",$receiver->district_id);
                foreach($allDistricts as $key => $value) {
                    $sectorUser = \App\Models\Levels::commission($receiver, 'sector',$value);
                    if($sectorUser){
                        $sectorUser = $sectorUser[0];
                        if(in_array($sectorUser->user_id, $uniqueUser)){
                            continue;
                        }
                        $uniqueUser[$sectorUser->user_id] = $sectorUser->user_id;
                        $user = User::find($sectorUser->user_id);
                        $percentage = $role['sector'];
                        $calc = ($service->price * $percentage)/100;
                        $user->reward_balance = $user->reward_balance + $calc;
                        $user->save();
                    }
                }
                break;

            case 'sector':
                $percentage = $role['counter'] + $role['taluka'] + $role['district'] + $role['region'] + $role['metro'] + $role['sector'];
                $ownReward = $calc = ($service->price * $percentage)/100;
                $receiver->reward_balance = $receiver->reward_balance + $calc;
                $receiver->save();
                break;

        }

        $statements["user_id"]=\Auth::User()->id;
        $statements["to_user_id"]=$receiver->id;
        $statements["descriptions"]="Debited for ".$service->name ."(". $service->code .") service - Tracking No: ".$serviceObject->tracking_no;
        $statements["price"]=$service->price;
        $statements["type"]="debited";
        event(new Statement($statements));

        if($ownReward > 0){
            $statements["user_id"]=\Auth::User()->id;
            $statements["to_user_id"]=$receiver->id;
            $statements["descriptions"]="You get the reward balance for ".$service->name ."(". $service->code .") service - Tracking No: ".$serviceObject->tracking_no;
            $statements["price"]=$ownReward;
            $statements["type"]="credited";
            event(new Statement($statements));
        }
    }
}

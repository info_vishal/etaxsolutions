<?php

namespace App\Listeners;

use App\Events\ET111;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


use App\Models\Et111 as EtModel;
use App\Models\Et111Childs as EtModelChild;
use App\Notifications\Et111 as Notified;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;
class ManageET111
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 11;
    public $service_code = "et-111";
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ET111  $event
     * @return void
     */
    public function handle(ET111 $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
    }

    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new EtModel;
        
        if($request->hasFile('trust_rule_file')){
            $img["key"]= "trust_rule_file";
            $img["file"]= $request->trust_rule_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->trust_rule_file = file_upload($request, $img);
        }

        if($request->hasFile('assets_file')){
            $img["key"]= "assets_file";
            $img["file"]= $request->assets_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->assets_file = file_upload($request, $img);
        }

        if($request->hasFile('address_proof')){
            $img["key"]= "address_proof";
            $img["file"]= $request->address_proof;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->address_proof = file_upload($request, $img);
        }

        if($request->hasFile('thrav_copy')){
            $img["key"]= "thrav_copy";
            $img["file"]= $request->thrav_copy;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->thrav_copy = file_upload($request, $img);
        }

        if($request->hasFile('any_other_file')){
            $img["key"]= "any_other_file";
            $img["file"]= $request->any_other_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->any_other_file = file_upload($request, $img);
        }

        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->amount_received = $request->amount_received;


        $newPancard->trustee_name = $request->trustee_name;
        $newPancard->trustee_narration = $request->trustee_narration;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        $newPancard->save();

        if(@$request->director){
            foreach ($request->director as $key => $value) {
                $child = new EtModelChild;
                $child->id = \Uuid::generate()->string;
                $child->service_type_id = $newPancard->id;
                $files = $request->files->all()['director'][$key];
                foreach ($value as $c_key => $c_value) {
                    if(@$request->files->all()['director'][$key][$c_key]){
                        $img["key"]= $request->files->all()['director'][$key][$c_key];
                        $img["file"]= $value[$c_key];
                        $img["dir"]= "services/".$this->service_code;
                        $img["old_file"]= null;
                        $c_value = file_upload($request, $img,1);
                    }else if($c_key == "mobile_number"){
                        $c_value = clean($c_value);
                    }
                    $child->$c_key = $c_value;
                }
                $child->save();
            }
        }

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new Notified($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){

        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = EtModel::find($request->service_id);

        $newPancard->accepted_by = $login_user->id;
        $oldStatus = $newPancard->status;
        $newPancard->status = $request->status;
        
        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new Notified($login_user,$body));
        return $newPancard;
    }
}

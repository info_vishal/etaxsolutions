<?php

namespace App\Listeners;

use App\Events\ET102;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Et102 as EtModel;
use App\Notifications\Et102 as Notified;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;
class ManageET102
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 2;
    public $service_code = "et-102";
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  ET102  $event
     * @return void
     */
    public function handle(ET102 $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
    }

    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new EtModel;
        
        if($request->hasFile('pancard_file')){
            $img["key"]= "pancard_file";
            $img["file"]= $request->pancard_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->pancard_file = file_upload($request, $img);
        }

        if($request->hasFile('aadharcard_file')){
            $img["key"]= "aadharcard_file";
            $img["file"]= $request->aadharcard_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->aadharcard_file = file_upload($request, $img);
        }

        if($request->hasFile('form16_file')){
            $img["key"]= "form16_file";
            $img["file"]= $request->form16_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->form16_file = file_upload($request, $img);
        }

        if($request->hasFile('bank_statement_file')){
            $img["key"]= "bank_statement_file";
            $img["file"]= $request->bank_statement_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->bank_statement_file = file_upload($request, $img);
        }

        if($request->hasFile('edu_file')){
            $img["key"]= "edu_file";
            $img["file"]= $request->edu_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->edu_file = file_upload($request, $img);
        }

        if($request->hasFile('itr_file')){
            $img["key"]= "itr_file";
            $img["file"]= $request->itr_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->itr_file = file_upload($request, $img);
        }

        if($request->hasFile('any_other_file')){
            $img["key"]= "any_other_file";
            $img["file"]= $request->any_other_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->any_other_file = file_upload($request, $img);
        }

        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->amount_received = $request->amount_received;

        $newPancard->cli_last_name = $request->cli_last_name;
        $newPancard->cli_first_name = $request->cli_first_name;
        $newPancard->cli_middle_name = $request->cli_middle_name;
        $newPancard->pancard_no = $request->pancard_no;
        $newPancard->cli_gender = $request->cli_gender;
        $newPancard->cli_dob = date("Y-m-d h:i:s",strtotime($request->cli_dob));

        $newPancard->father_full_name = $request->father_full_name;
        $newPancard->mother_full_name = $request->mother_full_name;

        $newPancard->aadhar_allotted = $request->aadhar_allotted;
        $newPancard->aadhar_not_allotted = $request->aadhar_not_allotted;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        
        $newPancard->landline_no = $request->landline_no;
        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->sec_mobile_number = $request->sec_mobile_number;
        $newPancard->email_id = $request->email_id;
        $newPancard->sec_email_id = $request->sec_email_id;
        $newPancard->contact_type = $request->contact_type;

        /*$newPancard->bank_ifsc = $request->bank_ifsc;
        $newPancard->bank_name = $request->bank_name;
        $newPancard->bank_branch = $request->bank_branch;
        $newPancard->bank_account_type = $request->bank_account_type;
        $newPancard->bank_account_number = $request->bank_account_number;
        $newPancard->bank_holder_name = $request->bank_holder_name;*/

        $newPancard->save();

        if(count(@$request->clientbank) > 0){
            foreach ($request->clientbank as $key => $value) {
                $bank = new \App\Models\ClientBankDetails;
                $bank->id = \Uuid::generate()->string;
                $bank->service_id = $this->service_id;
                $bank->service_type_id = $newPancard->id;
                $bank->created_by = $newPancard->created_by;
                $bank->bank_ifsc = $value["bank_ifsc"];
                $bank->bank_name = $value["bank_name"];
                $bank->bank_branch = $value["bank_branch"];
                $bank->bank_account_type = $value["bank_account_type"];
                $bank->bank_account_number = $value["bank_account_number"];
                $bank->bank_holder_name = $value["bank_holder_name"];
                $bank->save();
            }
        }

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new Notified($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){
        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = EtModel::find($request->service_id);

        $oldStatus = $newPancard->status;
        $newPancard->status = $request->status;
        $newPancard->accepted_by = $login_user->id;

        /*$newPancard->amount_received = $request->amount_received;
        $newPancard->cli_last_name = $request->cli_last_name;
        $newPancard->cli_first_name = $request->cli_first_name;
        $newPancard->cli_middle_name = $request->cli_middle_name;
        $newPancard->pancard_no = $request->pancard_no;
        $newPancard->cli_gender = $request->cli_gender;
        $newPancard->cli_dob = date("Y-m-d h:i:s",strtotime($request->cli_dob));

        $newPancard->father_full_name = $request->father_full_name;
        $newPancard->mother_full_name = $request->mother_full_name;

        $newPancard->aadhar_allotted = $request->aadhar_allotted;
        $newPancard->aadhar_not_allotted = $request->aadhar_not_allotted;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        
        $newPancard->landline_no = $request->landline_no;
        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->sec_mobile_number = $request->sec_mobile_number;
        $newPancard->email_id = $request->email_id;
        $newPancard->sec_email_id = $request->sec_email_id;
        $newPancard->contact_type = $request->contact_type;

        $newPancard->bank_ifsc = $request->bank_ifsc;
        $newPancard->bank_name = $request->bank_name;
        $newPancard->bank_branch = $request->bank_branch;
        $newPancard->bank_account_type = $request->bank_account_type;
        $newPancard->bank_account_number = $request->bank_account_number;
        $newPancard->bank_holder_name = $request->bank_holder_name;*/

        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new Notified($login_user,$body));
        return $newPancard;
    }
}

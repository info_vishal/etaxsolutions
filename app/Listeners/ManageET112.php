<?php

namespace App\Listeners;

use App\Events\ET112;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Et112 as EtModel;
use App\Notifications\Et112 as Notified;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;
class ManageET112
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 12;
    public $service_code = "et-112";
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ET112  $event
     * @return void
     */
    public function handle(ET112 $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
    }

    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new EtModel;
        
        $files = $request->files->all();
        foreach ($files as $c_key => $c_value) {
            $img = [];
            $img["key"]= $request->files->all()[$c_key];
            $img["file"]= $c_value;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $c_value = file_upload($request, $img,1);
            $newPancard->$c_key = $c_value;
        }


        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->amount_received = $request->amount_received;

        $newPancard->name_trust = $request->name_trust;
        $newPancard->trust_narration = $request->trust_narration;
        $newPancard->name_school = $request->name_school;
        $newPancard->financial_year = $request->financial_year;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;

        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->email_id = $request->email_id;

        $newPancard->save();

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new Notified($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){

        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = EtModel::find($request->service_id);

        $newPancard->accepted_by = $login_user->id;
        $oldStatus = $newPancard->status;
        $newPancard->status = $request->status;
        
        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new Notified($login_user,$body));
        return $newPancard;
    }
}

<?php

namespace App\Listeners;

use App\Events\ET107;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Et107 as EtModel;
use App\Notifications\Et107 as Notified;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;
class ManageET107
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 7;
    public $service_code = "et-107";
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  ET107  $event
     * @return void
     */
    public function handle(ET107 $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
    }

    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new EtModel;
        
        if($request->hasFile('financial_statement_file')){
            $img["key"]= "financial_statement_file";
            $img["file"]= $request->financial_statement_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->financial_statement_file = file_upload($request, $img);
        }

        if($request->hasFile('kyc_document_file')){
            $img["key"]= "kyc_document_file";
            $img["file"]= $request->kyc_document_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->kyc_document_file = file_upload($request, $img);
        }

        if($request->hasFile('any_other_file')){
            $img["key"]= "any_other_file";
            $img["file"]= $request->any_other_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->any_other_file = file_upload($request, $img);
        }

        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->amount_received = $request->amount_received;

        $newPancard->individaul_corporate = $request->individaul_corporate;
        $newPancard->pancard_no = $request->pancard_no;
        $newPancard->full_name = $request->full_name;
        $newPancard->cli_dob = date("Y-m-d h:i:s",strtotime($request->cli_dob));
        
        if($request->individaul_corporate == "Individual")
            $newPancard->cli_gender = $request->cli_gender;

        $newPancard->aadhar_allotted = $request->aadhar_allotted;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        
        $newPancard->landline_no = $request->landline_no;
        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->sec_mobile_number = $request->sec_mobile_number;
        $newPancard->email_id = $request->email_id;
        $newPancard->sec_email_id = $request->sec_email_id;
        $newPancard->contact_type = $request->contact_type;


        $newPancard->save();
        
        if(count(@$request->clientbank) > 0){
            foreach ($request->clientbank as $key => $value) {
                $bank = new \App\Models\ClientBankDetails;
                $bank->id = \Uuid::generate()->string;
                $bank->service_id = $this->service_id;
                $bank->service_type_id = $newPancard->id;
                $bank->created_by = $newPancard->created_by;
                $bank->bank_ifsc = $value["bank_ifsc"];
                $bank->bank_name = $value["bank_name"];
                $bank->bank_branch = $value["bank_branch"];
                $bank->bank_account_type = $value["bank_account_type"];
                $bank->bank_account_number = $value["bank_account_number"];
                $bank->bank_holder_name = $value["bank_holder_name"];
                $bank->save();
            }
        }
        

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new Notified($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){
        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = EtModel::find($request->service_id);

        $oldStatus = $newPancard->status;
        $newPancard->status = $request->status;
        $newPancard->accepted_by = $login_user->id;

        /*$newPancard->amount_received = $request->amount_received;
        $newPancard->individaul_corporate = $request->individaul_corporate;
        $newPancard->pancard_no = $request->pancard_no;
        $newPancard->full_name = $request->full_name;
        $newPancard->cli_dob = date("Y-m-d h:i:s",strtotime($request->cli_dob));
        
        if($request->individaul_corporate == "Individual")
            $newPancard->cli_gender = $request->cli_gender;

        $newPancard->aadhar_allotted = $request->aadhar_allotted;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        
        $newPancard->landline_no = $request->landline_no;
        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->sec_mobile_number = $request->sec_mobile_number;
        $newPancard->email_id = $request->email_id;
        $newPancard->sec_email_id = $request->sec_email_id;
        $newPancard->contact_type = $request->contact_type;*/

        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        if(count(@$request->clientbank) > 0){
            foreach ($request->clientbank as $key => $value) {

                if(@$value['bank_id']){
                    $bank = \App\Models\ClientBankDetails::find($value['bank_id']);
                }else{
                    $bank = new \App\Models\ClientBankDetails;
                    $bank->id = \Uuid::generate()->string;
                    $bank->service_id = $this->service_id;
                    $bank->service_type_id = $newPancard->id;
                    $bank->created_by = $newPancard->created_by;
                }
                $bank->bank_ifsc = $value["bank_ifsc"];
                $bank->bank_name = $value["bank_name"];
                $bank->bank_branch = $value["bank_branch"];
                $bank->bank_account_type = $value["bank_account_type"];
                $bank->bank_account_number = $value["bank_account_number"];
                $bank->bank_holder_name = $value["bank_holder_name"];
                $bank->save();
            }
        }

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new Notified($login_user,$body));
        return $newPancard;
    }
}

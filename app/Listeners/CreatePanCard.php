<?php

namespace App\Listeners;

use App\Events\PanCard;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\PanCards;
use App\Notifications\PanCard as PanCardNotify;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;

class CreatePanCard
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 1;
    public $service_code = "et-101";
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PanCard  $event
     * @return void
     */
    public function handle(PanCard $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
        
    }

    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new PanCards;
        

        if($request->hasFile('id_proof_file')){
            $img["key"]= "id_proof_file";
            $img["file"]= $request->id_proof_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->id_proof_file = file_upload($request, $img);
        }

        if($request->hasFile('address_proof_file')){
            $img["key"]= "address_proof_file";
            $img["file"]= $request->address_proof_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->address_proof_file = file_upload($request, $img);
        }

        if($request->hasFile('dob_file')){
            $img["key"]= "dob_file";
            $img["file"]= $request->dob_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->dob_file = file_upload($request, $img);
        }

        if($request->hasFile('signature_file')){
            $img["key"]= "signature_file";
            $img["file"]= $request->signature_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->signature_file = file_upload($request, $img);
        }

        if($request->hasFile('passport_size_file')){
            $img["key"]= "passport_size_file";
            $img["file"]= $request->passport_size_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->passport_size_file = file_upload($request, $img);
        }

        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->amount_received = $request->amount_received;
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->cli_last_name = $request->cli_last_name;
        $newPancard->cli_first_name = $request->cli_first_name;
        $newPancard->cli_middle_name = $request->cli_middle_name;
        $newPancard->cli_name_on_card = $request->cli_name_on_card;
        $newPancard->cli_gender = $request->cli_gender;
        $newPancard->cli_dob = date("Y-m-d h:i:s",strtotime($request->cli_dob));

        $newPancard->mot_last_name = $request->mot_last_name;
        $newPancard->mot_first_name = $request->mot_first_name;
        $newPancard->mot_middle_name = $request->mot_middle_name;

        $newPancard->fat_last_name = $request->fat_last_name;
        $newPancard->fat_first_name = $request->fat_first_name;
        $newPancard->fat_middle_name = $request->fat_middle_name;
        $newPancard->parent_name_on_card = $request->parent_name_on_card;

        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_country = $request->res_add_country;

        $newPancard->ofc_name = $request->ofc_name;
        $newPancard->ofc_add_flat = $request->ofc_add_flat;
        $newPancard->ofc_add_building = $request->ofc_add_building;
        $newPancard->ofc_add_street = $request->ofc_add_street;
        $newPancard->ofc_add_area = $request->ofc_add_area;
        $newPancard->ofc_add_pincode = $request->ofc_add_pincode;
        $newPancard->ofc_add_city = $request->ofc_add_city;
        $newPancard->ofc_add_state = $request->ofc_add_state;
        $newPancard->ofc_add_country = $request->ofc_add_country;

        $newPancard->address_of_com = $request->address_of_com;
        
        $newPancard->country_code = $request->country_code;
        $newPancard->area_code = $request->area_code;
        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->email_id = $request->email_id;

        $newPancard->status_of_applicant = $request->status_of_applicant;

        $newPancard->company_reg_number = $request->company_reg_number;

        $newPancard->aadhar_allotted = $request->aadhar_allotted;
        $newPancard->aadhar_not_allotted = $request->aadhar_not_allotted;
        $newPancard->aadhar_name = $request->aadhar_name;
        
        $newPancard->source_of_income = $request->source_of_income;

        //$newPancard->id_proof = $request->id_proof;

        //$newPancard->address_proof = $request->address_proof;
        $newPancard->save();

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new PanCardNotify($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){
        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = PanCards::find($request->service_id);
        $oldStatus = $newPancard->status;
        $newPancard->accepted_by = $login_user->id;
        
        $newPancard->status = $request->status;

        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new PanCardNotify($login_user,$body));
        return $newPancard;
    }
}

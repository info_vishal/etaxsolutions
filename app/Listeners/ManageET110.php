<?php

namespace App\Listeners;

use App\Events\ET110;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Et110 as EtModel;
use App\Models\Et110Childs as EtModelChild;
use App\Notifications\Et110 as Notified;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;
class ManageET110
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 10;
    public $service_code = "et-110";
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ET110  $event
     * @return void
     */
    public function handle(ET110 $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
    }

    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new EtModel;
        
        if($request->hasFile('company_address_file')){
            $img["key"]= "company_address_file";
            $img["file"]= $request->company_address_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->company_address_file = file_upload($request, $img);
        }

        if($request->hasFile('company_anyother_file')){
            $img["key"]= "company_anyother_file";
            $img["file"]= $request->company_anyother_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->company_anyother_file = file_upload($request, $img);
        }

        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->amount_received = $request->amount_received;

        $newPancard->company_type = $request->company_type;
        $name = array_filter($request->company_name);
        $name = implode("@$", $name);
        $newPancard->company_name = $name;
        $newPancard->company_object = $request->company_object;
        $newPancard->company_brief_details = $request->company_brief_details;

        $newPancard->company_add_flat = $request->company_add_flat;
        $newPancard->company_add_building = $request->company_add_building;
        $newPancard->company_add_street = $request->company_add_street;
        $newPancard->company_add_area = $request->company_add_area;
        $newPancard->company_add_pincode = $request->company_add_pincode;
        $newPancard->company_add_city = $request->company_add_city;
        $newPancard->company_add_state = $request->company_add_state;
        $newPancard->company_add_district = $request->company_add_district;
        $newPancard->company_add_country = $request->company_add_country;

        $newPancard->company_landline_no = clean($request->company_landline_no);
        $newPancard->save();
        if(@$request->director){
            foreach ($request->director as $key => $value) {
                $child = new EtModelChild;
                $child->id = \Uuid::generate()->string;
                $child->service_type_id = $newPancard->id;
                $files = $request->files->all()['director'][$key];
                foreach ($value as $c_key => $c_value) {
                    if(@$request->files->all()['director'][$key][$c_key]){
                        $img["key"]= $request->files->all()['director'][$key][$c_key];
                        $img["file"]= $value[$c_key];
                        $img["dir"]= "services/".$this->service_code;
                        $img["old_file"]= null;
                        $c_value = file_upload($request, $img,1);
                    }else if($c_key == "mobile_number"){
                        $c_value = clean($c_value);
                    }
                    $child->$c_key = $c_value;
                }
                $child->save();
            }
        }

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new Notified($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){

        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = EtModel::find($request->service_id);

        $newPancard->accepted_by = $login_user->id;
        $oldStatus = $newPancard->status;
        $newPancard->status = $request->status;
        
        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new Notified($login_user,$body));
        return $newPancard;
    }
}

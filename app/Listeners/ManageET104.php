<?php

namespace App\Listeners;

use App\Events\ET104;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Et104 as EtModel;
use App\Notifications\Et104 as Notified;
use App\Models\User;
use App\Models\ServiceLogs;
use App\Events\Rewards;
class ManageET104
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $service_id = 4;
    public $service_code = "et-104";
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ET104  $event
     * @return void
     */
    public function handle(ET104 $event)
    {
        $request =  $event->data;
        if($request->isMethod('post')){
            return $this->create($event);
        }
        else if($request->isMethod('put')){
            return $this->update($event);
        }
    }


    public function create($event){
        $request =  $event->data;
        $login_user = $event->loginUser;
        $newPancard = new EtModel;
        
        if($request->hasFile('pancard_file')){
            $img["key"]= "pancard_file";
            $img["file"]= $request->pancard_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->pancard_file = file_upload($request, $img);
        }

        if($request->hasFile('proof_business_reg_file')){
            $img["key"]= "proof_business_reg_file";
            $img["file"]= $request->proof_business_reg_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->proof_business_reg_file = file_upload($request, $img);
        }

        if($request->hasFile('identity_address_file')){
            $img["key"]= "identity_address_file";
            $img["file"]= $request->identity_address_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->identity_address_file = file_upload($request, $img);
        }

        if($request->hasFile('bank_statement_file')){
            $img["key"]= "bank_statement_file";
            $img["file"]= $request->bank_statement_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->bank_statement_file = file_upload($request, $img);
        }

        if($request->hasFile('address_business_file')){
            $img["key"]= "address_business_file";
            $img["file"]= $request->address_business_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->address_business_file = file_upload($request, $img);
        }

        if($request->hasFile('any_other_file')){
            $img["key"]= "any_other_file";
            $img["file"]= $request->any_other_file;
            $img["dir"]= "services/".$this->service_code;
            $img["old_file"]= null;
            $newPancard->any_other_file = file_upload($request, $img);
        }

        $newPancard->tracking_no = $this->service_code."-".date("Ymdhis");
        $newPancard->receipt_no = date("Ymdhis", strtotime("+1 day"));
        $newPancard->id = \Uuid::generate()->string;
        $newPancard->service_id = $this->service_id;
        $newPancard->created_by = $login_user->id;

        $newPancard->amount_received = $request->amount_received;

        $newPancard->legal_name_business = $request->legal_name_business;
        $newPancard->constitution_business = $request->constitution_business;
    
        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        
        $newPancard->name_of_director = $request->name_of_director;
        $newPancard->address_of_director = $request->address_of_director;
        $newPancard->nature_of_business = $request->nature_of_business;
        $newPancard->place_of_business = $request->place_of_business;


        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->sec_mobile_number = $request->sec_mobile_number;
        $newPancard->email_id = $request->email_id;
        $newPancard->sec_email_id = $request->sec_email_id;

        $newPancard->other_business = $request->other_business;
        $newPancard->any_other_details = $request->any_other_details;

        $newPancard->save();

        $body['type'] = "service-new-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." submitted ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;

        $admin = User::whereIn('roles_id',[8,7])->get();

        // notify to admin
        foreach ($admin as $key => $value) {
            $value->notify(new Notified($login_user,$body));
        }
        return $newPancard;
    }

    public function update($event){
        $request =  $event->data;
        $login_user = $event->loginUser;

        $newPancard = EtModel::find($request->service_id);

        $oldStatus = $newPancard->status;
        $newPancard->status = $request->status;
        $newPancard->accepted_by = $login_user->id;

        /*$newPancard->amount_received = $request->amount_received;

        $newPancard->legal_name_business = $request->legal_name_business;
        $newPancard->constitution_business = $request->constitution_business;
    
        $newPancard->res_add_flat = $request->res_add_flat;
        $newPancard->res_add_building = $request->res_add_building;
        $newPancard->res_add_street = $request->res_add_street;
        $newPancard->res_add_area = $request->res_add_area;
        $newPancard->res_add_pincode = $request->res_add_pincode;
        $newPancard->res_add_city = $request->res_add_city;
        $newPancard->res_add_state = $request->res_add_state;
        $newPancard->res_add_district = $request->res_add_district;
        $newPancard->res_add_country = $request->res_add_country;
        
        $newPancard->name_of_director = $request->name_of_director;
        $newPancard->address_of_director = $request->address_of_director;
        $newPancard->nature_of_business = $request->nature_of_business;
        $newPancard->place_of_business = $request->place_of_business;

        $newPancard->mobile_number = $request->mobile_number;
        $newPancard->sec_mobile_number = $request->sec_mobile_number;
        $newPancard->email_id = $request->email_id;
        $newPancard->sec_email_id = $request->sec_email_id;

        $newPancard->other_business = $request->other_business;
        $newPancard->any_other_details = $request->any_other_details;*/

        $isDirty = $newPancard->isDirty();
        $getDirty = $newPancard->getDirty();

        $newPancard->save();

        // receiver, user model, 
        $receiver = User::find($newPancard->created_by);
        if($request->status == "completed"){
            event(new Rewards($receiver, $newPancard));
        }

        if($isDirty){
            if(array_key_exists('status', $getDirty)){
                $newLog = new ServiceLogs;
                $newLog->id = \Uuid::generate()->string;
                $newLog->service_id = $this->service_id;
                $newLog->service_type_id = $newPancard->id;
                $newLog->created_by = $login_user->id;
                $newLog->descriptions = "Status changed from ".$oldStatus." to ".$getDirty['status'];
                $newLog->save();
            }
        }
        $body['type'] = "service-update-".$this->service_code;
        $body['id'] = $newPancard->id;
        $body['message'] = $login_user->employee_code." updated ".$this->service_code." service, Tracking No: ".$newPancard->tracking_no;
        $receiver->notify(new Notified($login_user,$body));
        return  $newPancard;
    }
}

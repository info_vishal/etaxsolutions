<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Statement' => [
            'App\Listeners\CreateStatement',
        ],
        'App\Events\PanCard' => [
            'App\Listeners\CreatePanCard',
        ],
        'App\Events\ET102' => [
            'App\Listeners\ManageET102',
        ],
        'App\Events\ET103' => [
            'App\Listeners\ManageET103',
        ],
        'App\Events\ET104' => [
            'App\Listeners\ManageET104',
        ],
        'App\Events\ET105' => [
            'App\Listeners\ManageET105',
        ],
        'App\Events\ET106' => [
            'App\Listeners\ManageET106',
        ],
        'App\Events\ET107' => [
            'App\Listeners\ManageET107',
        ],
        'App\Events\ET108' => [
            'App\Listeners\ManageET108',
        ],
        'App\Events\ET109' => [
            'App\Listeners\ManageET109',
        ],
        'App\Events\ET110' => [
            'App\Listeners\ManageET110',
        ],
        'App\Events\ET111' => [
            'App\Listeners\ManageET111',
        ],
        'App\Events\ET112' => [
            'App\Listeners\ManageET112',
        ],
        'App\Events\ET113' => [
            'App\Listeners\ManageET113',
        ],
        'App\Events\ET114' => [
            'App\Listeners\ManageET114',
        ],
        'App\Events\Rewards' => [
            'App\Listeners\GiveRewards',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

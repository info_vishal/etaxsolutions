var adminCommon = {
    init: function(){
       this.common(); 
    },
    common: function(){
        /*var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5c570f356cb1ff3c14caf02d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();*/

        if($('.select2').length)
          $(".select2").select2();
        
        $(document).on("click","a.delete",function(){
            var url = this.dataset.code;
            swal({
              title: "Are you sure?",
              text: "Your will not be able to recover this record!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                window.location.href = url;
            });
        });

        $(document).on("click","a.etax_notification",function(){
            var action = this.dataset.action;
            var id = this.dataset.notify_id;
            var route = this.dataset.route;
            var token = this.dataset.token;
            $.ajax({
                url: route,
                data: { 'id': id,'_token': token },
                dataType: 'JSON',
                type: 'POST',
                success: function(response){
                    window.location.href = action;
                }
            });
        });

        $('.datepicker-etax, #datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            endDate: '{{ date("d-m-Y") }}',
            format: 'dd-mm-yyyy',
        });

        $('.datepicker-etax, #dob').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            endDate: '{{ date("d-m-Y") }}',
        });
    },
};
$(document).ready(function(){
    adminCommon.init();
});
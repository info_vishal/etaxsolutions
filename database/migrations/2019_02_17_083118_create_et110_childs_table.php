<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEt110ChildsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('et110_childs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('service_type_id')->nullable();

            $table->string('director_post')->nullable();
            $table->string('director_name')->nullable();

            $table->string('res_add_flat')->nullable();
            $table->string('res_add_building')->nullable();
            $table->string('res_add_street')->nullable();
            $table->string('res_add_area')->nullable();
            $table->string('res_add_pincode')->nullable();
            $table->string('res_add_city')->nullable();
            $table->string('res_add_district')->nullable();
            $table->string('res_add_state')->nullable();
            $table->string('res_add_country')->nullable();

            $table->string('bank_ifsc')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_account_type')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('bank_holder_name')->nullable();

            $table->string('mobile_number')->nullable();
            $table->string('email_id')->nullable();

            $table->string('photo_file')->nullable();
            $table->string('pancard_file')->nullable();
            $table->string('aadharcard_file')->nullable();
            $table->string('bank_statement_file')->nullable();
            $table->string('dsc_file')->nullable();
            $table->string('any_other_file')->nullable();

            $table->foreign('service_type_id')->references('id')->on('et110s')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('et110_childs');
    }
}

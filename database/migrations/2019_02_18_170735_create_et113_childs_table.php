<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEt113ChildsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('et113_childs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('service_type_id')->nullable();

            $table->string('partner_name')->nullable();
            $table->string('partner_percentage')->nullable();

            $table->string('res_add_flat')->nullable();
            $table->string('res_add_building')->nullable();
            $table->string('res_add_street')->nullable();
            $table->string('res_add_area')->nullable();
            $table->string('res_add_pincode')->nullable();
            $table->string('res_add_city')->nullable();
            $table->string('res_add_district')->nullable();
            $table->string('res_add_state')->nullable();
            $table->string('res_add_country')->nullable();

            $table->string('mobile_number')->nullable();
            $table->string('sec_mobile_number')->nullable();
            $table->string('email_id')->nullable();
            $table->string('sec_email_id')->nullable();

            $table->string('idproof_file')->nullable();
            $table->string('address_proof_file')->nullable();
            $table->string('photo_file')->nullable();

            $table->foreign('service_type_id')->references('id')->on('et113s')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('et113_childs');
    }
}

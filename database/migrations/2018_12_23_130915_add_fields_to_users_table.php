<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('created_by')->nullable();
            $table->string('name')->nullable();
            $table->string('father_husband_name')->nullable();
            $table->float('available_balance',8,2)->default(0);
            $table->float('reward_balance',8,2)->default(0);
            $table->integer('roles_id')->unsigned()->nullable();
            $table->string('status')->nullable();
            $table->bigInteger('primary_mobile')->unique();
            $table->bigInteger('alternate_mobile')->nullable();
            $table->date('dob')->nullable();
            $table->string('sex')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('present_address')->nullable();
            $table->integer('pincode')->nullable();
            $table->integer('state_id')->unsigned()->nullable();
            $table->string('city')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('employee_code')->nullable();
            $table->string('nominee_name')->nullable();
            $table->string('relationship')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('id_proof')->nullable();
            $table->string('address_proof')->nullable();
            $table->string('passport_photo')->nullable();
            $table->string('passbook_copy')->nullable();
            $table->text('reject_reason')->nullable();
            $table->string('bank_ac_no')->nullable();
            $table->string('bank_ac_holder')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->text('district_id')->nullable();
            $table->text('taluka_id')->nullable();

            $table->softDeletes();
            
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('roles_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

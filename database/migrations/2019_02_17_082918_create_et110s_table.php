<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEt110sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('et110s', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('service_id')->unsigned()->default(10);
            $table->integer('created_by')->unsigned();
            $table->integer('accepted_by')->unsigned()->nullable();
            $table->string('status')->default('pending');
            $table->float('amount_received')->default(0);
            $table->string('receipt_no')->nullable();
            $table->string('tracking_no')->nullable();
            
            $table->string('company_type')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_object')->nullable();
            $table->string('company_brief_details')->nullable();

            $table->string('company_add_flat')->nullable();
            $table->string('company_add_building')->nullable();
            $table->string('company_add_street')->nullable();
            $table->string('company_add_area')->nullable();
            $table->string('company_add_pincode')->nullable();
            $table->string('company_add_city')->nullable();
            $table->string('company_add_district')->nullable();
            $table->string('company_add_state')->nullable();
            $table->string('company_add_country')->nullable();

            $table->string('company_landline_no')->nullable();

            $table->string('company_address_file')->nullable();
            $table->string('company_anyother_file')->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('et110s');
    }
}

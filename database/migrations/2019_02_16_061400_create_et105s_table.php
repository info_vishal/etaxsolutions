<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEt105sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('et105s', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('service_id')->unsigned()->default(5);
            $table->integer('created_by')->unsigned();
            $table->integer('accepted_by')->unsigned()->nullable();
            $table->string('status')->default('pending');
            $table->float('amount_received')->default(0);
            $table->string('receipt_no')->nullable();
            $table->string('tracking_no')->nullable();
            
            $table->string('gst_reg_no')->nullable();
            $table->string('legal_name_business')->nullable();
            $table->string('constitution_business')->nullable();

            $table->string('res_add_flat')->nullable();
            $table->string('res_add_building')->nullable();
            $table->string('res_add_street')->nullable();
            $table->string('res_add_area')->nullable();
            $table->string('res_add_pincode')->nullable();
            $table->string('res_add_city')->nullable();
            $table->string('res_add_district')->nullable();
            $table->string('res_add_state')->nullable();
            $table->string('res_add_country')->nullable();
            
            $table->string('name_of_director')->nullable();
            $table->string('address_of_director')->nullable();
            $table->string('nature_of_business')->nullable();
            $table->string('place_of_business')->nullable();

            $table->string('mobile_number')->nullable();
            $table->string('sec_mobile_number')->nullable();
            $table->string('email_id')->nullable();
            $table->string('sec_email_id')->nullable();

            $table->string('other_business')->nullable();
            $table->string('gst_site_id')->nullable();
            $table->string('gst_password')->nullable();

            $table->string('accounting_swft')->nullable();
            $table->string('details_of_transaction')->nullable();;
            $table->string('any_other_file')->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('et105s');
    }
}

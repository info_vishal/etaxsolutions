<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('archive_by_user_id')->unsigned()->nullable();
            $table->integer('assigned_user_id')->unsigned()->nullable();
            $table->string('level_type');
            $table->integer('state_id')->unsigned()->nullable();
            $table->string('name');
            $table->boolean('allocated_status')->default(0);
            $table->timestamp('archived_at')->nullable();
            $table->float('percentage_amount')->nullable();
            $table->float('bdm_percentage_amount')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('archive_by_user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('set null');
            $table->foreign('assigned_user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('set null');
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}

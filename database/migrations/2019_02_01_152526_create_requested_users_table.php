<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('referred_by')->unsigned();
            $table->integer('accepted_by')->unsigned()->nullable();
            $table->string('requested_email')->nullable();
            $table->string('referral_code')->nullable();
            $table->text('description')->nullable();
            $table->text('remark')->nullable();
            $table->string('status')->nullable();
            $table->string('level')->nullable();
            $table->string('token')->nullable();
            $table->string('tracking_no')->nullable();

            $table->foreign('accepted_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('referred_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_users');
    }
}

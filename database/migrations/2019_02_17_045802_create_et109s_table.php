<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEt109sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('et109s', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('service_id')->unsigned()->default(9);
            $table->integer('created_by')->unsigned();
            $table->integer('accepted_by')->unsigned()->nullable();
            $table->string('status')->default('pending');
            $table->float('amount_received')->default(0);
            $table->string('receipt_no')->nullable();
            $table->string('tracking_no')->nullable();
            
            $table->string('pancard_no')->nullable();
            $table->string('cli_last_name')->nullable();
            $table->string('cli_first_name')->nullable();
            $table->string('cli_middle_name')->nullable();
            $table->string('cli_gender')->nullable();
            $table->string('cli_dob')->nullable();

            $table->string('father_full_name')->nullable();
            $table->string('mother_full_name')->nullable();

            $table->string('aadhar_allotted')->nullable();
            $table->string('aadhar_not_allotted')->nullable();

            $table->string('res_add_flat')->nullable();
            $table->string('res_add_building')->nullable();
            $table->string('res_add_street')->nullable();
            $table->string('res_add_area')->nullable();
            $table->string('res_add_pincode')->nullable();
            $table->string('res_add_city')->nullable();
            $table->string('res_add_district')->nullable();
            $table->string('res_add_state')->nullable();
            $table->string('res_add_country')->nullable();

            $table->string('mobile_number')->nullable();
            $table->string('email_id')->nullable();

            $table->string('pancard_file')->nullable();
            $table->string('aadharcard_file')->nullable();
            $table->string('photo_file')->nullable();
            $table->string('any_other_file')->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('et109s');
    }
}

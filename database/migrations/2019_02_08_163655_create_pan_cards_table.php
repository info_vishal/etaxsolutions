<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePanCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_cards', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('service_id')->unsigned()->default(1);
            $table->integer('created_by')->unsigned();
            $table->integer('accepted_by')->unsigned()->nullable();
            $table->string('status')->default('pending');
            $table->float('amount_received')->default(0);
            $table->string('receipt_no')->nullable();
            $table->string('tracking_no')->nullable();
            
            $table->string('cli_last_name')->nullable();
            $table->string('cli_first_name')->nullable();
            $table->string('cli_middle_name')->nullable();
            $table->string('cli_name_on_card')->nullable();
            $table->string('cli_gender')->nullable();
            $table->string('cli_dob')->nullable();

            $table->string('mot_last_name')->nullable();
            $table->string('mot_first_name')->nullable();
            $table->string('mot_middle_name')->nullable();

            $table->string('fat_last_name')->nullable();
            $table->string('fat_first_name')->nullable();
            $table->string('fat_middle_name')->nullable();
            $table->string('parent_name_on_card')->nullable();

            $table->string('res_add_flat')->nullable();
            $table->string('res_add_building')->nullable();
            $table->string('res_add_street')->nullable();
            $table->string('res_add_area')->nullable();
            $table->string('res_add_pincode')->nullable();
            $table->string('res_add_city')->nullable();
            $table->string('res_add_state')->nullable();
            $table->string('res_add_country')->nullable();

            $table->string('ofc_name')->nullable();
            $table->string('ofc_add_flat')->nullable();
            $table->string('ofc_add_building')->nullable();
            $table->string('ofc_add_street')->nullable();
            $table->string('ofc_add_area')->nullable();
            $table->string('ofc_add_pincode')->nullable();
            $table->string('ofc_add_city')->nullable();
            $table->string('ofc_add_state')->nullable();
            $table->string('ofc_add_country')->nullable();

            $table->string('address_of_com')->nullable();
            
            $table->string('country_code')->nullable();
            $table->string('area_code')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('email_id')->nullable();

            $table->string('status_of_applicant')->nullable();

            $table->string('company_reg_number')->nullable();

            $table->string('aadhar_allotted')->nullable();
            $table->string('aadhar_not_allotted')->nullable();
            $table->string('aadhar_name')->nullable();
            
            $table->string('source_of_income')->nullable();

            $table->string('id_proof')->nullable();
            $table->string('id_proof_file')->nullable();

            $table->string('address_proof')->nullable();
            $table->string('address_proof_file')->nullable();

            $table->string('dob_file')->nullable();
            $table->string('signature_file')->nullable();
            $table->string('passport_size_file')->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_cards');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEt112sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('et112s', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('service_id')->unsigned()->default(12);
            $table->integer('created_by')->unsigned();
            $table->integer('accepted_by')->unsigned()->nullable();
            $table->string('status')->default('pending');
            $table->float('amount_received')->default(0);
            $table->string('receipt_no')->nullable();
            $table->string('tracking_no')->nullable();
            
            $table->string('name_trust')->nullable();
            $table->string('trust_narration')->nullable();
            $table->string('name_school')->nullable();

            $table->string('mobile_number')->nullable();
            $table->string('email_id')->nullable();
            $table->string('financial_year')->nullable();

            $table->string('res_add_flat')->nullable();
            $table->string('res_add_building')->nullable();
            $table->string('res_add_street')->nullable();
            $table->string('res_add_area')->nullable();
            $table->string('res_add_pincode')->nullable();
            $table->string('res_add_city')->nullable();
            $table->string('res_add_district')->nullable();
            $table->string('res_add_state')->nullable();
            $table->string('res_add_country')->nullable();

            $table->string('trust_deed_file')->nullable();
            $table->string('moa_file')->nullable();
            $table->string('aoa_file')->nullable();
            $table->string('assets_file')->nullable();
            $table->string('address_proof_file')->nullable();
            $table->string('financial_statement_file')->nullable();
            $table->string('bank_statement_file')->nullable();
            $table->string('bills_voucher_file')->nullable();
            $table->string('pancard_file')->nullable();
            $table->string('letter_file')->nullable();
            $table->string('any_other_file')->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('et112s');
    }
}

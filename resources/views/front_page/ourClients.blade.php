@extends('front_page.common.header')
@section('content')
<div class="container py-10">
            <div class="heading text-center pb-7">
                <h2><span>Our Clients</span></h2>
            </div>
            <div class="row align-items-center">
                <div class="col-sm-6 col-md-3 mb-2">
                    <a href="#" class="text-center text-secondary d-flex align-items-center justify-content-center">
                        <img src="images/cil-1.png" alt="" class="mw-100 mh-100">
                    </a>
                </div>
                <div class="col-sm-6 col-md-3 mb-2">
                    <a href="#" class="text-center text-secondary d-flex align-items-center justify-content-center">
                        <img src="images/cil-2.jpg" alt="" class="mw-100 mh-100">
                    </a>
                </div>
                <div class="col-sm-6 col-md-3 mb-2">
                    <a href="#" class="text-center text-secondary d-flex align-items-center justify-content-center">
                        <img src="images/cil-3.jpg" alt="" class="mw-100 mh-100">
                    </a>
                </div>
                <div class="col-sm-6 col-md-3 mb-2">
                    <a href="#" class="text-center text-secondary d-flex align-items-center justify-content-center">
                        <img src="images/cil-4.png" alt="" class="mw-100 mh-100">
                    </a>
                </div>
            </div>
        </div>

@endsection
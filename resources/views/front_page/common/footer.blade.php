<footer class="footer-main">
            <div class="container">
              <div class="row align-items-top text-center">
                <div class="col-12 col-md-3 col-lg-3 text-center mb-4 mb-md-0 text-sm-left">
                  <h5 class="text-white pb-2 mb-2">OurGroup</h5>
                  <p class="text-white"><iframe width="100%" height="240" src="https://www.youtube.com/embed/5Peo-ivmupE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                  
                </div>
          
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 mt-5 mt-sm-0 text-sm-left">
                  <h5 class="text-white pb-2 mb-2">Important Links</strong></h5>
                  <nav class="nav flex-column">
                    <a class="nav-link px-0 py-1 text-white active" href="{{ url('/') }}">Home</a>
                    <a class="nav-link px-0 py-1 text-white" href="{{ route('root.about') }}">About Us</a>
                    <a class="nav-link px-0 py-1 text-white" href="{{ route('root.services') }}">Services</a>
                    <a class="nav-link px-0 py-1 text-white" href="{{ route('root.ourClients') }}">Our Clients</a>
                    <a class="nav-link px-0 py-1 text-white" href="{{ route('root.contactUs') }}">Contact US</a>
                  </nav>
                </div>
          
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 text-md-left mt-5 mt-sm-0">
                    <h5 class="text-white pb-2 mb-2">Connect with Social</strong></h5>
                    <nav class="nav flex-column">
                        <a class="nav-link px-0 py-1 text-white" href="#"><span class="mdi mdi-facebook mr-2"></span>facebook</a>
                        <a class="nav-link px-0 py-1 text-white" href="#"><span class="mdi mdi-twitter mr-2"></span>Twitter</a>
                        <a class="nav-link px-0 py-1 text-white" href="#"><span class="mdi mdi-instagram mr-2"></span>Insta</a>
                        <a class="nav-link px-0 py-1 text-white" href="#"><span class="mdi mdi-youtube mr-2"></span>YouTube</a>
                    </nav>
                </div>
          
                <div class="col-12 col-sm-4 col-md-3 col-lg-3 ml-auto text-lg-left mt-lg-0">
                    <h5 class="text-white pb-2 mb-2">Languages</h5>
                    <div id="google_translate_element"></div>
                </div>
              </div>
            </div>
          </footer>
          <div class="footer-second">
            <div class="container py-2 text-center">
              <p class="text-white mb-0">© 2019 E-Tax Solutions India, All rights reserved </p>
            </div>
          </div>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </body>
</html>
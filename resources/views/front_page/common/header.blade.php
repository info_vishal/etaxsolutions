<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', "E-Tax Solutions India | Let's Tax Make Easy") }}</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
        <link href="{{ asset('scss/bootstrap.css') }}" type="text/css" rel="stylesheet">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


        <script src="{{ asset('js/respossive_tab.js') }}"></script>
        <script src="{{ asset('js/slick.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
    </head>
    <body>
        <header class="shadow bg-white main-header py-0">
            <div id="mobile-navigation" class="mobile-nav-drawer d-ex-lg-none">
                <div class="mobile-menu-sub">
                    <div class="menu-close-icon"><i class="ion ion-ios-close"></i></div>
                </div>
            </div>
            <div class="container">    
                <div class="row">
                    <div class="col-3 d-flex justify-content-start align-items-center d-ex-lg-none">
                        <a href="#" class="menu-toggle h1 mb-0"><i class="ion ion-ios-menu"></i></a>
                    </div>
                    <div class="col-6 col-ex-lg-2 text-center">
                        <a class="navbar-brand mr-0" href="{{ url('/') }}"><img src="{{ url('/') }}/images/logo.png" alt="" class="mw-100" /></a>
                    </div>
                    <nav id="main-nav" class="col-12 col-ex-lg-10">
                        <ul class="d-flex justify-content-between">
                            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="{{ route('root.about') }}">About Us</a>
                            </li>
                            <li class="nav-item {{ Request::is('downloads') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="{{ route('root.downloads') }}">Download</a>
                                    </li>
                                    <li class="nav-item {{ Request::is('services') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="{{ route('root.services') }}">Services</a>
                                    </li>
                                    <li class="nav-item {{ Request::is('ourClients') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="{{ route('root.ourClients') }}">Our clients</a>
                                    </li>
                                    <li class="nav-item {{ Request::is('contactUs') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="{{ route('root.contactUs') }}">Contact Us</a>
                                        <ul class="menu-drop">
                                            <li class="nav-item {{ Request::is('testimonial') ? 'active' : '' }}">
                                                <a href="{{ route('root.testimonial') }}" class="nav-link px-3 ls-p5">Testimonials</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item {{ Request::is('becomeApartner') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="{{ route('root.becomeApartner') }}">Become a Partner</a>
                                        <ul class="menu-drop">
                                            <li class="nav-item {{ Request::is('why') ? 'active' : '' }}">
                                                <a href="{{ route('root.why') }}" class="nav-link px-3 ls-p5">Why us</a>
                                            </li>
                                            <li class="nav-item {{ Request::is('faq') ? 'active' : '' }}">
                                                <a href="{{ route('root.faq') }}" class="nav-link px-3 ls-p5">FAQ</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item {{ Request::is('') ? 'active' : '' }}">
                                        <a class="nav-link px-3 ls-p5" href="#">Tax</a>
                                        <ul class="menu-drop">
                                            <li class="nav-item {{ Request::is('gst') ? 'active' : '' }}">
                                                <a href="{{ route('root.gst') }}" class="nav-link px-3 ls-p5">GST</a>
                                            </li>
                                            <li class="nav-item {{ Request::is('incomeTax') ? 'active' : '' }}">
                                                <a href="{{ route('root.incomeTax') }}" class="nav-link px-3 ls-p5">Income Tax</a>
                                            </li>
                                            <li class="nav-item {{ Request::is('anyother') ? 'active' : '' }}">
                                                <a href="{{ route('root.anyother') }}" class="nav-link px-3 ls-p5">Any Other</a>
                                            </li>
                                        </ul>
                                    </li>
                                     @if (Route::has('login'))
                                        <li>
                                            @auth
                                                <a href="{{ route('common.myProfile') }}">My Profile</a>
                                            @else
                                                 <a class="d-none d-sm-block text-secondary ls-p5" href="{{ route('login') }}">Login</a> 
                                            @endauth
                                        </li>
                                    @endif
                        </ul>
                    </nav>
                    <div class="col-3 d-flex justify-content-end align-items-center d-ex-lg-none">
                        <a href="#" class="menu-toggle h4 mb-0"><i class="ion ion-md-person"></i></a>
                    </div>
                </div>
            </div>
        </header>
        @yield('content')
@extends('front_page.common.footer')
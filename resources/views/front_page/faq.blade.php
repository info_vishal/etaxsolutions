@extends('front_page.common.header')
@section('content')
        <div class="container py-10">
            <div class="heading text-center pb-4 mb-4">
                <h2><span>FAQ</span> </h2>
            </div>
            <div class="accordion panel-group" id="accordionExample">
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingOne">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            What is a GST Registration? 
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="p-2">
                            <p class="mb-2">GST Registration of a business with the tax authorities implies obtaining a unique, 15-digit Goods and Service Tax Identification Number (GSTIN) from the GST authorities so that all the operations of and the data relating to the business can be collected and correlated. In any tax system this is the most fundamental requirement for identification of the business for tax purposes or for having any compliance verification program.</p>
                             
                        </div>
                    </div>
                </div>
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingtwo">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                            Why should I obtain a GST Registration?
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordionExample">
                        <div class="p-2">
                            <p class="mb-2">Registration under Goods and Services Tax (GST) regime will confer the following advantages to a business: </p>
                            <p class="mb-2"> Legally recognised as supplier of goods or services.</p>
                            <ul class="pl-3 mb-0">
                                <li>Proper accounting of taxes paid on the input goods or services which can be utilised for</li>
                                <li>payment of GST due on supply of goods and/or services by the business.  </li>
                                <li>Pass on the credit of the taxes paid on the goods and/or services supplied to purchasers or recipients.  </li>
                                <li>Authorization to a taxpayer to collect tax on behalf of the Government.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingThree">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            Where are the prerequisites for registration on the GST Portal?                                
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="p-2">
                            <ul class="pl-3 mb-0">
                                <li>PAN card/details of your business </li>
                                <li>Valid and accessible e-mail ID and Mobile Number</li>
                                <li>Documentary proof of constitution of your business</li>
                                <li>Documentary proof of promoters/partners </li>
                                <li>Documentary proof of principal place of business </li>
                                <li>Details of additional places of business, if applicable </li>
                                <li>Details of Authorised Signatories including photographs and proof of appointment </li>
                                <li>Details of Primary Authorised Signatory </li>
                                <li>Business bank account details along with bank statement or first page of bank passbook </li>
                                <li>Valid Class II or Class III DSC of authorised signatory in case of companies and LLPs; valid Class II or Class III DSC or Aadhaar (for E-Sign option) in case of other entities. </li>
                            </ul>
                            <p class="text-danger mb-1">Note: Your mobile number should be updated with the Aadhaar authorities otherwise you cannot use E-Sign option because OTP will be sent to the number in the Aadhaar database.</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
           
@endsection
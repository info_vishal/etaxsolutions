@extends('front_page.common.header')
@section('content')
        <div class="page-banner" style="background-image: url(images/slide.jpg);">
            <div class="container banner-caption text-ceter">
                <h1 class="text-white text-uppercase mb-0"></h1>
            </div>
        </div>

        <div class="container py-10">
           <div class="heading text-center pb-7">
               <h2><span>Our Services</span></h2>
           </div>
           <div class="row">
               <div class="col-md-2">
                   <div class="service-box">
                       <a href="{{ route('root.services') }}/#pancard" class="text-center text-secondary d-flex align-items-center justify-content-center">
                           <div class="p-4">
                               <img src="images/pro-1.png" alt="" class="mw-100 mb-2">
                               <h6 class="mb-0 ls-p5 font-weight-regular">Plan Card</h6>
                           </div>
                       </a>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="service-box">
                       <a href="{{ route('root.services') }}/#income-tax" class="text-center text-secondary d-flex align-items-center justify-content-center">
                           <div class="p-4">
                               <img src="images/pro-2.png" alt="" class="mw-100 mb-2">
                               <h6 class="mb-0 ls-p5 font-weight-regular">Income Tax</h6>
                           </div>
                       </a>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="service-box">
                       <a href="{{ route('root.services') }}" class="text-center text-secondary d-flex align-items-center justify-content-center">
                           <div class="p-4">
                               <img src="images/pro-3.png" alt="" class="mw-100 mb-2">
                               <h6 class="mb-0 ls-p5 font-weight-regular">Trade Mark</h6>
                           </div>
                       </a>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="service-box">
                       <a href="{{ route('root.services') }}/#gst" class="text-center text-secondary d-flex align-items-center justify-content-center">
                           <div class="p-4">
                               <img src="images/pro-4.png" alt="" class="mw-100 mb-2">
                               <h6 class="mb-0 ls-p5 font-weight-regular">GST</h6>
                           </div>
                       </a>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="service-box">
                       <a href="{{ route('root.services') }}" class="text-center text-secondary d-flex align-items-center justify-content-center">
                           <div class="p-4">
                               <img src="images/pro-5.png" alt="" class="mw-100 mb-2">
                               <h6 class="mb-0 ls-p5 font-weight-regular">Company Registration</h6>
                           </div>
                       </a>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="service-box">
                       <a href="{{ route('root.services') }}/#other" class="text-center text-secondary d-flex align-items-center justify-content-center">
                           <div class="p-4">
                               <img src="images/pro-6.png" alt="" class="mw-100 mb-2">
                               <h6 class="mb-0 ls-p5 font-weight-regular">Other</h6>
                           </div>
                       </a>
                   </div>
               </div>
           </div>
       </div>
 


        <div class="container-fluid py-6 review-section">
            <div class="overlay"></div>
            <div class="container position-relative">
                <div class="heading text-center mb-6">
                    <h2 class="text-white">Customer Reviews</h2>
                </div>
                <div class="multiple-items mb-6">
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/3.jpeg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Vishal Gohil</h6>
                                <p>I value customer service and tend to only leave reviews for those who really deserve it.I have been going to E-tax now for 7 years and his team is amazing. I am always taken care of as though I am part of his family. He takes the time to make sure I am getting back the most money possible since I always seem to exempt when I shouldn't and he also takes the time to give me advice on how to succeed financially.</p>
                            </div>
                        </div>
                    </div>
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/1.jpg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Piyush</h6>
                                <p>They are very committed in providing good customer services. Highly recommended to family and friends.</p>
                            </div>
                        </div>
                    </div>
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/2.jpg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Akash</h6>
                                <p>I highly recommend E-tax solutions. They are well worth the investment.</p>
                            </div>
                        </div>
                    </div>
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/4.jpeg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Bhargav</h6>
                                <p>He simply connects us to the right people for our business needs.  We are looking  forward to grow our business with him for years to come.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid py-10 number-section">
            <div class="container">
                <div class="heading text-center mb-6">
                    <h2><span>We are in Numbers</span></h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-sm-6 col-md-3 col-lg-3 mb-3 mb-md-0 text-center number-box">
                                <div class="test-number bg-theme-primary d-flex align-items-center justify-content-center">
                                    <h2 class="mb-0 text-white">12000+</h2>
                                </div>
                                <div class="test-number d-flex align-items-center justify-content-center">
                                    <h5 class="text-theme-primary">Pancard</h5>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 mb-3 mb-md-0 text-center number-box">
                                <div class="test-number d-flex align-items-center justify-content-center">
                                    <h5 class="text-theme-primary">Income Tax Return</h5>
                                </div>
                                <div class="test-number bg-theme-primary d-flex align-items-center justify-content-center">
                                    <h2 class="mb-0 text-white">200000+</h2>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 mb-3 mb-md-0 text0-center number-box">
                                <div class="test-number bg-theme-primary d-flex align-items-center justify-content-center">
                                    <h2 class="mb-0 text-white">25+</h2>
                                </div>
                                <div class="test-number d-flex align-items-center justify-content-center">
                                    <h5 class="text-theme-primary">States</h5>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 mb-3 mb-md-0 text-center number-box">
                                <div class="test-number d-flex align-items-center justify-content-center">
                                    <h5 class="text-theme-primary">Customers</h5>
                                </div>
                                <div class="test-number bg-theme-primary d-flex align-items-center justify-content-center">
                                    <h2 class="mb-0 text-white">300000+</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid py-10 patner-section">
            <div class="container">
                <div class="heading text-center mb-6">
                    <h2><span>Associates Partner</span></h2>
                </div>
                <div class="patner">
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-1.png" alt="" class="img-fluid" />
                    </div>
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-2.jpg" alt="" class="img-fluid" />
                    </div>
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-3.png" alt="" class="img-fluid" />
                    </div>
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-4.jpg" alt="" class="img-fluid" />
                    </div>
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-5.png" alt="" class="img-fluid" />
                    </div>
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-6.jpg" alt="" class="img-fluid" />
                    </div>
                    <div class="patner-item d-flex align-items justify-content-center">
                        <img src="images/pat-7.gif" alt="" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>

        <div class="py-10 number-section">
            <div class="container">
                <div class="heading text-center mb-6">
                    <h2><span>Customer Care</span></h2>
                </div>
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <img src="images/customer_care.png" class="img-fluid" alt="" />
                    </div>
                    <div class="col-12 col-md-6">
                        <ul class="list-unstyled mb-0">
                            <li class="border-bottom p-2 mb-2">
                                <div class="d-flex align-items-center mb-2">
                                    <p class="text-theme-primary mb-0" id=""><b>Contact No:</b> +91 81558 63603</p>
                                </div>
                            </li>
                            <li class="border-bottom p-2 mb-2">
                                <div class="d-flex align-items-center mb-2">
                                    <p class="text-theme-primary mb-0" id=""><b>Address:</b> 211-212 , Sanskruti AC market, dindoli road, opp. Parvat magob khadi bridge BRTS station, surat. Pin -395010 </p>
                                </div>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="py-10 number-section">
            <div class="container">
                <div class="heading text-center mb-6">
                    <h2><span>Latest News</span></h2>
                </div>
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <ul class="news list-unstyled mb-0">
                            @foreach($latestNews as $key => $val)
                            <li class="news-item border-bottom p-2 mb-2">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="mdi mdi-calendar h2 mb-0 mr-3"></span>
                                    <p class="text-theme-primary mb-0" id="news_date_{{ $val->id }}">{{ filterDate($val->news_date) }}</p>
                                </div>
                                <p class="mb-2" id="news_title_{{ $val->id }}">{{ $val->name }}</p>
                                <textarea class="d-none" id="news_descriptions_{{ $val->id }}">{{ $val->descriptions }}</textarea
                                    >
                                <a href="#" class="text-theme-primary view_news" id="{{ $val->id }}" data-toggle="modal" data-target="#exampleModal"><i>Read More</i></a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-md-6">
                        <img src="images/latest_news.jpg" class="img-fluid" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid bg-primary py-6">
            <div class="container">
                <div class="heading text-center mb-5">
                    <h2 class="text-white">Search Service Center</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6">
                        <div class="newslatter">
                            <form class="" action="{{ route('root.searchService') }}" method="POST" >
                                @csrf
                            <input type="text" class="form-control" name="pincode" placeholder="Enter Pincode">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid px-0">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14878.58280021189!2d72.85941096977541!3d21.2062291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2sin!4v1532366004345" width="100%" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content rounded-0">
       <div class="modal-header">
         <h6 class="modal-title" id="exampleModalLabel">Latest News</h6>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body"><font class="mdi mdi-calendar h2 mb-0 mr-3"> <font class="mb-2 display_news_date"></font></font>
          <br>
         <font class="mb-2 display_news_title"></font>
         <div class="col-md-12"> <hr/> </div>
         <p class="mb-2 display_news_descriptions"></p>
       </div>
     </div>
   </div>
 </div>
 <script type="text/javascript">
var page = {
    init: function(){
        $(document).on("click",'.view_news',function(){
            var id = this.id;
            $(".display_news_date").text($("#news_date_"+id).text());
            $(".display_news_title").text($("#news_title_"+id).text());
            $(".display_news_descriptions").html($("#news_descriptions_"+id).html());
        })
    }
};
$(document).ready(function(){
    page.init();
});
 </script>
@endsection
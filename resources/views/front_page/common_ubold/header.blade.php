<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ asset('theme/px/plugins/morris/morris.css') }}">
        <link href="{{ asset('theme/px/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
        
        <!-- custom css -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

        <!-- jQuery  -->
        <script src="{{ asset('theme/px/js/jquery.min.js') }}"></script> 

        <link href="{{ asset('theme/px/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ asset('theme/px/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ asset('theme/px/js/modernizr.min.js') }}"></script>
        
        <!-- <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->

        <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </head>


    <body class="">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="{{ url('/') }}" class="logo">
                             <img src="{{ url('/') }}/images/logo_in.png" style="width: 80%;" alt="" class="mw-100" />
                        </a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="col-md-2 col-md-offset-7 m-t-15">
                            <div id="google_translate_element"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="" style="margin-top:100px">
                    <div class="col-md-10 col-md-offset-1 " id="MAINCONTAINER">
                        @yield('content')
                    

                    @extends('front_page.common_ubold.footer')
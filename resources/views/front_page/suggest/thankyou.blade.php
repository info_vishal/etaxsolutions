@extends('front_page.common_ubold.header')

@section('content')

@if(session('status'))
        <div class="row">
        <div class="col-sm-12 text-center">
            <div class="home-wrapper">
                <h1 class="icon-main text-custom"><i class="md md-album"></i></h1>
                <h1 class="home-text"><span class="rotate" style="display: inline; opacity: 0.936631;">
                    {{ session('status') }}
                </span></h1>
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-sm-12 text-center">
            <div class="home-wrapper">
                <h1 class="icon-main text-custom"><i class="md md-album"></i></h1>
                <h1 class="home-text"><span class="rotate" style="display: inline; opacity: 0.936631;">Form Submitted...!</span></h1>
            </div>
        </div>
    </div>
@endif

@endsection

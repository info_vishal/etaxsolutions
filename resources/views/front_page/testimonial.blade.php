@extends('front_page.common.header')
@section('content')
<div class="container-fluid py-6 review-section">
            <div class="overlay"></div>
            <div class="container position-relative">
                <div class="heading text-center mb-6">
                    <h2 class="text-white">Customer Reviews</h2>
                </div>
                <div class="multiple-items mb-6">
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/3.jpeg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Vishal Gohil</h6>
                                <p>I value customer service and tend to only leave reviews for those who really deserve it.I have been going to E-tax now for 7 years and his team is amazing. I am always taken care of as though I am part of his family. He takes the time to make sure I am getting back the most money possible since I always seem to exempt when I shouldn't and he also takes the time to give me advice on how to succeed financially.</p>
                            </div>
                        </div>
                    </div>
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/1.jpg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Piyush</h6>
                                <p>They are very committed in providing good customer services. Highly recommended to family and friends.</p>
                            </div>
                        </div>
                    </div>
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/2.jpg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Akash</h6>
                                <p>I highly recommend E-tax solutions. They are well worth the investment.</p>
                            </div>
                        </div>
                    </div>
                    <div class="px-0 px-sm-4">
                        <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                            <div class="review-avtar">
                                <img src="images/4.jpeg" alt="" />
                            </div>
                            <div class="pt-2">
                                <h6>Bhargav</h6>
                                <p>He simply connects us to the right people for our business needs.  We are looking  forward to grow our business with him for years to come.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
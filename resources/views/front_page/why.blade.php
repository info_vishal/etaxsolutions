@extends('front_page.common.header')
@section('content')
        <div class="page-banner" style="background-image: url(images/slide.jpg);">
            <div class="container banner-caption text-ceter">
                <h1 class="text-white text-uppercase mb-0"></h1>
            </div>
        </div>
        <div class="container py-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Why Us?</li>
                </ol>
            </nav>
        </div>
        
        <div class="container py-3">
            <div class="row align-items-center">
                <div class="col-12">
                    <h6 class="mb-1">1. JET SERVICE</h6>
                    <p class="mb-2">-   E-TAX is giving the services as a speed of jet as compared to the others in the tax sector.</p>
                    <h6 class="mb-1">2. JACKPOT</h6>
                    <p class="mb-2">-   E-TAX providing you jackpot of services through which you can take more earnings as much as your ability to work.</p>
                    <h6 class="mb-1">3. CNC ACCURACY</h6>
                    <p class="mb-2">-   E-TAX providing you the hassle free services with CNC accuracy at your finger trips.</p>
                    <h6 class="mb-1">4. EFFORTLESS</h6>
                    <p class="mb-2">-   E-TAX gives you the effortless earning by excellent training.</p>
                    <h6 class="mb-1">5. HIGH DEMAND</h6>
                    <p class="mb-2">-   E-TAX providing the taxation services which is high demanded in the current scenario. </p>
                    <h6 class="mb-1">6. TAX EXPERT  </h6>
                    <p class="mb-2">-  E-TAX have the best  team of tax experts for providing tax services.</p>
                    <h6 class="mb-1">7. SECURITY</h6>
                    <p class="mb-2">-   E-TAX providing you high security to your legal documents and financial data.</p>
                    <h6 class="mb-1">8. ACCESS</h6>
                    <p class="mb-2">-   E-TAX providing you any time excess of your documents online with all data.</p>
                    <h6 class="mb-1">9. THOUGHTS</h6>
                    <p class="mb-2">-   The company believe in providing services benefits  to the clients in reality.</p>
                    
                </div>
            </div>
        </div>
@endsection
@extends('front_page.common.header')
@section('content')
<div class="container py-10 mb-2">
            <div class="heading text-center pb-4 mb-4">
                <h2><span>Contact with us</span> </h2>
            </div>
            <div class="row justify-content-center">
                @if(session('status'))
                    <div class="col-12 col-md-10" align="right" style="color:#0784c8">
                        <h6 class="text-primary">{{ session('status') }}</h6>
                    </div>
                @endif
                
                <div class="col-12 col-md-10">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-4 col-lg-3">
                            <div class="w-100 mb-2 contact-form">
                                <div class="w-100 h-100 widthhout-hover d-flex align-items-center justify-content-center">
                                    <div>
                                        <div class="con-icon text-center">
                                            <span class="mdi mdi-home h4"></span>
                                        </div>
                                        <h6 class="m-0 font-weight-regular">Our Address</h6>
                                    </div>
                                </div>
                                <div class="hover-box d-flex align-items-center justify-content-center">
                                        <p class="m-0 text-center">211-212 , Sanskruti AC market, dindoli road, opp. Parvat magob khadi bridge BRTS station, surat. Pin -395010</p>
                                </div>
                            </div>
                            <div class="w-100 mb-2 contact-form">
                                <div class="w-100 h-100 widthhout-hover d-flex align-items-center justify-content-center">
                                    <div>
                                        <div class="con-icon text-center">
                                            <span class="mdi mdi-phone h4"></span>
                                        </div>
                                        <h6 class="m-0 font-weight-regular">Our Phone</h6>
                                    </div>
                                </div>
                                <div class="hover-box d-flex align-items-center justify-content-center">
                                        <p class="m-0 text-center">+91 81558 63603 </p>
                                </div>
                            </div>
                            <div class="w-100 mb-2 contact-form">
                                <div class="w-100 h-100 widthhout-hover d-flex align-items-center justify-content-center">
                                    <div>
                                        <div class="con-icon text-center">
                                            <span class="mdi mdi-email h4"></span>
                                        </div>
                                        <h6 class="m-0 font-weight-regular">Our Email</h6>
                                    </div>
                                </div>
                                <div class="hover-box d-flex align-items-center justify-content-center">
                                    <p class="m-0 text-center">etaxsolutions5050@gmail.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-8 col-lg-9">
                            <form class="clearfix text-center" action="{{ route('root.contactUs.email') }}" method="POST" >
                                @csrf
                                @if($errors->has('name'))
                                    <div align="left" style="color:#f05050">{{ $errors->first('name') }}</div>
                                @endif
                                <input type="text" class="form-control mb-4" placeholder="Full Name*" name="name" />
                                @if($errors->has('email'))
                                    <div align="left" style="color:#f05050">{{ $errors->first('email') }}</div>
                                @endif
                                <input type="email" name="email" class="form-control mb-4" placeholder="Your Email*" />

                                @if($errors->has('subject'))
                                    <div align="left" style="color:#f05050">{{ $errors->first('subject') }}</div>
                                @endif
                                <input type="text" name="subject" class="form-control mb-4" placeholder="Enter Subject" />

                                @if($errors->has('type'))
                                    <div align="left" style="color:#f05050">{{ $errors->first('type') }}</div>
                                @endif
                                <select name="type" class="form-control mb-4">
                                    <option value="">-- Select --</option>
                                    <optgroup label="Levels">
                                        <option value="Level - Sector">Sector</option>
                                        <option value="Level - Metro">Metro</option>
                                        <option value="Level - Region">Region</option>
                                        <option value="Level - District">District</option>
                                        <option value="Level - Taluka">Taluka</option>
                                        <option value="Level - Counter">Counter</option>
                                    </optgroup>
                                    <optgroup label="Services">
                                        <option value="Service - PAN CARD">PAN CARD</option>
                                        <option value="Service - INCOME TAX RETURN (INDIVIDUAL & HUF )">INCOME TAX RETURN (INDIVIDUAL & HUF )</option>
                                        <option value="Service - INCOME TAX RETURN (FIRMS & COMPANY)">INCOME TAX RETURN (FIRMS & COMPANY)</option>
                                        <option value="Service - GST REGISTRATION (NEW)">GST REGISTRATION (NEW)</option>
                                        <option value="Service - GST RETURN GSTR 1 & GSTR 3B (FOR ONE TIME)">GST RETURN GSTR 1 & GSTR 3B (FOR ONE TIME)</option>
                                        <option value="Service - GST RETURN (YEARLY PACKAGE)">GST RETURN (YEARLY PACKAGE)</option>
                                        <option value="Service - INCOME TAX NOTICE & SCRUITINY">INCOME TAX NOTICE & SCRUITINY</option>
                                        <option value="Service - INCOME TAX AUDIT (ON TURNOVER BASIS) MINIMUM FEES">INCOME TAX AUDIT (ON TURNOVER BASIS) MINIMUM FEES</option>
                                        <option value="Service - DIGITAL SIGNATURE CERTIFICATE (CLASS-2)">DIGITAL SIGNATURE CERTIFICATE (CLASS-2)</option>
                                        <option value="Service - COMPANY REGISTRATION (PVT.LTD.)">COMPANY REGISTRATION (PVT.LTD.)</option>
                                        <option value="Service - TRUST REGISTRATION">TRUST REGISTRATION</option>
                                        <option value="Service - TRUST AUDIT/SCHOOL AUDIT">TRUST AUDIT/SCHOOL AUDIT</option>
                                        <option value="Service - PARTNERSHIP DEED DRAFTING">PARTNERSHIP DEED DRAFTING</option>
                                        <option value="Service - PARTNERSHIP REGISTRATION">PARTNERSHIP REGISTRATION</option>
                                    </optgroup>
                                </select>

                                @if($errors->has('phone'))
                                    <div align="left" style="color:#f05050">{{ $errors->first('phone') }}</div>
                                @endif
                                <input type="text" name="phone" class="form-control mb-4" placeholder="Your Phone" />
                                @if($errors->has('message_body'))
                                    <div align="left" style="color:#f05050">{{ $errors->first('message_body') }}</div>
                                @endif
                                <textarea rows="3" name="message_body" class="form-control mb-4" placeholder="Your Message*"></textarea>
                                <button type="submit" class="btn btn-primary">Send Message</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="container-fluid px-0">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14878.58280021189!2d72.85941096977541!3d21.2062291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2sin!4v1532366004345" width="100%" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
@endsection
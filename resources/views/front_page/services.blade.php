@extends('front_page.common.header')
@section('content')
<div class="py-10">
    <div class="container">
        <div class="heading text-center mb-4">
            <h2><span>Our Services</span></h2>
        </div>
        <div class="row">
            <div class="col-12">
                <h3>Our Products</h3>
                <ul class="list-inline">
                    <li class="list-inline-item align-middle"><a href="#pancard" class="text-secondary">Pancard</a></li>
                    <li class="list-inline-item align-middle"><a href="#income-tax" class="text-secondary">Income Tax</a></li>
                    <li class="list-inline-item align-middle"><a href="#gst" class="text-secondary">GST</a></li>
                    <li class="list-inline-item align-middle"><a href="#company-registration" class="text-secondary">Company Registration</a></li>
                    <li class="list-inline-item align-middle"><a href="#other" class="text-secondary">Other Products</a></li>
                </ul>
                <div class="w-100 mt-5" id="pancard">
                    <h3>PAN CARD</h3>
                    <h6 class="text-primary">What Is Pan Card ?</h6>
                    <p>PAN is an electronic system through which, all tax related information for a person/company is recorded against a single PAN number. This acts as the primary key for storage of information and is shared across the country. Hence no two tax paying entities can have the same PAN.</p>
                    <h6 class="text-primary">Types of PAN</h6>
                    <p class="mb-2">1.Individual</p>
                    <p class="mb-2">2.HUF-Hindu undivided family</p>
                    <p class="mb-2">3.Company</p>
                    <p class="mb-2">4.Firms/Partnerships</p>
                    <p class="mb-2">5.Trusts</p>
                    <p class="mb-2">6.Society</p>
                    <p class="mb-2">7.Foreigners</p>
                    <p class="mb-2">PAN requires two types of documents. Proof of address (POA) and Proof of Identity (POI). Any two of the following documents should meet the criteria</p>
                    <table class="table table-bordered mb-3">
                        <tr>
                            <td class="w-50"><b>Individual Applicant</b></td>
                            <td class="w-50">POI/ POA- Aadhaar, Passport, Voter ID, Driving Licence</td>
                        </tr>
                        <tr>
                            <td class="w-50"><b>Hindu Undivided Family</b></td>
                            <td class="w-50">An affidavit of the HUF issued by the head of HUF along with POI/POA details</td>
                        </tr>
                        <tr>
                            <td class="w-50"><b>Company registered in India</b></td>
                            <td class="w-50">Certificate of Registration issued by Registrar of Companies</td>
                        </tr>
                        <tr>
                            <td class="w-50"><b>Firms/ Partnership (LLP)</b></td>
                            <td class="w-50">   Certificate of Registration issued by the Registrar of Firms/ Limited Liability Partnerships and Partnership Deed.</td>
                        </tr>
                        <tr>
                            <td class="w-50"><b>Trust</b></td>
                            <td class="w-50">   Copy of Trust Deed or a copy of the Certificate of Registration Number issued by a Charity Commissioner.</td>
                        </tr>
                        <tr>
                            <td class="w-50"><b>Society</b></td>
                            <td class="w-50">   Certificate of Registration Number from Registrar of Co-operative Society or Charity Commissioner</td>
                        </tr>
                        <tr>
                            <td class="w-50"><b>Foreigners</b></td>
                            <td class="w-50">Passport<br />
                                PIO/ OCI card issued by the Indian Government
                                Bank statement of the residential country
                                Copy of NRE bank statement in India</td>
                        </tr>
                    </table>
                    <h6 class="mb-2">Why do you need PAN?</h6>
                    <p>PAN is a unique identification number that enables each tax paying entity of India with the following:</p>
                    <ul class="pl-3">
                        <li>Proof of Identity</li>
                        <li>Proof of Address</li>
                        <li>Mandatory for Filing Taxes</li>
                        <li>Registration of Business</li>
                        <li>Financial transactions</li>
                        <li>Eligibility to open and operate Bank Accounts</li>
                        <li>Phone Connection</li>
                        <li>Gas Connection</li>
                        <li><a href="#">Mutual Fund</a> – PAN is beneficial to complete e-KYC for mutual fund investments.</li>
                    </ul>
                </div>
                <div class="w-100 mt-5" id="income-tax">
                    <h3>Income tax return</h3>
                    <p>An income tax (IT) return is the tax form or forms used to file income tax with the Income Tax Department. The tax returnis usually in a predefined worksheet format where the income figures used to calculate the tax liability are written into the documents themselves.</p>
                    <p>The law states that tax returns must be filed every year for an individual or business that received income during the year, whether through regular income (wages), dividends, interest, capital gains or other sources.</p>
                    <p>Tax returns, regardless of whether it relates to an individual or a business, must be filed by a specific date.</p>
                    <p>If the return shows excess tax has been paid during a given year, the assesse is eligible for a ‘tax refund’, subject to the department’s interpretations and calculations.</p>
                    <h2>Income tax slab rate for individual</h2>
                    <h6>Income Tax Rate AY 2019-20 | FY 2018-19 – Individuals less than 60 years</h6>
                    <table class="table table-bordered mb-3">
                        <tr>
                            <th class="w-75">Taxable income</th>
                            <th class="w-25">Tax Rate</th>
                        </tr>
                        <tr>
                            <td class="w-75">Up to Rs. 2,50,000</td>
                            <td class="w-25"><i>nil</i></td>
                        </tr>
                        <tr>
                            <td class="w-75">Rs. 2,50,000 to Rs. 5,00,000</td>
                            <td class="w-25">5%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Rs. 5,00,000 to Rs. 10,00,000</td>
                            <td class="w-25">20%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Above Rs. 10,00,000</td>
                            <td class="w-25">30%</td>
                        </tr>
                    </table>
                    <h6>Income Tax Rate AY 2019-20 | FY 2018-19 – Individuals between 60 years and 80 years</h6>
                    <table class="table table-bordered mb-3">
                        <tr>
                            <th class="w-75">Taxable income</th>
                            <th class="w-25">Tax Rate</th>
                        </tr>
                        <tr>
                            <td class="w-75">Up to Rs. 3,00,000</td>
                            <td class="w-25"><i>nil</i></td>
                        </tr>
                        <tr>
                            <td class="w-75">Rs. 3,00,000 to Rs. 5,00,000</td>
                            <td class="w-25">5%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Rs. 5,00,000 to Rs. 10,00,000</td>
                            <td class="w-25">20%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Above Rs. 10,00,000</td>
                            <td class="w-25">30%</td>
                        </tr>
                    </table>
                    <h6>Income Tax Rate AY 2019-20 | FY 2018-19 – Individuals above 80 years</h6>
                    <table class="table table-bordered mb-3">
                        <tr>
                            <th class="w-75">Taxable income</th>
                            <th class="w-25">Tax Rate</th>
                        </tr>
                        <tr>
                            <td class="w-75">Up to Rs. 5,00,000</td>
                            <td class="w-25"><i>nil</i></td>
                        </tr>
                        <tr>
                            <td class="w-75">Rs. 5,00,000 to Rs. 10,00,000</td>
                            <td class="w-25">20%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Above Rs. 10,00,000</td>
                            <td class="w-25">30%</td>
                        </tr>
                    </table>
                    <p>In addition to the Income Tax amount calculated, based on the above-mentioned tax slabs, individuals are required to pay Surcharge and Cess.</p>
                    <ul class="pl-3">
                        <li><b>Surcharge:</b> 10% of income tax, where total income exceeds Rs.50 lakh up to Rs.1 crore.</li>
                        <li><b>Surcharge:</b> 15% of income tax, where the total income exceeds Rs.1 crore.</li>
                        <li><b>Health & Education Cess:</b> 4% of Income Tax. (Newly introduced through 2018 Budget)</li>
                    </ul>
                    <h2>Income tax slab rate for others</h2>
                    <h6>Income Tax Rates for AY 2019-20 / FY 2018-19 for Partnership Firm and LLP</h6>
                    <p>For the Assessment Year 2019-20, a partnership firm (including LLP) is taxable at 30%.</p>
                    <ul class="pl-3">
                        <li><b>Surcharge:</b> The amount of income-tax shall be increased by a surcharge at the rate of 12% of such tax, where total income exceeds one crore rupees. However, the surcharge shall be subject to marginal relief (where income exceeds one crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of one crore rupees by more than the amount of income that exceeds one crore rupees).</li>
                        <li><b>Health and Education Cess:</b> The amount of income-tax and the applicable surcharge, shall be further increased by health and education cess calculated at the rate of four percent of such income-tax and surcharge.</li>
                    </ul>
                    <h6>Income Tax Rates for AY 2019-20 / FY 2018-19 for Local Authority</h6>
                    <p>For the Assessment Year 2019-20, a local authority is taxable at 30%.</p>
                    <ul class="pl-3">
                        <li><b>Surcharge:</b> The amount of income-tax shall be increased by a surcharge at the rate of 12% of such tax, where total income exceeds one crore rupees. However, the surcharge shall be subject to marginal relief (where income exceeds one crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of one crore rupees by more than the amount of income that exceeds one crore rupees).</li>
                        <li><b>Health and Education Cess:</b>  The amount of income-tax and the applicable surcharge, shall be further increased by health and education cess calculated at the rate of four percent of such income-tax and surcharge.</li>
                    </ul>
                    <h6>Income Tax Rates for AY 2019-20 / FY 2018-19 for Domestic Company</h6>
                    <p>For the assessment year 2019-20, a domestic company is taxable at 30%. However, the tax rate would be 25% if turnover or gross receipt of the company does not exceed Rs. 250 crore in the previous year 2016-17.</p>
                    <ul class="pl-3">
                        <li><b>Surcharge:</b> The amount of income-tax shall be increased by a surcharge at the rate of 7% of such tax, where total income exceeds one crore rupees but not exceeding ten crore rupees and at the rate of 12% of such tax, where total income exceeds ten crore rupees. However, the surcharge shall be subject to marginal relief, which shall be as under:</li>
                        <li>Where income exceeds one crore rupees but not exceeding ten crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of one crore rupees by more than the amount of income that exceeds one crore rupees.</li>
                        <li>Where income exceeds ten crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of ten crore rupees by more than the amount of income that exceeds ten crore rupees.</li>
                        <li><b>Health and Education Cess:</b>  The amount of income-tax and the applicable surcharge, shall be further increased by health and education cess calculated at the rate of four percent of such income-tax and surcharge.</li>
                    </ul>
                    <h6>Income Tax Rates for AY 2019-20 / FY 2018-19 for Foreign Company</h6>
                    <table class="table table-bordered mb-3">
                        <tr>
                            <th class="w-75">Nature of Income</th>
                            <th class="w-25">Tax Rate</th>
                        </tr>
                        <tr>
                            <td class="w-75">Royalty received from Government or an Indian concern in pursuance of an agreement made with the Indian concern after March 31, 1961, but before April 1, 1976, or fees for rendering technical services in pursuance of an agreement made after February 29, 1964 but before April 1, 1976 and where such agreement has, in either case, been approved by the Central Government</td>
                            <td class="w-25">50%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Any other income</td>
                            <td class="w-25">40%</td>
                        </tr>
                    </table>
                    <ul class="pl-3">
                        <li><b>Surcharge:</b> The amount of income-tax shall be increased by a surcharge at the rate of 2% of such tax, where total income exceeds one crore rupees but not exceeding ten crore rupees and at the rate of 5% of such tax, where total income exceeds ten crore rupees. However, the surcharge shall be subject to marginal relief, which shall be as under:</li>
                        <li>Where income exceeds one crore rupees but not exceeding ten crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of one crore rupees by more than the amount of income that exceeds one crore rupees.</li>
                        <li>Where income exceeds ten crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of ten crore rupees by more than the amount of income that exceeds ten crore rupees.</li>
                        <li><b>Health and Education Cess:</b>  The amount of income-tax and the applicable surcharge, shall be further increased by health and education cess calculated at the rate of four percent of such income-tax and surcharge.</li>
                    </ul>
                    <h6>Income Tax Rates for AY 2019-20 / FY 2018-19 applicable to Co-operative Society</h6>
                    <table class="table table-bordered mb-3">
                        <tr>
                            <th class="w-75">Nature of Income</th>
                            <th class="w-25">Tax Rate</th>
                        </tr>
                        <tr>
                            <td class="w-75">Up to Rs. 10,000</td>
                            <td class="w-25">10%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Rs. 10,000 to Rs. 20,000</td>
                            <td class="w-25">20%</td>
                        </tr>
                        <tr>
                            <td class="w-75">Above Rs. 20,000</td>
                            <td class="w-25">30%</td>
                        </tr>
                    </table>
                    <ul class="pl-3">
                        <li><b>Surcharge:</b> The amount of income-tax shall be increased by a surcharge at the rate of 12% of such tax, where total income exceeds one crore rupees. However, the surcharge shall be subject to marginal relief (where income exceeds one crore rupees, the total amount payable as income-tax and surcharge shall not exceed total amount payable as income-tax on total income of one crore rupees by more than the amount of income that exceeds one crore rupees).</li>
                        <li><b>Health and Education Cess:</b> The amount of income-tax and the applicable surcharge, shall be further increased by health and education cess calculated at the rate of four percent of such income-tax and surcharge.</li>
                    </ul>
                    <h3>Penalty for Not Filing Income Tax Return</h3>
                    <p>Not filing your Income Tax Return would result in :</p>
                    <ul class="pl-3">
                        <li>Receiving a notice from Income Tax department</li>
                        <li>Not being able to obtain refund of excess TDS Deducted.</li>
                        <li>Penalty interest of  1% per month or part thereof will be charged until the date of payment of taxes. For returns of FY 2017-18 and onwards, penalty of Rs 5,000 will be charged for returns filed after due date but before 31st December. If returns are filed after 31st December, a penalty of Rs 10,000 shall apply. However, penalty will be Rs 1,000 for those with income upto Rs 5 Lakhs.</li>
                        <li>Not being able to set off Losses. Losses incurred (other than house property loss) will not be allowed to be carried forward to subsequent years,  to be set off against the future gains.</li>
                    </ul>
                </div>
                <div class="w-100 mt-5" id="gst">
                    <h3>GST</h3>
                    <h6 class="text-primary">GST REGISTRATION</h6>
                    <p>In the GST Regime, businesses whose turnover exceeds Rs. 40 lakhs (Rs 20 lakhs for NE and hill states) is required to register as a normal taxable person. This process of registration is called GST registration.</p>
                    <p>For certain businesses, registration under GST is mandatory. If the organization carries on business without registering under GST, it will be an offence under GST and heavy penalties will apply.</p>
                    <p>GST registration usually takes between 2-6 working days. We’ll help you to register for GST in 3 easy steps.</p>
                    <h6>Who Should Register for GST?</h6>
                    <ul class="pl-3">
                        <li>Individuals registered under the Pre-GST law (i.e., Excise, VAT, Service Tax etc.)</li>
                        <li>Businesses with turnover above the threshold limit of Rs. 20 Lakhs (Rs. 10 Lakhs for North-Eastern States, J&K, Himachal Pradesh and Uttarakhand)</li>
                        <li>Agents of a supplier & Input service distributor</li>
                        <li>Those paying tax under the reverse charge mechanism</li>
                        <li>Person who supplies via e-commerce aggregator</li>
                        <li>Every e-commerce aggregator</li>
                        <li>Person supplying online information and database access or retrieval services from a place outside India to a person in India, other than a registered taxable person</li>
                    </ul>
                    <h6>Documents Required for GST Registration</h6>
                    <ul class="pl-3">
                        <li>PAN of the Applicant</li>
                        <li>Aadhaar card</li>
                        <li>Proof of business registration or Incorporation certificate</li>
                        <li>Identity and Address proof of Promoters/Director with Photographs</li>
                        <li>Address proof of the place of business</li>
                        <li>Bank Account statement/Cancelled cheque</li>
                        <li>Digital Signature</li>
                        <li>Letter of Authorization/Board Resolution for Authorized Signatory</li>
                    </ul>
                    <h6>Penalty for not registering under GST</h6>
                    <p>An offender not paying tax or making short payments (genuine errors) has to pay a penalty of 10% of the tax amount due subject to a minimum of Rs.10,000.</p>
                    <p>The penalty will at 100% of the tax amount due when the offender has deliberately evaded paying taxes</p>
                    <h6>What are the benefits of registering under GST?</h6>
                    <ul class="pl-3">
                        <li><b>For normal registered businesses:</b>
                            <ul>
                                <li>Take input tax credit</li>
                                <li>Make interstate sales without restrictions</li>
                                <li>To know more about the benefits of GST click here.</li>
                            </ul>
                        </li>
                        <li>
                            <b>For Composition dealers:</b>
                            <ul>
                                <li>Limited compliance</li>
                                <li>Less tax liability</li>
                                <li>High working capital</li>
                                <li>To know more about composition scheme click here</li>
                            </ul>
                        </li>
                        <li>
                            <b>For businesses that voluntarily opt-in for GST registration (Below Rs. 20 lakhs)</b>
                            <ul>
                                <li>Take input tax credit</li>
                                <li>Make interstate sales without restrictions</li>
                                <li>Register on e-commerce websites</li>
                                <li>Have a competitive advantage compared to other businesses</li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="w-100 mt-5" id="other">
                    <h3 class="mb-4">Other services</h3>
                    <ul class="pl-3 mb-0" style="column-count: 3;">
                        <li>T.D.S. RETURN</li>
                        <li>TRADEMARK REGISTRATION</li>
                        <li>GST RETURN</li>
                        <li>TRUST/NGO REGISTRATION</li>
                        <li>GST COMPLIANCE</li>
                        <li>INCOME TAX SCRUITINY</li>
                        <li>COMPANY REGISTRATION</li>
                        <li>COMPANY RETURNS</li>
                        <li>ROC WORK</li>
                        <li>COMPANY LIQUIDATION WORK</li>
                        <li>LLP REGISTRATION</li>
                        <li>TAX AUDIT</li>
                        <li>PROJECT REPORT</li>
                        <li>DIGITAL SIGNATURE CERTIFICATE</li>
                        <li>PARTNERSHIP DRAFTING AND REGISTRATION</li>
                        <li>COPYRIGHTS AND PATENTS </li>
                        <li>TDS/TCS  RETURNS</li>
                        <li>TAN REGISTRATION</li>
                        <li>CA CERTIFICATION</li>
                        <li>80G REGISTRATION</li>
                        <li>WEB DEVELOPMENT    </li>
                        <li>ACCOUNTING SOFTWARE</li>
                        <li>GST SOFTWARE</li>
                        <li>INCOME TAX SOFTWARE</li>
                        <li>U.S. INCOME TAX RETURN</li>
                        <li>AUSTRALIA INCOME TAX RETURN</li>
                        <li>ISO REGISTRATION</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
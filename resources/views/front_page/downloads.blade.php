@extends('front_page.common.header')
@section('content')
<div class="container py-10">
    <div class="heading text-center pb-7">
        <h2><span>Downloads</span></h2>
    </div>
    <div class="row align-items-center">
        <table class="table table-bordered mb-3">
            @foreach(@$downloads as $key => $val)
            <tr>
                <td class="w-70"><b>{{ $val->title }}</b></td>
                <td class="w-30"><a href="{{ image_url($val->file_path) }}" download=""> Download </a></td>
            </tr>
            @endforeach
        </table>
    </div>
</div>

@endsection
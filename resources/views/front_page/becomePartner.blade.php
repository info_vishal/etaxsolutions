@extends('front_page.common.header')
@section('content')
<div class="page-banner" style="background-image: url(images/slide.jpg);">
            <div class="container banner-caption text-ceter">
                <h1 class="text-white text-uppercase mb-0"></h1>
            </div>
        </div>
        <div class="container py-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Become a Partner</li>
                </ol>
            </nav>
        </div>
        
        <div class="container py-3">
            <div class="row align-items-center">
                <div class="col-12">
                   
                    <h2>E-TAX AGENCY ACQUISITION PROCESS
                    </h2>
                    
                    <h6>1 STEP</h6>
                    <ul>
                        <li>Apply through our website <a href="#" class="text-primary">www.etaxsolutionsindia.com</a> or Contact on customer care number or Email us : <a href="#" class="text-primary">etaxsolutions5050@gmail.com</a> or Contact branch Distribution Authority. or Contact nearby E-TAX agency(here)</li>
                    </ul>
                    <h6>2 STEP</h6>
                    <ul>
                        <li>2.  The company will contact you regarding agency and our executives will support you to fill the form with necessary documents with registration fees as mentioned in the Annexure.</li>
                    </ul>
                    <h6>3 STEP</h6>
                    <ul>
                        <li>3.  After the submission of form with registration fees within 2 working days company allot you the ID & Password of your agency, through which you can start your business.
                        </li>
                    </ul>
                    <h6>4 STEP</h6>
                    <ul>
                        <li>After registration you can get the training about the web operating of services of the company as per your convenience.
                            After starting your business within 15 working days company will provide you the one time stationery materials and issue authorized certificate and ID card which will authorized as E-TAX Business partner.                            
                        </li>
                    </ul>
                    <h2>DOCUMENTS REQUIRED FOR AGENCY :</h2>
                    <ul class="pl-3">
                        <li>Passport Size Photograph</li>
                        <li>Pan card</li>
                        <li>Identity proof</li>
                        <li>Address proof</li>
                        <li>Education qualification</li>
                        <li>Cancel cheque</li>
                        <li>Bank details</li>
                        <li>Registration fees</li>
                        <li>Any other details as required…</li>
                    </ul>
                    <h2>Term & Conditions : -                            
                    </h2>
                <ul class="pl-3">
                    <li>Our company will not pay any type of Courier and Post & D.D. Charges.</li>
                    <li>If your cheque dishonored amount of 150/- to 200/- will be deducted from your wallet .
                    </li>
                    <li>T.D.S. have to be deducted from your commission as per your work base.
                    </li>
                    <li>Company will not pay any of your office rent or employees salary.
                    </li>
                    <li>Company will not pay any of your office expenses like telephone,internet,electricity etc.
                    </li>
                    <li>Branch deposit amount is neither refundable nor transferable.
                    </li>
                    <li>A branch can engage in any other business of the same nature and in the same industry
                    </li>
                    <li>Remaining amount should be paid as per the installment scheme available by the company.
                    </li>
                    <li>After completing the all information and procedures for agency allotment company will isuue the agency code.
                    </li>
                    <li>The amount taken by the company for the allotment of the branch is provided to the branch holder as wallet balance (excluding GST) and it is non refundable.ening amount is neither refundable nor transferable.
                    </li>
                    <li>Branch holder can not charge more than MRP in any services of company, if any found to break the rules, the company will take legal action on agency holder and it will be fine upto 10000 and agency will suspended also.   
                    </li>
                    <li>While training time only a person will be allowed for training from one destination and for extra person please Contact trainer for further information.
                    </li>
                    <li>After training within 24 hours your online code will be issued.
                    </li>
                </ul>
                <h2>MATERIALS AVAILABLE FOR AGENCY HOLDER (FOR ONE TIME ONLY)</h2>
                <ol class="pl-3">
                    <li>BANNER</li>
                    <li>ID CARD</li>
                    <li>VISITING CARD</li>
                    <li>AUTHORISED CERTIFICATE</li>
                    <li>STATIONERY KIT </li>
                    <li>BROCHURE & BOOKLET.</li>
                </ol>
                <h2>Agency acquisition cost & levels</h2>
                <ol class="pl-3">
                    <li><b>SECTOR LEVEL</b> >> RS. 7,50,000 /- + 18% GST ==> TOTAL = RS. 8,85,000/-</li>
                    <li><b>METRO  LEVEL</b> >>  RS. 3,80,000 /- + 18% GST ==> TOTAL = RS. 4,48,400/-</li>
                    <li><b>REGION LEVEL</b> >>  RS. 2,20,000 /- + 18% GST ==> TOTAL = RS. 2,59,600/-</li>
                    <li><b>DISTRICT LEVEL</b> >> RS. 80,000 /- + 18% GST ==> TOTAL = RS. 94,400/-</li>
                    <li><b>TALUKA LEVEL</b> >>  RS. 35,000 /- + 18% GST ==> TOTAL = RS. 41,300/-</li>
                    <li><b>E-TAX AGENCY</b>  >>  RS. 15,000 /- + 18% GST ==> TOTAL = RS. 17,700/-</li>
                </ol>
                </div>
            </div>
        </div>
@endsection
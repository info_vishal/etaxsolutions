@extends('front_page.common.header')
@section('content')
<div class="container py-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">About Us</li>
        </ol>
    </nav>
</div>
<div class="container py-3">
    <div class="row align-items-center">
        <div class="col-12 col-md-6">
            <h2 class="mb-2">About E-TAX - An Introduction</h2>
            <h5 class="mb-2">Who we are?</h5>
            <h2 class="mb-2">What is E-TAX  ?</h2>
            <h6 class="mb-2 text-primary">"E-TAX is India's fastest premium service provider Company."</h6>
            <p>E-Tax is a Online tax consultancy services provider company providing more than 51 services in india.</p>
            <p>We believe that we’re only as good as the people’s expectations.</p>
            <p>All the facts and figures that talk to our size and diversity and years of history, as notable and important as they may be, are secondary to the truest measure of E-tax: the impact we make in the world.</p>
            <p>So, when people ask, “what’s different about E-tax?” the answer resides in the many specific examples of where we have helped our people, and sections of society to achieve remarkable goals, solve complex problems or make meaningful progress.</p>
            <p>With over 20 years of hard work and commitment to making a real difference, our organization has grown in scale and diversity providing audit, tax, legal, financial advisory and consulting services.</p>
            <p>For us, good isn’t good enough. We aim to be the best at all that we do—to help clients realize their ambitions; to make a positive difference in society; and to maximize the success of our people. This drive fuels the commitment and humanity that run deep through our every action.</p>
            <p>That’s what makes us truly different at E-tax. Not how big we are, where we are, nor what services we offer. What really defines us is our drive to make an impact that matters in the india.</p>
        </div>
        <div class="col-12 col-md-6">
            <img src="images/about-page.jpg" class="img-fluid" alt="" />
        </div>
    </div>
    <div class="row py-5 align-items-center justify-content-center">
        <div class="col-12 col-md-10 col-lg-8 text-center">
            <h2 class="mb-4">OUR VISION</h2>
            <ul class="text-left">
                <li>GIVE THE FAST SERVICE TO THE PEOPLES OF INDIA.</li>
                <li>TO PLAY THE VITAL ROLE IN INDIAN TAXATION SECTOR</li>
                <li>TO BE THE PART OF EVERYONE’S TAX PLANNING AND PLANNING THEIR FUTURE…</li>
                <li>SPREAD THE AWARENESS  ABOUT THE INDIAN TAXATION SYSTEM IN PEOPLES OF INDIA.</li>
                <li>ELIMINATE THE FEAR ABOUT THE TAX STRUCTURE FROM MINDS OF PEOPLES.</li>
                <li>CREATE THE JOB OPPORTUNITY TO THE INDIAN ECONOMY.</li>
                <li>TO BRING THE  INNOVATIONS IN THE INDIAN TAX STRUCTURE.</li>
            </ul>
        </div>
    </div>
</div>
@endsection
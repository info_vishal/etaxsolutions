@extends('front_page.common.header')
@section('content')
        <div class="page-banner" style="background-image: url(images/slide.jpg);">
            <div class="container banner-caption text-ceter">
                <h1 class="text-white text-uppercase mb-0"></h1>
            </div>
        </div>

        <div class="container py-10">
            <div class="heading text-center pb-7">
                <h2><span>Service Provider</span></h2>
            </div>
            <div class="mb-6">
                    
                    @forelse($users as $user)

                        <div class="px-0 px-sm-4">
                            <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                                <div class="review-avtar">
                                    <img src="{{ image_url($user->profile_image) }}" alt="{{ $user->name }}" />
                                </div>
                                <div class="pt-2">
                                    <h6 class="text-uppercase">{{ $user->name }}</h6>
                                    <p>
                                        <div> <b> Address: </b>
                                            {{ $user->present_address }}
                                        </div>
                                        <div> <b> Pincode: </b>
                                            {{ $user->pincode }}
                                        </div>
                                        <div> <b> Contact No: </b>
                                            {{ $user->primary_mobile }}, {{ $user->alternate_mobile }}
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="px-0 px-sm-4">
                            <div class="bg-white d-sm-flex align-items-top p-4 review-box text-center text-sm-left">
                                <div class="review-avtar">
                                    
                                </div>
                                <div class="pt-2" align="center">
                                    <h6>No Search results found...!</h6>
                                </div>
                            </div>
                        </div>
                    @endforelse
                </div>
        </div>
@endsection
@extends('front_page.common.header')
@section('content')

<div class="container py-10">
            <div class="heading text-center mb-4">
                <h2><span>COMPLIANCE WITH RETURNS</span></h2>
            </div>
            <h3>Compliance With Returns</h3>
            <p>Under the current indirect tax regime, a taxpayer is obligated to file multiple returns under various statutes which could be manual/online filing. Reporting the same transaction repeatedly in varying formats to multiple authorities has only proven to be cumbersome to the tax payers.</p>
            <p>In a move towards 'Ease of Doing Business', GST regime is knocking out the multiplicity of return compliance and moving towards paperless tax compliance. Returns under GST envisage that tax payers will freeze the actual credit eligibility and impending tax liability after due co-ordination between the customers and vendors respectively.</p>
            <h3>List of returns to be filed under GST regime:</h3>
           
            <div class="accordion panel-group" id="accordionExample">
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingOne">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                GSTR-1                                
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="p-2">
                            <p class="mb-2"><strong>Applicable to:</strong> Registered taxable person</p>
                            <p class="mb-2"><strong>Description of form:</strong> Details of outward supplies of taxable goods                                 and/or services.</p>
                            <p class="mb-2"><strong>Time limit:</strong> 10th of the succeeding month</p>             
                        </div>
                    </div>
                </div>
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingtwo">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                GSTR-1A                                
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordionExample">
                        <div class="p-2">
                            <p class="mb-2"><strong>Applicable to:</strong> Registered taxable person</p>
                            <p class="mb-2"><strong>Description of form:</strong> Auto-populatedDetails of outward supplies as added, corrected or deleted by the recipient form GSTR-2 will be made available to supplier</p>
                        </div>
                    </div>
                </div>
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingThree">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                GSTR-2                                
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="p-2">
                            <p class="mb-2"><strong>Applicable to:</strong> Registered taxable person</p>
                            <p class="mb-2"><strong>Description of form:</strong> Details of inward supplies of taxable goods and/or services for claiming input tax credit.</p>
                            <p class="mb-2"><strong>Time limit:</strong> 15th of the succeeding month</p>             
                        </div>
                    </div>
                </div>
                <div class="panel-new panel-new border-0 py-1">
                    <div class="panel-heading bg-transparent border-0" id="headingFour">
                    <h6 class="mb-2">
                        <a class="d-flex align-items-center justify-content-between text-secondary" role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                GSTR-1A                                
                        <i class="more-less mdi mdi-plus"></i>
                        </a>
                    </h6>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="p-2">
                            <p class="mb-2"><strong>Applicable to:</strong> Registered taxable person</p>
                            <p class="mb-2"><strong>Description of form:</strong> Auto-populatedDetails of outward supplies as added, corrected or deleted by the recipient form GSTR-2 will be made available to supplier</p>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
 </script>
@endsection
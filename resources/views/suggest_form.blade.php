<div class="col-md-12"> 
    <h4 class="header-title"><b>{{ __('Suggesations') }}</b></h4>
    <hr/>
</div> 
@if(@$suggestData)

<div class="col-md-12"> 
  <div class="container form-group text-capitalize">
    <label for="status">Status: </label> {{ @$suggestData->status }}
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
<label for="cheque_dd">Friend Email: </label> {{ @$suggestData->requested_email }}
  </div>
</div>
                
<div class="col-md-12">                    
    <div class="container form-group">
        <label for="narration">Narration: </label> {{ @$suggestData->description }}
    </div>
</div>

  <div class="col-md-12">                    
      <div class="container form-group">
          <label for="narration">Remark: </label> {{ @$suggestData->remark }}
      </div>
  </div>
@else

<div class="col-md-12"> 
    <div class="form-group">
      <label for="inputPassword4">Friend Email*</label>
      <input type="email" id="email" parsley-trigger="change" required name="email" class="form-control" placeholder="" required value="{{ (@$suggestData)?@$suggestData->email:old('email') }}">
        @if($errors->has('email'))
            @php 
                echo showError($errors->first('email'));
            @endphp
        @endif
    </div>
</div>

<div class="col-md-12">                    
    <div class="form-group">
        <label for="narration">Narration*</label>
        <textarea name="description" rows="10" required placeholder="Descriptions about friend, Ex: Name, Mobile, etc." class="form-control">{{ (@$suggestData)?@$suggestData->description:old('description') }}</textarea>
    </div>
</div>

@endif
@php
  $role = \Auth::User()->role();
  if( ($role->slug == "admin" || $role->slug == "sub_admin") && $suggestData->status != "accepted" )
  {
    @endphp
    <div class="col-md-12"> 
        <div class="container form-group"> 
            <label for="status" class="control-label">Status *</label> 
             <select class="form-control" id="status" name="status">
                @foreach(rechargeStatus() as $key => $val)
                @php $selected = ($key == $suggestData->status)?'selected':''; @endphp
                    <option {{ $selected }} value="{{ $key }}">{{ $val }}</option>
                @endforeach
             </select>
              @if($errors->has('status'))
                  @php 
                      echo showError($errors->first('status'));
                  @endphp
              @endif
        </div> 
    </div> 

    <div class="col-md-12">                    
        <div class="container form-group">
          <label for="narration">Remark</label>
          <textarea name="remark" class="form-control">{{ (@$suggestData)?@$suggestData->remark:old('remark') }}</textarea>
          @if($errors->has('remark'))
              @php 
                  echo showError($errors->first('remark'));
              @endphp
          @endif
          <input type="hidden" name="suggest_id" value="{{ @$suggestData->id }}">
        </div>
    </div>
    @php
  } @endphp
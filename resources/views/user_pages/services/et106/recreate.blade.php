@extends('user_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Gst Return (Yearly Package)') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('user.et106.index') }}">List</a></li>
        </ol>
    </div>
</div>

@if(\Request()->id == "" && \Auth::User()->available_balance <= limiteServicePrice())
    @php echo errorAlert('Your Wallet balance is less') @endphp
@else
<form class="service_form" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
 @csrf
    <div class="row">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="col-md-12">
            
            @include('include_service.et106.clientdetails')
            @include('include_service.et106.address')
            @include('include_service.et106.contact')
            
            @include('include_service.et106.documents')
            <div class="">
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-offset-10">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</form>
@endif

<script type="text/javascript">
var page = {
    init: function(){
       this.common();
    },
    common: function(){
        $('form').parsley();
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

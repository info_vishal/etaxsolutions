@extends('user_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Redeem List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
            <li>Redeem List</li>
        </ol>
    </div>
    
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <div class="col-md-8">
            <h4 class="m-t-0 header-title"><b>{{ __('Redeem Logs') }}</b></h4>
            </div>
            <div class="col-md-4" align="right">
                @if($data > 0)
                    <font color="red">Please wait until your last redeemption updates.</font>
                @elseif( \Auth::user()->reward_balance > 0)
                <a href="{{ route('user.redeem.save') }}" class=""><i class="fa fa-plus"></i> Request: {{ \Auth::user()->reward_balance }} </a>
                @endif
            </div>
            <div class="col-md-12">
            <hr/>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_recharges">
                            <thead>
                                <tr>
                                    <th> Tracking No </th>
                                    <th> Narration </th>
                                    <th> Amount </th>
                                    <th> Status </th>
                                    <th> Requested Date </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.customDataTable(); 
    },
    formattedDate: function(d){
      d = new Date(d);
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}-${month}-${year}`;
    },
    customDataTable: function(){
        $table_datatable = $('#table_recharges').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("common.redeemJson") }}?from=user',
           columns: [
                { data: 'tracking_no' },
                { data: 'description' },
                { data: 'amount' },
                { data: 'status' },
                { data: 'created_at' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.status == "pending")
                            return  '<span class="label label-table label-inverse">'+row.status+'</span>';          
                        else if(row.status == "rejected")
                            return  '<span class="label label-table label-danger">'+row.status+'</span>';          
                        else
                            return  '<span class="label label-table label-success">'+row.status+'</span>';
                    },
                    "targets": 3,
                },
                {
                    "render": function (data, type, row) {
                        
                        return page.formattedDate(data);   
                    },
                    "targets": 4,
                },
                /*{
                    "render": function (data, type, row) {
                        //return  page.getActions(row);                
                    },
                    "targets": 4,
                },*/
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                <a href="{{ route('user.recharge.save') }}?id=`+row.recharge_id+`"
                class="label label-primary waves-effect waves-light">View</a>

                <a href="#" data-code="{{ route('user.rechargeIndex') }}?type=delete&id=`+row.recharge_id+`"
                class="label label-danger waves-effect waves-light delete">Delete</a>
                </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

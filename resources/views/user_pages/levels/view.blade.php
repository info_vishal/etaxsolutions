@extends('user_pages.common.header')

@section('content')

<div class="row">
<div class="col-sm-12">
    <h4 class="page-title">View Levels</h4>
    <ol class="breadcrumb">
        <li><a href="#">Levels</a></li>
        <li class="active">View</li>
    </ol>
</div>
</div>

<div class="row">
    <div class="col-sm-5 col-md-4 col-lg-3">
        <div class="profile-detail card-box">
            <div>

                <div class="text-left">
                    <p class="font-13"><strong>Title :</strong><span class="text-muted m-l-15 text-trans-cap">{{ $data->name }}</span></p>

                    <p class="font-13"><strong>Level :</strong><span class="text-muted m-l-15 text-trans-cap">{{ $data->level_type }}</span></p>

                    <p class="font-13"><strong>State :</strong><span class="text-muted m-l-15 text-trans-cap">{{ $state->name }}</span></p>

                    <p class="font-13"><strong>Assigned Date :</strong> <span class="text-muted m-l-15 text-trans-cap">{{ (@$data->archived_at)?ddmmyy(@$data->archived_at):'-/-/-' }}</span></p>

                    <p class="font-13"><strong>Level % :</strong> <span class="text-muted m-l-15 text-trans-cap">{{ $data->percentage_amount }}</span></p>

                </div>
            </div>

        </div>
    </div>


    <div class="col-lg-9">
        <div class="card-box view-level">
            <div class="comment">

                @if($data->level_type == 'sector' || $data->level_type == 'metro' || $data->level_type == 'region')
                @php
                    $allocated = @array_column(@$allocatedLevels['district'],'status','type_id');
                @endphp
                <h4 class="text-dark header-title m-t-0">Districts</h4>
                <div class="comment">
                    <div class="comment-text">
                        <div class="comment-header">
                            <a href="#" title="">Allocated</a>
                        </div>
                        @foreach($sub_levels as $key => $val)
                            @php
                                if(!@array_key_exists($val->district_id,$allocated) || @$allocated[$val->district_id] == 0){
                                    continue;
                                }
                                unset($sub_levels[$key]);
                                $name = \Auth::user()->getdistricts($val->district_id,1)->name; 
                            @endphp
                            <font class="label label-table label-inverse"> {{ $name }}</font>
                        @endforeach
                    </div>
                </div>

                <div class="comment">
                    <div class="comment-text">
                        <div class="comment-header">
                            <a href="#" title="">Available</a>
                        </div>
                        @foreach($sub_levels as $key => $val)
                            @php
                                $name = \Auth::user()->getdistricts($val->district_id,1)->name; 
                            @endphp
                            <font class="label label-table label-inverse"> {{ $name }}</font>
                        @endforeach
                    </div>
                </div>
                @endif

                @if($data->level_type == 'district')
                @php
                    $allocated = @array_column(@$allocatedLevels['taluka'],'status','type_id');
                @endphp
                <h4 class="text-dark header-title m-t-0">Taluka</h4>
                <div class="comment">
                    <div class="comment-text">
                        <div class="comment-header">
                            <a href="#" title="">Allocated</a>
                        </div>
                        @foreach($sub_levels as $key => $val)
                            @php 
                                if(!@array_key_exists($val->taluka_id,$allocated) || @$allocated[$val->taluka_id] == 0){
                                    continue;
                                }
                                unset($sub_levels[$key]);
                                $name = \Auth::user()->getTaluka($val->taluka_id,1)->name; 
                            @endphp
                            <font class="label label-table label-inverse"> {{ $name }}</font>
                        @endforeach
                    </div>
                </div>

                <div class="comment">
                    <div class="comment-text">
                        <div class="comment-header">
                            <a href="#" title="">Available</a>
                        </div>
                        @foreach($sub_levels as $key => $val)
                            @php
                                $name = \Auth::user()->getTaluka($val->taluka_id,1)->name;
                            @endphp
                            <font class="label label-table label-inverse"> {{ $name }}</font>
                        @endforeach
                    </div>
                </div>

                @endif

                @if($data->level_type == 'taluka')
                @php
                    $allocated = @array_column(@$allocatedLevels['counter'],'status','level_id');
                @endphp
                <h4 class="text-dark header-title m-t-0">Counter</h4>
                <div class="comment">
                    <div class="comment-text">
                        <div class="comment-header">
                            <a href="#" title="">Allocated</a>
                        </div>
                        @foreach($sub_levels as $key => $val)
                            @php
                                if(!@array_key_exists($val->id,$allocated) || @$allocated[$val->id] == 0){
                                    continue;
                                }
                                unset($sub_levels[$key]);
                            @endphp
                            <font class="label label-table label-inverse"> {{ $val->name }}</font>
                        @endforeach
                    </div>
                </div>

                <div class="comment">
                    <div class="comment-text">
                        <div class="comment-header">
                            <a href="#" title="">Available</a>
                        </div>
                        @foreach($sub_levels as $key => $val)
                            @php
                                unset($sub_levels[$key]);
                            @endphp
                            <font class="label label-table label-inverse"> {{ $val->name }}</font>
                        @endforeach
                    </div>
                </div>
                @endif
                @if($data->level_type == 'counter')
                    <div class="alert alert-danger">
                        <strong>Oops!</strong> You don't have any levels
                    </div>
                @endif
            </div>
        </div>
    </div>

</div>
@endsection

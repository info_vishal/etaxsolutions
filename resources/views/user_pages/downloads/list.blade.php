@extends('user_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/footable/css/footable.core.css') }}" rel="stylesheet">

<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Downloads Center') }}</h4>
        <ol class="breadcrumb">
            
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
                      
            <table id="demo-foo-filtering" class="table table-striped toggle-circle m-b-0" data-page-size="15">
                <thead>
                    <tr>
                        <th data-toggle="true">Download Center</th>
                        <th data-hide="phone, tablet"></th>
                    </tr>
                </thead>
                <!-- <div class="form-inline m-b-20">
                    <div class="row">
                        <div class="col-sm-6 text-xs-center">
                        </div>
                        <div class="col-sm-6 text-xs-center text-right">
                            <div class="form-group">
                                <input id="demo-foo-search" type="text" placeholder="Search" class="form-control input-sm" autocomplete="on">
                            </div>
                        </div>
                    </div>
                </div> -->
                <tbody>
                    @foreach( $downloads as $key => $val)
                    <tr>
                        <td>{{ $val->title }}</td>
                            <td>
                                @foreach( $val->hasManyFiles()->get() as $key1 => $val1)
                                <div class="form-inline m-b-20">
                                        <div class="row">
                                            <div class="col-sm-10 text-xs-center">
                                                {{ $val1->descriptions }}
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="{{ image_url($val1->file_path) }}" target="_blank" download>Download</a>
                                            </div>
                                        </div>
                                </div>
                                @endforeach
                            </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <div class="text-right">
                                <ul class="pagination pagination-split m-t-30 m-b-0"></ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<!--FooTable-->
<script src="{{ asset('theme/px/plugins/footable/js/footable.all.min.js') }}"></script>

<!--FooTable Example-->
<script src="{{ asset('theme/px/pages/jquery.footable.js') }}"></script>

@endsection

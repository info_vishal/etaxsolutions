@extends('user_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Save Levels') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
            <li>Change Password</li>
        </ol>
    </div>
</div>

    <div class="row">
        <div class="col-md-8 col-md-push-2">
            @if(session('status'))
                @php echo successAlert(session('status')) @endphp
            @endif
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="m-t-0 header-title"><b>{{ __('Change Password') }}</b></h4>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate=""> 
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="current_password">Old Password *</label>
                                <div class="col-md-8">
                                    <input type="password" id="current_password" parsley-trigger="change" name="current_password" class="form-control" placeholder="Enter Current Password" required value="{{ old('current_password') }}">

                                    @if($errors->has('current_password'))
                                        @php 
                                            echo showError($errors->first('current_password'));
                                        @endphp
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="password">New Password *</label>
                                <div class="col-md-8">
                                    <input type="password" id="password" parsley-trigger="change" name="password" class="form-control" placeholder="Password" required value="{{ old('password') }}">

                                    @if($errors->has('password'))
                                        @php 
                                            echo showError($errors->first('password'));
                                        @endphp
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="password_confirmation">Confirm Password *</label>
                                <div class="col-md-8">
                                    <input type="password" id="password_confirmation" parsley-trigger="change" name="password_confirmation" class="form-control" placeholder="Password confirmation" required value="{{ old('password_confirmation') }}">

                                    @if($errors->has('password_confirmation'))
                                        @php 
                                            echo showError($errors->first('password_confirmation'));
                                        @endphp
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
    },
    common: function(){
        $('form').parsley();

        $(document).on("change",".select_levels",function(){ 
            var value = $(this).find(':selected').data('level')
            var level_arr = ['sector','metro','region'];
            $(".div_district, .div_taluka").addClass('hide');
            $("#district, #taluka").removeClass();
            $("#district, #taluka").removeAttr('multiple required');
            
            if($.inArray(value,level_arr) !== -1){
                $(".div_district").removeClass('hide');
                $("#district").addClass("select2 select2-multiple");
                $("#district").attr({
                            'multiple':'multiple',
                            'required':'required'
                            });
                $("#district").select2();
                return true;    
            }

            if(value == 'district'){
                $(".div_taluka").removeClass('hide');
                $("#taluka").addClass("select2 select2-multiple");
                $("#taluka").attr({
                            'multiple':'multiple',
                            'required':'required'
                            });
                $("#taluka").select2();
                
            }else if(value == 'taluka' || value == 'counter'){
                $(".div_taluka").removeClass('hide');
                $("#taluka").addClass("form-control");
                $("#taluka").addClass("select2");
                $("#taluka").select2();
                $("#taluka").attr({
                            'required':'required'
                            });
            }

            $(".div_district").removeClass('hide');
                $("#district").addClass("form-control");
                $("#district").addClass("select2");
                $("#district").select2();
                $("#district").attr({
                            'required':'required'
                            });

        });

        $(document).on("change",".select_state",function(){
            var id = this.dataset.child;
            var state_id = this.value;
            var level = $(".select_levels").find(':selected').data('level');
            $.ajax({
                url: '{{ route("common.districtAjax") }}',
                data: { 'state_id': state_id,'level':level,'_token': '{{ csrf_token() }}' },
                dataType: 'JSON',
                type: 'POST',
                success: function(response){
                    $('#'+id).html();
                    $('#'+id).html(response.data);
                }
            });
            $('#'+id).val(null).trigger('change');
            $('#'+id).select2();
        });

        $(document).on("change","#district",function(){
            var id = this.dataset.child;
            var district_id = this.value;
            var level = $(".select_levels").find(':selected').data('level');
            var state_id = $(".select_state").find(':selected').data('state');
            $.ajax({
                url: '{{ route("common.talukaAjax") }}',
                data: { 'district_id': district_id,'state_id': state_id,'level':level, '_token': '{{ csrf_token() }}' },
                dataType: 'JSON',
                type: 'POST',
                success: function(response){
                    $('#'+id).html();
                    $('#'+id).html(response.data);
                }
            });
            $('#'+id).val(null).trigger('change');
            $('#'+id).select2();
        });
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

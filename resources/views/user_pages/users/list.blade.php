@extends('bdm_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Users List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('bdm.dashboard') }}">Dashboard</a></li>
            <li>Users List</li>
        </ol>
    </div>
    <div class="col-md-2">
        <a href="{{ route('bdm.users.save') }}"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Create Users</button></a>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Users  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_users">
                            <thead>
                                <tr>
                                    <th> Code </th>
                                    <th> Image </th>
                                    <th> Name </th>
                                    <th> Mobile </th>
                                    <!-- <th> Available Balance </th> -->
                                    <th> Status </th>
                                    <th> Actions </th>
                                </tr>
                            </thead> 

                            <tfoot>
                                <tr>
                                    <th> Code </th>
                                    <th> Image </th>
                                    <th> Name </th>
                                    <th> Mobile </th>
                                    <!-- <th> Available Balance </th> -->
                                    <th> Status </th>
                                    <th> Actions </th>
                                </tr>
                            </tfoot>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
    },
    customDataTable: function(){
        $table_datatable = $('#table_users').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("bdm.users.json") }}',
           columns: [
                { data: 'employee_code' },
                { data: 'profile_image' },
                { data: 'user_name' },
                { data: 'primary_mobile' },
                //{ data: 'available_balance' },
                { data: 'status' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.profile_image){
                            var img = "@php echo image_url('NULL'); @endphp";
                            img += "/"+row.profile_image;
                            return '<img src="'+ img +'" width="50" class="img-rounded" />';
                        }
                        else
                            return '<img src="{{ noImg() }}" width="50" class="img-rounded" />';         
                    },
                    "targets": 1,
                },
                {
                    "render": function (data, type, row) {
                        if(row.status == "review")
                            return  '<span class="label label-table label-inverse">'+row.status+'</span>';          
                        else if(row.status == "reject" || row.status == "suspend")
                            return  '<span class="label label-table label-danger">'+row.status+'</span>';          
                        else
                            return  '<span class="label label-table label-success">'+row.status+'</span>';
                    },
                    "targets": 4,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 5,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                  <a title="Edit" class="label label-primary" href="{{ route('bdm.users.save') }}/`+row.user_id+`">Edit</a>
                </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

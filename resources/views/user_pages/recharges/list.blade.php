@extends('user_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Recharges List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Recharges List</li>
        </ol>
    </div>
    <div class="col-md-2"><a href="{{ route('user.recharge.save') }}" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Request </a>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Recharges  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_recharges">
                            <thead>
                                <tr>
                                    <th> Narration </th>
                                    <th> Requested Price </th>
                                    <th> Status </th>
                                    <th> Requested Date </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    formattedDate: function(d){
      d = new Date(d);
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}-${month}-${year}`;
    },
    common: function(){
        $('form').parsley();
        $(document).on("click",".open_model",function(){
            console.log(this.dataset);
            $("#user_name").text(this.dataset.username);
            $("#user_code").text(this.dataset.code);
            $("#recharge_amount").val(this.dataset.recharge_amount);
            $("#recharge_id").val(this.dataset.recharge);
            $(".recharge_select").val(this.dataset.status);
        });
    },
    customDataTable: function(){
        $table_datatable = $('#table_recharges').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("common.rechargeJson") }}?from=user',
           columns: [
                { data: 'descriptions' },
                { data: 'recharge_price' },
                { data: 'recharge_status' },
                { data: 'created_at' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.recharge_status == "requested")
                            return  '<span class="label label-table label-inverse">'+row.recharge_status+'</span>';          
                        else if(row.recharge_status == "rejected")
                            return  '<span class="label label-table label-danger">'+row.recharge_status+'</span>';          
                        else
                            return  '<span class="label label-table label-success">'+row.recharge_status+'</span>';
                    },
                    "targets": 2,
                },
                {
                    "render": function (data, type, row) {
                        
                        return page.formattedDate(data);   
                    },
                    "targets": 3,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 4,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                <a href="{{ route('user.recharge.save') }}?id=`+row.recharge_id+`"
                class="label label-primary waves-effect waves-light">View</a>

                <a href="#" data-code="{{ route('user.rechargeIndex') }}?type=delete&id=`+row.recharge_id+`"
                class="label label-danger waves-effect waves-light delete">Delete</a>
                </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

@extends('user_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Recharge') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('user.rechargeIndex') }}">Recharge</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <form class="" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
                        @csrf
                        @include('recharge')
                        @if(\Request()->id == "")
                        <div class="form-group">
                            <div class="col-md-offset-9">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-danger" type="reset">Clear</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common();
    },
    common: function(){
        $('form').parsley();
       
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

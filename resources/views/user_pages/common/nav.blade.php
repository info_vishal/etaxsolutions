<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="">
                    <a href="{{ route('user.dashboard') }}" class="waves-effect {{ Request::is('user') ? 'active' : '' }}"><i class="ti-home"></i> <span> Dashboard </span> </a>
                </li>

                @if(\Auth::user()->status == "active")
                <li class="">
                    <a href="{{ route('user.levels') }}" class="waves-effect {{ Request::is('user/levels*') ? 'active' : '' }}"><i class="ti-home"></i> <span> My Level </span> </a>
                </li>
                <li class="">
                    <a href="{{ route('user.rechargeIndex') }}" class="waves-effect {{ Request::is('user/recharge*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Recharges </span> </a>
                </li>

                <li class="">
                    <a href="{{ route('user.listDownload') }}" class="waves-effect {{ Request::is('user/downloads*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Downloads </span> </a>
                </li>

                <li class="">
                    <a href="{{ route('user.statements') }}" class="waves-effect {{ Request::is('user/statements*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Statements </span> </a>
                </li>

                <li class="">
                    <a href="{{ route('user.suggest.userIndex') }}" class="waves-effect {{ Request::is('user/suggest*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Suggest Friend </span> </a>
                </li>
                @endif
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


</div> <!-- container -->
</div> <!-- content -->
<footer class="footer text-right">
	<div class="col-md-10">
    	{{ config('app.footer_name', 'E-Tax Solutions India') }}
	</div>
	<div class="col-md-2">
		<div id="google_translate_element"></div>
	</div>
</footer>
</div>
<script>
	var resizefunc = [];
</script>

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<script src="{{ asset('theme/px/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/px/js/detect.js') }}"></script>
<script src="{{ asset('theme/px/js/fastclick.js') }}"></script>

<script src="{{ asset('theme/px/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('theme/px/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('theme/px/js/waves.js') }}"></script>
<script src="{{ asset('theme/px/js/wow.min.js') }}"></script>
<script src="{{ asset('theme/px/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('theme/px/js/jquery.scrollTo.min.js') }}"></script>



<script src="{{ asset('theme/px/plugins/peity/jquery.peity.min.js') }}"></script>

<!-- jQuery  -->
<script src="{{ asset('theme/px/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('theme/px/plugins/counterup/jquery.counterup.min.js') }}"></script>
    
<script src="{{ asset('theme/px/js/jquery.core.js') }}"></script>
<script src="{{ asset('theme/px/js/jquery.app.js') }}"></script>        


<script src="{{ asset('theme/px/pages/jquery.sweet-alert.init.js') }}"></script>
<script src="{{ asset('theme/px/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('theme/px/plugins/parsleyjs/dist/parsley.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('theme/px/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>


<script src="{{ asset('js/adminjs.js') }}"></script>

</body>
</html>
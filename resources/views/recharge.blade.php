<div class="col-md-12"> 
    <h4 class="header-title"><b>{{ __('Recharge Info') }}</b></h4>
    <hr/>
</div> 
@if(@$rechargeData)

<div class="col-md-12"> 
  <div class="container form-group text-capitalize">
    <label for="status">Status: </label> <span class="label label-table label-inverse">{{ @$rechargeData->status }}</span>
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
<label for="cheque_dd">Date: </label> {{ ddmmyy(@$rechargeData->e_date) }}
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
    <label for="cheque_dd">Amount: </label> {{ @$rechargeData->price }}
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
    <label for="cheque_dd">Payment Method: </label>
    @foreach(paymentMethod() as $key => $val)
      @if($rechargeData->payment_method == $key)
          {{ $val }}
      @endif
    @endforeach
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
    <label for="cheque_dd">D.D/Cheque No: </label> {{ @$rechargeData->cheque_dd }}
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
    <label for="bank_acc">Bank Account: </label>
        @foreach($bank as $key => $val)
        @if($rechargeData->bank_acc == $key)
            {{ $val->bank_name }}
        @endif
        @endforeach
  </div>
</div>

<div class="col-md-12"> 
  <div class="container form-group">
  <label for="receipt" class="control-label">Download Receipt: <a target="_blank" download href="{{ image_url($rechargeData->receipt) }}" title="Download"><i class="fa fa-download"></i></a>   </label>
</div>
</div>                   
<div class="col-md-12">                    
    <div class="container form-group">
        <label for="narration">Narration: </label> {{ (@$rechargeData)?@$rechargeData->descriptions:old('descriptions') }}
    </div>
</div>

    <div class="col-md-12">                    
      <div class="container form-group">
          <label for="narration">Remark: </label> {{ (@$rechargeData)?@$rechargeData->remark:old('remark') }}
      </div>
  </div>
@else
<div class="form-row">
    <div class="form-group col-md-4">
        <div class="col-sm-12">
        <label for="inputPassword4201">Enter Date*</label>
            <div class="input-group">
                <input type="text" class="form-control" parsley-trigger="change" required placeholder="dd/mm/yyyy" name="e_date" id="datepicker-autoclose" value="{{ (@$rechargeData)?ddmmyy(@$rechargeData->e_date):old('e_date') }}">
                
            </div><!-- input-group -->
            @if($errors->has('e_date'))
                @php 
                    echo showError($errors->first('e_date'));
                @endphp
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
      <label for="inputPassword4202">Enter Amount*</label>
      <input type="text" id="price" parsley-trigger="change" min="0" required name="price" class="form-control" placeholder="" required value="{{ (@$rechargeData)?@$rechargeData->price:old('price') }}">
        @if($errors->has('price'))
            @php 
                echo showError($errors->first('price'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-4">
      <label for="inputPassword4203">Payment Method</label>
        <select class="form-control" name="payment_method" parsley-trigger="change">
            @foreach(paymentMethod() as $key => $val)
            @php 
                $selected = (old('payment_method') == $key)?'selected':'';
                if($selected == ""){
                    $selected = (@$rechargeData->payment_method  == $key)?'selected':'';
                }
            @endphp
                <option {{ $selected }} value='{{ $key }}'>{{ $val }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class=" col-md-12">
<div class="form-row">
    <div class="form-group col-md-4">
      <label for="cheque_dd">D.D/Cheque No. *</label>
      <input type="text" id="cheque_dd" parsley-trigger="change" required name="cheque_dd" class="form-control" placeholder="" required value="{{ (@$rechargeData)?@$rechargeData->cheque_dd:old('cheque_dd') }}">
        @if($errors->has('cheque_dd'))
            @php 
                echo showError($errors->first('cheque_dd'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-4">
      <label for="bank_acc">Bank Account</label>
        <select class="form-control" name="bank_acc" id="bank_acc" parsley-trigger="change">
            @foreach($bank as $key => $val)
            @php 
                $selected = (old('bank_acc') == $val->id)?'selected':'';
                if($selected == ""){
                    $selected = (@$rechargeData->bank_acc == $val->id)?'selected':'';
                }
            @endphp
                <option {{ $selected }} value='{{ $val->id }}'>{{ $val->bank_name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-4">
        @if(@$rechargeData)
          <label for="receipt" class="control-label"> <a target="_blank" download href="{{ image_url($rechargeData->receipt) }}" title="Download"><i class="fa fa-download"></i> Receipt </a> </label>
        @else
          <label for="receipt" class="control-label"> Receipt </label>
        @endif
        <input type="file" class="form-control" name="receipt" required="" parsley-trigger="change" />
        @if($errors->has('receipt'))
            @php 
                echo showError($errors->first('receipt'));
            @endphp
        @endif
    </div>
</div>
</div>
<div class="col-md-12">                    
    <div class="container form-group">
        <label for="narration">Narration</label>
        <textarea name="descriptions" class="form-control">{{ (@$rechargeData)?@$rechargeData->descriptions:old('descriptions') }}</textarea>
    </div>
</div>
@endif

@php
if((\Request::route()->getName() != "suggest.emailInvitation"))
{
  $role = \Auth::User()->role();
  if( ($role->slug == "admin" || $role->slug == "sub_admin") && @$rechargeData->status != 'accepted' )
  {
    @endphp
    @if(\Request::route()->getName() == "admin.users.save" && @$rechargeData->status)
    <div class="col-md-12"> 
      <div class="form-group">
          <div class="col-md-12">
              <a class="btn btn-link waves-effect" href="{{ route('admin.rechargeIndex') }}?type=view&id={{ @$rechargeData->id }}">Take Action.</a>
          </div>
      </div>
    </div>
    @else
    <div class="col-md-12"> 
        <div class="container form-group"> 
            <label for="recharge_status" class="control-label">Status *</label> 
             <select class="form-control" id="recharge_status" name="status">
                @foreach(rechargeStatus() as $key => $val)
                @php $selected = ($key == @$rechargeData->status)?'selected':''; @endphp
                    <option {{ $selected }} value="{{ $key }}">{{ $val }}</option>
                @endforeach
             </select>
        </div> 
    </div> 

    <div class="col-md-12">                    
        <div class="container form-group">
          <label for="narration">Remark</label>
          <textarea name="remark" class="form-control">{{ (@$rechargeData)?@$rechargeData->remark:old('remark') }}</textarea>
        </div>
    </div>
    @endif
    @php
  }  @endphp
  <input type="hidden" name="recharge_id" value="{{ @$rechargeData->id }}">

  @php } @endphp

<html>
    <head>
        <title>{{ config('app.name') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    </head>
    <body style="margin: 0;padding: 0;background-color: #eaeaea;">
        <div style="font-family: 'Roboto', sans-serif;width:600px;margin:0 auto;background-color: #fff;">
            <div style="padding:20px;text-align: center;border-bottom: 1px solid #dcd8d8;">
                <a hreg="#"> <img src="{{ url('/') }}/images/logo.png" alt="" class="mw-100" /></a>
            </div>
            <div style="padding:25px 20px;">
                <p style="margin:0;margin-bottom:15px;font-size: 15px;">
                    <b style="margin-right:5px;">Message from Admin:</b>
                {{ $remark }}</p>

                <p style="margin:0;margin-bottom:15px;font-size: 15px;"><b style="margin-right:5px;">Requested:</b>
                {{ $description }}</p>

                <p style="margin:0;margin-bottom:0px;font-size: 15px;">
                    <b style="margin-right:5px;"><a href="{{ route('suggest.emailInvitation',$token) }}"> Open Link </a></b>
                </p>
            </div>
            <div style="padding:20px 20px;text-align: center;border-top: 1px solid #dcd8d8;">
                <p style="margin:0;font-size: 15px;color: #8e8b8b;font-weight: 400;">
                    {{ config('app.emailTextFooter') }}
                </p>
            </div>
        </div>
    </body>
</html>
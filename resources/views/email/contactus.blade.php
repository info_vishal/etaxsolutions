<html>
    <head>
         <title>{{ config('app.name') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    </head>
    <body style="margin: 0;padding: 0;background-color: #eaeaea;">
        <div style="font-family: 'Roboto', sans-serif;width:600px;margin:0 auto;background-color: #fff;">
            <div style="padding:20px;text-align: center;border-bottom: 1px solid #dcd8d8;">
                <a hreg="#"> <img src="{{ url('/') }}/images/logo.png" alt="" class="mw-100" /></a>
            </div>
            <div style="padding:25px 20px;">
                <p style="margin:0;margin-bottom:15px;font-size: 15px;"><b style="margin-right:5px;">Full Name :</b>{{ $name }}</p>
                <p style="margin:0;margin-bottom:15px;font-size: 15px;"><b style="margin-right:5px;">Email :</b>{{ $email }}</p>
                <p style="margin:0;margin-bottom:15px;font-size: 15px;"><b style="margin-right:5px;">Subject :</b>{{ $subject }}</p>
                <p style="margin:0;margin-bottom:15px;font-size: 15px;"><b style="margin-right:5px;">Type :</b>{{ $type }}</p>
                <p style="margin:0;margin-bottom:15px;font-size: 15px;"><b style="margin-right:5px;">Phone :</b>{{ $phone }}</p>
                <p style="margin:0;margin-bottom:0px;font-size: 15px;"><b style="margin-right:5px;">Message :</b>{{ $message_body }}</p>
            </div>
            <div style="padding:20px 20px;text-align: center;border-top: 1px solid #dcd8d8;">
                <p style="margin:0;font-size: 15px;color: #8e8b8b;font-weight: 400;">{{ config('app.emailTextFooter') }} </p>
            </div>
        </div>
    </body>
</html>
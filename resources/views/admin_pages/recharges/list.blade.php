@extends('admin_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Recharges List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Recharges List</li>
        </ol>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Recharges  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_recharges">
                            <thead>
                                <tr>
                                    <th> Code </th>
                                    <th> Name </th>
                                    <th> Requested Price </th>
                                    <th> Status </th>
                                    <th> Date </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog"> 
    <div class="modal-content"> 
    <form method="POST" action="{{ route('admin.recharge.save') }}" role="form" data-parsley-validate="">
        @csrf
        @method('PUT')
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">Update Recharge</h4> 
        </div> 
        <div class="modal-body">
        
            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="form-group"> 
                        <label for="field-2" class="control-label">Code: </label> 
                        <label id="user_code" class="control-label"></label> 
                    </div> 
                </div> 
                <div class="col-md-6"> 
                    <div class="form-group"> 
                        <label for="field-1" class="control-label">Name: </label> 
                        <label id="user_name" class="control-label"></label> 
                    </div> 
                </div>
            </div> 
            <div class="row"> 
                <div class="col-md-3"> 
                    <div class="form-group"> 
                        <label for="e_date" class="control-label">Date: </label> 
                        <font id="e_date"></font>
                    </div> 
                </div>
                <div class="col-md-3"> 
                    <div class="form-group"> 
                        <label for="recharge_amount" class="control-label">Price: </label> 
                        <font id="recharge_amount"></font>
                    </div> 
                </div>  

                <div class="col-md-3"> 
                    <div class="form-group"> 
                        <label for="bank_acc" class="control-label">Bank Account: </label> 
                        <font id="bank_acc"></font>
                    </div> 
                </div>
            </div> 
            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="form-group no-margin"> 
                        <label for="description" class="control-label">Narration</label> 
                        <textarea class="form-control autogrow" id="description" disabled readonly="" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                    </div> 
                </div> 

                <div class="col-md-12"> 
                    <div class="form-group"> 
                        <label for="field-4" class="control-label">Status *</label> 
                         <select class="form-control recharge_select" name="status">
                            @foreach(rechargeStatus() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                         </select>
                    </div> 
                </div> 
                <div class="col-md-12"> 
                    <div class="form-group no-margin"> 
                        <label for="recharge_remark" class="control-label">Remark</label> 
                        <textarea class="form-control autogrow" id="recharge_remark" name="remark" placeholder="Enter remark (optional)" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                    </div> 
                </div>
            </div> 
        </div> 
        <div class="modal-footer"> 
            <input type="hidden" id="recharge_id" name="recharge_id" required> 
            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button> 
            <button type="submit" id="open_model" class="btn btn-default waves-effect waves-light">Save changes</button> 
        </div>
        </form>
    </div> 
</div>
</div><!-- /.modal -->


<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    formattedDate: function(d){
      d = new Date(d);
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}-${month}-${year}`;
    },
    common: function(){
        $('form').parsley();
    },
    customDataTable: function(){
        $table_datatable = $('#table_recharges').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("common.rechargeJson") }}',
           columns: [
                { data: 'user.employee_code' },
                { data: 'user.name' },
                { data: 'price' },
                { data: 'status' },
                { data: 'created_at' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.status == "requested")
                            return  '<span class="label label-table label-inverse">'+row.status+'</span>';          
                        else if(row.status == "rejected")
                            return  '<span class="label label-table label-danger">'+row.status+'</span>';          
                        else
                            return  '<span class="label label-table label-success">'+row.status+'</span>';
                    },
                    "targets": 3,
                },
                {
                    "render": function (data, type, row) {
                        
                        return page.formattedDate(data);   
                    },
                    "targets": 4,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 5,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        
    },
    getActions: function(row){
        var html = '';
        html = `
                <div class="btn-group" role="group">
                <a href="{{ route('admin.rechargeIndex') }}?type=view&id=`+row.id+`" 
                class="label label-primary waves-effect waves-light"  data-recharge="`+row.id+`">View / Edit</a>
                  <a href="#" data-code="{{ route('admin.rechargeIndex') }}?type=delete&id=`+row.id+`"
                class="label label-danger waves-effect waves-light delete">Delete</a>
                </div>`;
        return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

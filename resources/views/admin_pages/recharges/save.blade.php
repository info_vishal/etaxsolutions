@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Recharge') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.rechargeIndex') }}">Recharge</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <form class="" method="POST" action="{{ route('admin.recharge.save') }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
                        @csrf
                        @method('PUT')
                        @include('recharge')
                        @if(@$rechargeData->status != "accepted")
                        <div class="form-group">
                            <div class="col-md-offset-9">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-danger" type="reset">Clear</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common();
    },
    common: function(){
        $('form').parsley();
       
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

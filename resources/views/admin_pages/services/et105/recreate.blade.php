@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Gst Return (GSTR 3B and GSTR 1)') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.et105.index') }}">List</a></li>
        </ol>
    </div>
</div>

<form class="service_form" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
 @csrf
    <div class="row">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="col-md-12">
            
            @include('include_service.et105.clientdetails')
            @include('include_service.et105.address')
            @include('include_service.et105.contact')
            
            @include('include_service.et105.documents')
            <div class="">
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-offset-10">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
var page = {
    init: function(){
       this.common();
    },
    common: function(){
        $('form').parsley();
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('INCOME TAX RETURN (FIRMS & COMPANY)') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.et103.index') }}">List</a></li>
        </ol>
    </div>
</div>
            
<div class="row service_form">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-md-12">
        @include('include_service.et103.clientdetails')
        @include('include_service.et103.address')
        @include('include_service.et103.contact')
        @include('include_service.bank_all')
        @include('include_service.et103.viewDocuments')
    </div>
</div>


<form class="status_form" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
 @csrf
@method('PUT')

    <div class="card-box">
        <h4 class="m-t-0 header-title"><b>{{ __('Service Status') }}</b></h4>
        <hr/>
        <div class="row">
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="status">Service Status </label>
                        <select class="form-control serviceAdminStatus" name="status">
                            @foreach(serviceStatus() as $key => $val)
                                @php
                                    $select_stated = (old('status') == $key)?'selected':'';
                                    if($select_stated == ""){
                                        $select_stated = (@$data->status == $key)?'selected':'';
                                    }
                                @endphp
                                <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="service_id" value="{{ \Request()->id  }}">
                </div>
            </div>
        </div>
    </div>
    @if(@$data->status != 'completed')
    <div class="">
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-offset-10">
                            <button class="btn btn-primary" type="submit">Save</button>
                            <button class="btn btn-danger" type="reset">Clear</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    @endif

</form>


@if(\Request()->id != "")
@include('include_service.logs.form')
@include('include_service.messages.form')
@endif
<script type="text/javascript">
var page = {
    init: function(){
        @if(\Request()->id != "")
            $(".service_form input, .service_form select, .service_form textarea").attr({
                "disabled":"disabled",
                "readonly":"readonly"   
            });
        @endif

        @if(@$data->status == 'completed')
            $('.serviceAdminStatus').attr({
                "disabled":"disabled",
                "readonly":"readonly"   
            });
        @endif
       this.common();
    },
    common: function(){
        $(".message_tbl").DataTable({
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        $('form').parsley();
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

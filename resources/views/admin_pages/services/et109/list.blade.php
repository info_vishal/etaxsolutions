@extends('admin_pages.common.header')
@section('content')

<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('DIGITAL SIGNATURE CERTIFICATE CLASS-2') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>List</li>
        </ol>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_suggest">
                            <thead>
                                <tr>
                                    <th> Track No. </th>
                                    <th> Employee Code </th>
                                    <th> Wallet Bal </th>
                                    <th> Pan Card </th>
                                    <th> Email-Id </th>
                                    <th> Mobile </th>
                                    <th> Status </th>
                                    <th> Date </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
    },
    formattedDate: function(d){
      d = new Date(d);
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}-${month}-${year}`;
    },
    customDataTable: function(){
        $table_datatable = $('#table_suggest').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.et109.json") }}?type=admin',
           columns: [
                { data: 'tracking_no' },
                { data: 'user.employee_code' },
                { data: 'user.available_balance' },
                { data: 'pancard_no' },
                { data: 'email_id' },
                { data: 'mobile_number' },
                { data: 'status' },
                { data: 'created_at' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.user.available_balance < 1000)
                            return  '<span class="label label-table label-danger">'+row.user.available_balance+'</span>';          
                        else if(row.user.available_balance < 3000)
                            return  '<span class="label label-table label-warning">'+row.user.available_balance+'</span>';    
                        else
                            return  '<span class="label label-table label-success">'+row.user.available_balance+'</span>';
                    },
                    "targets": 2,
                },
                {
                    "render": function (data, type, row) {
                        if(row.status == "pending")
                            return  '<span class="label label-table label-inverse">'+row.status+'</span>';          
                        else if(row.status == "rejected")
                            return  '<span class="label label-table label-danger">'+row.status+'</span>';          
                        else if(row.status == "process")
                            return  '<span class="label label-table label-warning">'+row.status+'</span>';
                        else
                            return  '<span class="label label-table label-success">'+row.status+'</span>';
                    },
                    "targets": 6,
                },
                {
                    "render": function (data, type, row) {
                        return page.formattedDate(data);   
                    },
                    "targets": 7,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 8,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                <a href="{{ route('admin.et109.save') }}?id=`+row.id+`"
                class="label label-primary waves-effect waves-light">View</a>

                <a href="{{ route('admin.et109.print') }}?id=`+row.id+`"
                 class="label label-primary waves-effect waves-light" title="Print" target="_blank">Print Receipt</a>
                 </div>

                 <a href="{{ route('admin.et109.printout') }}?id=`+row.id+`"
                 class="fa fa-print" target="_blank"></a>
                 </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>

<iframe id="print" name="print" style="display:none;"></iframe>
@endsection

@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Update Redeemion') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.redeem') }}">Redeem Logs</a></li>
            <li>Save</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-push-2">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Update Redeemion') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1"> 
                        @csrf
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="">Status *</label>
                            <div class="col-md-10">
                                <select class="form-control" name="redeem_status" required="">
                                    <option value="">Select States</option>
                                    @foreach(redmeeStatus() as $key => $val)
                                        @php
                                            $selected = (@$data->status == $key)?'selected':'';
                                        @endphp
                                        <option {{ $selected }} value='{{ $key }}'>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="name">Narration</label>
                            <div class="col-md-10">
                                <textarea name="description" class="form-control">{{ $data->description }}</textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-danger" type="reset">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

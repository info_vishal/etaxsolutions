@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Edit User') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.listUsers') }}">Users</a></li>
            <li>Edit</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-push-1">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Edit User') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="container">
                    <form class="" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
                        @csrf
                        @method('PUT')
                        <div class="form-row">                            
                            <div class="form-group">
                                <label class="control-label">Profile Image</label>
                                <input type="file" accept="image/*" class="filestyle" name="profile_image" data-input="false" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);"><div class="bootstrap-filestyle input-group"><span class="group-span-filestyle " tabindex="0"><label for="filestyle-1" class="btn btn-default "><span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> <span class="buttonText">Choose file</span></label></span></div>
                            </div>
                            <div class="row"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                              <label for="inputEmail4">First Name *</label>
                              <input type="text" id="name" parsley-trigger="change" name="first_name" class="form-control" placeholder="Enter First Name" required value="{{ ($data)?$data->first_name:old('first_name') }}">
                                @if($errors->has('first_name'))
                                    @php 
                                        echo showError($errors->first('first_name'));
                                    @endphp
                                @endif
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputPassword4">Middle Name *</label>
                              <input type="text" id="middle_name" parsley-trigger="change" name="middle_name" class="form-control" placeholder="Enter Middle Name" required value="{{ ($data)?$data->middle_name:old('middle_name') }}">
                                @if($errors->has('middle_name'))
                                    @php 
                                        echo showError($errors->first('middle_name'));
                                    @endphp
                                @endif
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputPassword4">Last Name *</label>
                              <input type="text" id="last_name" parsley-trigger="change" name="last_name" class="form-control" placeholder="Enter Last Name" required value="{{ ($data)?$data->last_name:old('last_name') }}">
                                @if($errors->has('last_name'))
                                    @php 
                                        echo showError($errors->first('last_name'));
                                    @endphp
                                @endif
                            </div>
                        </div>
                        
                        <div class="container form-group">
                            <label for="father_husband_name">Father / Husband Name</label>
                            <input type="text" name="father_husband_name" id="father_husband_name" placeholder="Father / Husband Name" parsley-trigger="change" value="{{ ($data)?$data->father_husband_name:old('father_husband_name') }}" class="form-control">
                        </div>

                        <div class="container form-group">
                            <label for="email">Email *</label>
                            <input type="email" class="form-control" readonly="" disabled="" value="{{ ($data)?$data->email:old('email') }}">
                        </div>

                        <div class="container form-group">
                            <label for="address">Present Address *</label>
                            <textarea name="present_address" id="address" required placeholder="Present Address" parsley-trigger="change" class="form-control">{{ ($data)?$data->present_address:old('present_address') }}</textarea>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">City</label>
                              <input type="text" name="city" class="form-control" id="inputCity" value="{{ ($data)?$data->city:old('city') }}">
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputState">State *</label>
                              <select class="form-control select2 select_state" name="state_id" parsley-trigger="change" required="">
                                    <option value="">Select States</option>
                                    @foreach($states as $key => $val)
                                        @php
                                            $select_stated = (@$data->state_id == $val->id)?'selected':'';
                                        @endphp
                                        <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('states'))
                                    @php 
                                        echo showError($errors->first('states'));
                                    @endphp
                                @endif
                            </div>

                            <div class="form-group col-md-2">
                              <label for="inputZip">Pincode *</label>
                              <input type="text" name="pincode" class="form-control" parsley-trigger="change" required id="inputZip" value="{{ ($data)?$data->pincode:old('pincode') }}">
                              @if($errors->has('pincode'))
                                    @php 
                                        echo showError($errors->first('pincode'));
                                    @endphp
                                @endif
                            </div>
                          </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputPassword4">Alternate Mobile</label>
                              <input type="text" id="alternate_mobile" data-mask="(999) 999-9999" name="alternate_mobile" class="form-control" value="{{ ($data)?$data->alternate_mobile:old('alternate_mobile') }}">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Primary Mobile *</label>
                              <input type="text" class="form-control" readonly="" disabled="" value="{{ ($data)?$data->primary_mobile:old('primary_mobile') }}">
                                @if($errors->has('primary_mobile'))
                                    @php 
                                        echo showError($errors->first('primary_mobile'));
                                    @endphp
                                @endif
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                              <label for="inputState">Role</label>
                                <select class="form-control text-trans-cap" readonly="" disabled>
                                    @foreach($roles as $key => $val)
                                        @php
                                            $select_stated = (@$data->roles_id == $val->id)?'selected':'';
                                        @endphp
                                        <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @php //dd($data->dob); @endphp
                            <div class="form-group col-md-4">
                                <div class="col-sm-12">
                                <label for="inputPassword4">Date of Birth</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-etax" placeholder="dd/mm/yyyy" name="dob" id="datepicker-autoclose" value="{{ ($data)? ddmmyy($data->dob):old('dob') }}">
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputState">Sex</label>
                                <select class="form-control" name="sex">
                                    @php
                                        $selected_male = ($data->sex == 'male')?'selected':'';
                                        $selected_female = ($data->sex == 'female')?'selected':'';
                                        $selected_other = ($data->sex == 'other')?'selected':'';
                                    @endphp
                                    <option {{ $selected_male}} value='male'>Male</option>
                                    <option {{ $selected_female }} value='female'>Female</option>
                                    <option {{ $selected_other}} value='other'>Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                              <label for="inputPassword4">Account Status</label>
                                <select class="form-control" readonly="" disabled>
                                    @foreach(userStatus() as $key => $val)
                                        @php
                                            $select_stated = (@$data->status == $key)?'selected':'';
                                        @endphp
                                        <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-8">
                              <label for="inputPassword4">Marital Status</label>
                              <select class="form-control" name="marital_status">
                                    @php
                                        $selected_married = ($data->marital_status == 'married')?'selected':'';
                                        $selected_unmarried = ($data->marital_status == 'unmarried')?'selected':'';
                                        
                                    @endphp
                                    <option {{ $selected_married }} value='married'>Married</option>
                                    <option {{ $selected_unmarried }} value='unmarried'>Unmarried</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="col-md-offset-10">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
    },
    common: function(){
        $('form').parsley();
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

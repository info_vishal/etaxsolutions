@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Create User') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.listUsers') }}">Users</a></li>
            <li>Create</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-push-1">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Create User') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="container">
                    <form class="" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-4">
                              <label for="inputEmail4">First Name *</label>
                              <input type="text" id="name" parsley-trigger="change" name="first_name" class="form-control" placeholder="Enter First Name" required value="{{ ($data)?$data->first_name:old('first_name') }}">
                                @if($errors->has('first_name'))
                                    @php 
                                        echo showError($errors->first('first_name'));
                                    @endphp
                                @endif
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputPassword45">Middle Name *</label>
                              <input type="text" id="middle_name" parsley-trigger="change" name="middle_name" class="form-control" placeholder="Enter Middle Name" required value="{{ ($data)?$data->middle_name:old('middle_name') }}">
                                @if($errors->has('middle_name'))
                                    @php 
                                        echo showError($errors->first('middle_name'));
                                    @endphp
                                @endif
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputPassword46">Last Name *</label>
                              <input type="text" id="last_name" parsley-trigger="change" name="last_name" class="form-control" placeholder="Enter Last Name" required value="{{ ($data)?$data->last_name:old('last_name') }}">
                                @if($errors->has('last_name'))
                                    @php 
                                        echo showError($errors->first('last_name'));
                                    @endphp
                                @endif
                            </div>
                        </div>
                        
                        <div class="container form-group">
                            <label for="father_husband_name">Father / Husband Name</label>
                            <input type="text" name="father_husband_name" id="father_husband_name" placeholder="Father / Husband Name" parsley-trigger="change" value="{{ ($data)?$data->father_husband_name:old('father_husband_name') }}" class="form-control">
                        </div>

                        <div class="container form-group">
                            <label for="email">Email *</label>
                            <input type="email" name="email" id="email" placeholder="Email" parsley-trigger="change" class="form-control" required value="{{ ($data)?$data->email:old('email') }}">
                            @if($errors->has('email'))
                                @php 
                                    echo showError($errors->first('email'));
                                @endphp
                            @endif
                        </div>

                        <div class="container form-group">
                            <label for="address">Present Address *</label>
                            <textarea name="present_address" id="address" required placeholder="Present Address" parsley-trigger="change" class="form-control">{{ ($data)?$data->present_address:old('present_address') }}</textarea>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">City</label>
                              <input type="text" name="city" class="form-control" id="inputCity" value="{{ ($data)?$data->city:old('city') }}">
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputState7111">State *</label>
                              <select class="form-control select2 select_state" name="state_id" parsley-trigger="change" required="">
                                    <option value="">Select States</option>
                                    @foreach($states as $key => $val)
                                        @php
                                            $select_stated = (old('state_id') == $val->id)?'selected':'';
                                            if($select_stated == ""){
                                                $select_stated = (@$data->state_id == $val->id)?'selected':'';
                                            }
                                        @endphp
                                        <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('states'))
                                    @php 
                                        echo showError($errors->first('states'));
                                    @endphp
                                @endif
                            </div>

                            <div class="form-group col-md-2">
                              <label for="inputZip">Pincode *</label>
                              <input type="text" name="pincode" class="form-control" parsley-trigger="change" required id="inputZip" value="{{ ($data)?$data->pincode:old('pincode') }}">
                              @if($errors->has('pincode'))
                                    @php 
                                        echo showError($errors->first('pincode'));
                                    @endphp
                                @endif
                            </div>
                          </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="inputPassword41">Alternate Mobile</label>
                              <input type="text" id="alternate_mobile" data-mask="(999) 999-9999" name="alternate_mobile" class="form-control" value="{{ ($data)?$data->alternate_mobile:old('alternate_mobile') }}">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">Primary Mobile *</label>
                              <input type="text" id="name" data-mask="(999) 999-9999" parsley-trigger="change" name="primary_mobile" class="form-control" required value="{{ ($data)?$data->primary_mobile:old('primary_mobile') }}">
                                @if($errors->has('primary_mobile'))
                                    @php 
                                        echo showError($errors->first('primary_mobile'));
                                    @endphp
                                @endif
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                              <label for="inputPassword42">Marital Status</label>
                              <select class="form-control" name="marital_status">
                                    <option value='married'>Married</option>
                                    <option value='unmarried'>Unmarried</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <div class="col-sm-12">
                                <label for="inputPassword43">Date of Birth</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="dob" id="dob">
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputState2">Sex</label>
                                <select class="form-control" name="sex">
                                    <option value='male'>Male</option>
                                    <option value='female'>Female</option>
                                    <option value='other'>Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                              <label for="inputPassword44">Account Status</label>
                                <select class="form-control" name="user_status">
                                    @foreach(userStatus() as $key => $val)
                                        @php
                                            $select_stated = (@$data->status == $key)?'selected':'';
                                        @endphp
                                        <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if(\Request::route()->getName() == "admin.users.save")
                            <div class="form-group col-md-4">
                              <label for="inputState3">Role</label>
                                <select class="form-control text-trans-cap" name="roles_id">
                                    @foreach($roles->where('display_in_levels','=',1)->get() as $key => $val)
                                        @php
                                            $select_stated = (old('roles_id') == $val->id)?'selected':'';
                                            if($select_stated == ""){
                                                $select_stated = (@$data->roles_id == $val->id)?'selected':'';
                                            }
                                        @endphp

                                        <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @else
                            <div class="form-group col-md-4">
                              <label for="inputState4">Role </label>
                                <select class="form-control text-trans-cap" name="roles_id">
                                    @foreach($roles->where('display_in_levels','=',0)->get() as $key => $val)
                                        @php
                                            $select_stated = (old('roles_id') == $val->id)?'selected':'';
                                            if($select_stated == ""){
                                                $select_stated = (@$data->roles_id == $val->id)?'selected':'';
                                            }
                                        @endphp
                                        <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                              <label for="inputZip">Wallet Balance </label>
                              <input type="number" name="available_balance" min="0" class="form-control" parsley-trigger="change" value="{{ ($data)?$data->available_balance:old('available_balance') }}">
                              @if($errors->has('available_balance'))
                                    @php 
                                        echo showError($errors->first('available_balance'));
                                    @endphp
                                @endif
                            </div>
                            @endif

                            
                        </div>
                        @include('bank_detail')
                        @if(\Request::route()->getName() == "admin.users.save")
                            @include('recharge')
                        @endif
                        <div class="form-group col-md-12">
                            <div class="col-md-offset-10">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-danger" type="reset">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
    },
    common: function(){
        $('form').parsley();
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

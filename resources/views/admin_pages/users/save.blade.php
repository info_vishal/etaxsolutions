@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Save Levels') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.listLevels') }}">Levels</a></li>
            <li>Save</li>
        </ol>
    </div>
</div>

@php 
    if(@$data){

    @endphp
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            @if(session('status'))
                @php echo successAlert(session('status')) @endphp
            @endif
            <div class="card-box table-responsive">
                <h4 class="m-t-0 header-title"><b>{{ __('Save Levels') }}</b></h4>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate=""> 
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name">Name *</label>
                                <div class="col-md-10">
                                    <input type="text" id="name" parsley-trigger="change" name="name" class="form-control" placeholder="Enter Name" required value="{{ ($data)?$data->name:old('name') }}">

                                    @if($errors->has('name'))
                                        @php 
                                            echo showError($errors->first('name'));
                                        @endphp
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" >Levels </label>
                                <div class="col-md-10">
                                    <input type="text" readonly disabled class="form-control" value="{{ $data->level_type }}">
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="">States </label>
                                <div class="col-md-10">
                                        @foreach($states as $key => $val)
                                            @php
                                               if(@$data->state_id == $val->id){
                                                    $selected = $val->name;
                                                }
                                            @endphp
                                        @endforeach
                                    <input type="text" readonly disabled class="form-control" value="{{ $selected }}">
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="">Districts </label>
                                <div class="col-md-10">
                                    <select class="form-control" disabled multiple size="3">
                                        @php
                                        if($data->level_type == 'district'){
                                         $district = \Auth::user()->getdistricts($sub_level[0]['district_id'],1);
                                            @endphp
                                                <option>{{ $district->name }}</option>
                                            @php
                                        }else{
                                            foreach($sub_level as $key => $val){
                                                $district = \Auth::user()->getdistricts($val['district_id'],1);
                                                @endphp
                                                <option>{{ $district->name }}</option>
                                                @php
                                            }
                                        }
                                        @endphp
                                    </select>
                                </div> 
                            </div>
                            
                            @if($data->level_type == 'district' || $data->level_type == 'taluka' || $data->level_type == 'counter')
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="">Taluka</label>
                                    <div class="col-md-10">
                                        <select class="form-control" disabled multiple size="3">
                                            @php
                                            if($data->level_type == 'taluka'){
                                                 $taluka = \Auth::user()->getTaluka($sub_level[0]['taluka_id'],1);
                                                    @endphp
                                                        <option>{{ $taluka->name }}</option>
                                                    @php
                                                }else{
                                                foreach($sub_level as $key => $val){
                                                    $taluka = \Auth::user()->getTaluka($val['taluka_id'],1);
                                                    @endphp
                                                    <option>{{ $taluka->name }}</option>
                                                    @php
                                                }
                                            }
                                        @endphp
                                        </select>
                                    </div> 
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="percentage_amount">Level % *</label>
                                <div class="col-md-10">
                                    <input type="text" id="percentage_amount" parsley-trigger="change" name="percentage_amount" class="form-control" placeholder="Enter Percentage Rate" min="0" required value="{{ ($data)?$data->percentage_amount:old('percentage_amount') }}">

                                    @if($errors->has('percentage_amount'))
                                        @php 
                                            echo showError($errors->first('percentage_amount'));
                                        @endphp
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="bdm_percentage_amount">Bdm % *</label>
                                <div class="col-md-10">
                                    <input type="text" id="bdm_percentage_amount" parsley-trigger="change" name="bdm_percentage_amount" class="form-control" min="0" placeholder="Enter Bdm Percentage Rate" required value="{{ ($data)?$data->bdm_percentage_amount:old('bdm_percentage_amount') }}">

                                    @if($errors->has('bdm_percentage_amount'))
                                        @php 
                                            echo showError($errors->first('bdm_percentage_amount'));
                                        @endphp
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@php }else{ @endphp
<div class="row">
    <div class="col-md-8 col-md-push-2">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><b>{{ __('Save Levels') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate=""> 
                        @csrf

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="name">Name *</label>
                            <div class="col-md-10">
                                <input type="text" id="name" parsley-trigger="change" name="name" class="form-control" placeholder="Enter Name" required value="{{ ($data)?$data->name:old('name') }}">

                                @if($errors->has('name'))
                                    @php 
                                        echo showError($errors->first('name'));
                                    @endphp
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" >Levels *</label>
                            <div class="col-md-10">
                                <select parsley-trigger="change" class="form-control select_levels text-trans-cap" name="levels" required>
                                    <option value="">Select Levels</option>
                                    @foreach( config("app.levels") as $key => $val)
                                        @php
                                            $selected = (@$data->level_type == $val)?'selected':'';
                                        @endphp
                                        <option {{ $selected }} value='{{ $val }}'>{{ $val }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('levels'))
                                    @php 
                                        echo showError($errors->first('levels'));
                                    @endphp
                                @endif
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="">States *</label>
                            <div class="col-md-10">
                                <select class="form-control select2 select_state" name="states" data-child='district' parsley-trigger="change" required="">
                                    <option value="">Select States</option>
                                    @foreach($states as $key => $val)
                                        @php
                                            $selected = (@$data->state_id == $val->id)?'selected':'';
                                        @endphp
                                        <option {{ $selected }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('states'))
                                    @php 
                                        echo showError($errors->first('states'));
                                    @endphp
                                @endif
                            </div> 
                        </div>

                        <div class="form-group hide div_district">
                            <label class="col-md-2 control-label" for="">District *</label>
                            <div class="col-md-10">
                                <select class="" name='district[]' id="district" data-child="taluka">
                                    @if(@$data->district_id)
                                        <option selected value="{{ @$data->district_id }}">{{ @$data->district_name }}</option>
                                    @endif                                    
                                </select>
                                @if($errors->has('district'))
                                    @php 
                                        echo showError($errors->first('district'));
                                    @endphp
                                @endif
                            </div> 
                        </div>
                        
                        <div class="form-group hide div_taluka">
                            <label class="col-md-2 control-label" for="">Taluka *</label>
                            <div class="col-md-10">
                                <select class="" name='taluka[]' id="taluka">
                                    @if(@$data->district_id)
                                        <option selected value="{{ @$data->district_id }}">{{ @$data->district_name }}</option>
                                    @endif                                    
                                </select>
                                @if($errors->has('taluka'))
                                    @php 
                                        echo showError($errors->first('taluka'));
                                    @endphp
                                @endif
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="percentage_amount">Level % *</label>
                            <div class="col-md-10">
                                <input type="text" id="percentage_amount" parsley-trigger="change" name="percentage_amount" class="form-control" placeholder="Enter Percentage Rate" min="0" required value="{{ ($data)?$data->percentage_amount:old('percentage_amount') }}">

                                @if($errors->has('percentage_amount'))
                                    @php 
                                        echo showError($errors->first('percentage_amount'));
                                    @endphp
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="bdm_percentage_amount">Bdm % *</label>
                            <div class="col-md-10">
                                <input type="text" id="bdm_percentage_amount" parsley-trigger="change" name="bdm_percentage_amount" class="form-control" min="0" placeholder="Enter Bdm Percentage Rate" required value="{{ ($data)?$data->bdm_percentage_amount:old('bdm_percentage_amount') }}">

                                @if($errors->has('bdm_percentage_amount'))
                                    @php 
                                        echo showError($errors->first('bdm_percentage_amount'));
                                    @endphp
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-danger" type="reset">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@php } @endphp
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
    },
    common: function(){
        $('form').parsley();

        $(document).on("change",".select_levels",function(){ 
            var value = this.value;
            var level_arr = ['sector','metro','region'];
            $(".div_district, .div_taluka").addClass('hide');
            $("#district, #taluka").removeClass();
            $("#district, #taluka").removeAttr('multiple required');
            
            if($.inArray(value,level_arr) !== -1){
                $(".div_district").removeClass('hide');
                $("#district").addClass("select2 select2-multiple");
                $("#district").attr({
                            'multiple':'multiple',
                            'required':'required'
                            });
                $("#district").select2();
                return true;    
            }

            if(value == 'district'){
                $(".div_taluka").removeClass('hide');
                $("#taluka").addClass("select2 select2-multiple");
                $("#taluka").attr({
                            'multiple':'multiple',
                            'required':'required'
                            });
                $("#taluka").select2();
                
            }else if(value == 'taluka' || value == 'counter'){
                $(".div_taluka").removeClass('hide');
                $("#taluka").addClass("form-control");
                $("#taluka").addClass("select2");
                $("#taluka").select2();
                $("#taluka").attr({
                            'required':'required'
                            });
            }

            $(".div_district").removeClass('hide');
                $("#district").addClass("form-control");
                $("#district").addClass("select2");
                $("#district").select2();
                $("#district").attr({
                            'required':'required'
                            });

        });

        $(document).on("change",".select_state",function(){
            var id = this.dataset.child;
            var state_id = this.value;
            
            $.ajax({
                url: '{{ route("common.districtAjax") }}',
                data: { 'state_id': state_id,'_token': '{{ csrf_token() }}' },
                dataType: 'JSON',
                type: 'POST',
                success: function(response){
                    $('#'+id).html();
                    $('#'+id).html(response.data);
                }
            });
            $('#'+id).val(null).trigger('change');
            $('#'+id).select2();
        });

        $(document).on("change","#district",function(){
            var id = this.dataset.child;
            var district_id = this.value;
            $.ajax({
                url: '{{ route("common.talukaAjax") }}',
                data: { 'district_id': district_id,'_token': '{{ csrf_token() }}' },
                dataType: 'JSON',
                type: 'POST',
                success: function(response){
                    $('#'+id).html();
                    $('#'+id).html(response.data);
                }
            });
            $('#'+id).val(null).trigger('change');
            $('#'+id).select2();
        });
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

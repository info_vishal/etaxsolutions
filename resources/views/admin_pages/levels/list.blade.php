@extends('admin_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-8">
        <h4 class="page-title">{{ __('Levels List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Levels List</li>
        </ol>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-primary waves-effect waves-light open_model" data-toggle="modal" data-target="#full-width-modal"><i class="fa fa-plus"></i> Update Levels %</button>
    </div>
    <div class="col-md-2">
        <a href="{{ route('admin.levels.save') }}"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Create levels</button></a>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Levels  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_levels">
                            <thead>
                                <tr>
                                <th> Name </th>
                                <th data-head="State"> State </th>
                                <th> Levels </th>
                                <th> Status </th>
                                <th> BDM </th>
                                <th> Actions </th>
                            </tr>
                            </thead> 

                            <tfoot>
                                <tr>
                                <th> Name </th>
                                <th> State </th>
                                <th> Levels </th>
                                <th> Status </th>
                                <th> BDM </th>
                                <th> Actions </th>
                                </tr>
                            </tfoot>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="full-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full"> 
    <div class="modal-content"> 
    <form method="POST" id="center" action="{{ route('admin.listLevels') }}" role="form" data-parsley-validate="">
        @csrf
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">Update Level %</h4> 
        </div> 
        <div class="modal-body">
            <div class="row"> 
                @foreach($role as $key => $val)
                @php
                    if(! array_key_exists($val->slug, levels() ) && $val->slug != 'bdm')
                    continue;
                @endphp
                <div class="col-lg-4">
                                <div class="portlet">
                                    <div class="portlet-heading bg-custom">
                                        <h3 class="portlet-title">
                                            {{ $val->name }}
                                        </h3>
                                       
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="bg-primary" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="form-group"> 
                                                <label for="field-4" class="control-label">Percentage</label> 
                                                 <input type="text"  parsley-trigger="change" min="0" class="form-control" id="center_title" name="{{ $val->slug }}[percentage]" value="{{ $val->percentage }}"> 
                                                 @if($val->slug != 'bdm')
                                                 <label for="field-4" class="control-label">Price</label> 
                                                 <input type="text"  parsley-trigger="change" min="0" class="form-control" id="center_title" name="{{ $val->slug }}[price]" value="{{ $val->level_price }}"> 
                                                 @else
                                                 <input type="hidden" name="{{ $val->slug }}[price]" value="{{ $val->level_price }}"> 
                                                 @endif
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>

                @endforeach
            </div>
        </div> 
        <div class="modal-footer">  
            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button> 
            <button type="submit" class="btn btn-default waves-effect waves-light">Save</button> 
        </div>
        </form>
    </div> 
    </div>
</div><!-- /.modal -->

<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
    },
    customDataTable: function(){
        $table_datatable = $('#table_levels').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.levels.json") }}',
           columns: [
                { data: 'level_name' },
                { data: 'state_name' },
                { data: 'level_type' },
                { data: 'allocated_status' },
                { data: 'user_name' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.allocated_status == 1)
                            return  '<span class="label label-table label-danger">Allocated</span>';          
                        else if(row.archived_at)
                            return  '<span class="label label-table label-inverse">Archived</span>';          
                        else
                            return  '<span class="label label-table label-success">Available</span>';
                    },
                    "targets": 3,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 5,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,

            initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(column[0] != 1) return false;
                title = this.header().dataset.head;
                var select = $('<select class="form-control" ><option value="">'+title+'</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
        });
        
    },
    getActions: function(row){ 
        var html = '';
        if(row.allocated_status==1 || row.archived_at != null ){
            html = `
                <div class="btn-group" role="group">
                  <a title="Edit" class="label label-primary waves-effect waves-light" href="{{ route('admin.levels.save') }}/`+row.level_id+`">Edit</a> 
                </div>`;
        }else{
            html = `
                <div class="btn-group" role="group">
                  <a title="Edit" class="label label-primary waves-effect waves-light" href="{{ route('admin.levels.save') }}/`+row.level_id+`">Edit</a> 
                  <a title="Delete" href="#" class="label label-danger waves-effect waves-light delete" data-code="{{ route('admin.listLevels') }}/?id=`+row.level_id+`&type=delete" >Delete</a>
                </div>`;
        }    
        return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

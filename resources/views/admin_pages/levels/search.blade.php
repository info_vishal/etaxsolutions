@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Search Levels') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Search</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="col-md-12">
                <div class="col-md-11">
                    <h4 class="m-t-0 header-title"><b>{{ __('Search Levels') }}</b></h4>
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary searchBtn" type="button">Search</button>
                </div>
                <div class="col-md-12">
                    <hr/>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <form class="" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate=""> 
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-3">
                              <label for="inputState">Levels *</label>
                                <select parsley-trigger="change" class="form-control select_levels text-trans-cap" name="levels" required>
                                    <option value="">Select Levels</option>
                                    @foreach( levels() as $key => $val)
                                        @php
                                            $selected = (@$data->level_type == $key)?'selected':'';
                                        @endphp
                                        <option {{ $selected }} value='{{ $key }}'>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group col-md-3">
                                <div class="col-sm-12">
                                <label for="inputPassword4">States *</label>
                                    <select class="form-control select2 select_state" name="states" data-child='district' parsley-trigger="change" required="">
                                    <option value="">Select States</option>
                                    @foreach($states as $key => $val)
                                        @php
                                            $selected = (@$data->state_id == $val->id)?'selected':'';
                                        @endphp
                                        <option {{ $selected }} value='{{ $val->id }}'>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>

                            <div class="form-group col-md-3 hide div_district">
                              <label for="inputState">District *</label>
                                <select class="" name='district' id="district" data-child="taluka">
                                    @if(@$data->district_id)
                                        <option selected value="{{ @$data->district_id }}">{{ @$data->district_name }}</option>
                                    @endif                                    
                                </select>
                            </div>

                            <div class="form-group col-md-3 hide div_taluka">
                              <label for="inputState">Taluka *</label>
                                <select class="" name='taluka' id="taluka">
                                    @if(@$data->district_id)
                                        <option selected value="{{ @$data->district_id }}">{{ @$data->district_name }}</option>
                                    @endif                                    
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card-box">
            <div class="table-responsive">
                    <table class="table table-striped table-bordered dataTable no-footer" id="table_levels">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th data-head="State"> State </th>
                                <th> Levels </th>
                                <th> Status </th>
                                <th> Actions </th>
                            </tr>
                        </thead> 
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th> Name </th>
                                <th> State </th>
                                <th> Levels </th>
                                <th> Status </th>
                                <th> Actions </th>
                            </tr>
                        </tfoot>    
                    </table>
                </div>
         </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common();
    },
    common: function(){
        $('form').parsley();
        $table_datatable = $('#table_levels').DataTable({"lengthChange": false});
        $(document).on("click",".searchBtn",function(){ 
            var form = $('form').serialize();
            page.customDataTable(form);

        });

        $(document).on("change",".select_levels",function(){ 
            $('.select_state').val(null).trigger('change');
            $(".div_district, .div_taluka").addClass('hide');
            $("#district, #taluka").removeClass();
            $("#district, #taluka").removeAttr('multiple required');
        });

        $(document).on("change",".select_state",function(){

            var level = $(".select_levels").val();
            if( (level == "taluka" || level == "counter") && this.value.length > 0){

                var state_id = this.value;
                $.ajax({
                    url: '{{ route("common.districtAjax") }}',
                    data: { 'state_id': state_id,'_token': '{{ csrf_token() }}' },
                    dataType: 'JSON',
                    type: 'POST',
                    success: function(response){
                        $(".div_district").removeClass('hide');
                        $('#district').html(response.data);
                        $("#district").addClass("form-control select2");
                        $("#district").attr({'required':'required'});
                        $("#district").select2();
                    }
                });
            }
        });

        $(document).on("change","#district",function(){
            var id = this.dataset.child;
            var district_id = this.value;
            var level = $(".select_levels").val();
            if(level == "counter" && this.value.length > 0){
                $.ajax({
                    url: '{{ route("common.talukaAjax") }}',
                    data: { 'district_id': district_id,'_token': '{{ csrf_token() }}' },
                    dataType: 'JSON',
                    type: 'POST',
                    success: function(response){
                        $(".div_taluka").removeClass('hide');
                        $('#taluka').html(response.data);
                        $("#taluka").addClass("form-control");
                        $("#taluka").addClass("select2");
                        $("#taluka").select2();
                        $("#taluka").attr({'required':'required'});
                    }
                });
            }
        });
    },
    customDataTable: function( form ){
        $table_datatable = $('#table_levels').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.levels.json") }}?'+form,
           columns: [
                { data: 'level_name' },
                { data: 'state_name' },
                { data: 'level_type' },
                { data: 'allocated_status' }
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.allocated_status == 1)
                            return  '<span class="label label-table label-danger">Allocated</span>';          
                        else if(row.archived_at)
                            return  '<span class="label label-table label-inverse">Archived</span>';          
                        else
                            return  '<span class="label label-table label-success">Available</span>';
                    },
                    "targets": 3,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 4,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
            "destroy": true,
        });
        
    },
    getActions: function(row){ 
        var html = '';
        if(row.allocated_status == 1){
            html = `
                <div class="btn-group" role="group">
                  <a class="label label-primary" title="View" href="{{ route('admin.levels.save') }}/`+row.level_id+`">View Level</a> 
                  <a class="label label-primary" title="View" href="{{ route('admin.users.save') }}/`+row.assigned_user_id+`">View User</a> 
                </div>`;
        }else{
            html = `
                <div class="btn-group" role="group">
                  <a class="label label-primary" title="View" href="{{ route('admin.levels.save') }}/`+row.level_id+`">View Level</a>
                </div>`;
        }
            
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="">
                    <a href="{{ route('admin.dashboard') }}" class="waves-effect {{ Request::is('admin') ? 'active' : '' }}"><i class="ti-home"></i> <span> Dashboard </span> </a>
                </li>
                <li class="">
                    <a href="{{ route('admin.listUsers') }}" class="waves-effect {{ Request::is('admin/users*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Users </span> </a>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect {{ Request::is('admin/levels*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Levels </span> </a>
                    <ul class="list-unstyled">
                        <li class="{{ Request::is('admin/levels') ? 'active' : '' }}">
                            <a href="{{ route('admin.listLevels') }}"><i class="fa fa-sticky-note"></i> <span>Levels </span> </a>
                        </li>
                        <li class="{{ Request::is('admin/levels/search') ? 'active' : '' }}">
                            <a href="{{ route('admin.levels.search') }}"><i class="fa fa-sticky-note"></i> Search Levels </a>
                        </li>
                    </ul>
                </li>


                <li class="">
                    <a href="{{ route('admin.rechargeIndex') }}" class="waves-effect {{ Request::is('admin/recharge*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Recharges </span> </a>
                </li>

                <li class="">
                    <a href="{{ route('admin.listDistrict') }}" class="waves-effect {{ Request::is('admin/district*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Districts </span> </a>
                </li>
                
                <li class="">
                    <a href="{{ route('admin.listTaluka') }}" class="waves-effect {{ Request::is('admin/taluka*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Talukas </span> </a>
                </li>
                
                <li class="">
                    <a href="{{ route('admin.listLatestNews') }}" class="waves-effect {{ Request::is('admin/latestNews*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Latest News </span> </a>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect {{ Request::is('admin/downloads*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Downloads </span> </a>
                    <ul class="list-unstyled">
                        <li class="{{ Request::is('admin/downloads') ? 'active' : '' }}">
                            <a href="{{ route('admin.downloadsIndex') }}"><i class="fa fa-sticky-note"></i> <span>User</span> </a>
                        </li>
                        <li class="{{ Request::is('admin/downloads-client') ? 'active' : '' }}">
                            <a href="{{ route('admin.downloads.client') }}"><i class="fa fa-sticky-note"></i>Client</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="{{ route('admin.notify') }}" class="waves-effect {{ Request::is('admin/notification*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Notification </span> </a>
                </li>

                <li class="">
                    <a href="{{ route('admin.suggest.adminIndex') }}" class="waves-effect {{ Request::is('admin/suggest*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Suggestion </span> </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


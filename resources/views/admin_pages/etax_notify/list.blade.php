@extends('admin_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/summernote/dist/summernote.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Notification List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Notification List</li>
        </ol>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-primary waves-effect waves-light open_model" data-toggle="modal" data-target="#con-close-modal"><i class="fa fa-plus"></i> Create Notification</button>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Notification  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_downloads">
                            <thead>
                                <tr>
                                    <th> Title </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="con-close-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog modal-lg"> 
    <div class="modal-content"> 
    <form method="POST" action="{{ route('admin.notify.save') }}" role="form" data-parsley-validate="">
        @csrf
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">Create Notification</h4> 
        </div> 
        <div class="modal-body">
            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="form-group"> 
                        <label for="field-4" class="control-label">Title *</label> 
                         <input type="text"  parsley-trigger="change" class="form-control" id="center_title" name="title" required> 
                    </div> 
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="form-group"> 
                        <label for="field-4" class="control-label">Descriptions *</label> 
                        <textarea name="descriptions" class="summernote"></textarea>
                    </div> 
                </div>
            </div>
        </div> 
        <div class="modal-footer"> 
            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button> 
            <button type="submit" class="btn btn-default waves-effect waves-light">Save</button> 
        </div>
        </form>
    </div> 
    </div>
</div><!-- /.modal -->

<div id="con-close-modal-1" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog modal-lg"> 
    <div class="modal-content"> 
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">View Notification</h4> 
        </div> 
        <div class="modal-body">
            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="form-group"> 
                        <label for="field-4" class="control-label" id="view_title"></label> 
                    </div> 
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="form-group">  
                        <div id="view_descriptions" class=""></div>
                    </div> 
                </div>
            </div>
        </div> 
    </div> 
    </div>
</div><!-- /.modal -->

<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/summernote/dist/summernote.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
        $('form').parsley();

        $('.summernote').summernote({
            height: 350,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        $('.inline-editor').summernote({
            airMode: true            
        });

        $(document).on("click",".open_model_2",function(){
            var id = this.dataset.id;
            console.log($("#desc_"+id).val());
            $("#view_title").text(this.dataset.title);
            $("#view_descriptions").html($("#desc_"+id).val());
        });
    },
    customDataTable: function(){

        $table_datatable = $('#table_downloads').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.notify.json") }}',
           columns: [
                { data: 'title' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 1,
                }
            ],
            bFilter: true, 
            bInfo: true,
            "lengthChange": false,
            "pageLength": 15,
            "orderable": false,
            "ordering": false
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                <textarea class="hide" id="desc_`+row.id+`">`+row.descriptions+`</textarea>
                <a href="javascript:void(0)" 
                data-id="`+row.id+`"
                data-title="`+row.title+`"
                class="label label-primary open_model waves-effect waves-light open_model_2" data-toggle="modal" data-target="#con-close-modal-1">View</a>
                  <a href="#" data-code="{{ route('admin.notify') }}?type=delete&id=`+row.id+`"
                class="label label-danger waves-effect waves-light delete">Delete</a>
                </div>`;
        return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

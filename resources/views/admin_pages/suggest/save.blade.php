@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('Suggest') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.suggest.adminIndex') }}">Save</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card-box">
            <div class="row">
                <div class="container">
                    <form class="" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
                        @csrf
                        @method('put')
                        @include('suggest_form')
                        @if(@$suggestData->status != "accepted")
                        <div class="col-md-12" align="right">
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-danger" type="reset">Clear</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common();
    },
    common: function(){
        $('form').parsley();
       
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

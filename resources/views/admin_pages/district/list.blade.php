@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('District List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>District List</li>
        </ol>
    </div>
    <div class="col-md-2">
        <a href="{{ route('admin.district.save') }}"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Create District</button></a>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('District  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="table_district">
                            <thead>
                                <th> State </th>
                                <th> District </th>
                                <th> Actions </th>
                            </thead>     
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
    },
    customDataTable: function(){
        $table_datatable = $('#table_district').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.districtJson") }}',
           columns: [
                { data: 'state_name' },
                { data: 'name' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 2,
                },
            ],
            "lengthChange": false,
            "pageLength":   15,
            "orderable": true,
        });
        
    },
    getActions: function(row){ 
        var html = '';
            html = `
                <div class="btn-group" role="group">
                  <a title="Edit" class="label label-primary waves-effect waves-light"  href="{{ route('admin.district.save') }}/`+row.id+`">Edit</a> 
                  <a title="Delete" href="#" class="label label-danger waves-effect waves-light delete" data-code="{{ route('admin.listDistrict') }}/?id=`+row.id+`&type=delete" >Delete</a>
                </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

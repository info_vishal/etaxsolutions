@extends('admin_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-8">
        <h4 class="page-title">{{ __('Downloads List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Downloads List</li>
        </ol>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-primary waves-effect waves-light open_model" data-toggle="modal" data-target="#con-close-modal"><i class="fa fa-plus"></i> Create Center</button>
    </div>

    <div class="col-md-2">
        <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal-1"><i class="fa fa-plus"></i> Add Files </button>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Downloads  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_downloads">
                            <thead>
                                <tr>
                                    <th> Title </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($downloads as $key => $val)
                                <tr>
                                    <td>{{ $val->title }}</td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="javascript:void(0)" 
                                            data-title="{{ $val->title }}"
                                            data-id="{{ $val->id }}"
                                            class="label label-primary open_model waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">Edit</a>
                                            
                                            <a href="#" data-code="{{ route('admin.downloads.centerSave') }}?type=delete&id={{ $val->id }}"
                                            class="label label-danger waves-effect waves-light delete">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_download_files">
                            <thead>
                                <tr>
                                    <th> Download Center </th>
                                    <th> Title </th>
                                    <th> File </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <form method="POST" id="center" action="{{ route('admin.downloads.centerSave') }}" role="form" data-parsley-validate="">
        @csrf
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">Download Center</h4> 
        </div> 
        <div class="modal-body">
            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="form-group"> 
                        <label for="field-4" class="control-label">Title *</label> 
                         <input type="text"  parsley-trigger="change" class="form-control" id="center_title" name="title" required> 
                    </div> 
                </div>
            </div>
        </div> 
        <div class="modal-footer"> 
            <input type="hidden" id="center_id" name="center_id"> 
            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button> 
            <button type="submit" class="btn btn-default waves-effect waves-light">Save</button> 
        </div>
        </form>
    </div> 
    </div>
</div><!-- /.modal -->

<div id="con-close-modal-1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <form method="POST" id="files" action="{{ route('admin.downloads.fileSave') }}" role="form" data-parsley-validate="" enctype="multipart/form-data">
        @csrf
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">Add Files</h4> 
        </div> 
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6"> 
                    <div class="form-group"> 
                        <label for="field-4" class="control-label">Center *</label> 
                         <select class="form-control file_select" name="center">
                            @foreach($downloads as $key => $val)
                                <option value="{{ $val->id }}">{{ $val->title }}</option>
                            @endforeach
                         </select>
                    </div> 
                </div> 
                <div class="col-md-6"> 
                     <div class="form-group"> 
                        <label for="field-4" class="control-label">Title *</label> 
                         <input type="text"  parsley-trigger="change" class="form-control" id="title" name="title" required> 
                    </div> 
                </div> 
            </div>
            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label class="control-label">File *</label>
                        <input type="file" class="filestyle" name="file" data-input="false" id="file_path" tabindex="-1" parsley-trigger="change" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);"><div class="bootstrap-filestyle input-group">
                            <span class="group-span-filestyle " tabindex="0"><label for="file_path" class="btn btn-default "><span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> <span class="buttonText">Choose file</span></label></span></div>
                    </div>
                </div>
            </div> 
        </div> 
        <div class="modal-footer"> 
            <input type="hidden" id="file_id" name="file_id"> 
            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button> 
            <button type="submit" class="btn btn-default waves-effect waves-light">Save</button> 
        </div>
        </form>
    </div> 
    </div>
</div><!-- /.modal -->

<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
        $('form').parsley();
        $(document).on("click",".open_model",function(){
            $('#center').trigger("reset");
            $("#center_title").val(this.dataset.title);
            $("#center_id").val(this.dataset.id);
        });

        $(document).on("click",".open_model_2",function(){
            $('#files').trigger("reset");
            $("#title").val(this.dataset.descriptions);
            $("#file_id").val(this.dataset.id);
            $(".file_select").val(this.dataset.center_id);
        });
    },
    customDataTable: function(){
        $('#table_downloads').DataTable({
            "bPaginate": false,
            bFilter: false, 
            bInfo: false
        });

        $table_datatable = $('#table_download_files').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.downloads.fileJson") }}',
           columns: [
                { data: 'title' },
                { data: 'descriptions' },
                { data: 'file_path' }
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        var file = "@php echo image_url('NULL'); @endphp";
                            file += "/"+data;
                        return `<a href="`+file+`" target="_blank" download> Download </a>`;
                    },
                    "targets": 2,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 3,
                }
            ],
            bFilter: false, 
            bInfo: false,
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                <a href="javascript:void(0)" 
                data-id="`+row.df_id+`"
                data-descriptions="`+row.descriptions+`"
                data-center_id="`+row.center_id+`"
                data-file_path="`+row.file_path+`"
                class="label label-primary open_model waves-effect waves-light open_model_2" data-toggle="modal" data-target="#con-close-modal-1">Edit</a>
                  <a href="#" data-code="{{ route('admin.downloads.fileSave') }}?type=delete&id=`+row.df_id+`"
                class="label label-danger waves-effect waves-light delete">Delete</a>
                </div>`;
        return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

@extends('admin_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Downloads List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Downloads List</li>
        </ol>
    </div>

    <div class="col-md-2">
        <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal-1"><i class="fa fa-plus"></i> Create </button>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Downloads  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_download_files">
                            <thead>
                                <tr>
                                    <th> Title </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(@$downloads as $key => $val)
                                <tr>
                                    <td>{{ $val->title }}</td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="{{ image_url($val->file_path) }}" class="label label-primary waves-effect waves-light" download >Download</a>

                                            <a href="#" data-code="{{ route('admin.downloads.client') }}?type=delete&id={{ $val->id }}"
                                            class="label label-danger waves-effect waves-light delete">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="con-close-modal-1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <form method="POST" id="files" action="{{ route('admin.downloads.client') }}" role="form" data-parsley-validate="" enctype="multipart/form-data">
        @csrf
        <div class="modal-header"> 
            <button type="reset" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h4 class="modal-title">Create</h4> 
        </div> 
        <div class="modal-body">
            <div class="row"> 
                <div class="col-md-6"> 
                     <div class="form-group"> 
                        <label for="field-4" class="control-label">Title *</label> 
                         <input type="text"  parsley-trigger="change" class="form-control" id="title" name="title" required> 
                    </div> 
                </div> 
            </div>
            <div class="row"> 
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label class="control-label">File *</label>
                        <input type="file" class="filestyle" name="file" data-input="false" id="file_path" tabindex="-1" parsley-trigger="change" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);"><div class="bootstrap-filestyle input-group">
                            <span class="group-span-filestyle " tabindex="0"><label for="file_path" class="btn btn-default "><span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> <span class="buttonText">Choose file</span></label></span></div>
                    </div>
                </div>
            </div> 
        </div> 
        <div class="modal-footer"> 
            <input type="hidden" id="file_id" name="file_id"> 
            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button> 
            <button type="submit" class="btn btn-default waves-effect waves-light">Save</button> 
        </div>
        </form>
    </div> 
    </div>
</div><!-- /.modal -->

<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
    },
    common: function(){
        $('form').parsley();
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

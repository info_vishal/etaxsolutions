<html>
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
         window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="2" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Company Details </center> </strong></td>
               </tr>
               <td colspan="4">

                  <div class="div_2 d_m_b">
                     <strong>Company Type: </strong> 
                     <br>
                     <font> {{ $data->company_type }} </font>
                  </div>

                  <div class="div_2 d_m_b"> 
                     <strong>5 Proposed Name of Company: </strong>
                     <br>
                     @php $name = str_replace('@$',', ',$data->company_name) @endphp
                     <font> {{ $name}} </font>
                  </div>

                  <div class="div_2 d_m_b"> 
                     <strong>Object of company: </strong> 
                     <br>
                     <font> {{ $data->company_object }} </font>
                  </div>

                  <div class="div_2 d_m_b"> 
                     <strong>Brief business details: </strong> 
                     <br>
                     <font> {{ $data->company_brief_details }} </font>
                  </div>
               
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2">
                     <strong>Flat / Room No: </strong> <font> {{ $data->company_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->company_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->company_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->company_add_pincode }} </font>
                  </div>
                 
                  <div class="div_2">
                     <strong>City: </strong> <font> {{ $data->company_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->company_add_country }} </font>
                  </div>
                  <div class="div_1"><strong>LandLine no: </strong> <font> {{ $data->company_landline_no }} </font></div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Directors Details</center> </strong></td>
               </tr>
               <td colspan="4">

                  @foreach($childData as $c_key => $c_val)
                     <div class="div_3">
                        <strong>Post Type: </strong> <br> <font> {{ $c_val->director_post }} </font>
                     </div>
                     <div class="div_3">
                        <strong>Director Name: </strong> <br> <font> {{ $c_val->director_name }} </font>
                     </div>
                     <div class="div_3">
                        <strong>Mobile No: </strong> <br> <font> {{ $c_val->mobile_number }} </font>
                     </div>
                     <div class="div_3">
                        <strong>Email-Id : </strong> <br> <font> {{ $c_val->email_id }} </font>
                     </div> 
                     <div style="clear: both; margin-bottom: 20px;border-bottom: 1px dotted; text-align: center;">
                     <b>Address</b>
                     </div>

                     @php
                        $state = \Auth::user()->getStates($c_val->res_add_state);
                        $district = \Auth::user()->getdistricts($c_val->res_add_district,1);
                     @endphp
                     <div class="div_2">
                        <strong>Flat / Room No: </strong> <font> {{ $c_val->res_add_building }} </font><br>
                        <strong>Street / Road / Post Office: </strong> <font> {{ $c_val->res_add_street }} </font><br>
                        <strong>Area / Locality / Taluka: </strong> <font> {{ $c_val->res_add_area }} </font><br>
                        <strong>Pincode: </strong> <font> {{ $c_val->res_add_pincode }} </font>
                     </div>
                     <div class="div_2">
                        <strong>City: </strong> <font> {{ $c_val->res_add_city }} </font><br>
                        <strong>District: </strong> <font> {{ @$district->name }} </font><br>
                        <strong>State: </strong> <font> {{ @$state->name }} </font><br>
                        <strong>Country: </strong> <font> {{ $c_val->res_add_country }} </font>
                     </div>         

                     <div style="clear: both; margin: 20px 0px; border-bottom: 1px dotted; text-align: center;">
                     <b>Bank Details</b>
                     </div>

                     <div class="div_3"><strong>IFSC CODE: </strong> <br> <font> {{ $c_val->bank_ifsc }} </font></div>
                     <div class="div_3"> <strong>Bank Name: </strong> <br> <font> {{ $c_val->bank_name }} </font> </div>
                     <div class="div_3"> <strong>Branch: </strong> <br> <font> {{ $c_val->bank_branch}} </font></div>

                     <div class="div_3"><strong>A/C type: </strong> <br> <font> {{ $c_val->bank_account_type }} </font></div>
                     <div class="div_3"> <strong>A/C Number: </strong> <br> <font> {{ $c_val->bank_account_number }} </font> </div>
                     <div class="div_3"> <strong>A/C Holder Name: </strong> <br> <font> {{ $c_val->bank_holder_name}} </font></div>

                     <div style="clear: both; margin: 20px 0px; border-bottom:double; "></div>        
                  @endforeach
                 
               </td>
            </tr>

         </tfoot>
      </table>
   </body>
</html>
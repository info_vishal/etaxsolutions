@if(\Request()->id == "")
<div class="col-md-12">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-8">
                  <label for="company_address_file">Address proof of  company(utility bills and rent agreement or property registry copy) *</label>
                  <input type="file" class="form-control" name="company_address_file" required="" parsley-trigger="change" />
                    @if($errors->has('company_address_file'))
                        @php 
                            echo showError($errors->first('company_address_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="company_anyother_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="company_anyother_file"  parsley-trigger="change" />
                    @if($errors->has('company_anyother_file'))
                        @php 
                            echo showError($errors->first('company_anyother_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="col-md-12">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-8">
                    <div class="col-md-12 m-t-10">
                      <label for="company_address_file"><a target="_blank" download href="{{ image_url($data->company_address_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Address proof of  company(utility bills and rent agreement or property registry copy)
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="col-md-12 m-t-10">
                      <label for="company_anyother_file"><a target="_blank" download href="{{ image_url($data->company_anyother_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Any other documents as needed
                      </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="col-md-12"></div>
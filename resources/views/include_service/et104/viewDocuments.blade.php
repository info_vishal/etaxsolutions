<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
          
          <div class="col-md-12">
              <div class="form-row">
              <div class="form-group col-md-6">
                
                <div class="col-md-12 m-t-10">
                  <label for="pancard_file"><a target="_blank" download href="{{ image_url($data->pancard_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Pan card of gst registration applicant
                  </label>
                </div>
              </div>

              <div class="form-group col-md-6">
                
                <div class="col-md-12 m-t-10">
                  <label for="proof_business_reg_file"><a target="_blank" download href="{{ image_url($data->proof_business_reg_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Proof of business registration or incorporation certificate (if any)
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12">
              <div class="form-row">
              <div class="form-group col-md-6">
                
                <div class="col-md-12 m-t-10">
                  <label for="identity_address_file"><a target="_blank" download href="{{ image_url($data->identity_address_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Identity and address proof of promoters with photographs
                  </label>
                </div>
              </div>

              <div class="form-group col-md-6">
                
                <div class="col-md-12 m-t-10">
                  <label for="address_business_file"><a target="_blank" download href="{{ image_url($data->address_business_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Address proof for the place of business ( rent agreement and utility bills)
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12">
              <div class="form-row">
              <div class="form-group col-md-6">
                
                <div class="col-md-12 m-t-10">
                  <label for="bank_statement_file"><a target="_blank" download href="{{ image_url($data->bank_statement_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> All banks statements (Firms & Company)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-6">
                
                <div class="col-md-12 m-t-10">
                  <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Any other documents as needed
                  </label>
                </div>
              </div>
            </div>
          </div>

        </div>
    </div>
</div>
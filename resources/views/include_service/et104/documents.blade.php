<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-6">
                  <label for="pancard_file">Pan card of gst registration applicant *</label>
                  <input type="file" class="form-control" name="pancard_file" required="" parsley-trigger="change" />
                    @if($errors->has('pancard_file'))
                        @php 
                            echo showError($errors->first('pancard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="proof_business_reg_file">Proof of business registration or incorporation certificate (if any) *</label>
                  <input type="file" class="form-control" name="proof_business_reg_file" required="" parsley-trigger="change" />
                    @if($errors->has('proof_business_reg_file'))
                        @php 
                            echo showError($errors->first('proof_business_reg_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="identity_address_file">Identity and address proof of promoters with photographs *</label>
                  <input type="file" class="form-control" name="identity_address_file" required="" parsley-trigger="change" />
                    @if($errors->has('identity_address_file'))
                        @php 
                            echo showError($errors->first('identity_address_file'));
                        @endphp
                    @endif
                </div>

                
                <div class="form-group col-md-6">
                  <label for="address_business_file">Address proof for the place of business ( rent agreement and utility bills)</label>
                  <input type="file" class="form-control" name="address_business_file"  parsley-trigger="change" />
                    @if($errors->has('address_business_file'))
                        @php 
                            echo showError($errors->first('address_business_file'));
                        @endphp
                    @endif
                </div>
                <div class="col-md-12"></div>

                <div class="form-group col-md-6">
                  <label for="bank_statement_file">All banks statements (Firms & Company)</label>
                  <input type="file" class="form-control" name="bank_statement_file"  parsley-trigger="change" />
                    @if($errors->has('bank_statement_file'))
                        @php 
                            echo showError($errors->first('bank_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
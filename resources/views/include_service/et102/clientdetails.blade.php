@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Client Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Client Last Name/Surname *</label>
                  <input type="text" id="name" parsley-trigger="change" name="cli_last_name" class="form-control" required value="{{ (@$data)?@$data->cli_last_name:old('cli_last_name') }}">
                    @if($errors->has('cli_last_name'))
                        @php 
                            echo showError($errors->first('cli_last_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputPassword4">Client First Name *</label>
                  <input type="text" id="cli_first_name" parsley-trigger="change" name="cli_first_name" class="form-control"  required value="{{ (@$data)?@$data->cli_first_name:old('cli_first_name') }}">
                    @if($errors->has('cli_first_name'))
                        @php 
                            echo showError($errors->first('cli_first_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputPassword4">Client Middle Name *</label>
                  <input type="text" id="cli_middle_name" parsley-trigger="change" name="cli_middle_name" class="form-control"  required value="{{ (@$data)?@$data->cli_middle_name:old('cli_middle_name') }}">
                    @if($errors->has('cli_middle_name'))
                        @php 
                            echo showError($errors->first('cli_middle_name'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="pancard_no">Pancard No *</label>
                  <input type="text" id="pancard_no" parsley-trigger="change" name="pancard_no" class="form-control"  required value="{{ (@$data)?@$data->pancard_no:old('pancard_no') }}">
                    @if($errors->has('pancard_no'))
                        @php 
                            echo showError($errors->first('pancard_no'));
                        @endphp
                    @endif
                </div>
                
                <div class="form-group col-md-4">
                    <div class="col-sm-12">
                    <label for="inputPassword4">Date of Birth *</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="cli_dob" id="datepicker-autoclose" value="{{ (@$data)?filterDate(@$data->cli_dob):old('cli_dob') }}">
                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                        </div><!-- input-group -->
                        @if($errors->has('cli_dob'))
                        @php 
                            echo showError($errors->first('cli_dob'));
                        @endphp
                    @endif
                    </div>
                </div>

                <div class="form-group col-md-4">
                  <label for="inputState">Gender *</label>
                  <select class="form-control select2" name="cli_gender" parsley-trigger="change" required="">
                        @foreach(customerGender() as $key => $val)
                            @php
                                $select_stated = (old('cli_gender') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->cli_gender == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('cli_gender'))
                        @php 
                            echo showError($errors->first('cli_gender'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
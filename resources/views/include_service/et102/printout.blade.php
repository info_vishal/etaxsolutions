<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
           window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="2" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Client Details </center> </strong></td>
               </tr>
               <td colspan="3">
                  <strong>Last Name: </strong> <font> {{ $data->cli_last_name }} </font><br>
                  <strong>First Name: </strong> <font> {{ $data->cli_first_name }} </font><br>
                  <strong>Middle Name: </strong> <font> {{ $data->cli_middle_name}} </font><br>
                  
               </td>
               <td colspan="1">
                  <strong>Gender: </strong> <font> {{ $data->cli_gender}} </font><br>
                  <strong>DOB: </strong> <font> {{ ddmmyy($data->cli_dob)}} </font><br>
               </td>
            </tr>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Parents Details </center> </strong></td>
               </tr>
               <td colspan="4">
                  <strong>Father Full Name : </strong> <font> {{ $data->father_full_name }} </font><br>
                  <strong>Mother Full Name : </strong> <font> {{ $data->mother_full_name }} </font><br>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Aadhar Details </center> </strong></td>
               </tr>
               <td colspan="4">
                  <strong>Alloted Aadhar Number(If Available) : </strong> <font> {{ $data->aadhar_allotted }} </font><br>
                  <strong>Enrolment ID : </strong> <font> {{ $data->aadhar_not_allotted }} </font><br>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div style="width:50%;float:left">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                  <div style="width:50%;float:left">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Contact Details </center> </strong></td>
               </tr>
               <td colspan="4">

                  <div style="width:33.33%;float:left"><strong>LandLine no: </strong> <font> {{ $data->landline_no }} </font></div>
                  <div style="width:33.33%;float:left"> <strong>Mobile No: </strong> <font> {{ $data->mobile_number }} </font> </div>
                  <div style="width:33.33%;float:left"> <strong>Secondary Mobile No: </strong> <font> {{ $data->sec_mobile_number}} </font></div>

                  <div style="width:33.33%;float:left"><strong>Email-ID: </strong> <font> {{ $data->email_id }} </font></div>
                  <div style="width:33.33%;float:left"> <strong>Secondary Email-ID: </strong> <font> {{ $data->sec_email_id }} </font> </div>
                  <div style="width:33.33%;float:left"> <strong>Contact Type: </strong> <font> {{ $data->contact_type}} </font></div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Bank Details </center> </strong></td>
               </tr>
               <td colspan="4">
               
                  @foreach($banks as $b_key => $b_val)
                     <div class="div_3"><strong>IFSC CODE: </strong> <br> <font> {{ $b_val->bank_ifsc }} </font></div>
                     <div class="div_3"> <strong>Bank Name: </strong> <br> <font> {{ $b_val->bank_name }} </font> </div>
                     <div class="div_3"> <strong>Branch: </strong> <br> <font> {{ $b_val->bank_branch}} </font></div>

                     <div class="div_3"><strong>A/C type: </strong> <br> <font> {{ $b_val->bank_account_type }} </font></div>
                     <div class="div_3"> <strong>A/C Number: </strong> <br> <font> {{ $b_val->bank_account_number }} </font> </div>
                     <div class="div_3"> <strong>A/C Holder Name: </strong> <br> <font> {{ $b_val->bank_holder_name}} </font></div>
                     <hr>
                  @endforeach
               </td>
            </tr>

         </tfoot>
      </table>
   </body>
</html>
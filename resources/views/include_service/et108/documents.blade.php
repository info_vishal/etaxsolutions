<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-12">
                  <label for="financial_statement_file">All financial statements (balance sheet,P&L statement,Trading Account,all ledger accounts) *</label>
                  <input type="file" class="form-control" name="financial_statement_file" required="" parsley-trigger="change" />
                    @if($errors->has('financial_statement_file'))
                        @php 
                            echo showError($errors->first('financial_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="bank_statement_file">All bank details (all bank statement with front page) *</label>
                  <input type="file" class="form-control" name="bank_statement_file" required="" parsley-trigger="change" />
                    @if($errors->has('bank_statement_file'))
                        @php 
                            echo showError($errors->first('bank_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="transaction_bill_file">All transaction bills ( expense bills,income voucher,credit/debit notes,any other bills) *</label>
                  <input type="file" class="form-control" name="transaction_bill_file" required="" parsley-trigger="change" />
                    @if($errors->has('transaction_bill_file'))
                        @php 
                            echo showError($errors->first('transaction_bill_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="sales_purchase_file">All sales and purchase (all bills of sales and purchase and sales/purchase return)</label>
                  <input type="file" class="form-control" name="sales_purchase_file"  parsley-trigger="change" />
                    @if($errors->has('sales_purchase_file'))
                        @php 
                            echo showError($errors->first('sales_purchase_file'));
                        @endphp
                    @endif
                </div>


                <div class="col-md-12"></div>
                
                <div class="form-group col-md-4">
                  <label for="software_backup_file">Software backup (if available)</label>
                  <input type="file" class="form-control" name="software_backup_file"  parsley-trigger="change" />
                    @if($errors->has('software_backup_file'))
                        @php 
                            echo showError($errors->first('software_backup_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="last_audit_file">Last audit report copy</label>
                  <input type="file" class="form-control" name="last_audit_file"  parsley-trigger="change" />
                    @if($errors->has('last_audit_file'))
                        @php 
                            echo showError($errors->first('last_audit_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
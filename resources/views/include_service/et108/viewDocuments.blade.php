<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="form-group col-md-12">
                
                <div class="col-md-12 m-t-10">
                  <label for="financial_statement_file"><a target="_blank" download href="{{ image_url($data->financial_statement_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> All financial statements (balance sheet,P&L statement,Trading Account,all ledger accounts)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-12">
                
                <div class="col-md-12 m-t-10">
                  <label for="bank_statement_file"><a target="_blank" download href="{{ image_url($data->bank_statement_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> All bank details (all bank statement with front page)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-12">
              
              <div class="col-md-12 m-t-10">
                <label for="transaction_bill_file"><a target="_blank" download href="{{ image_url($data->transaction_bill_file) }}" title="Download"><i class="fa fa-download"></i></a> All transaction bills ( expense bills,income voucher,credit/debit notes,any other bills)</label>
              </div>
            </div> 

              <div class="form-group col-md-12">
                
                <div class="col-md-12 m-t-10">
                  <label for="sales_purchase_file"><a target="_blank" download href="{{ image_url($data->sales_purchase_file) }}" title="Download"><i class="fa fa-download"></i></a> All sales and purchase (all bills of sales and purchase and sales/purchase return)</label>
                </div>
              </div> 

            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="form-group col-md-4">
                
                <div class="col-md-12 m-t-10">
                  <label for="software_backup_file"><a target="_blank" download href="{{ image_url($data->software_backup_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Software backup (if available)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4">
                
                <div class="col-md-12 m-t-10">
                  <label for="last_audit_file"><a target="_blank" download href="{{ image_url($data->last_audit_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Last audit report copy  
                  </label>
                </div>
              </div>

            <div class="form-group col-md-4">
              
              <div class="col-md-12 m-t-10">
                <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download"><i class="fa fa-download"></i></a> Any other documents as needed</label>
              </div>
            </div>

            </div>
        </div>
    </div>
</div>
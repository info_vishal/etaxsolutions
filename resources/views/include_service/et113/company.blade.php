@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>

    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="col-md-6">
                      <label class="col-md-12" for="firm_name">Proposed name of firm *</label>
                      <div class="form-group col-md-12">
                      <input type="text" id="firm_name" required="" parsley-trigger="change" name="firm_name"  class="form-control"  value="{{ (@$data)?@$data->firm_name:old('firm_name') }}" />
                    </div>
                    
                </div>

                <div class="col-md-6">
                    <div class="form-group col-md-12">
                    <label  for="business_details" >Purpose of trust</label>
                        <textarea  id="business_details" name="business_details" class="form-control">{{ (@$data)?@$data->business_details:old('business_details') }}</textarea>
                        @if($errors->has('business_details'))
                            @php 
                                echo showError($errors->first('business_details'));
                            @endphp
                        @endif
                    </div>
                </div>

            </div>
            @include('include_service.et113.company_address')
            @include('include_service.et113.company_documents')
        </div>
    </div>
</div>
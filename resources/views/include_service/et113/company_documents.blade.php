@if(\Request()->id == "")
<div class="col-md-12">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="address_proof_firm">Address proof of firm(utility bills or rent agreement) *</label>
                  <input type="file" class="form-control" name="address_proof_firm" required="" parsley-trigger="change" />
                    @if($errors->has('address_proof_firm'))
                        @php 
                            echo showError($errors->first('address_proof_firm'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="col-md-12">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="address_proof_firm"><a target="_blank" download href="{{ image_url($data->address_proof_firm) }}" title="Download">
                        <i class="fa fa-download"></i></a> Address proof of firm(utility bills or rent agreement)
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Any other documents as needed
                      </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="col-md-12"></div>
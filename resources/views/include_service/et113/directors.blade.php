@if(\Request()->id == "")
<div class="card-box">
    <div class="col-md-10">
        <h4 class="m-t-0 header-title"><b>{{ __('Partners Details') }}</b></h4>
    </div>
    <div class="col-md-2" align="right">
        <button type="button" class="btn btn-primary waves-effect waves-light addDirectors"><i class="fa fa-plus"></i> Add </button>
    </div> 
    <div class="col-md-12"> <hr/> </div>
    <div class="all_directors">
        <div class="row row_section" id="set_0">
            <div class="container">
                <div class="form-row">

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="partner_name0">Name *</label>
                          <input type="text" id="partner_name0" parsley-trigger="change" name="director[0][partner_name]" data-unique_id="0" data-field_name="partner_name" class="form-control" required value="{{ (@$data)?@$data->partner_name:old('partner_name') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="partner_percentage0">Percentage (profit/Loss) *</label>
                          <input type="text" id="partner_percentage0" min="0" parsley-trigger="change" name="director[0][partner_percentage]" data-unique_id="0" data-field_name="partner_percentage" class="form-control" required value="{{ (@$data)?@$data->partner_percentage:old('partner_percentage') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="mobile_number0">Mobile No *</label>
                          <input type="text" id="mobile_number0" data-mask="(999) 999-9999" name="director[0][mobile_number]" data-unique_id="0" data-field_name="mobile_number"  class="form-control" parsley-trigger="change" required value="{{ (@$data)?@$data->mobile_number:old('mobile_number') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="sec_mobile_number0">Secondary Mobile No </label>
                          <input type="text" id="sec_mobile_number0" data-mask="(999) 999-9999" name="director[0][sec_mobile_number]" data-unique_id="0" data-field_name="sec_mobile_number"  class="form-control" value="{{ (@$data)?@$data->sec_mobile_number:old('sec_mobile_number') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="email_id0">Email-Id *</label>
                          <input type="email" id="email_id0" name="director[0][email_id]" data-unique_id="0" data-field_name="email_id" class="form-control" parsley-trigger="change" required value="{{ (@$data)?@$data->email_id:old('email_id') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="sec_email_id0">Secondary Email-Id </label>
                          <input type="email" id="sec_email_id0" name="director[0][sec_email_id]" data-unique_id="0" data-field_name="sec_email_id" class="form-control" value="{{ (@$data)?@$data->sec_email_id:old('sec_email_id') }}" />
                        </div>
                    </div>

                    @include('include_service.et113.director_address')
                    @include('include_service.et113.director_documents')
                </div>
                <div class="col-md-12 removeBankDiv">
                         <hr>
                 </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="card-box">
    <div class="col-md-10">
        <h4 class="m-t-0 header-title"><b>{{ __('Partners Details') }}</b></h4>
    </div>
    <div class="col-md-12"> <hr/> </div>
    <div class="all_directors">
        <div class="row row_section" id="set_0">
            <div class="container">
                @foreach(@$directors as $d_key => $d_val)
                <div class="form-row">
                    
                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="partner_name0">Name *</label>
                          <input type="text" id="partner_name0" parsley-trigger="change" name="director[0][partner_name]" data-unique_id="0" data-field_name="partner_name" class="form-control" required value="{{ (@$d_val)?@$d_val->partner_name:old('partner_name') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="partner_percentage0">Percentage (profit/Loss) *</label>
                          <input type="text" id="partner_percentage0" min="0" parsley-trigger="change" name="director[0][partner_percentage]" data-unique_id="0" data-field_name="partner_percentage" class="form-control" required value="{{ (@$d_val)?@$d_val->partner_percentage:old('partner_percentage') }}" />
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="mobile_number0">Mobile No *</label>
                          <input type="text" id="mobile_number0" data-mask="(999) 999-9999" name="director[0][mobile_number]" data-unique_id="0" data-field_name="mobile_number"  class="form-control" parsley-trigger="change" required value="{{ (@$d_val)?@$d_val->mobile_number:old('mobile_number') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="sec_mobile_number0">Secondary Mobile No </label>
                          <input type="text" id="sec_mobile_number0" data-mask="(999) 999-9999" name="director[0][sec_mobile_number]" data-unique_id="0" data-field_name="sec_mobile_number"  class="form-control" parsley-trigger="change" required value="{{ (@$d_val)?@$d_val->sec_mobile_number:old('sec_mobile_number') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="email_id0">Email-Id *</label>
                          <input type="email" id="email_id0" name="director[0][email_id]" data-unique_id="0" data-field_name="email_id" class="form-control" parsley-trigger="change" required value="{{ (@$d_val)?@$d_val->email_id:old('email_id') }}" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <label class="col-md-12" for="sec_email_id0">Secondary Email-Id </label>
                          <input type="email" id="sec_email_id0" name="director[0][sec_email_id]" data-unique_id="0" data-field_name="sec_email_id" class="form-control" parsley-trigger="change" required value="{{ (@$d_val)?@$d_val->sec_email_id:old('sec_email_id') }}" />
                        </div>
                    </div>

                    @include('include_service.et113.director_address')
                    @include('include_service.et113.director_documents')
                </div>
                <div class="col-md-12 removeBankDiv"><hr> </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif
<script>
var template = $('.row_section:last').clone();

var sectionsCount = 1;

var director = {
    init: function(){
       this.bankSearch();
       this.add();
       this.remove();
       this.addressSearch();
    },
    add: function(){
        $('.addDirectors').on('click',function(){

            var section = template.clone().find(':input').each(function(){
                var newId = this.id + sectionsCount;

                var field_name = this.dataset.field_name;
                var name = "director["+sectionsCount+"]["+field_name+"]";

                //update for label
                $(this).prev().attr('for', newId);
                $(this).attr('data-unique_id', sectionsCount);

                //update id
                this.name = name;
                this.value = "";
                if(this.id == "bank_id"){
                    this.value = "";
                }else{
                    this.id = newId;
                }

                if(this.tagName == "SELECT"){
                    $(this).val(null).trigger('change');
                    $(this).select2();
                }

            }).end()
            //inject new section
            .appendTo('.all_directors');
            $(section[0]).attr("id","set_0"+sectionsCount);
            $("#set_0"+sectionsCount+" .ifsc_error").attr("id","ifsc_error0"+sectionsCount);
            $("#set_0"+sectionsCount+" .removeBankDiv").html('<div align="right"> <a href="#" class="removeBank">Remove</a></div><hr>');
            sectionsCount++;
            return false;
        });
    },
    remove : function(){
        $(document).on('click', '.row_section .removeBank', function() {
            //fade out section
            console.log($(this).parent().parent().parent());
            //remove parent element (main section)
            $(this).parent().parent().parent().parent().empty();
            return false;

        });
    },
    bankSearch: function(){
        $(document).on('click','.searchBankDetails',function(){
            var id = this.dataset.unique_id;
            if(id  == 0){
                id="";
            }
            var ifsc = $("#bank_ifsc0"+id).val();
            $.ajax({
                url: 'https://ifsc.razorpay.com/'+ifsc,
                dataType: 'JSON',
                type: 'GET',
                success: function(response,textStatus, request){
                    console.log(response);
                    $("#set_0"+id+" #ifsc_error0"+id).addClass("hide");
                    $("#bank_name0"+id).val(response.BANK);
                    $("#bank_branch0"+id).val(response.BRANCH);
                },
                error: function(response,textStatus, request){
                    console.log("#set_0"+id+" #ifsc_error0"+id+" li");
                    $("#set_0"+id+" #ifsc_error0"+id).removeClass("hide");
                    $("#set_0"+id+" #ifsc_error0"+id+" li").text('Not Found');
                    $("#bank_branch0"+id+", #bank_name0"+id).val('');
                }
            });
        });
    },
    addressSearch: function(){
        $(document).on("change",".select_state",function(){
            var id = this.dataset.unique_id;
            var state_id = this.value;
            var level = $(".select_levels").find(':selected').data('level');
            $.ajax({
                url: '{{ route("common.districtAjax") }}',
                data: { 'state_id': state_id,'level':level,'_token': '{{ csrf_token() }}' },
                dataType: 'JSON',
                type: 'POST',
                success: function(response){
                    console.log(response);
                    console.log('res_add_district'+id);
                    $('#res_add_district'+id).html();
                    $('#res_add_district'+id).removeAttr("disabled");
                    $('#res_add_district'+id).html(response.data);
                }
            });
            $('#res_add_district'+id).val(null).trigger('change');
            $('#res_add_district'+id).select2();
        });
    },
};
$(document).ready(function(){
    director.init();
});
</script>
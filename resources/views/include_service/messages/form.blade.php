 <form class="" method="POST" action="{{ route('user.msgLog.msgSave') }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
@csrf
<div class="row">
    <div class="col-md-4">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Messages') }}</b></h4>
                    <hr/>
            <div class="row">
                <div class="container">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="inputPassword4">Descriptions *</label>
                          <textarea id="descriptions" parsley-trigger="change" name="descriptions" class="form-control"  required>{{ (@$data)?@$data->descriptions:old('descriptions') }}</textarea>
                            @if($errors->has('descriptions'))
                                @php 
                                    echo showError($errors->first('descriptions'));
                                @endphp
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                          <label for="file">Atteched File</label>
                          <input type="file" class="form-control" name="file" />
                            @if($errors->has('file'))
                                @php 
                                    echo showError($errors->first('file'));
                                @endphp
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="hidden" name="message_service_id" value="{{ @$data->service_id }}">
                                    <input type="hidden" name="service_type_id" value="{{ \Request()->id }}">
                                    <input type="hidden" name="tracking_no" value="{{ @$data->tracking_no }}">
                                    <input type="hidden" name="service_created_by" value="{{ @$data->created_by }}">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Messages List') }}</b></h4>
                    <hr/>
            <div class="row">
                <div class="container">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer message_tbl">
                            <thead>
                                <tr>
                                    <th> Created By </th>
                                    <th> Description </th>
                                    <th> File </th>
                                    <th> Date </th>
                                </tr>
                                <tbody>
                                    @foreach(@$message as $key => $val)
                                    <tr>
                                        <td>{{ $val->user->employee_code }}</td>
                                        <td>{{ $val->descriptions }}</td>
                                        <td>
                                            @if($val->file != "")
                                            <a target="_blank" download href="{{ image_url($val->file) }}" title="Download"><i class="fa fa-download"></i></a>
                                            @else
                                            ---
                                            @endif
                                        </td>
                                        <td>{{ filterDate($val->created_at) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

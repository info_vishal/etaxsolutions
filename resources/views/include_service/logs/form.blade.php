<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Logs List') }}</b></h4>
                    <hr/>
            <div class="row">
                <div class="container">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer message_tbl">
                            <thead>
                                <tr>
                                    <th> Created By </th>
                                    <th> Description </th>
                                    <th> Date </th>
                                </tr>
                                <tbody>
                                    @foreach(@$serviceLogs as $key => $val)
                                    <tr>
                                        <td>{{ $val->user->employee_code }}</td>
                                        <td>{{ $val->descriptions }}</td>
                                        <td>{{ dbDate($val->created_at,1) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


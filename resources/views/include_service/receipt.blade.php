<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>
         {{ @$service->name }}
      </title>
      <style type="text/css">
         p.MsoNormal {
         margin-top: 0cm;
         margin-right: 0cm;
         margin-bottom: 10.0pt;
         margin-left: 0cm;
         line-height: 115%;
         font-size: 11.0pt;
         font-family: "Calibri", "sans-serif";
         }
      </style>
      <style type="text/css">
         @font-face {
         font-weight: 400;
         font-style:  normal;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-Regular.woff2') format('woff2');
         }
         @font-face {
         font-weight: 400;
         font-style:  italic;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-Italic.woff2') format('woff2');
         }
         @font-face {
         font-weight: 500;
         font-style:  normal;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-Medium.woff2') format('woff2');
         }
         @font-face {
         font-weight: 500;
         font-style:  italic;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-MediumItalic.woff2') format('woff2');
         }
         @font-face {
         font-weight: 700;
         font-style:  normal;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-Bold.woff2') format('woff2');
         }
         @font-face {
         font-weight: 700;
         font-style:  italic;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-BoldItalic.woff2') format('woff2');
         }
         @font-face {
         font-weight: 900;
         font-style:  normal;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-Black.woff2') format('woff2');
         }
         @font-face {
         font-weight: 900;
         font-style:  italic;
         font-family: 'Inter-Loom';
         src: url('https://cdn.useloom.com/assets/fonts/inter/Inter-UI-BlackItalic.woff2') format('woff2');
         }
      </style>
            <script>
    window.onload = function(){
        window.print();
    }
</script>
   </head>
   <body >
      <div class="whole">
         <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tbody>
               <tr>
                  <td>
                     <table cellpadding="0" cellspacing="0" width="850px">
                        <tbody>
                           <tr>
                              <td align="right" style="padding-right: 15px">&nbsp;
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p align="center" class="MsoNormal" style="text-align: center">
                                    <b style="mso-bidi-font-weight: normal"><u><span lang="EN-US" style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; font-family: &quot; times new roman&quot; ,&quot; serif&quot;"><span id="lblheading">APPLICATION RECEIPT</span></span></u></b>
                                 </p>
                              </td>
                           </tr>
                           <tr>
                              <td>&nbsp;
                              </td>
                           </tr>
                           <tr>
                              <td align="center">
                                 <table cellpadding="0" cellspacing="0" width="850px">
                                    <tbody>
                                       <tr>
                                          <td>
                                             <table cellpadding="0" cellspacing="0" width="100%" align="left" border="1">
                                                <tbody>
                                                   <tr>
                                                      <td align="left" colspan="2">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                               <tr>
                                                                  <td>
                                                                     <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td>
                                                                                 <table cellpadding="5" cellspacing="0" width="100%">
                                                                                    <tbody>
                                                                                       <tr>
                                                                                          <td align="left">Receipt no
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$data->receipt_no }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>
                                                                                       
                                                                                       <tr>
                                                                                          <td align="left">Acknowledgement no
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$data->tracking_no }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>

                                                                                       <tr>
                                                                                          <td align="left">Date
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ ddmmyy(@$data->created_at) }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>

                                                                                       <tr>
                                                                                          <td align="left">User
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$data->user->name }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>

                                                                                       <tr>
                                                                                          <td align="left">Services codes
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$service->code }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td align="left">Services
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$service->name }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td align="left">Name of client
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$name  }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td align="left">Amount received
                                                                                          </td>
                                                                                          <td align="center">:
                                                                                          </td>
                                                                                          <td align="left">
                                                                                             <span id="lblApplicantNm1" style="font-weight:bold;">{{ @$data->amount_received }}</span>
                                                                                          </td>
                                                                                       </tr>
                                                                                       <tr>
                                                                                          <td colspan="7" align="left" style="padding: 0pt; margin: 0pt;" height="1">
                                                                                             <hr width="100%" style="padding: 0px; margin: 0px;">
                                                                                          </td>
                                                                                       </tr>
                                                                                    </tbody>
                                                                                 </table>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td align="left" style="padding: 15px">
                                                         <b>Signature:</b>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </body>
</html>
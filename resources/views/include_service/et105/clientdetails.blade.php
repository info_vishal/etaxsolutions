@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Business Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client *"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>
    
    <div class="row">
        <div class="container">

            <div class="form-group col-md-4">
                  <label for="gst_reg_no">GST registration number *</label>
                  <input type="text" id="gst_reg_no" parsley-trigger="change" name="gst_reg_no" class="form-control" required value="{{ (@$data)?@$data->gst_reg_no:old('gst_reg_no') }}">
                    @if($errors->has('gst_reg_no'))
                        @php 
                            echo showError($errors->first('gst_reg_no'));
                        @endphp
                    @endif
                </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="legal_name_business">Legal name of business *</label>
                  <input type="text" id="legal_name_business" parsley-trigger="change" name="legal_name_business" class="form-control"  required value="{{ (@$data)?@$data->legal_name_business:old('legal_name_business') }}">
                    @if($errors->has('legal_name_business'))
                        @php 
                            echo showError($errors->first('legal_name_business'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="constitution_business">Constitution of business *</label>
                  <input type="text" id="constitution_business" parsley-trigger="change" name="constitution_business" class="form-control" required value="{{ (@$data)?@$data->constitution_business:old('constitution_business') }}">
                    @if($errors->has('constitution_business'))
                        @php 
                            echo showError($errors->first('constitution_business'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="col-md-12"></div>

            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name_of_director">Name of the Proprietor / Director(s) / Partner(s) / Promoter(s) *</label>
                  <input type="text" id="name_of_director" parsley-trigger="change" name="name_of_director" class="form-control" required value="{{ (@$data)?@$data->name_of_director:old('name_of_director') }}">
                    @if($errors->has('name_of_director'))
                        @php 
                            echo showError($errors->first('name_of_director'));
                        @endphp
                    @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="address_of_director">Address of the Proprietor / Director(s) / Partner(s) / Promoter(s)</label>
                  <input type="text" id="address_of_director" name="address_of_director" class="form-control"  value="{{ (@$data)?@$data->address_of_director:old('address_of_director') }}">
                    @if($errors->has('address_of_director'))
                        @php 
                            echo showError($errors->first('address_of_director'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="col-md-12"></div>

            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="nature_of_business">Nature of business activity and brief details *</label>
                  <textarea id="nature_of_business" parsley-trigger="change" name="nature_of_business" class="form-control" required >{{ (@$data)?@$data->nature_of_business:old('nature_of_business') }}</textarea>
                    @if($errors->has('nature_of_business'))
                        @php 
                            echo showError($errors->first('nature_of_business'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="place_of_business">Place of business</label>
                  <input type="text" id="place_of_business" name="place_of_business" class="form-control" value="{{ (@$data)?@$data->place_of_business:old('place_of_business') }}">
                    @if($errors->has('place_of_business'))
                        @php 
                            echo showError($errors->first('place_of_business'));
                        @endphp
                    @endif
                </div>
            </div>
            <div class="col-md-12"></div>
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="other_business">Other business</label>
                  <textarea id="other_business" name="other_business" class="form-control" >{{ (@$data)?@$data->other_business:old('other_business') }}</textarea>
                    @if($errors->has('other_business'))
                        @php 
                            echo showError($errors->first('other_business'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="gst_site_id">GST Site ID</label>
                  <input type="text" id="gst_site_id" name="gst_site_id" class="form-control"  value="{{ (@$data)?@$data->gst_site_id:old('gst_site_id') }}">
                    @if($errors->has('gst_site_id'))
                        @php 
                            echo showError($errors->first('gst_site_id'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="gst_password">GST Password </label>
                  <input type="text" id="gst_password" name="gst_password" class="form-control" value="{{ (@$data)?@$data->gst_password:old('gst_password') }}">
                    @if($errors->has('gst_password'))
                        @php 
                            echo showError($errors->first('gst_password'));
                        @endphp
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>
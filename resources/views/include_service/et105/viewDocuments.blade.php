<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
          
          <div class="col-md-12">
              <div class="form-row">
              <div class="form-group col-md-12">
                <div class="col-md-12 m-t-10">
                  <label for="accounting_swft"><a target="_blank" download href="{{ image_url($data->accounting_swft) }}" title="Download">
                    <i class="fa fa-download"></i></a> Accounting software backup (if any)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-12">
                <div class="col-md-12 m-t-10">
                  <label for="details_of_transaction"><a target="_blank" download href="{{ image_url($data->details_of_transaction) }}" title="Download">
                    <i class="fa fa-download"></i></a> All details of transactions of relevant month (sales,purchase,sales return, purchase return,debit note,credit note)
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12">
              <div class="form-row">
              <div class="form-group col-md-6">
                <div class="col-md-12 m-t-10">
                  <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Any other documents as needed
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
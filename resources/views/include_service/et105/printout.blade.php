<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
         window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="2" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Business Details </center> </strong></td>
               </tr>
               <td colspan="4">

                  <div class="div_3">
                     <strong>GST registration number: </strong> 
                     <br>
                     <font> {{ $data->gst_reg_no }} </font>
                  </div>

                  <div class="div_3">
                     <strong>Legal name of business: </strong> 
                     <br>
                     <font> {{ $data->legal_name_business }} </font>
                  </div>
                  
                  <div class="div_3"> 
                     <strong>Constitution of business: </strong> 
                     <br>
                     <font> {{ $data->constitution_business }} </font>
                  </div>
                  
                  <div class="div_3"> 
                     <strong>Place of business: </strong> 
                     <br>
                     <font> {{ $data->place_of_business }} </font>
                  </div>
                  
                  <div class="div_3"> 
                     <strong>Name of the Proprietor: </strong>
                     <br>
                     <font> {{ $data->name_of_director}} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>Address of the Proprietor: </strong><br> 
                     <font> {{ $data->address_of_director}} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>Nature of business activity and brief details: </strong><br> 
                     <font> {{ $data->nature_of_business}} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>GST Site ID: </strong><br> 
                     <font> {{ $data->other_business}} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>GST Password: </strong><br> 
                     <font> {{ $data->gst_password}} </font>
                  </div>

                  <div style="clear:both;"></div>
                  <div class="div_1"> 
                     <strong>Other business: </strong><br> 
                     <font> {{ $data->other_business}} </font>
                  </div>
                  
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                  <div class="div_2">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Contact Details </center> </strong></td>
               </tr>
               <td colspan="4">
                 
                  <div class="div_2"> <strong>Mobile No: </strong> <font> {{ $data->mobile_number }} </font> </div>
                  <div class="div_2"> <strong>Secondary Mobile No: </strong> <font> {{ $data->sec_mobile_number}} </font></div>

                  <div class="div_2"><strong>Email-ID: </strong> <font> {{ $data->email_id }} </font></div>
                  <div class="div_2"> <strong>Secondary Email-ID: </strong> <font> {{ $data->sec_email_id }} </font> </div>
         
               </td>
            </tr>

         </tfoot>
      </table>
   </body>
</html>
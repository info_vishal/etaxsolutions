<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-12">
                  <label for="accounting_swft">Accounting software backup (if any)</label>
                  <input type="file" class="form-control" name="accounting_swft" />
                    @if($errors->has('accounting_swft'))
                        @php 
                            echo showError($errors->first('accounting_swft'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="details_of_transaction">All details of transactions of relevant month (sales,purchase,sales return, purchase return,debit note,credit note) *</label>
                  <input type="file" class="form-control" name="details_of_transaction" required="" parsley-trigger="change" />
                    @if($errors->has('details_of_transaction'))
                        @php 
                            echo showError($errors->first('details_of_transaction'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
                <div class="col-md-12"></div>
            </div>
        </div>
    </div>
</div>
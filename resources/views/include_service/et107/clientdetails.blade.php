@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Income Tax Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client *"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="individaul_corporate">Individual / Corporate *</label>
                  <select class="form-control" id="individaul_corporate" parsley-trigger="change" required name="individaul_corporate">
                        @foreach(et107ServiceType() as $key => $val)
                            @php
                                $select_stated = (old('individaul_corporate') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->individaul_corporate == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('individaul_corporate'))
                        @php 
                            echo showError($errors->first('individaul_corporate'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="full_name">Full Name (Individual / Corporate)*</label>
                  <input type="text" id="full_name" parsley-trigger="change" name="full_name" class="form-control" required value="{{ (@$data)?@$data->full_name:old('full_name') }}">
                    @if($errors->has('full_name'))
                        @php 
                            echo showError($errors->first('full_name'));
                        @endphp
                    @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="pancard_no">Pancard No *</label>
                  <input type="text" id="pancard_no" parsley-trigger="change" name="pancard_no" class="form-control"  required value="{{ (@$data)?@$data->pancard_no:old('pancard_no') }}">
                    @if($errors->has('pancard_no'))
                        @php 
                            echo showError($errors->first('pancard_no'));
                        @endphp
                    @endif
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <div class="col-sm-12">
                    <label for="datepicker-autoclose">Date of (Birth / Registration / Incorporation) *</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="cli_dob" id="datepicker-autoclose" value="{{ (@$data)?filterDate(@$data->cli_dob):old('cli_dob') }}">
                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                        </div><!-- input-group -->
                        @if($errors->has('cli_dob'))
                        @php 
                            echo showError($errors->first('cli_dob'));
                        @endphp
                    @endif
                    </div>
                </div>

                <div class="form-group col-md-4" id="cli_gender_div">
                  <label for="cli_gender">Gender </label>
                  <select class="form-control select2" id="cli_gender" name="cli_gender">
                        @foreach(customerGender() as $key => $val)
                            @php
                                $select_stated = (old('cli_gender') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->cli_gender == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('cli_gender'))
                        @php 
                            echo showError($errors->first('cli_gender'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="aadhar_allotted">Aadhar no (If Alloted)</label>
                  <input type="text" id="aadhar_allotted"  name="aadhar_allotted" class="form-control" value="{{ (@$data)?@$data->aadhar_allotted:old('aadhar_allotted') }}">
                    @if($errors->has('aadhar_allotted'))
                        @php 
                            echo showError($errors->first('aadhar_allotted'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

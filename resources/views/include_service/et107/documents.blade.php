<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-12">
                  <label for="financial_statement_file">All financial documents (all bank statement, balance sheet,profit & loss statement,all ccount statement of business or individual) *</label>
                  <input type="file" class="form-control" name="financial_statement_file" required="" parsley-trigger="change" />
                    @if($errors->has('financial_statement_file'))
                        @php 
                            echo showError($errors->first('financial_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="kyc_document_file"> KYC documents (pan card, aadhar  card , any other document for corporate) *</label>
                  <input type="file" class="form-control" name="kyc_document_file" required="" parsley-trigger="change" />
                    @if($errors->has('kyc_document_file'))
                        @php 
                            echo showError($errors->first('kyc_document_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="form-group col-md-12">
                
                <div class="col-md-12 m-t-10">
                  <label for="financial_statement_file"><a target="_blank" download href="{{ image_url($data->financial_statement_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> All financial documents (all bank statement, balance sheet,profit & loss statement,all ccount statement of business or individual)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-8">
                
                <div class="col-md-12 m-t-10">
                  <label for="kyc_document_file"><a target="_blank" download href="{{ image_url($data->kyc_document_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> KYC documents (pan card, aadhar  card , any other document for corporate)
                  </label>
                </div>
              </div>


            <div class="form-group col-md-4">
              
              <div class="col-md-12 m-t-10">
                <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download"><i class="fa fa-download"></i></a> Any other documents as needed</label>
              </div>
            </div>

            </div>
        </div>
    </div>
</div>
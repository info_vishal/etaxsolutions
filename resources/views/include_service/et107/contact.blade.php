<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Contact Details') }}</b></h4>
            <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="landline_no">LandLine no</label>
                 <input type="text" id="landline_no" data-mask="(999) 999-9999" parsley-trigger="change" name="landline_no" class="form-control"  value="{{ (@$data)?@$data->landline_no:old('landline_no') }}">
                    @if($errors->has('landline_no'))
                        @php 
                            echo showError($errors->first('landline_no'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="mobile_number">Mobile Number *</label>
                  <input type="text" id="mobile_number" data-mask="(999) 999-9999" required parsley-trigger="change" name="mobile_number" class="form-control"  value="{{ (@$data)?@$data->mobile_number:old('mobile_number') }}">
                    @if($errors->has('mobile_number'))
                        @php 
                            echo showError($errors->first('mobile_number'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="sec_mobile_number"> Secondary Mobile Number</label>
                  <input type="text" id="sec_mobile_number" data-mask="(999) 999-9999" parsley-trigger="change" name="sec_mobile_number" class="form-control"  value="{{ (@$data)?@$data->sec_mobile_number:old('sec_mobile_number') }}">
                    @if($errors->has('sec_mobile_number'))
                        @php 
                            echo showError($errors->first('sec_mobile_number'));
                        @endphp
                    @endif
                </div>
            </div>

<div class="col-md-12"></div>
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="email_id">Email-ID *</label>
                  <input type="email" id="email_id" parsley-trigger="change" required name="email_id" class="form-control"  value="{{ (@$data)?@$data->email_id:old('email_id') }}">
                    @if($errors->has('email_id'))
                        @php 
                            echo showError($errors->first('email_id'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="sec_email_id">Secondary Email-ID </label>
                  <input type="email" id="sec_email_id" parsley-trigger="change" name="sec_email_id" class="form-control"  value="{{ (@$data)?@$data->sec_email_id:old('sec_email_id') }}">
                    @if($errors->has('sec_email_id'))
                        @php 
                            echo showError($errors->first('sec_email_id'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputState">Contact Type</label>
                  <select class="form-control" name="contact_type">
                        @foreach(et102ContactType() as $key => $val)
                            @php
                                $select_stated = (old('contact_type') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->contact_type == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('contact_type'))
                        @php 
                            echo showError($errors->first('contact_type'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
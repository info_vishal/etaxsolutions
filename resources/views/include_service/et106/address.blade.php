<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Address of business') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="res_add_flat">Flat / Room No.  </label>
                  <input type="text" id="res_add_flat"  name="res_add_flat" class="form-control" value="{{ (@$data)?@$data->res_add_flat:old('res_add_flat') }}">
                    @if($errors->has('res_add_flat'))
                        @php 
                            echo showError($errors->first('res_add_flat'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="res_add_building">Society / Building</label>
                  <input type="text" id="res_add_building"  name="res_add_building" class="form-control"  value="{{ (@$data)?@$data->res_add_building:old('res_add_building') }}">
                    @if($errors->has('res_add_building'))
                        @php 
                            echo showError($errors->first('res_add_building'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_street">Street / Road / Post Office</label>
                  <input type="text" id="res_add_street"  name="res_add_street" class="form-control"  value="{{ (@$data)?@$data->res_add_street:old('res_add_street') }}">
                    @if($errors->has('res_add_street'))
                        @php 
                            echo showError($errors->first('res_add_street'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_area">Area / Locality / Taluka</label>
                  <input type="text" id="res_add_area" name="res_add_area" class="form-control"  value="{{ (@$data)?@$data->res_add_area:old('res_add_area') }}">
                    @if($errors->has('res_add_area'))
                        @php 
                            echo showError($errors->first('res_add_area'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="res_add_pincode">Pincode</label>
                  <input type="text" id="res_add_pincode" name="res_add_pincode" class="form-control"   value="{{ (@$data)?@$data->res_add_pincode:old('res_add_pincode') }}">
                    @if($errors->has('res_add_pincode'))
                        @php 
                            echo showError($errors->first('res_add_pincode'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-3">
                  <label for="res_add_country">Country *</label>
                  <input type="text" id="res_add_country" name="res_add_country" class="form-control" required="" parsley-trigger="change" value="{{ (@$data)?@$data->res_add_country:old('res_add_country','India') }}">
                    @if($errors->has('res_add_country'))
                        @php 
                            echo showError($errors->first('res_add_country'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_state">State *</label>
                  <select class="form-control select2 select_state" data-child='district' required="" parsley-trigger="change" name="res_add_state">
                        @foreach($states as $key => $val)
                            @php
                                $select_stated = (old('res_add_state') == $val->id)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->res_add_state == $val->id)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('res_add_state'))
                        @php 
                            echo showError($errors->first('res_add_state'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                    <label class="" for="">Districts *</label>
                    <div class="col-md-10">
                        <select class="form-control select2" required="" parsley-trigger="change" name='res_add_district' id="district" >
                            @php
                                if(@$district && @$data->res_add_district > 0){
                                    foreach(@$district as $key => $val){
                                        $selected = ($val->id == @$data->res_add_district)?"selected":'';
                                        @endphp
                                        <option {{ $selected }} value="{{ $val->id }}">{{ $val->name }}</option>
                                        @php
                                    }
                                }
                            @endphp
                        </select>
                        @if($errors->has('res_add_district'))
                            @php 
                                echo showError($errors->first('res_add_district'));
                            @endphp
                        @endif
                    </div> 
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_city">City</label>
                  <input type="text" id="res_add_city" name="res_add_city" class="form-control"  value="{{ (@$data)?@$data->res_add_city:old('res_add_city') }}">
                    @if($errors->has('res_add_city'))
                        @php 
                            echo showError($errors->first('res_add_city'));
                        @endphp
                    @endif
                </div>

            </div>

        </div>
    </div>
</div>

<script type=""> 
$(document).on("change",".select_state",function(){
        var id = this.dataset.child;
        var state_id = this.value;
        var level = $(".select_levels").find(':selected').data('level');
        $.ajax({
            url: '{{ route("common.districtAjax") }}',
            data: { 'state_id': state_id,'level':level,'_token': '{{ csrf_token() }}' },
            dataType: 'JSON',
            type: 'POST',
            success: function(response){
                $('#'+id).html();
                $('#'+id).removeAttr("disabled");
                $('#'+id).html(response.data);
            }
        });
        $('#'+id).val(null).trigger('change');
        $('#'+id).select2();
    });
</script>
@if(@$bank)
<div class="card-box">
    <div class="col-md-12" data-bank_group="0" >
        <div class="col-md-10">
            <h4 class="m-t-0 header-title"><b>{{ __('All Bank Details') }} </b></h4>
        </div>

        @if( $currentRole == 'admin' || $currentRole == 'sub_admin')
        <div class="col-md-2" align="right">
            <button class="btn btn-primary waves-effect waves-light addBankDetails"><i class="fa fa-plus"></i> Add </button>
        </div> 
        @endif

        <div class="col-md-12">
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="container bank_section">

            @foreach($bank as $key => $val)

                <input type="hidden"  name="clientbank[{{ $key }}][bank_id]" id="bank_id" value="{{ $val->id }}" data-field_name="bank_id">

                <div class="col-md-12 set_0" data-bank_group="{{ $key }}" id="set_{{ $key }}">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="bank_ifsc{{ $key }}">IFSC CODE *</label>
                          <div class="input-group">
                            <span class="input-group-btn">
                            <button type="button" class="btn waves-effect waves-light btn-primary searchBankDetails" id="{{ $key }}"><i class="fa fa-search"></i></button>
                            </span>
                            <input type="text" id="bank_ifsc{{ $key }}" name="clientbank[{{ $key }}][bank_ifsc]" class="form-control" placeholder="Enter IFSC CODE" required value="{{ ($val)?$val->bank_ifsc:old('bank_ifsc') }}" data-field_name="bank_ifsc">
                        </div>
                            @if($errors->has('bank_ifsc'))
                                @php 
                                    echo showError($errors->first('bank_ifsc'));
                                @endphp
                            @endif
                        <ul class="parsley-errors-list ifsc_error{{ $key }} filled hide" id="ifsc_error{{ $key }}"><li class="parsley-required">Not Found</li></ul>
                        </div>

                        <div class="form-group col-md-4">
                          <label for="bank_name{{ $key }}">Bank Name *</label>
                          <input type="text" id="bank_name{{ $key }}" parsley-trigger="change" name="clientbank[{{ $key }}][bank_name]" class="form-control" required value="{{ ($val)?$val->bank_name:old('bank_name') }}" data-field_name="bank_name">
                            @if($errors->has('bank_name'))
                                @php 
                                    echo showError($errors->first('bank_name'));
                                @endphp
                            @endif
                        </div>

                        <div class="form-group col-md-4">
                          <label for="bank_branch{{ $key }}">Branch *</label>
                          <input type="text" id="bank_branch{{ $key }}" parsley-trigger="change" name="clientbank[{{ $key }}][bank_branch]" class="form-control" required value="{{ ($val)?$val->bank_branch:old('bank_branch') }}" data-field_name="bank_branch">
                            @if($errors->has('bank_branch'))
                                @php 
                                    echo showError($errors->first('bank_branch'));
                                @endphp
                            @endif
                        </div>

                        <div class="col-md-12"></div>
                        <div class="form-group col-md-4">
                          <label for="bank_account_type{{ $key }}">Account type *</label>
                          <select class="form-control" name="clientbank[{{ $key }}][bank_account_type]" parsley-trigger="change"  required="" data-field_name="bank_account_type" >
                                @foreach(bankAccountType() as $ackey => $acval)
                                    @php
                                        $select_stated = (old('bank_account_type') == $ackey)?'selected':'';
                                        if($select_stated == ""){
                                            $select_stated = ($val->bank_account_type == $ackey)?'selected':'';
                                        }
                                    @endphp
                                    <option {{ $select_stated }} value='{{ $ackey }}'>{{ $acval }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('bank_account_type'))
                                @php 
                                    echo showError($errors->first('bank_account_type'));
                                @endphp
                            @endif
                        </div>

                        <div class="form-group col-md-4">
                          <label for="bank_account_number{{ $key }}">Account Number *</label>
                          <input type="text" id="bank_account_number{{ $key }}" parsley-trigger="change" name="clientbank[{{ $key }}][bank_account_number]" class="form-control" min="{{ $key }}" required value="{{ ($val)?$val->bank_account_number:old('bank_account_number') }}" data-field_name="bank_account_number">
                            @if($errors->has('bank_account_number'))
                                @php 
                                    echo showError($errors->first('bank_account_number'));
                                @endphp
                            @endif
                        </div>

                        <div class="form-group col-md-4">
                          <label for="bank_holder_name{{ $key }}">Account Holder Name *</label>
                          <input type="text" id="bank_holder_name{{ $key }}" parsley-trigger="change" name="clientbank[{{ $key }}][bank_holder_name]" class="form-control"  required placeholder="Name-1, Name-2" value="{{ ($val)?$val->bank_holder_name:old('bank_holder_name') }}" data-field_name="bank_holder_name">
                            @if($errors->has('bank_holder_name'))
                                @php 
                                    echo showError($errors->first('bank_holder_name'));
                                @endphp
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 removeBankDiv">
                     <hr> 
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</div>
@else
<div class="card-box">
    <div class="col-md-12" data-bank_group="0" >
        <div class="col-md-10">
            <h4 class="m-t-0 header-title"><b>{{ __('All Bank Details') }}</b></h4>
        </div>
        <div class="col-md-2" align="right">
            <button class="btn btn-primary waves-effect waves-light addBankDetails"><i class="fa fa-plus"></i> Add </button>
        </div>
        <div class="col-md-12">
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="container bank_section">
            <div class="col-md-12 set_0" data-bank_group="0" id="set_0">
                <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="bank_ifsc0">IFSC CODE *</label>
                      <div class="input-group">
                        <span class="input-group-btn">
                        <button type="button" class="btn waves-effect waves-light btn-primary searchBankDetails" id="0"><i class="fa fa-search"></i></button>
                        </span>
                        <input type="text" id="bank_ifsc0" name="clientbank[0][bank_ifsc]" class="form-control" placeholder="Enter IFSC CODE" required value="{{ (@$data)?@$data->bank_ifsc:old('bank_ifsc') }}" data-field_name="bank_ifsc">
                    </div>
                        @if($errors->has('bank_ifsc'))
                            @php 
                                echo showError($errors->first('bank_ifsc'));
                            @endphp
                        @endif
                    <ul class="parsley-errors-list ifsc_error0 filled hide" id="ifsc_error0"><li class="parsley-required">Not Found</li></ul>
                    </div>

                    <div class="form-group col-md-4">
                      <label for="bank_name0">Bank Name *</label>
                      <input type="text" id="bank_name0" parsley-trigger="change" name="clientbank[0][bank_name]" class="form-control" required value="{{ (@$data)?@$data->bank_name:old('bank_name') }}" data-field_name="bank_name">
                        @if($errors->has('bank_name'))
                            @php 
                                echo showError($errors->first('bank_name'));
                            @endphp
                        @endif
                    </div>

                    <div class="form-group col-md-4">
                      <label for="bank_branch0">Branch *</label>
                      <input type="text" id="bank_branch0" parsley-trigger="change" name="clientbank[0][bank_branch]" class="form-control" required value="{{ (@$data)?@$data->bank_branch:old('bank_branch') }}" data-field_name="bank_branch">
                        @if($errors->has('bank_branch'))
                            @php 
                                echo showError($errors->first('bank_branch'));
                            @endphp
                        @endif
                    </div>

                    <div class="col-md-12"></div>
                    <div class="form-group col-md-4">
                      <label for="bank_account_type0">Account type *</label>
                      <select class="form-control" name="clientbank[0][bank_account_type]" parsley-trigger="change"  required="" data-field_name="bank_account_type" >
                            @foreach(bankAccountType() as $key => $val)
                                @php
                                    $select_stated = (old('bank_account_type') == $key)?'selected':'';
                                    if($select_stated == ""){
                                        $select_stated = (@$data->bank_account_type == $key)?'selected':'';
                                    }
                                @endphp
                                <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('bank_account_type'))
                            @php 
                                echo showError($errors->first('bank_account_type'));
                            @endphp
                        @endif
                    </div>

                    <div class="form-group col-md-4">
                      <label for="bank_account_number0">Account Number *</label>
                      <input type="text" id="bank_account_number0" parsley-trigger="change" name="clientbank[0][bank_account_number]" class="form-control" min="0" required value="{{ (@$data)?@$data->bank_account_number:old('bank_account_number') }}" data-field_name="bank_account_number">
                        @if($errors->has('bank_account_number'))
                            @php 
                                echo showError($errors->first('bank_account_number'));
                            @endphp
                        @endif
                    </div>

                    <div class="form-group col-md-4">
                      <label for="bank_holder_name0">Account Holder Name *</label>
                      <input type="text" id="bank_holder_name0" parsley-trigger="change" name="clientbank[0][bank_holder_name]" class="form-control"  required placeholder="Name-1, Name-2" value="{{ (@$data)?@$data->bank_holder_name:old('bank_holder_name') }}" data-field_name="bank_holder_name">
                        @if($errors->has('bank_holder_name'))
                            @php 
                                echo showError($errors->first('bank_holder_name'));
                            @endphp
                        @endif
                    </div>
                </div>
                <div class="col-md-12 removeBankDiv">
                 <hr>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<script>
var template = $('.bank_section .set_0:first').clone();
@if(@$bank)
var sectionsCount = "{{ count($bank) }}";
@else
var sectionsCount = 1;
@endif
var bank = {
    init: function(){
       this.bankSearch();
       this.add();
       this.remove();
    },
    add: function(){
        $('.addBankDetails').on('click',function(){
            var section = template.clone().find(':input').each(function(){
                var newId = this.id + sectionsCount;

                var field_name = this.dataset.field_name;
                var name = "clientbank["+sectionsCount+"]["+field_name+"]";

                //update for label
                $(this).prev().attr('for', newId);

                //update id
                this.name = name;
                this.value = "";
                if(this.id == "bank_id"){
                    this.value = "";
                }else{
                    this.id = newId;
                }

            }).end()
            //inject new section
            .appendTo('.bank_section');
            $(section[0]).attr("id","set_0"+sectionsCount);
            $("#set_0"+sectionsCount+" .ifsc_error0").attr("id","ifsc_error0"+sectionsCount);
            $("#set_0"+sectionsCount+" .removeBankDiv").html('<div align="right"> <a href="#" class="removeBank">Remove</a></div><hr>');
            sectionsCount++;
            return false;
        });
    },
    remove : function(){
        $('.bank_section').on('click', '.removeBank', function() {
            //fade out section
            console.log($(this).parent().parent().parent());
            //remove parent element (main section)
            $(this).parent().parent().parent().empty();
            return false;
            return false;
        });
    },
    bankSearch: function(){
        $(document).on('click','.searchBankDetails',function(){
            var id = this.id;
            console.log(id);
            var ifsc = $("#bank_ifsc"+id).val();
            $.ajax({
                url: 'https://ifsc.razorpay.com/'+ifsc,
                dataType: 'JSON',
                type: 'GET',
                success: function(response,textStatus, request){
                    console.log(response);
                    $("#set_"+id+" #ifsc_error"+id).addClass("hide");
                    $("#bank_name"+id).val(response.BANK);
                    $("#bank_branch"+id).val(response.BRANCH);
                },
                error: function(response,textStatus, request){
                    $("#set_"+id+" #ifsc_error"+id).removeClass("hide");
                    $("#set_"+id+" #ifsc_error"+id+" li").text('Not Found');
                    $("#bank_branch"+id+", #bank_name"+id).val('');
                }
            });
        });
    },
};
$(document).ready(function(){
    bank.init();
});
</script>
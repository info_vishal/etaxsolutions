<html>
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
         window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="1" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Registration Details </center> </strong></td>
               </tr>
               <td colspan="4">


                  <div class="div_2 d_m_b"> 
                     <strong>Proposed name of firm: </strong>
                     <br>
                     <font> {{ $data->firm_name}} </font>
                  </div>

                  <div class="div_2 d_m_b"> 
                     <strong>Pancard number of firm: </strong>
                     <br>
                     <font> {{ $data->pancard}} </font>
                  </div>

                  <div class="div_1"> 
                     <strong>Nature of activity - business details: </strong> 
                     <br>
                     <font> {{ $data->business_details }} </font>
                  </div>
               
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                  <div class="div_2">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Partners Details</center> </strong></td>
               </tr>
               <td colspan="4">

                  @foreach($childData as $c_key => $c_val)
                     <div class="div_3">
                        <strong>Name: </strong> <br> <font> {{ $c_val->partner_name }} </font>
                     </div>

                     <div class="div_3">
                        <strong>Mobile No: </strong> <br> <font> {{ $c_val->mobile_number }} </font>
                     </div>

                     <div class="div_3">
                        <strong>Secondary mobile No: </strong> <br> <font> {{ $c_val->sec_mobile_number }} </font>
                     </div>
                     <div class="div_3">
                        <strong>Email-Id : </strong> <br> <font> {{ $c_val->email_id }} </font>
                     </div> 
                     <div class="div_3">
                        <strong>Secondary Email-Id : </strong> <br> <font> {{ $c_val->sec_email_id }} </font>
                     </div> 
                     <div style="clear: both; margin-bottom: 20px; border-bottom: 1px dotted; text-align: center;">
                        <b>Address</b> 
                     </div>
                     @php
                        $state = \Auth::user()->getStates($c_val->res_add_state);
                        $district = \Auth::user()->getdistricts($c_val->res_add_district,1);
                     @endphp
                     <div class="div_2">
                        <strong>Flat / Room No: </strong> <font> {{ $c_val->res_add_building }} </font><br>
                        <strong>Street / Road / Post Office: </strong> <font> {{ $c_val->res_add_street }} </font><br>
                        <strong>Area / Locality / Taluka: </strong> <font> {{ $c_val->res_add_area }} </font><br>
                        <strong>Pincode: </strong> <font> {{ $c_val->res_add_pincode }} </font>
                     </div>
                     <div class="div_2">
                        <strong>City: </strong> <font> {{ $c_val->res_add_city }} </font><br>
                        <strong>District: </strong> <font> {{ @$district->name }} </font><br>
                        <strong>State: </strong> <font> {{ @$state->name }} </font><br>
                        <strong>Country: </strong> <font> {{ $c_val->res_add_country }} </font>
                     </div>         

                     <div style="clear: both; margin: 20px 0px; border-bottom:double; ">
                     </div>      
                  @endforeach
               </td>
            </tr>
         </tfoot>
      </table>
   </body>
</html>
@if(\Request()->id == "")
<div class="col-md-12">
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-3">
                  <label for="idproof_file0"> Id proof *</label>
                  <input type="file" class="form-control" id="idproof_file0" name="director[0][idproof_file]" data-unique_id="0" data-field_name="idproof_file" required="" parsley-trigger="change" />
                    @if($errors->has('idproof_file'))
                        @php 
                            echo showError($errors->first('idproof_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="address_proof_file0"> Address proof *</label>
                  <input type="file" class="form-control" id="address_proof_file0" name="director[0][address_proof_file]" data-unique_id="0" data-field_name="address_proof_file" required="" parsley-trigger="change" />
                    @if($errors->has('address_proof_file'))
                        @php 
                            echo showError($errors->first('address_proof_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="photo_file0">Photo of Partner *</label>
                  <input type="file" class="form-control" id="photo_file0" name="director[0][photo_file]" data-unique_id="0" data-field_name="photo_file" required="" parsley-trigger="change" />
                    @if($errors->has('photo_file'))
                        @php 
                            echo showError($errors->first('photo_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="notarized_file0">Notarized photos *</label>
                  <input type="file" class="form-control" id="notarized_file0" name="director[0][notarized_file]" data-unique_id="0" data-field_name="notarized_file" required="" parsley-trigger="change"/>
                    @if($errors->has('notarized_file'))
                        @php 
                            echo showError($errors->first('notarized_file'));
                        @endphp
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
@else
<div class="col-md-12">
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <div class="col-md-12 m-t-10">
                      <label for="idproof_file"><a target="_blank" download href="{{ image_url($d_val->idproof_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Id proof
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <div class="col-md-12 m-t-10">
                      <label for="address_proof_file"><a target="_blank" download href="{{ image_url($d_val->address_proof_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Address proof
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <div class="col-md-12 m-t-10">
                      <label for="photo_file"><a target="_blank" download href="{{ image_url($d_val->photo_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Photo of Partner
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <div class="col-md-12 m-t-10">
                      <label for="notarized_file"><a target="_blank" download href="{{ image_url($d_val->notarized_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Notarized photos
                      </label>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endif
<div class="col-md-12"></div>
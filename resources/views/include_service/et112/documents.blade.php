@if(\Request()->id == "")
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="col-md-12">
                
                <div class="form-group col-md-2">
                  <label for="trust_deed_file">Trust Deed *</label>
                  <input type="file" class="form-control" name="trust_deed_file" required="" parsley-trigger="change" />
                    @if($errors->has('trust_deed_file'))
                        @php 
                            echo showError($errors->first('trust_deed_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="moa_file">MOA *</label>
                  <input type="file" class="form-control" name="moa_file" required="" parsley-trigger="change" />
                    @if($errors->has('moa_file'))
                        @php 
                            echo showError($errors->first('moa_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="aoa_file">AOA *</label>
                  <input type="file" class="form-control" name="aoa_file" required="" parsley-trigger="change" />
                    @if($errors->has('aoa_file'))
                        @php 
                            echo showError($errors->first('aoa_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="assets_file">Trust asset documents</label>
                  <input type="file" class="form-control" name="assets_file" />
                    @if($errors->has('assets_file'))
                        @php 
                            echo showError($errors->first('assets_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="address_proof_file">Trust address proof *</label>
                  <input type="file" class="form-control" required="" parsley-trigger="change" name="address_proof_file" />
                    @if($errors->has('address_proof_file'))
                        @php 
                            echo showError($errors->first('address_proof_file'));
                        @endphp
                    @endif
                </div>

              </div>

              <div class="col-md-12">
                
                <div class="form-group col-md-12">
                  <label for="financial_statement_file">Financial statement of trust(income&expenditure account, balance sheet,any other if available)</label>
                  <input type="file" class="form-control" name="financial_statement_file" />
                    @if($errors->has('financial_statement_file'))
                        @php 
                            echo showError($errors->first('financial_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_statement_file">All bank statement (with front page) *</label>
                  <input type="file" class="form-control" name="bank_statement_file" required="" parsley-trigger="change" />
                    @if($errors->has('bank_statement_file'))
                        @php 
                            echo showError($errors->first('bank_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-5">
                  <label for="bills_voucher_file">All bills and vouchers supporting financial statements</label>
                  <input type="file" class="form-control" name="bills_voucher_file"/>
                    @if($errors->has('bills_voucher_file'))
                        @php 
                            echo showError($errors->first('bills_voucher_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="pancard_file">Pan card of trust or school *</label>
                  <input type="file" class="form-control" name="pancard_file" required="" parsley-trigger="change"/>
                    @if($errors->has('pancard_file'))
                        @php 
                            echo showError($errors->first('pancard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="letter_file">Authority letter from president *</label>
                  <input type="file" class="form-control" required="" parsley-trigger="change" name="letter_file" />
                    @if($errors->has('letter_file'))
                        @php 
                            echo showError($errors->first('letter_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>

              </div>
            
            </div>
        </div>
    </div>
</div>
@else
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="col-md-12">
                
                <div class="form-group col-md-2">
                  <label for="trust_deed_file"><a target="_blank" download href="{{ image_url($data->trust_deed_file) }}" title="Download"> <i class="fa fa-download"></i></a> Trust Deed</label>
                </div>

                <div class="form-group col-md-2">
                  <label for="moa_file"><a target="_blank" download href="{{ image_url($data->moa_file) }}" title="Download"> <i class="fa fa-download"></i></a> MOA </label>
                </div>

                <div class="form-group col-md-2">
                  <label for="aoa_file"><a target="_blank" download href="{{ image_url($data->aoa_file) }}" title="Download"> <i class="fa fa-download"></i></a> AOA </label>
                </div>

                <div class="form-group col-md-3">
                  <label for="assets_file"><a target="_blank" download href="{{ image_url($data->assets_file) }}" title="Download"> <i class="fa fa-download"></i></a> Trust asset documents</label>
                </div>

                <div class="form-group col-md-3">
                  <label for="address_proof_file"><a target="_blank" download href="{{ image_url($data->address_proof_file) }}" title="Download"> <i class="fa fa-download"></i></a> Trust address proof </label>
                </div>

              </div>

              <div class="col-md-12">
                
                <div class="form-group col-md-12">
                  <label for="financial_statement_file"><a target="_blank" download href="{{ image_url($data->financial_statement_file) }}" title="Download"> <i class="fa fa-download"></i></a> Financial statement of trust(income&expenditure account, balance sheet,any other if available)</label>
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_statement_file"><a target="_blank" download href="{{ image_url($data->bank_statement_file) }}" title="Download"> <i class="fa fa-download"></i></a> All bank statement (with front page) </label>
                </div>

                <div class="form-group col-md-5">
                  <label for="bills_voucher_file"><a target="_blank" download href="{{ image_url($data->bills_voucher_file) }}" title="Download"> <i class="fa fa-download"></i></a> All bills and vouchers supporting financial statements</label>
                </div>

                <div class="form-group col-md-3">
                  <label for="pancard_file"><a target="_blank" download href="{{ image_url($data->pancard_file) }}" title="Download"> <i class="fa fa-download"></i></a> Pan card of trust or school </label>
                </div>

                <div class="form-group col-md-4">
                  <label for="letter_file"><a target="_blank" download href="{{ image_url($data->letter_file) }}" title="Download"> <i class="fa fa-download"></i></a> Authority letter from president </label>
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download"> <i class="fa fa-download"></i></a> Any other documents as needed</label>
                </div>

              </div>
            
            </div>
        </div>
    </div>
</div>
@endif
<html>
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
         window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="1" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Audit Details </center> </strong></td>
               </tr>
               <td colspan="4">


                  <div class="div_3"> 
                     <strong>Name of Trust: </strong>
                     <br>
                     <font> {{ $data->name_trust}} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>Name of school under trust: </strong> 
                     <br>
                     <font> {{ $data->name_school }} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>Financial year selection for audit being carried out: </strong> 
                     <br>
                     <font> {{ $data->financial_year }} </font>
                  </div>
                  <div style="clear: both;"></div>
                  
                  <div class="div_3"> 
                     <strong>Mobile no of president or managing trustee: </strong> 
                     <br>
                     <font> {{ $data->mobile_number }} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>Email-Id of president or managing trustee: </strong> 
                     <br>
                     <font> {{ $data->email_id }} </font>
                  </div>
                  <div style="clear: both;"></div>
                  <div class="div_1"> 
                     <strong>Purpose of trust, details about work: </strong> 
                     <br>
                     <font> {{ $data->trust_narration }} </font>
                  </div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                  <div class="div_2">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
               </td>
            </tr>
         </tfoot>
      </table>
   </body>
</html>
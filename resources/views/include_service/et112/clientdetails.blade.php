@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Audit Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>
    
    <div class="row">
        <div class="container">
            <div class="col-md-12">
            
                <div class="form-group col-md-3">
                  <label for="name_trust">Name of Trust *</label>
                  <input type="text" id="name_trust" parsley-trigger="change" name="name_trust" class="form-control"  required value="{{ (@$data)?@$data->name_trust:old('name_trust') }}">
                    @if($errors->has('name_trust'))
                        @php 
                            echo showError($errors->first('name_trust'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="name_school">Name of school under trust *</label>
                  <input type="text" id="name_school" parsley-trigger="change" name="name_school" class="form-control"  required value="{{ (@$data)?@$data->name_school:old('name_school') }}">
                    @if($errors->has('name_school'))
                        @php 
                            echo showError($errors->first('name_school'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="financial_year">Financial year selection for audit being carried out *</label>
                  <input type="text" id="financial_year" min="0" parsley-trigger="change" name="financial_year" class="form-control"  required value="{{ (@$data)?@$data->financial_year:old('financial_year') }}">
                    @if($errors->has('financial_year'))
                        @php 
                            echo showError($errors->first('financial_year'));
                        @endphp
                    @endif
                </div>

            </div>
            <div class="col-md-12">
            
                <div class="form-group col-md-4">
                  <label for="mobile_number">Mobile no of president or managing trustee *</label>
                  <input type="text" id="mobile_number" parsley-trigger="change" name="mobile_number" class="form-control"  required value="{{ (@$data)?@$data->mobile_number:old('mobile_number') }}">
                    @if($errors->has('mobile_number'))
                        @php 
                            echo showError($errors->first('mobile_number'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="email_id">Email-Id of president or managing trustee *</label>
                  <input type="email" id="email_id" parsley-trigger="change" name="email_id" class="form-control"  required value="{{ (@$data)?@$data->email_id:old('email_id') }}">
                    @if($errors->has('email_id'))
                        @php 
                            echo showError($errors->first('email_id'));
                        @endphp
                    @endif
                </div>

            </div>

            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="form-group">
                    <label  for="trust_narration" >Purpose of trust, details about work</label>
                        <textarea  id="trust_narration" name="trust_narration" class="form-control">{{ (@$data)?@$data->trust_narration:old('trust_narration') }}</textarea>
                        @if($errors->has('trust_narration'))
                            @php 
                                echo showError($errors->first('trust_narration'));
                            @endphp
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(\Request()->id == "")
<div class="col-md-12">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="trust_rule_file">Trust rules and regulations and trust purpose details *</label>
                  <input type="file" class="form-control" name="trust_rule_file" required="" parsley-trigger="change" />
                    @if($errors->has('trust_rule_file'))
                        @php 
                            echo showError($errors->first('trust_rule_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="assets_file">Trust asset documents</label>
                  <input type="file" class="form-control" name="assets_file" />
                    @if($errors->has('assets_file'))
                        @php 
                            echo showError($errors->first('assets_file'));
                        @endphp
                    @endif
                </div>

                <div class="col-md-12"></div>

                <div class="form-group col-md-4">
                  <label for="address_proof">Trust address proof</label>
                  <input type="file" class="form-control" name="address_proof" />
                    @if($errors->has('address_proof'))
                        @php 
                            echo showError($errors->first('address_proof'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="thrav_copy"> Tharav copy</label>
                  <input type="file" class="form-control" name="thrav_copy" />
                    @if($errors->has('thrav_copy'))
                        @php 
                            echo showError($errors->first('thrav_copy'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="col-md-12">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="trust_rule_file"><a target="_blank" download href="{{ image_url($data->trust_rule_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Trust rules and regulations and trust purpose details
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="assets_file"><a target="_blank" download href="{{ image_url($data->assets_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Trust asset documents
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="address_proof"><a target="_blank" download href="{{ image_url($data->address_proof) }}" title="Download">
                        <i class="fa fa-download"></i></a> Trust address proof
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="thrav_copy"><a target="_blank" download href="{{ image_url($data->thrav_copy) }}" title="Download">
                        <i class="fa fa-download"></i></a> Tharav copy
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <div class="col-md-12 m-t-10">
                      <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Any other documents as needed
                      </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="col-md-12"></div>
<div class="col-md-12">
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="res_add_flat0">Flat / Room No.  </label>
                  <input type="text" id="res_add_flat0"  name="director[0][res_add_flat]" data-unique_id="0" data-field_name="res_add_flat" class="form-control" value="{{ (@$d_val)?@$d_val->res_add_flat:old('res_add_flat') }}">
                    @if($errors->has('res_add_flat'))
                        @php 
                            echo showError($errors->first('res_add_flat'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="res_add_building0">Society / Building</label>
                  <input type="text" id="res_add_building0"  name="director[0][res_add_building]" data-unique_id="0" data-field_name="res_add_building" class="form-control"  value="{{ (@$d_val)?@$d_val->res_add_building:old('res_add_building') }}">
                    @if($errors->has('res_add_building'))
                        @php 
                            echo showError($errors->first('res_add_building'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_street0">Street / Road / Post Office</label>
                  <input type="text" id="res_add_street0"  name="director[0][res_add_street]" data-unique_id="0" data-field_name="res_add_street" class="form-control"  value="{{ (@$d_val)?@$d_val->res_add_street:old('res_add_street') }}">
                    @if($errors->has('res_add_street'))
                        @php 
                            echo showError($errors->first('res_add_street'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_area0">Area / Locality / Taluka</label>
                  <input type="text" id="res_add_area0" name="director[0][res_add_area]" data-unique_id="0" data-field_name="res_add_area" class="form-control"  value="{{ (@$d_val)?@$d_val->res_add_area:old('res_add_area') }}">
                    @if($errors->has('res_add_area'))
                        @php 
                            echo showError($errors->first('res_add_area'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="res_add_pincode0">Pincode</label>
                  <input type="text" id="res_add_pincode0" name="director[0][res_add_pincode]" data-unique_id="0" data-field_name="res_add_pincode" class="form-control"   value="{{ (@$d_val)?@$d_val->res_add_pincode:old('res_add_pincode') }}">
                    @if($errors->has('res_add_pincode'))
                        @php 
                            echo showError($errors->first('res_add_pincode'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-3">
                  <label for="res_add_country0">Country *</label>
                  <input type="text" id="res_add_country0" name="director[0][res_add_country]" data-unique_id="0" data-field_name="res_add_country" class="form-control"  required="" parsley-trigger="change" value="{{ (@$d_val)?@$d_val->res_add_country:old('res_add_country','India') }}">
                    @if($errors->has('res_add_country'))
                        @php 
                            echo showError($errors->first('res_add_country'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_state0">State *</label>
                  <select class="form-control select2 select_state" id="res_add_state0" data-child='res_add_district' name="director[0][res_add_state]" data-unique_id="" data-field_name="res_add_state" required="" parsley-trigger="change">
                        @foreach($states as $key => $val)
                            @php
                                $select_stated = (old('res_add_state') == $val->id)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$d_val->res_add_state == $val->id)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('res_add_state'))
                        @php 
                            echo showError($errors->first('res_add_state'));
                        @endphp
                    @endif
                </div>

                @if(@$d_val->res_add_district)
                    @php 
                        $district = \Auth::user()->getdistricts($d_val->res_add_district,1);
                    @endphp
                    <div class="form-group col-md-3">
                      <label for="res_add_district0">District *</label>
                      <input type="text" id="res_add_district0" name="director[0][res_add_district]" data-unique_id="0" data-field_name="res_add_district" class="form-control"  value="{{ (@$district->name)?@$district->name:old('res_add_district') }}" required="" parsley-trigger="change">
                        @if($errors->has('res_add_district'))
                            @php 
                                echo showError($errors->first('res_add_district'));
                            @endphp
                        @endif
                    </div>
                @else
                <div class="form-group col-md-3">
                    <label class="" for="res_add_district0">Districts *</label>
                    <div class="col-md-10">
                        <select class="form-control select2" name="director[0][res_add_district]" required="" parsley-trigger="change" data-unique_id="0" data-field_name="res_add_district" id="res_add_district" >
                            @php
                            if(@$district && @$d_val->res_add_district > 0){
                                foreach($district as $key => $val){
                                    $selected = ($val->id == @$d_val->res_add_district)?"selected":'';
                                    @endphp
                                    <option {{ $selected }} value="{{ $val->id }}">{{ $val->name }}</option>
                                    @php
                                }
                            }
                            @endphp
                        </select>
                        @if($errors->has('res_add_district'))
                            @php 
                                echo showError($errors->first('res_add_district'));
                            @endphp
                        @endif
                    </div> 
                </div>
                @endif

                <div class="form-group col-md-3">
                  <label for="res_add_city0">City</label>
                  <input type="text" id="res_add_city0" name="director[0][res_add_city]" data-unique_id="0" data-field_name="res_add_city" class="form-control"  value="{{ (@$d_val)?@$d_val->res_add_city:old('res_add_city') }}">
                    @if($errors->has('res_add_city'))
                        @php 
                            echo showError($errors->first('res_add_city'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if(\Request()->id == "")
<div class="col-md-12">
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-4">
                  <label for="idproof_file0"> Id proof *</label>
                  <input type="file" class="form-control" id="idproof_file0" name="director[0][idproof_file]" data-unique_id="0" data-field_name="idproof_file" required="" parsley-trigger="change" />
                    @if($errors->has('idproof_file'))
                        @php 
                            echo showError($errors->first('idproof_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="address_proof_file0"> Address proof *</label>
                  <input type="file" class="form-control" id="address_proof_file0" name="director[0][address_proof_file]" data-unique_id="0" data-field_name="address_proof_file" required="" parsley-trigger="change" />
                    @if($errors->has('address_proof_file'))
                        @php 
                            echo showError($errors->first('address_proof_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="trustee_affidavit_file0">Trustee Affidavit *</label>
                  <input type="file" class="form-control" id="trustee_affidavit_file0" name="director[0][trustee_affidavit_file]" data-unique_id="0" data-field_name="trustee_affidavit_file" required="" parsley-trigger="change" />
                    @if($errors->has('trustee_affidavit_file'))
                        @php 
                            echo showError($errors->first('trustee_affidavit_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="trustee_letter_file0">Trustee Convince Letter *</label>
                  <input type="file" class="form-control" id="trustee_letter_file0" name="director[0][trustee_letter_file]" data-unique_id="0" data-field_name="trustee_letter_file" required="" parsley-trigger="change" />
                    @if($errors->has('trustee_letter_file'))
                        @php 
                            echo showError($errors->first('trustee_letter_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file0"> Any other documents as needed </label>
                  <input type="file" class="form-control" id="any_other_file0" name="director[0][any_other_file]" data-unique_id="0" data-field_name="any_other_file" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
@else
<div class="col-md-12">
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <div class="col-md-12 m-t-10">
                      <label for="idproof_file"><a target="_blank" download href="{{ image_url($d_val->idproof_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Id proof
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="col-md-12 m-t-10">
                      <label for="address_proof_file"><a target="_blank" download href="{{ image_url($d_val->address_proof_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Address proof
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="col-md-12 m-t-10">
                      <label for="trustee_affidavit_file"><a target="_blank" download href="{{ image_url($d_val->trustee_affidavit_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Trustee Affidavit
                      </label>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <div class="col-md-12 m-t-10">
                      <label for="trustee_letter_file"><a target="_blank" download href="{{ image_url($d_val->trustee_letter_file) }}" title="Download">
                        <i class="fa fa-download"></i></a> Trustee Convince Letter
                      </label>
                    </div>
                </div>
                
                <div class="form-group col-md-4">
                    <div class="col-md-12 m-t-10">
                      <label for="any_other_file"><a target="_blank" download href="{{ image_url($d_val->any_other_file) }}" title="Download">
                        <i class="fa fa-download"></i></a>  Any other documents as needed
                      </label>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endif
<div class="col-md-12"></div>
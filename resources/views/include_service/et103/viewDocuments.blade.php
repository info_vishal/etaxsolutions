<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="form-group col-md-4">

                <div class="col-md-12 m-t-10">
                  <label for="pancard_file"><a target="_blank" download href="{{ image_url($data->pancard_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Pan card of firms or company
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4">

                <div class="col-md-12 m-t-10">
                  <label for="registration_file"><a target="_blank" download href="{{ image_url($data->registration_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Registration certificate (if Company)
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4">
              <div class="col-md-12 m-t-10">
                <label for="moa_file"><a target="_blank" download href="{{ image_url($data->moa_file) }}" title="Download"><i class="fa fa-download"></i></a> MOA (if company)</label>
              </div>
            </div> 

            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="form-row">

              <div class="form-group col-md-4">

                <div class="col-md-12 m-t-10">
                  <label for="audit_report_file"><a target="_blank" download href="{{ image_url($data->audit_report_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> Audit report of firms or company
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4">

                <div class="col-md-12 m-t-10">
                  <label for="bank_statement_file"><a target="_blank" download href="{{ image_url($data->bank_statement_file) }}" title="Download">
                    <i class="fa fa-download"></i></a> All Banks Statements with front page  
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4">
              <div class="col-md-12 m-t-10">
                <label for="financial_statement_file"><a target="_blank" download href="{{ image_url($data->financial_statement_file) }}" title="Download"><i class="fa fa-download"></i></a> Financial Statements of Firms & Company</label>
              </div>
            </div> 

            <div class="form-group col-md-4">
              <div class="col-md-12 m-t-10">
                <label for="partnership_deed"><a target="_blank" download href="{{ image_url($data->partnership_deed) }}" title="Download"><i class="fa fa-download"></i></a> Partnership Deed</label>
              </div>
            </div> 

            <div class="form-group col-md-4">
              <div class="col-md-12 m-t-10">
                <label for="any_other_file"><a target="_blank" download href="{{ image_url($data->any_other_file) }}" title="Download"><i class="fa fa-download"></i></a> Any other documents as needed</label>
              </div>
            </div>

            </div>
        </div>
    </div>
</div>
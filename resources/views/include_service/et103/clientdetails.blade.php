@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Company Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>
    
    <div class="row">
        <div class="container">

            <div class="form-group col-md-2">
                  <label for="firm_or_company">Firm or Company *</label>
                  <select class="form-control" parsley-trigger="change" required name="firm_or_company">
                        @foreach(et103FirmOrCom() as $key => $val)
                            @php
                                $select_stated = (old('firm_or_company') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->firm_or_company == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('firm_or_company'))
                        @php 
                            echo showError($errors->first('firm_or_company'));
                        @endphp
                    @endif
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                  <label for="pancard_no">Pancard No. (Firm or Company) *</label>
                  <input type="text" id="pancard_no" parsley-trigger="change" name="pancard_no" class="form-control"  required value="{{ (@$data)?@$data->pancard_no:old('pancard_no') }}">
                    @if($errors->has('pancard_no'))
                        @php 
                            echo showError($errors->first('pancard_no'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="name_of_firm">Name of firm or company *</label>
                  <input type="text" id="name_of_firm" parsley-trigger="change" name="name_of_firm" class="form-control" required value="{{ (@$data)?@$data->name_of_firm:old('name_of_firm') }}">
                    @if($errors->has('name_of_firm'))
                        @php 
                            echo showError($errors->first('name_of_firm'));
                        @endphp
                    @endif
                </div>
                
                <div class="form-group col-md-4">
                    <div class="col-sm-12">
                    <label for="date_of_reg">Date of incorporation or registration *</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="date_of_reg" id="datepicker-autoclose" value="{{ (@$data)?filterDate(@$data->date_of_reg):old('date_of_reg') }}">
                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                        </div><!-- input-group -->
                        @if($errors->has('date_of_reg'))
                        @php 
                            echo showError($errors->first('date_of_reg'));
                        @endphp
                    @endif
                    </div>
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name_of_director">Name of partners or directors of company(with MD) *</label>
                  <input type="text" id="name_of_director" parsley-trigger="change" name="name_of_director" class="form-control" required value="{{ (@$data)?@$data->name_of_director:old('name_of_director') }}">
                    @if($errors->has('name_of_director'))
                        @php 
                            echo showError($errors->first('name_of_director'));
                        @endphp
                    @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="address_of_director">Address of partners or directors of company(with MD) </label>
                  <input type="text" id="address_of_director" name="address_of_director" class="form-control"  value="{{ (@$data)?@$data->address_of_director:old('address_of_director') }}">
                    @if($errors->has('address_of_director'))
                        @php 
                            echo showError($errors->first('address_of_director'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
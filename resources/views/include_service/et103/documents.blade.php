<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-4">
                  <label for="pancard_file">Pan card of firms or company *</label>
                  <input type="file" class="form-control" name="pancard_file" required="" parsley-trigger="change" />
                    @if($errors->has('pancard_file'))
                        @php 
                            echo showError($errors->first('pancard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="registration_file">Registration certificate (if Company) *</label>
                  <input type="file" class="form-control" name="registration_file" required="" parsley-trigger="change" />
                    @if($errors->has('registration_file'))
                        @php 
                            echo showError($errors->first('registration_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="moa_file">MOA (if company)</label>
                  <input type="file" class="form-control" name="moa_file"  parsley-trigger="change" />
                    @if($errors->has('moa_file'))
                        @php 
                            echo showError($errors->first('moa_file'));
                        @endphp
                    @endif
                </div>


<div class="col-md-12"></div>
                
                <div class="form-group col-md-4">
                  <label for="audit_report_file">Audit report of firms or company</label>
                  <input type="file" class="form-control" name="audit_report_file"  parsley-trigger="change" />
                    @if($errors->has('audit_report_file'))
                        @php 
                            echo showError($errors->first('audit_report_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_statement_file">All banks statements (Firms & Company)</label>
                  <input type="file" class="form-control" name="bank_statement_file"  parsley-trigger="change" />
                    @if($errors->has('bank_statement_file'))
                        @php 
                            echo showError($errors->first('bank_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="financial_statement_file">Financial Statements of Firms & Company</label>
                  <input type="file" class="form-control" name="financial_statement_file"  parsley-trigger="change" />
                    @if($errors->has('financial_statement_file'))
                        @php 
                            echo showError($errors->first('financial_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="partnership_deed">Partnership Deed</label>
                  <input type="file" class="form-control" name="partnership_deed"  parsley-trigger="change" />
                    @if($errors->has('partnership_deed'))
                        @php 
                            echo showError($errors->first('partnership_deed'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
         window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="2" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Company Details </center> </strong></td>
               </tr>
               <td colspan="4">

                  <div class="div_3">
                     <strong>Firm or Company: </strong> 
                     <br>
                     <font> {{ $data->firm_or_company }} </font>
                  </div>
                  
                  <div class="div_3"> 
                     <strong>Pancard No. (Firm or Company) : </strong> 
                     <br>
                     <font> {{ $data->pancard_no }} </font>
                  </div>
                  
                  <div class="div_3"> 
                     <strong>Name of firm or company: </strong>
                     <br>
                     <font> {{ $data->name_of_firm}} </font>
                  </div>
                  
                  <div style="clear:both;"></div>
                  
                  <div class="div_3">
                     <strong>Date of incorporation or registration: </strong>
                     <br>
                     <font> {{ ddmmyy($data->date_of_reg) }} </font>
                  </div>

                  <div class="div_3"> 
                     <strong>Name of partners or directors of company(with MD): </strong>
                     <br>
                     <font> {{ $data->name_of_director }} </font> 
                  </div>

                  <div class="div_1"> 
                     <strong>Address of partners or directors of company(with MD): </strong><br> 
                     <font> {{ $data->address_of_director}} </font>
                  </div>
                  
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                  <div class="div_2">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Contact Details </center> </strong></td>
               </tr>
               <td colspan="4">

                  <div class="div_3"><strong>LandLine no: </strong> <font> {{ $data->landline_no }} </font></div>
                  <div class="div_3"> <strong>Mobile No: </strong> <font> {{ $data->mobile_number }} </font> </div>
                  <div class="div_3"> <strong>Secondary Mobile No: </strong> <font> {{ $data->sec_mobile_number}} </font></div>

                  <div class="div_3"><strong>Email-ID: </strong> <font> {{ $data->email_id }} </font></div>
                  <div class="div_3"> <strong>Secondary Email-ID: </strong> <font> {{ $data->sec_email_id }} </font> </div>
                  <div class="div_3"> <strong>Contact Type: </strong> <font> {{ $data->contact_type}} </font></div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Bank Details </center> </strong></td>
               </tr>
               <td colspan="4">
               
                  @foreach($banks as $b_key => $b_val)
                     <div class="div_3"><strong>IFSC CODE: </strong> <br> <font> {{ $b_val->bank_ifsc }} </font></div>
                     <div class="div_3"> <strong>Bank Name: </strong> <br> <font> {{ $b_val->bank_name }} </font> </div>
                     <div class="div_3"> <strong>Branch: </strong> <br> <font> {{ $b_val->bank_branch}} </font></div>

                     <div class="div_3"><strong>A/C type: </strong> <br> <font> {{ $b_val->bank_account_type }} </font></div>
                     <div class="div_3"> <strong>A/C Number: </strong> <br> <font> {{ $b_val->bank_account_number }} </font> </div>
                     <div class="div_3"> <strong>A/C Holder Name: </strong> <br> <font> {{ $b_val->bank_holder_name}} </font></div>
                     <hr>
                  @endforeach
               </td>
            </tr>

         </tfoot>
      </table>
   </body>
</html>
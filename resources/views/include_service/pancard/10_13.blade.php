<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-3">
                  <label for="status_of_applicant">Status of applicant *</label>
                  <select class="form-control select2" name="status_of_applicant" parsley-trigger="change" required="">
                        @foreach(status_of_applicant() as $key => $val)
                            @php
                                $select_stated = (old('status_of_applicant') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->status_of_applicant == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('status_of_applicant'))
                        @php 
                            echo showError($errors->first('status_of_applicant'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="source_of_income">Source of Income *</label>
                  <select class="form-control select2" name="source_of_income" parsley-trigger="change" required="">
                        @foreach(source_of_income() as $key => $val)
                            @php
                                $select_stated = (old('source_of_income') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->source_of_income == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('source_of_income'))
                        @php 
                            echo showError($errors->first('source_of_income'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="company_reg_number">Registration Number (for company, firms, LLps, etc.)</label>
                  <input type="text" id="company_reg_number" name="company_reg_number" class="form-control"   value="{{ (@$data)?@$data->company_reg_number:old('company_reg_number') }}">
                    @if($errors->has('company_reg_number'))
                        @php 
                            echo showError($errors->first('company_reg_number'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="aadhar_allotted">Alloted AADHAR number</label>
                  <input type="text" id="aadhar_allotted"  name="aadhar_allotted" class="form-control" value="{{ (@$data)?@$data->aadhar_allotted:old('aadhar_allotted') }}">
                    @if($errors->has('aadhar_allotted'))
                        @php 
                            echo showError($errors->first('aadhar_allotted'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="aadhar_not_allotted">Enrolment ID</label>
                  <font>(If AADHAr number is not alloted, mention the enrilment ID of Aadhar application form)</font>
                  <input type="text" id="aadhar_not_allotted" name="aadhar_not_allotted" class="form-control"  value="{{ (@$data)?@$data->aadhar_not_allotted:old('aadhar_not_allotted') }}">
                    @if($errors->has('aadhar_not_allotted'))
                        @php 
                            echo showError($errors->first('aadhar_not_allotted'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="aadhar_name">Name *</label>
                  <font>(Name as pr AADHAR letter/card or as per the Enrolment ID of Aadhaar application form)</font>
                  <input type="text" id="aadhar_name" name="aadhar_name" required="" parsley-trigger="change" class="form-control"  value="{{ (@$data)?@$data->aadhar_name:old('aadhar_name') }}">
                    @if($errors->has('aadhar_name'))
                        @php 
                            echo showError($errors->first('aadhar_name'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
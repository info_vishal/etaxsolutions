<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
           window.print();
       }
      </script>
   <body class="print_out_table" >
      <table border="2" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Client Details </center> </strong></td>
               </tr>
               <td colspan="3">
                  <strong>Last Name: </strong> <font> {{ $data->cli_last_name }} </font><br>
                  <strong>First Name: </strong> <font> {{ $data->cli_first_name }} </font><br>
                  <strong>Middle Name: </strong> <font> {{ $data->cli_middle_name}} </font><br>
                  
               </td>
               <td colspan="1">
                  <strong>Gender: </strong> <font> {{ $data->cli_gender}} </font><br>
                  <strong>DOB: </strong> <font> {{ ddmmyy($data->cli_dob)}} </font><br>
               </td>
            </tr>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Parents Details </center> </strong></td>
               </tr>
               <td colspan="2">
                  <strong> <center> Father </center> </strong><hr/>
                  <strong>Last Name: </strong> <font> {{ $data->fat_last_name }} </font><br>
                  <strong>First Name: </strong> <font> {{ $data->fat_first_name }} </font><br>
                  <strong>Middle Name: </strong> <font> {{ $data->fat_middle_name}} </font><hr>
                  <strong>Display name on card: </strong> <font> {{ $data->parent_name_on_card }} </font>
               </td>
               <td colspan="2">
                  <strong><center> Mother </center></strong>  <hr/>
                  <strong>Last Name: </strong> <font> {{ $data->mot_last_name }} </font><br>
                  <strong>First Name: </strong> <font> {{ $data->mot_first_name }} </font><br>
                  <strong>Middle Name: </strong> <font> {{ $data->mot_middle_name}} </font><br>
                  <br>
                  <br>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address </center> </strong></td>
               </tr>
               <td colspan="2">
                  <strong> <center> Residence Address </center></strong><hr>
                  <div style="width:50%;float:left">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                 <div style="width:50%;float:left">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>State: </strong> <font> {{ $res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
                  <div style="width:100%;clear:both">
                     <hr>
                     <strong>Address For Communication: </strong> <font> {{ $data->address_of_com }} </font>
                  </div>
               </td>
               <td colspan="2">
                  <strong> <center> Office Address </center></strong><hr>
                  <div style="width:50%;float:left">
                     <strong>Office Name: </strong> <font> {{ $data->ofc_name }} </font><br>
                     <strong>Flat / Room No: </strong> <font> {{ $data->ofc_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->ofc_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->ofc_add_area }} </font>
                  </div>
                 <div style="width:50%;float:left">
                     <strong>Pincode: </strong> <font> {{ $data->ofc_add_pincode }} </font><br>
                     <strong>City: </strong> <font> {{ $data->ofc_add_city }} </font><br>
                     <strong>State: </strong> <font> {{ $ofc_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->ofc_add_country }} </font>
                  </div>
                  <div style="width:100%;clear:both">
                     <br>
                  </div>
                  <br>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>MOBILE & EMAIL </center> </strong></td>
               </tr>
               <td colspan="4">
                  <div style="width:25%;float:left"><strong>Country Code: </strong> <font> {{ $data->country_code }} </font></div>
                  <div style="width:25%;float:left"> <strong>Area / STD Code: </strong> <font> {{ $data->area_code }} </font> </div>
                  <div style="width:25%;float:left"> <strong>Mobile Number: </strong> <font> {{ $data->mobile_number}} </font></div>
                  <div style="width:25%;float:left"> <strong>Email-ID: </strong> <font> {{ $data->email_id }} </font></div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Other Details </center> </strong></td>
               </tr>
               <td colspan="4">
                  <div style="width:33.33%;float:left"><strong>Status of applicant: </strong> <font> {{ $data->status_of_applicant }} </font></div>
                  <div style="width:33.33%;float:left"> <strong>Source of Income: </strong> <font> {{ $data->source_of_income }} </font> </div>
                  <div style="width:33.33%;float:left"> <strong>Registration Number (for company, firms, LLps, etc.): </strong> <font> {{ $data->company_reg_number}} </font></div>
                  <hr>
                  <div style="width:33.33%;float:left"><strong>Alloted Aadhar number (If available): </strong> <font> {{ $data->aadhar_allotted }} </font></div>
                  <div style="width:33.33%;float:left"> <strong>Enrolment ID: </strong> <font> {{ $data->aadhar_not_allotted }} </font> </div>
                  <div style="width:33.33%;float:left"> <strong>Name as per aadhar card: </strong> <font> {{ $data->aadhar_name}} </font></div>
                  
               </td>
            </tr>

         </tfoot>
      </table>
   </body>
</html>
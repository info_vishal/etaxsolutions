<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Parents Details') }}</b></h4>
     <hr/>
    <div class="col-md-12">

    </div>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Father Last Name/Surname *</label>
                  <input type="text" id="fat_last_name" parsley-trigger="change" name="fat_last_name" class="form-control" required value="{{ (@$data)?@$data->fat_last_name:old('fat_last_name') }}">
                    @if($errors->has('fat_last_name'))
                        @php 
                            echo showError($errors->first('fat_last_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputPassword4">Father First Name *</label>
                  <input type="text" id="fat_first_name" parsley-trigger="change" name="fat_first_name" class="form-control"  required value="{{ (@$data)?@$data->fat_first_name:old('fat_first_name') }}">
                    @if($errors->has('fat_first_name'))
                        @php 
                            echo showError($errors->first('fat_first_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputPassword4">Father Middle Name *</label>
                  <input type="text" id="fat_middle_name" parsley-trigger="change" name="fat_middle_name" class="form-control"  required value="{{ (@$data)?@$data->fat_middle_name:old('fat_middle_name') }}">
                    @if($errors->has('fat_middle_name'))
                        @php 
                            echo showError($errors->first('fat_middle_name'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Mother Last Name/Surname</label>
                  <input type="text" id="mot_last_name" name="mot_last_name" class="form-control"  value="{{ (@$data)?@$data->mot_last_name:old('mot_last_name') }}">
                    @if($errors->has('mot_last_name'))
                        @php 
                            echo showError($errors->first('mot_last_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputPassword4">Mother First Name</label>
                  <input type="text" id="mot_first_name" name="mot_first_name" class="form-control"   value="{{ (@$data)?@$data->mot_first_name:old('mot_first_name') }}">
                    @if($errors->has('mot_first_name'))
                        @php 
                            echo showError($errors->first('mot_first_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputPassword4">Mother Middle Name</label>
                  <input type="text" id="mot_middle_name" name="mot_middle_name" class="form-control"   value="{{ (@$data)?@$data->mot_middle_name:old('mot_middle_name') }}">
                    @if($errors->has('mot_middle_name'))
                        @php 
                            echo showError($errors->first('mot_middle_name'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="inputState">Display name on card </label>
                  <select class="form-control" name="parent_name_on_card">
                        @foreach(parentName() as $key => $val)
                            @php
                                $select_stated = (old('parent_name_on_card') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->parent_name_on_card == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('parent_name_on_card'))
                        @php 
                            echo showError($errors->first('parent_name_on_card'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
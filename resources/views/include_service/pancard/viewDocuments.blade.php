<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <div class="col-md-12 m-t-10">
                                  <label for="id_proof_file"><a target="_blank" download href="{{ image_url($data->id_proof_file) }}" title="Download">
                                    <i class="fa fa-download"></i></a> Identity Proof
                                  </label>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <div class="col-md-12 m-t-10">
                                  <label for="address_proof_file"><a target="_blank" download href="{{ image_url($data->address_proof_file) }}" title="Download">
                                    <i class="fa fa-download"></i></a> Address proof
                                  </label>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <div class="col-md-12 m-t-10">
                                  <label for="dob_file"><a target="_blank" download href="{{ image_url($data->dob_file) }}" title="Download">
                                    <i class="fa fa-download"></i></a> Date Of Birth
                                  </label>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <div class="col-md-12 m-t-10">
                                  <label for="signature_file"><a target="_blank" download href="{{ image_url($data->signature_file) }}" title="Download">
                                    <i class="fa fa-download"></i></a> Signature
                                  </label>
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <div class="col-md-12 m-t-10">
                                  <label for="passport_size_file"><a target="_blank" download href="{{ image_url($data->passport_size_file) }}" title="Download">
                                    <i class="fa fa-download"></i></a> Passport Size Photo
                                  </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Mobile & Email') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-3">
                  <label for="country_code">Country Code</label>
                  <input type="text" id="country_code" name="country_code" class="form-control"   value="{{ (@$data)?@$data->country_code:old('country_code') }}">
                    @if($errors->has('country_code'))
                        @php 
                            echo showError($errors->first('country_code'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="area_code">Area / STD Code</label>
                  <input type="text" id="area_code"  name="area_code" class="form-control" value="{{ (@$data)?@$data->area_code:old('area_code') }}">
                    @if($errors->has('area_code'))
                        @php 
                            echo showError($errors->first('area_code'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="mobile_number">Mobile Number *</label>
                  <input type="text" id="mobile_number" data-mask="(999) 999-9999" required parsley-trigger="change" name="mobile_number" class="form-control"  value="{{ (@$data)?@$data->mobile_number:old('mobile_number') }}">
                    @if($errors->has('mobile_number'))
                        @php 
                            echo showError($errors->first('mobile_number'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="email_id">Email-ID *</label>
                  <input type="email" id="email_id" parsley-trigger="change" required name="email_id" class="form-control"  value="{{ (@$data)?@$data->email_id:old('email_id') }}">
                    @if($errors->has('email_id'))
                        @php 
                            echo showError($errors->first('email_id'));
                        @endphp
                    @endif
                </div>                
            </div>
        </div>
    </div>
</div>
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Address') }}</b></h4>
     <hr/>

     <div class="col-md-12">
        <p><b>Office Address</b></p>
    </div>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="ofc_name">Office Name</label>
                  <input type="text" id="ofc_name" name="ofc_name" class="form-control"   value="{{ (@$data)?@$data->ofc_name:old('ofc_name') }}">
                    @if($errors->has('ofc_name'))
                        @php 
                            echo showError($errors->first('ofc_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="ofc_add_flat">Flat / Room No.  </label>
                  <input type="text" id="ofc_add_flat"  name="ofc_add_flat" class="form-control" value="{{ (@$data)?@$data->ofc_add_flat:old('ofc_add_flat') }}">
                    @if($errors->has('ofc_add_flat'))
                        @php 
                            echo showError($errors->first('ofc_add_flat'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="ofc_add_building">Society / Building</label>
                  <input type="text" id="ofc_add_building"  name="ofc_add_building" class="form-control"  value="{{ (@$data)?@$data->ofc_add_building:old('ofc_add_building') }}">
                    @if($errors->has('ofc_add_building'))
                        @php 
                            echo showError($errors->first('ofc_add_building'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="ofc_add_street">Street / Road / Post Office</label>
                  <input type="text" id="ofc_add_street"  name="ofc_add_street" class="form-control"  value="{{ (@$data)?@$data->ofc_add_street:old('ofc_add_street') }}">
                    @if($errors->has('ofc_add_street'))
                        @php 
                            echo showError($errors->first('ofc_add_street'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="ofc_add_area">Area / Locality / Taluka</label>
                  <input type="text" id="ofc_add_area" name="ofc_add_area" class="form-control"  value="{{ (@$data)?@$data->ofc_add_area:old('ofc_add_area') }}">
                    @if($errors->has('ofc_add_area'))
                        @php 
                            echo showError($errors->first('ofc_add_area'));
                        @endphp
                    @endif
                </div>

                
            </div>

            <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="ofc_add_pincode">Pincode</label>
                  <input type="text" id="ofc_add_pincode" name="ofc_add_pincode" class="form-control"   value="{{ (@$data)?@$data->ofc_add_pincode:old('ofc_add_pincode') }}">
                    @if($errors->has('ofc_add_pincode'))
                        @php 
                            echo showError($errors->first('ofc_add_pincode'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="ofc_add_city">City</label>
                  <input type="text" id="ofc_add_city" name="ofc_add_city" class="form-control"  value="{{ (@$data)?@$data->ofc_add_city:old('ofc_add_city') }}">
                    @if($errors->has('ofc_add_city'))
                        @php 
                            echo showError($errors->first('ofc_add_city'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="ofc_add_state">State</label>
                  <select class="form-control select2 select_state" name="ofc_add_state">
                        @foreach($states as $key => $val)
                            @php
                                $select_stated = (old('ofc_add_state') == $val->id)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->ofc_add_state == $val->id)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('ofc_add_state'))
                        @php 
                            echo showError($errors->first('ofc_add_state'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="ofc_add_country">Country</label>
                  <input type="text" id="ofc_add_country" name="ofc_add_country" class="form-control"  value="{{ (@$data)?@$data->ofc_add_country:old('ofc_add_country','India') }}">
                    @if($errors->has('ofc_add_country'))
                        @php 
                            echo showError($errors->first('ofc_add_country'));
                        @endphp
                    @endif
                </div>
            </div>


        </div>
    </div>

    <div class="col-md-12">
        <hr>
        <p><b>Residence Address</b></p>
    </div>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="res_add_flat">Flat / Room No.  </label>
                  <input type="text" id="res_add_flat"  name="res_add_flat" class="form-control" value="{{ (@$data)?@$data->res_add_flat:old('res_add_flat') }}">
                    @if($errors->has('res_add_flat'))
                        @php 
                            echo showError($errors->first('res_add_flat'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="res_add_building">Society / Building</label>
                  <input type="text" id="res_add_building"  name="res_add_building" class="form-control"  value="{{ (@$data)?@$data->res_add_building:old('res_add_building') }}">
                    @if($errors->has('res_add_building'))
                        @php 
                            echo showError($errors->first('res_add_building'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_street">Street / Road / Post Office</label>
                  <input type="text" id="res_add_street"  name="res_add_street" class="form-control"  value="{{ (@$data)?@$data->res_add_street:old('res_add_street') }}">
                    @if($errors->has('res_add_street'))
                        @php 
                            echo showError($errors->first('res_add_street'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_area">Area / Locality / Taluka</label>
                  <input type="text" id="res_add_area" name="res_add_area" class="form-control"  value="{{ (@$data)?@$data->res_add_area:old('res_add_area') }}">
                    @if($errors->has('res_add_area'))
                        @php 
                            echo showError($errors->first('res_add_area'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-2">
                  <label for="res_add_pincode">Pincode</label>
                  <input type="text" id="res_add_pincode" name="res_add_pincode" class="form-control"   value="{{ (@$data)?@$data->res_add_pincode:old('res_add_pincode') }}">
                    @if($errors->has('res_add_pincode'))
                        @php 
                            echo showError($errors->first('res_add_pincode'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="res_add_city">City</label>
                  <input type="text" id="res_add_city" name="res_add_city" class="form-control"  value="{{ (@$data)?@$data->res_add_city:old('res_add_city') }}">
                    @if($errors->has('res_add_city'))
                        @php 
                            echo showError($errors->first('res_add_city'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="inputState">State</label>
                  <select class="form-control select2 select_state" name="res_add_state">
                        @foreach($states as $key => $val)
                            @php
                                $select_stated = (old('res_add_state') == $val->id)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->res_add_state == $val->id)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $val->id }}'>{{ $val->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('res_add_state'))
                        @php 
                            echo showError($errors->first('res_add_state'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="res_add_country">Country</label>
                  <input type="text" id="res_add_country" name="res_add_country" class="form-control"   value="{{ (@$data)?@$data->res_add_country:old('res_add_country','India') }}">
                    @if($errors->has('res_add_country'))
                        @php 
                            echo showError($errors->first('res_add_country'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-3">
                  <label for="inputState">Address For Communication</label>
                  <select class="form-control" name="address_of_com">
                        @foreach(addOfComunication() as $key => $val)
                            @php
                                $select_stated = (old('address_of_com') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->address_of_com == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('address_of_com'))
                        @php 
                            echo showError($errors->first('address_of_com'));
                        @endphp
                    @endif
                </div>
            </div>

        </div>
    </div>

</div>
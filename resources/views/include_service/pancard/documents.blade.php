
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <!-- <div class="form-group col-md-3">
                  <label for="id_proof">Identity Proof *</label>
                  <select class="form-control select2" name="id_proof" parsley-trigger="change" required="">
                        @foreach(pan_proof() as $key => $val)
                            @php
                                $select_stated = (old('id_proof') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->id_proof == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('id_proof'))
                        @php 
                            echo showError($errors->first('id_proof'));
                        @endphp
                    @endif
                </div> -->
                <div class="form-group col-md-4">
                  <label for="id_proof_file">Identity Proof *</label>
                  <input type="file" class="form-control" name="id_proof_file" required="" parsley-trigger="change" />
                    @if($errors->has('id_proof_file'))
                        @php 
                            echo showError($errors->first('id_proof_file'));
                        @endphp
                    @endif
                </div>
 
                <!-- <div class="form-group col-md-3">
                  <label for="address_proof">Address Proof </label>
                  <select class="form-control select2" name="address_proof" parsley-trigger="change" required="">
                        @foreach(pan_proof() as $key => $val)
                            @php
                                $select_stated = (old('address_proof') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->address_proof == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('address_proof'))
                        @php 
                            echo showError($errors->first('address_proof'));
                        @endphp
                    @endif
                </div> -->
                <div class="form-group col-md-4">
                  <label for="address_proof_file">Address Proof </label>
                  <input type="file" class="form-control" name="address_proof_file" />
                    @if($errors->has('address_proof_file'))
                        @php 
                            echo showError($errors->first('address_proof_file'));
                        @endphp
                    @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="dob_file">Date Of Birth *</label>
                  <input type="file" class="form-control" name="dob_file" required="" parsley-trigger="change" />
                    @if($errors->has('dob_file'))
                        @php 
                            echo showError($errors->first('dob_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="form-row">
            <div class="form-group col-md-4">
              <label for="signature_file">Signature *</label>
              <input type="file" class="form-control" name="signature_file" required="" parsley-trigger="change" />
                @if($errors->has('signature_file'))
                    @php 
                        echo showError($errors->first('signature_file'));
                    @endphp
                @endif
            </div>
        
            <div class="form-group col-md-4">
              <label for="passport_size_file">Passport Size Photo *</label>
              <input type="file" class="form-control" name="passport_size_file" required="" parsley-trigger="change" />
                @if($errors->has('passport_size_file'))
                    @php 
                        echo showError($errors->first('passport_size_file'));
                    @endphp
                @endif
            </div>
        </div>
        </div>
    </div>
</div>

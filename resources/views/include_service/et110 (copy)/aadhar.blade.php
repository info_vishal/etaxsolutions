<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Aadhar Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-12">
                  <label for="aadhar_allotted">Alloted AADHAR number</label>
                  <input type="text" id="aadhar_allotted"  name="aadhar_allotted" class="form-control" value="{{ (@$data)?@$data->aadhar_allotted:old('aadhar_allotted') }}">
                    @if($errors->has('aadhar_allotted'))
                        @php 
                            echo showError($errors->first('aadhar_allotted'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="aadhar_not_allotted">Enrolment ID</label>
                  <font>(If AADHAR number is not alloted, mention the enrilment ID of Aadhar application form)</font>
                  <input type="text" id="aadhar_not_allotted" name="aadhar_not_allotted" class="form-control"  value="{{ (@$data)?@$data->aadhar_not_allotted:old('aadhar_not_allotted') }}">
                    @if($errors->has('aadhar_not_allotted'))
                        @php 
                            echo showError($errors->first('aadhar_not_allotted'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-12">
                  <label for="amount_received">Amount Received *</label>
                  <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
                    @if($errors->has('amount_received'))
                        @php 
                            echo showError($errors->first('amount_received'));
                        @endphp
                    @endif
                </div>
            
            </div>
        </div>
    </div>
</div>
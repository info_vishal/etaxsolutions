<div class="col-md-12 bank_div">
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="bank_ifsc0">IFSC CODE *</label>
                  <div class="input-group">
                    <span class="input-group-btn">
                    <button type="button" class="btn waves-effect waves-light btn-primary searchBankDetails" data-unique_id="0"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" id="bank_ifsc0" name="director[0][bank_ifsc]" data-unique_id="0" data-field_name="bank_ifsc" class="form-control" placeholder="Enter IFSC CODE" required value="{{ (@$data)?@$data->bank_ifsc:old('bank_ifsc') }}">
                </div>
                    @if($errors->has('bank_ifsc'))
                        @php 
                            echo showError($errors->first('bank_ifsc'));
                        @endphp
                    @endif
                <ul class="parsley-errors-list ifsc_error filled hide" id="ifsc_error0"><li class="parsley-required">Not Found</li></ul>
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_name0">Bank Name *</label>
                  <input type="text" id="bank_name0" parsley-trigger="change" name="director[0][bank_name]" data-unique_id="0" data-field_name="bank_name" class="form-control" required value="{{ (@$data)?@$data->bank_name:old('bank_name') }}">
                    @if($errors->has('bank_name'))
                        @php 
                            echo showError($errors->first('bank_name'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_branch0">Branch *</label>
                  <input type="text" id="bank_branch0" parsley-trigger="change" name="director[0][bank_branch]" data-unique_id="0" data-field_name="bank_branch" class="form-control" required value="{{ (@$data)?@$data->bank_branch:old('bank_branch') }}">
                    @if($errors->has('bank_branch'))
                        @php 
                            echo showError($errors->first('bank_branch'));
                        @endphp
                    @endif
                </div>

                <div class="col-md-12"></div>
                <div class="form-group col-md-4">
                  <label for="bank_account_type0">Account type *</label>
                  <select class="form-control select2" id="bank_account_type0" name="director[0][bank_account_type]" data-unique_id="0" data-field_name="bank_account_type" parsley-trigger="change" required="">
                        @foreach(bankAccountType() as $key => $val)
                            @php
                                $select_stated = (old('bank_account_type') == $key)?'selected':'';
                                if($select_stated == ""){
                                    $select_stated = (@$data->bank_account_type == $key)?'selected':'';
                                }
                            @endphp
                            <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('bank_account_type'))
                        @php 
                            echo showError($errors->first('bank_account_type'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_account_number0">Account Number *</label>
                  <input type="text" id="bank_account_number0" parsley-trigger="change" name="director[0][bank_account_number]" data-unique_id="0" data-field_name="bank_account_number" class="form-control" min="0" required value="{{ (@$data)?@$data->bank_account_number:old('bank_account_number') }}">
                    @if($errors->has('bank_account_number'))
                        @php 
                            echo showError($errors->first('bank_account_number'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_holder_name0">Account Holder Name *</label>
                  <input type="text" id="bank_holder_name0" parsley-trigger="change" name="director[0][bank_holder_name]" data-unique_id="0" data-field_name="bank_holder_name" class="form-control"  required placeholder="Name-1, Name-2" value="{{ (@$data)?@$data->bank_holder_name:old('bank_holder_name') }}">
                    @if($errors->has('bank_holder_name'))
                        @php 
                            echo showError($errors->first('bank_holder_name'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

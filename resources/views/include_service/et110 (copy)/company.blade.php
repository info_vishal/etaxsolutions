@if(@$data && \Request()->type != 're-create')
<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Receipt no: </b> {{ @$data->receipt_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Acknowledgement no: </b> {{ @$data->tracking_no }}</h4>
                </div>
                <div class="form-group col-md-12">
                      <h4 class="m-t-0 header-title"><b for="">Amount Received: </b> {{ @$data->amount_received }}</h4>
                </div>
            </div>    
        </div>
    </div>
</div>
@endif
<div class="card-box">
    <div class="col-md-8">
        <h4 class="m-t-0 header-title"><b>{{ __('Company Details') }}</b></h4>
    </div>
    <div class="col-md-4">
        <div class=" col-md-12">
          <input type="text" id="amount_received" min="0" required="" parsley-trigger="change" name="amount_received" class="form-control" Placeholder="Amount received from client"  value="{{ (@$data)?@$data->amount_received:old('amount_received') }}">
            @if($errors->has('amount_received'))
                @php 
                    echo showError($errors->first('amount_received'));
                @endphp
            @endif
        </div>
    </div>
    <div class="col-md-12"> <hr/> </div>

    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="col-md-12">
                    <div class="form-group col-md-2">
                      <label for="company_type">Company Type</label>
                      <select class="form-control" name="company_type">
                            @foreach(et110compType() as $key => $val)
                                @php
                                    $select_stated = (old('company_type') == $key)?'selected':'';
                                    if($select_stated == ""){
                                        $select_stated = (@$data->company_type == $key)?'selected':'';
                                    }
                                @endphp
                                <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('company_type'))
                            @php 
                                echo showError($errors->first('company_type'));
                            @endphp
                        @endif
                    </div>
                </div>

                @if(\Request()->id == "")
                    <div class="col-md-12">
                          <label class="col-md-12" for="company_name">5 Proposed Name of Company</label>
                        @for($i=0; $i<5; $i++)
                        <div class="form-group col-md-4">
                          <input type="text" id="company_name" name="company_name[]" placeholder="Company Name" class="form-control"  value="{{ (@$data)?@$data->company_name:old('company_name') }}" />
                        </div>
                        @endfor
                    </div>
                @else
                    <div class="col-md-12">
                        <label class="col-md-12" for="company_name">5 Proposed Name of Company</label>
                        @php $name = str_replace('@$',', ',$data->company_name) @endphp
                        <div class="form-group col-md-12">
                          <input type="text" id="company_name" placeholder="Company Name" class="form-control"  value="{{ (@$data)?@$name:old('company_name') }}" />
                        </div>
                    </div>
                @endif

                <div class="col-md-6">
                    <div class="form-group col-md-12">
                    <label  for="company_object">Object of company</label>
                        <textarea  id="company_object" name="company_object" class="form-control">{{ (@$data)?@$data->company_object:old('company_object') }}</textarea>
                        @if($errors->has('company_object'))
                            @php 
                                echo showError($errors->first('company_object'));
                            @endphp
                        @endif
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group col-md-12">
                    <label  for="company_brief_details">Brief business details</label>
                        <textarea  id="company_brief_details" name="company_brief_details" class="form-control">{{ (@$data)?@$data->company_brief_details:old('company_brief_details') }}</textarea>
                        @if($errors->has('company_brief_details'))
                            @php 
                                echo showError($errors->first('company_brief_details'));
                            @endphp
                        @endif
                    </div>
                </div>

            </div>
            @include('include_service.et110.company_address')
            @include('include_service.et110.company_documents')
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-4">
                  <label for="photo_file0"> Photos of director *</label>
                  <input type="file" class="form-control" id="photo_file0" name="director[0][photo_file]" data-unique_id="0" data-field_name="photo_file" required="" parsley-trigger="change" />
                    @if($errors->has('photo_file'))
                        @php 
                            echo showError($errors->first('photo_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="pancard_file0"> Pancard of director *</label>
                  <input type="file" class="form-control" id="pancard_file0" name="director[0][pancard_file]" data-unique_id="0" data-field_name="pancard_file" required="" parsley-trigger="change" />
                    @if($errors->has('pancard_file'))
                        @php 
                            echo showError($errors->first('pancard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="aadharcard_file0">Aadhar Card of directors *</label>
                  <input type="file" class="form-control" id="aadharcard_file0" name="director[0][aadharcard_file]" data-unique_id="0" data-field_name="aadharcard_file" required="" parsley-trigger="change" />
                    @if($errors->has('aadharcard_file'))
                        @php 
                            echo showError($errors->first('aadharcard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="bank_statement_file0">Last 3 months bank statement of director *</label>
                  <input type="file" class="form-control" id="bank_statement_file0" name="director[0][bank_statement_file]" data-unique_id="0" data-field_name="bank_statement_file" required="" parsley-trigger="change" />
                    @if($errors->has('bank_statement_file'))
                        @php 
                            echo showError($errors->first('bank_statement_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="dsc_file0">DSC of director(if available) </label>
                  <input type="file" class="form-control" id="dsc_file0" name="director[0][dsc_file]" data-unique_id="0" data-field_name="dsc_file" />
                    @if($errors->has('dsc_file'))
                        @php 
                            echo showError($errors->first('dsc_file'));
                        @endphp
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-md-12"></div>
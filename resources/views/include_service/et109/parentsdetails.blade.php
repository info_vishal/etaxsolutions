<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Parents Details') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="father_full_name">Father full name *</label>
                  <input type="text" id="father_full_name" parsley-trigger="change" name="father_full_name" class="form-control" required value="{{ (@$data)?@$data->father_full_name:old('father_full_name') }}">
                    @if($errors->has('father_full_name'))
                        @php 
                            echo showError($errors->first('father_full_name'));
                        @endphp
                    @endif
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="mother_full_name">Mother full name</label>
                  <input type="text" id="mother_full_name" name="mother_full_name" class="form-control"  value="{{ (@$data)?@$data->mother_full_name:old('mother_full_name') }}">
                    @if($errors->has('mother_full_name'))
                        @php 
                            echo showError($errors->first('mother_full_name'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
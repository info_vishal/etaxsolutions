<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title> {{ @$service->name }}</title>
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
      <script>
       window.onload = function(){
         window.print();
       }
      </script>
   <body class="print_out_table">
      <table border="2" style="width:100%">
         <caption> 
            <h3> {{ $service->name }} </h3> 
            Receipt #{{ $data->receipt_no }} | Acknowledgement no: {{ $data->tracking_no }} | Date: {{ ddmmyy($data->created_at) }}
            <br>
            <br>
         </caption>
         <tbody>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Client Details </center> </strong></td>
               </tr>
               <td colspan="3">
                  <strong>Last Name: </strong> <font> {{ $data->cli_last_name }} </font><br>
                  <strong>First Name: </strong> <font> {{ $data->cli_first_name }} </font><br>
                  <strong>Middle Name: </strong> <font> {{ $data->cli_middle_name}} </font><br>
                  
               </td>
               <td colspan="1">
                  <strong>Pancard No: </strong> <font> {{ $data->pancard_no}} </font><br>
                  <strong>Aadhar No: </strong> <font> {{ $data->aadhar_allotted}} </font><br>
                  <strong>Gender: </strong> <font> {{ $data->cli_gender}} </font><br>
                  <strong>DOB: </strong> <font> {{ ddmmyy($data->cli_dob)}} </font><br>
               </td>
            </tr>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Parents Details </center> </strong></td>
               </tr>
               <td colspan="4">
                  <strong>Father Full Name : </strong> <font> {{ $data->father_full_name }} </font><br>
                  <strong>Mother Full Name : </strong> <font> {{ $data->mother_full_name }} </font><br>
               </td>
            </tr>
            <tr>
               <tr>
                  <td colspan="4"><strong><center>Address Details</center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2">
                     <strong>Flat / Room No: </strong> <font> {{ $data->res_add_building }} </font><br>
                     <strong>Street / Road / Post Office: </strong> <font> {{ $data->res_add_street }} </font><br>
                     <strong>Area / Locality / Taluka: </strong> <font> {{ $data->res_add_area }} </font><br>
                     <strong>Pincode: </strong> <font> {{ $data->res_add_pincode }} </font>
                  </div>
                 
                  <div class="div_2">
                     <strong>City: </strong> <font> {{ $data->res_add_city }} </font><br>
                     <strong>District: </strong> <font> {{ @$res_district->name }} </font><br>
                     <strong>State: </strong> <font> {{ @$res_state->name }} </font><br>
                     <strong>Country: </strong> <font> {{ $data->res_add_country }} </font>
                  </div>
               </td>
            </tr>

            <tr>
               <tr>
                  <td colspan="4"><strong><center>Contact Details </center> </strong></td>
               </tr>
               <td colspan="4">
                  <div class="div_2"> <strong>Secondary Mobile No: </strong> <font> {{ $data->sec_mobile_number}} </font></div>
                  <div class="div_2"><strong>Email-ID: </strong> <font> {{ $data->email_id }} </font></div>
               </td>
            </tr>
         </tfoot>
      </table>
   </body>
</html>
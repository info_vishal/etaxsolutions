<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Documents') }}</b></h4>
     <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-4">
                  <label for="pancard_file">Pan Card *</label>
                  <input type="file" class="form-control" name="pancard_file" required="" parsley-trigger="change" />
                    @if($errors->has('pancard_file'))
                        @php 
                            echo showError($errors->first('pancard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="aadharcard_file">Aadhar Card *</label>
                  <input type="file" class="form-control" name="aadharcard_file" required="" parsley-trigger="change" />
                    @if($errors->has('aadharcard_file'))
                        @php 
                            echo showError($errors->first('aadharcard_file'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="photo_file">Photo</label>
                  <input type="file" class="form-control" name="photo_file"  parsley-trigger="change" />
                    @if($errors->has('photo_file'))
                        @php 
                            echo showError($errors->first('photo_file'));
                        @endphp
                    @endif
                </div>


                <div class="col-md-12"></div>

                <div class="form-group col-md-4">
                  <label for="any_other_file">Any other documents as needed</label>
                  <input type="file" class="form-control" name="any_other_file"  parsley-trigger="change" />
                    @if($errors->has('any_other_file'))
                        @php 
                            echo showError($errors->first('any_other_file'));
                        @endphp
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
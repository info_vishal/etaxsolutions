<div class="card-box">
    <h4 class="m-t-0 header-title"><b>{{ __('Contact Details') }}</b></h4>
            <hr/>
    <div class="row">
        <div class="container">
            <div class="form-row">

                <div class="form-group col-md-4">
                  <label for="mobile_number">Mobile Number *</label>
                  <input type="text" id="mobile_number" data-mask="(999) 999-9999" required parsley-trigger="change" name="mobile_number" class="form-control"  value="{{ (@$data)?@$data->mobile_number:old('mobile_number') }}">
                    @if($errors->has('mobile_number'))
                        @php 
                            echo showError($errors->first('mobile_number'));
                        @endphp
                    @endif
                </div>

                <div class="form-group col-md-4">
                  <label for="email_id">Email-ID *</label>
                  <input type="email" id="email_id" parsley-trigger="change" required name="email_id" class="form-control"  value="{{ (@$data)?@$data->email_id:old('email_id') }}">
                    @if($errors->has('email_id'))
                        @php 
                            echo showError($errors->first('email_id'));
                        @endphp
                    @endif
                </div>
            </div>
            <div class="col-md-12"></div>
        </div>
    </div>
</div>
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="">
                    <a href="{{ route('bdm.dashboard') }}" class="waves-effect {{ Request::is('bdm') ? 'active' : '' }}"><i class="ti-home"></i> <span> Dashboard </span> </a>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect {{ Request::is('bdm/levels*') ? 'active' : '' }}"><i class="ti-home"></i> <span> Levels </span> </a>
                    <ul class="list-unstyled">
                        <li class="{{ Request::is('bdm/levels') ? 'active' : '' }}">
                            <a href="{{ route('bdm.sell.levels') }}"><i class="fa fa-sticky-note"></i> <span>Search Levels </span> </a>
                        </li>
                        <li class="{{ Request::is('bdm/levels/myArchived') ? 'active' : '' }}"><a href="{{ route('bdm.levels.myArchived') }}"><i class="fa fa-sticky-note"></i> My Archived</a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="{{ route('bdm.listUsers') }}" class="waves-effect {{ Request::is('bdm/users*') ? 'active' : '' }}"><i class="ti-home"></i> <span> Users </span> </a>
                </li>
                <li class="">
                    <a href="{{ route('bdm.statements') }}" class="waves-effect {{ Request::is('bdm/statements*') ? 'active' : '' }}"><i class="fa fa-sticky-note"></i> <span> Statements </span> </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


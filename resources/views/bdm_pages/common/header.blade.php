<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ asset('theme/px/plugins/morris/morris.css') }}">
        <link href="{{ asset('theme/px/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/px/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
        
        <!-- custom css -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

        <!-- jQuery  -->
        <script src="{{ asset('theme/px/js/jquery.min.js') }}"></script> 

        <link href="{{ asset('theme/px/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ asset('theme/px/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ asset('theme/px/js/modernizr.min.js') }}"></script>
        
        <!-- <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->
    </head>


    <body class="fixed-left full_page_{{ Request::segment(2) }}">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="{{ url('/') }}" class="logo">
                            <img src="{{ url('/') }}/images/logo_in.png" alt=""  style="width: 80%;" class="mw-100" />
                        </a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">

                            <div class="pull-left">
                                <button class="btn-top"> <small>Level: {{ \Auth::user()->role()->name }}</small>
                                </button>
                                <span class="clearfix"></span>
                            </div>
                            
                            
                            <div class="pull-left">
                                <button class="btn-top"> <small>Wallet Bal: {{ \Auth::user()->available_balance }}</small> 
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <!-- <div class="pull-left">
                                <button class="btn-top"> <small>Reward Bal: {{ \Auth::user()->reward_balance }}</small>
                                </button>
                                <span class="clearfix"></span>
                            </div> -->


                            <ul class="nav navbar-nav navbar-right pull-right">
                                
                                <li class="dropdown hidden-xs">
                                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">{{ \Auth::user()->unreadNotifications->count() }}</span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-lg">
                                        <li class="notifi-title">
                                            <span class="label label-default pull-right"><a href="{{ route('common.markNotificationAsRead')}}" class="btn-custom">Mark all as read</a></span>Notification</li>
                                        <li class="list-group nicescroll notification-list">
                                        @foreach (\Auth::user()->unreadNotifications as $val)
                                            @php
                                                $type = $val->data['type'];
                                                $id = $val->data['id'];
                                                $message = $val->data['message'];
                                                $route = NULL;
                                                switch($type){
                                                    case 'account-update' :
                                                        $route = route('common.myProfile');
                                                        break;
                                                    case 'service-new-et-101' :
                                                    case 'service-update-et-101' :
                                                        $route = route('bdm.pancard.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-102' :
                                                    case 'service-update-et-102' :
                                                        $route = route('bdm.et102.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-103' :
                                                    case 'service-update-et-103' :
                                                        $route = route('bdm.et103.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-104' :
                                                    case 'service-update-et-104' :
                                                        $route = route('bdm.et104.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-105' :
                                                    case 'service-update-et-105' :
                                                        $route = route('bdm.et105.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-106' :
                                                    case 'service-update-et-106' :
                                                        $route = route('bdm.et106.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-107' :
                                                    case 'service-update-et-107' :
                                                        $route = route('bdm.et107.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-108' :
                                                    case 'service-update-et-108' :
                                                        $route = route('bdm.et108.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-109' :
                                                    case 'service-update-et-109' :
                                                        $route = route('bdm.et109.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-110' :
                                                    case 'service-update-et-110' :
                                                        $route = route('bdm.et110.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-111' :
                                                    case 'service-update-et-111' :
                                                        $route = route('bdm.et111.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-112' :
                                                    case 'service-update-et-112' :
                                                        $route = route('bdm.et112.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-113' :
                                                    case 'service-update-et-113' :
                                                        $route = route('bdm.et113.save')."?id=".$id;
                                                        break;
                                                    case 'service-new-et-114' :
                                                    case 'service-update-et-114' :
                                                        $route = route('bdm.et114.save')."?id=".$id;
                                                        break;
                                                                                                    }
                                            @endphp
                                           <a href="#" data-route="{{ route('common.markNotificationAsRead') }}" data-token="{{ csrf_token() }}" data-action="{{ $route }}" data-notify_id="{{ $val->id }}" class="list-group-item etax_notification">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-bell-o fa-2x text-primary"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading text-trans-cap">{{ str_replace('-', ' ', $type) }}</h5>
                                                    <p class="m-0">
                                                        <small>{{ $message }}</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>
                                           @endforeach

                                       </li>
                                    </ul>
                                </li>
                                
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="{{ image_url(\Auth::user()->profile_image) }}" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('common.myProfile') }}" >
                                            <i class="fa fa-user m-r-5"></i>{{ __('My Profile') }}</a>
                                        </li>
                                        <li><a href="{{ route('common.changePassword') }}" >
                                            <i class="fa fa-lock m-r-5"></i>{{ __('Change Password') }}</a>
                                        </li>
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="ti-power-off m-r-5"></i>{{ __('Logout') }}</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
            @include('bdm_pages.common.nav')
            <div class="content-page">
                <div class="content">
                    <div class="container" id="MAINCONTAINER">
                        @yield('content')
                    
            @extends('bdm_pages.common.footer')
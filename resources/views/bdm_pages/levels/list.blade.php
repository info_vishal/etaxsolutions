@extends('admin_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Levels List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li>Levels List</li>
        </ol>
    </div>
    <div class="col-md-2">
        <a href="{{ route('admin.levels.save') }}"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Create levels</button></a>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Levels  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_levels">
                            <thead>
                                <tr>
                                <th> Name </th>
                                <th data-head="State"> State </th>
                                <th> Levels </th>
                                <th> Status </th>
                                <th> Allocated By </th>
                                <th> Actions </th>
                            </tr>
                            </thead> 

                            <tfoot>
                                <tr>
                                <th> Name </th>
                                <th> State </th>
                                <th> Levels </th>
                                <th> Status </th>
                                <th> Allocated By </th>
                                <th> Actions </th>
                                </tr>
                            </tfoot>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
    },
    customDataTable: function(){
        $table_datatable = $('#table_levels').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("admin.levels.json") }}',
           columns: [
                { data: 'level_name' },
                { data: 'state_name' },
                { data: 'level_type' },
                { data: 'allocated_status' },
                { data: 'user_name' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.archived_at)
                            return  '<span class="label label-table label-inverse">Archived</span>';          
                        else if(row.allocated_status)
                            return  '<span class="label label-table label-danger">Allocated</span>';          
                        else
                            return  '<span class="label label-table label-success">Available</span>';
                    },
                    "targets": 3,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 5,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,

            initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(column[0] != 1) return false;
                title = this.header().dataset.head;
                var select = $('<select class="form-control" ><option value="">'+title+'</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
        });
        
    },
    getActions: function(row){ 
        var html = '';
            html = `
                <div class="btn-group" role="group">
                  <a title="Edit" href="{{ route('admin.levels.save') }}/`+row.level_id+`"><i class="fa fa-edit"></i></a> 
                  <a title="Delete" href="#" class="delete" data-code="{{ route('admin.listLevels') }}/?id=`+row.level_id+`&type=delete" ><i class="fa fa-trash"></i></a>
                </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

@extends('bdm_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('My Archived Levels') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('bdm.dashboard') }}">Dashboard</a></li>
            <li>My Archived Levels</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
                <h4 class="m-t-0 header-title"><b>{{ __('My Archived Levels') }}</b></h4>
                <hr/>
            <div class="table-responsive">
                    <table class="table table-striped table-bordered dataTable no-footer" id="table_levels">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th data-head="State"> State </th>
                                <th> Levels </th>
                                <th> Archived At </th>
                                <th> Assigned Status </th>
                                <th> Action </th>
                            </tr>
                        </thead> 
                        <tbody>
                            @foreach($records as $key => $val)
                            <tr>
                                <td>{{ $val->level_name }}</td>
                                <td>{{ $val->state_name }}</td>
                                <td>{{ $val->level_type }}</td>
                                <td>{{ ddmmyy($val->archived_at) }}</td>
                                <td>
                                    @if(@$val->assigned_user_id)
                                        <label class="label label-danger">Assigned</label>
                                    @else
                                        <label class="label label-success">Free</label>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a class="label label-primary" title="View" href="{{ route('bdm.levels.view').'/'.$val->level_id }}">View</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th> Name </th>
                                <th> State </th>
                                <th> Levels </th>
                                <th> Archived At </th>
                                <th> Assigned Status </th>
                                <th> Action </th>
                            </tr>
                        </tfoot>    
                    </table>
                </div>
         </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.customDataTable();
    },
    customDataTable: function( form ){
        $table_datatable = $('#table_levels').DataTable();
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

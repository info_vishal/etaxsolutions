@extends('bdm_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/footable/css/footable.core.css') }}" rel="stylesheet">

<div class="row">
    <div class="col-md-6 col-lg-3">
        <div class="widget-bg-color-icon card-box fadeInDown animated">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-account-circle text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ \Auth::user()->employee_code }}</b></h3>
                <p class="text-muted">Your Code</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-6 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-pink pull-left">
                <i class="fa fa-rupee text-pink"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ \Auth::user()->available_balance }}</b></h3>
                <p class="text-muted">Wallet Balance</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@if(\Auth::user()->status == 'active')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Services</b></h4>
            <hr>
            <table id="demo-foo-filtering" class="table table-striped toggle-circle m-b-0" data-page-size="15">
                <thead>
                    <tr>
                        <th data-toggle="">Code</th>
                        <th data-toggle="true">Name</th>
                        <th data-toggle="true">Description</th>
                        <th data-toggle="true">Price</th>
                        <th data-toggle="true">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $services as $key => $val)
                    @php
                        $slug = str_replace('-','',strtolower($val->code));

                        if($slug == 'et101')
                            $slug = 'pancard';

                        $route = 'bdm.'.$slug.'.index';
                    @endphp
                    <tr>
                        <td> <b>{{ $val->code }}</b></td>
                        <td> <b> {{ $val->name }} </b></td>
                        <td>
                            @php echo $val->descriptions @endphp
                        </td>
                        <td> <b>{{ $val->price }} </b></td>
                        <td> <a href="{{ route($route) }}"> View </a> </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <div class="text-right">
                                <ul class="pagination pagination-split m-t-30 m-b-0"></ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Services</b></h4>
            <hr>
            <table id="demo-foo-filtering" class="table table-striped toggle-circle m-b-0" data-page-size="15">
                <thead>
                    <tr>
                        <th data-toggle="">Code</th>
                        <th data-toggle="true">Name</th>
                        <th data-toggle="true">Description</th>
                        <th data-toggle="true">Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $services as $key => $val)
                    @php
                        $slug = str_replace('-','',strtolower($val->code));

                        if($slug == 'et101')
                            $slug = 'pancard';

                        $route = 'user.'.$slug.'.index';
                    @endphp
                    <tr>
                        <td> <b>{{ $val->code }}</b></td>
                        <td> <b> {{ $val->name }} </b></td>
                        <td>
                            @php echo $val->descriptions @endphp
                        </td>
                        <td> <b>{{ $val->price }} </b></td>
                        
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <div class="text-right">
                                <ul class="pagination pagination-split m-t-30 m-b-0"></ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endif
@endsection

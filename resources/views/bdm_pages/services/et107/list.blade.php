@extends('bdm_pages.common.header')
@section('content')

<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Income Tax Notice and Scrutiny') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('bdm.dashboard') }}">Dashboard</a></li>
            <li>List</li>
        </ol>
    </div>
    <div class="col-md-2"><a href="{{ route('bdm.et107.save') }}" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Create </a>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_suggest">
                            <thead>
                                <tr>
                                    <th> Track No. </th>
                                    <th> Pan Card </th>
                                    <th> Email-Id </th>
                                    <th> Mobile </th>
                                    <th> Status </th>
                                    <th> Date </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var page = {
    init: function(){
       this.common(); 
       this.customDataTable(); 
    },
    common: function(){
    },
    formattedDate: function(d){
      d = new Date(d);
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}-${month}-${year}`;
    },
    customDataTable: function(){
        $table_datatable = $('#table_suggest').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("bdm.et107.json") }}?type=user',
           columns: [
                { data: 'tracking_no' },
                { data: 'pancard_no' },
                { data: 'email_id' },
                { data: 'mobile_number' },
                { data: 'status' },
                { data: 'created_at' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(row.status == "pending")
                            return  '<span class="label label-table label-inverse">'+row.status+'</span>';          
                        else if(row.status == "rejected")
                            return  '<span class="label label-table label-danger">'+row.status+'</span>';          
                        else if(row.status == "process")
                            return  '<span class="label label-table label-warning">'+row.status+'</span>';
                        else
                            return  '<span class="label label-table label-success">'+row.status+'</span>';
                    },
                    "targets": 4,
                },
                {
                    "render": function (data, type, row) {
                        return page.formattedDate(data);   
                    },
                    "targets": 5,
                },
                {
                    "render": function (data, type, row) {
                        return  page.getActions(row);                
                    },
                    "targets": 6,
                },
            ],
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        
    },
    getActions: function(row){
        var html = '';
            html = `
                <div class="btn-group" role="group">
                <a href="{{ route('bdm.et107.save') }}?id=`+row.id+`"
                class="label label-primary waves-effect waves-light">View</a>

                <a href="{{ route('bdm.et107.print') }}?id=`+row.id+`"
                class="label label-primary waves-effect waves-light printPage">Print Receipt</a>
                </div>`;
            return html;
    },
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

@extends('bdm_pages.common.header')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h4 class="page-title">{{ __('GST Registration') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('bdm.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('bdm.et104.index') }}">List</a></li>
        </ol>
    </div>
</div>

@if(\Request()->id == "" && \Auth::User()->available_balance <= limiteServicePrice())
    @php echo errorAlert('Your Wallet balance is less') @endphp
@else
<form class="service_form" method="POST" action="{{ Request::url() }}" role="form" _lpchecked="1" data-parsley-validate="" enctype="multipart/form-data"> 
 @csrf
    <div class="row">
        @if(session('status'))
            @php echo successAlert(session('status')) @endphp
        @endif
        <div class="col-md-12">
            
            @include('include_service.et104.clientdetails')
            @include('include_service.et104.address')
            @include('include_service.et104.contact')
            
            @if(\Request()->id == "")
                @include('include_service.et104.documents')
            <div class="">
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-offset-10">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-danger" type="reset">Clear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            @else
                @include('include_service.et104.viewDocuments')
            @endif
        </div>
    </div>
</form>
@endif

@if(\Request()->id != "")
@include('include_service.logs.form')
@include('include_service.messages.form')
@endif
<script type="text/javascript">
var page = {
    init: function(){
        @if(\Request()->id != "")
            $(".service_form input, .service_form select, .service_form textarea").attr({
                "disabled":"disabled",
                "readonly":"readonly"   
            });
        @endif
       this.common();
    },
    common: function(){
        $(".message_tbl").DataTable({
            "lengthChange": false,
            "pageLength": 15,
            "orderable": true,
        });
        $('form').parsley();
    }
};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

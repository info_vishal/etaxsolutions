@extends('bdm_pages.common.header')

@section('content')
<link href="{{ asset('theme/px/plugins/summernote/dist/summernote.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/px/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
<div class="row">
    <div class="col-md-10">
        <h4 class="page-title">{{ __('Statements List') }}</h4>
        <ol class="breadcrumb">
            <li><a href="{{ route('bdm.dashboard') }}">Dashboard</a></li>
            <li>Statements List</li>
        </ol>
    </div>
</div>
<div class="row">
    @if(session('status'))
        @php echo successAlert(session('status')) @endphp
    @endif
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>{{ __('Statements  Listing') }}</b></h4>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable no-footer" id="table_downloads">
                            <thead>
                                <tr>
                                    <th> Descriptions </th>
                                    <th> Price </th>
                                    <th> Type </th>
                                    <th> Date </th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('theme/px/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/custombox/dist/legacy.min.js') }}"></script>
<script src="{{ asset('theme/px/plugins/summernote/dist/summernote.min.js') }}"></script>

<script type="text/javascript">
var page = {
    init: function(){
       this.customDataTable(); 
    },
    formattedDate: function(d){
      d = new Date(d);
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}-${month}-${year}`;
    },
    customDataTable: function(){

        $table_datatable = $('#table_downloads').DataTable({
           processing: true,
           serverSide: false,
           ajax: '{{ route("bdm.statements") }}?type=json',
           columns: [
                { data: 'descriptions' },
                { data: 'price' },
                { data: 'type' },
                { data: 'created_at' },
            ],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        if(data == "credited")
                            return  '<span class="label label-table label-success">Credited</span>';
                        else                
                            return  '<span class="label label-table label-danger">Debited</span>';                
                    },
                    "targets": 2,
                },
                {
                    "render": function (data, type, row) {
                        
                        return page.formattedDate(data);   
                    },
                    "targets": 3,
                }
            ],
            bFilter: true, 
            bInfo: true,
            "lengthChange": false,
            "pageLength": 15,
            "orderable": false,
            "ordering": false
        });
        
    },

};
$(document).ready(function(){
    page.init();
});
</script>
@endsection

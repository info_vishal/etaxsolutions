<div class="col-md-12"> 
    <h4 class="header-title"><b>{{ __('Documents') }}</b></h4>
    <hr/>
</div>  
@if(@$data)
<div class="form-row">                           
    <div class="form-group col-md-3">
        <label class="control-label"><a target="_blank" download href="{{ image_url($data->id_proof) }}" title="Download"><i class="fa fa-download"></i></a> Identity Proof  </label>

        <input type="file" class="form-control" name="id_proof" />

        @if($errors->has('id_proof'))
            @php 
                echo showError($errors->first('id_proof'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-3">
        <label class="control-label"><a target="_blank" download href="{{ image_url($data->address_proof) }}" title="Download"><i class="fa fa-download"></i></a> Address Proof </label>
       <input type="file" class="form-control" name="address_proof" />
        @if($errors->has('address_proof'))
            @php 
                echo showError($errors->first('address_proof'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-3">
        <label class="control-label"><a target="_blank" download href="{{ image_url($data->passport_photo) }}" title="Download"><i class="fa fa-download"></i></a> Passport Size Photo</label>
        <input type="file" class="form-control" name="passport_photo" />
        @if($errors->has('passport_photo'))
            @php 
                echo showError($errors->first('passport_photo'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-3">
        <label class="control-label"><a target="_blank" download href="{{ image_url($data->passbook_copy) }}" title="Download"><i class="fa fa-download"></i></a> Pass Book Copy </label>
        <input type="file" class="form-control" name="passbook_copy" />
        @if($errors->has('passbook_copy'))
            @php 
                echo showError($errors->first('passbook_copy'));
            @endphp
        @endif
    </div>
</div>
@else                         
<div class="form-row">                           
    <div class="form-group col-md-3">
        <label class="control-label">Identity Proof *</label>
        <input type="file" class="form-control" name="id_proof" required="" parsley-trigger="change" />
        @if($errors->has('id_proof'))
            @php 
                echo showError($errors->first('id_proof'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-3">
        <label class="control-label">Address Proof *</label>
        <input type="file" class="form-control" name="address_proof" required="" parsley-trigger="change" />
        @if($errors->has('address_proof'))
            @php 
                echo showError($errors->first('address_proof'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-3">
        <label class="control-label">Passport Size Photo *</label>
        <input type="file" class="form-control" name="passport_photo" required="" parsley-trigger="change" />
        @if($errors->has('passport_photo'))
            @php 
                echo showError($errors->first('passport_photo'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-3">
        <label class="control-label">Pass Book Copy *</label>
        <input type="file" class="form-control" name="passbook_copy" required="" parsley-trigger="change" />
        @if($errors->has('passbook_copy'))
            @php 
                echo showError($errors->first('passbook_copy'));
            @endphp
        @endif
    </div>
</div>
@endif

<div class="col-md-12"> 
    <h4 class="header-title"><b>{{ __('Nominee') }}</b></h4>
    <hr/>
</div>  
<div class="form-row">                           
    <div class="form-group col-md-6">
      <label for="inputPassword4123">Nominee Name</label>
      <input type="text" id="nominee_name" parsley-trigger="change" name="nominee_name" class="form-control" placeholder="Enter Nominee Name" value="{{ ($data)?$data->nominee_name:old('nominee_name') }}">
        @if($errors->has('nominee_name'))
            @php 
                echo showError($errors->first('nominee_name'));
            @endphp
        @endif
    </div>
    <div class="form-group col-md-6">
      <label for="inputState1235">Relation Ship</label>
      <select class="form-control select2" name="relationship" parsley-trigger="change" >
            <option value="">Select Relation Ship</option>
            @foreach(relationship() as $key => $val)
                @php
                    $select_stated = (old('relationship') == $key)?'selected':'';
                    if($select_stated == ""){
                        $select_stated = (@$data->relationship == $key)?'selected':'';
                    }
                @endphp
                <option {{ $select_stated }} value='{{ $key }}'>{{ $val }}</option>
            @endforeach
        </select>
        @if($errors->has('states'))
            @php 
                echo showError($errors->first('states'));
            @endphp
        @endif
    </div>
</div>

<div class="col-md-12"> 
    <h4 class="header-title"><b>{{ __('Bank Details') }}</b></h4>
    <hr/>
</div>  

<div class="form-row">
    <div class="form-group col-md-6">
      <label for="ifsc_code">IFSC CODE *</label>
      <div class="input-group">
        <span class="input-group-btn">
        <button type="button" class="btn waves-effect waves-light btn-primary searchBankDetails"><i class="fa fa-search"></i></button>
        </span>
        <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Enter IFSC CODE" required value="{{ (@$data)?@$data->ifsc_code:old('ifsc_code') }}">
    </div>
        @if($errors->has('ifsc_code'))
            @php 
                echo showError($errors->first('ifsc_code'));
            @endphp
        @endif
    <ul class="parsley-errors-list ifsc_error filled hide"><li class="parsley-required">Not Found</li></ul>
    </div>

    <div class="form-group col-md-6">
      <label for="bank_name">Bank Name *</label>
      <input type="text" id="bank_name" parsley-trigger="change" name="bank_name" class="form-control" required value="{{ (@$data)?@$data->bank_name:old('bank_name') }}">
        @if($errors->has('bank_name'))
            @php 
                echo showError($errors->first('bank_name'));
            @endphp
        @endif
    </div>

    <div class="col-md-12"></div>

    <div class="form-group col-md-6">
      <label for="bank_ac_no">Account Number *</label>
      <input type="text" id="bank_ac_no" parsley-trigger="change" name="bank_ac_no" class="form-control" min="0" required value="{{ (@$data)?@$data->bank_ac_no:old('bank_ac_no') }}">
        @if($errors->has('bank_ac_no'))
            @php 
                echo showError($errors->first('bank_ac_no'));
            @endphp
        @endif
    </div>

    <div class="form-group col-md-6">
      <label for="bank_ac_holder">Account Holder Name *</label>
      <input type="text" id="bank_ac_holder" parsley-trigger="change" name="bank_ac_holder" class="form-control"  required  value="{{ (@$data)?@$data->bank_ac_holder:old('bank_ac_holder') }}">
        @if($errors->has('bank_ac_holder'))
            @php 
                echo showError($errors->first('bank_ac_holder'));
            @endphp
        @endif
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.searchBankDetails').on('click',function(){
            var ifsc = $("#ifsc_code").val();
            $.ajax({
                url: 'https://ifsc.razorpay.com/'+ifsc,
                dataType: 'JSON',
                type: 'GET',
                success: function(response,textStatus, request){
                    $(".ifsc_error").addClass("hide");
                    $("#bank_name").val(response.BANK);
                    //$("#bank_branch").val(response.BRANCH);
                },
                error: function(response,textStatus, request){
                    $(".ifsc_error").removeClass("hide");
                    $(".ifsc_error li").text(request);
                    $("#bank_name").val('');
                }
            });
        })
    });
</script>
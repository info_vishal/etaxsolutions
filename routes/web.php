<?php


Route::get('/', "FrontController@index")->name('root');
Route::get('/about', "FrontController@about")->name('root.about');
Route::get('/downloads', "FrontController@downloads")->name('root.downloads');
Route::get('/services', "FrontController@services")->name('root.services');
Route::get('/ourClients', "FrontController@ourClients")->name('root.ourClients');
Route::get('/contactUs', "FrontController@contactUs")->name('root.contactUs');
Route::get('/testimonial', "FrontController@testimonial")->name('root.testimonial');
Route::match(['get','post'],'/contactUs/email', "FrontController@email")->name('root.contactUs.email');
Route::get('/become-a-partner', "FrontController@becomeApartner")->name('root.becomeApartner');
Route::get('/faq', "FrontController@faq")->name('root.faq');
Route::get('/why', "FrontController@why")->name('root.why');
Route::get('/gst', "FrontController@gst")->name('root.gst');
Route::get('/income-tax', "FrontController@incomeTax")->name('root.incomeTax');
Route::get('/anyother', "FrontController@anyOther")->name('root.anyother');
Route::get('/cron', "FrontController@cron")->name('root.cron');

Route::match(['get','post'],'/search-service', "FrontController@searchService")->name('root.searchService');

Route::match(['get','post'], '/suggest/invitation/{token}', 'SuggestController@emailInvitation')->name('suggest.emailInvitation');


Auth::routes();
Route::group(['middleware' => ['auth']], function () {

	//CommonControllers
	Route::match(['get','post'], '/districtAjax', 'CommonController@districtAjax')->name('common.districtAjax');
	Route::match(['get','post'], '/talukaAjax', 'CommonController@talukaAjax')->name('common.talukaAjax');
	Route::match(['get','post'], '/markNotificationAsRead', 'CommonController@markNotificationAsRead')->name('common.markNotificationAsRead');

	Route::match(['get','put'], '/my-profile', 'CommonController@myProfile')->name('common.myProfile');
	Route::match(['get','put'], '/change-password', 'CommonController@changePassword')->name('common.changePassword');
	Route::match(['get'], '/redeem', 'CommonController@redeem')->name('common.user.redeem');

	//Recharge
	Route::match(['get','post'], '/recharge/json', 'CommonController@rechargeJson')->name('common.rechargeJson');
	Route::match(['get','post'], '/redeem/json', 'CommonController@redeemJson')->name('common.redeemJson');
	
	//Suggest Json
	Route::match(['get','post'], '/suggest/json', 'SuggestController@json')->name('suggest.json');

	// Admin Group
	Route::group(['middleware' => ['role'],'prefix' => 'admin'], function () {

		//Redeem
		Route::match(['get'], '/redeem', 'RedeemController@index')->name('admin.redeem');
		Route::match(['get','post'], '/redeem/save/{id?}', 'RedeemController@save')->name('admin.redeem.save');

		// Admin Dashboard
		Route::match(['get'], '/', 'Admin\Dashboard@index')->name('admin.dashboard');

		// Latest News
		Route::match(['get'], '/latestNews', 'Admin\Dashboard@listLatestNews')->name('admin.listLatestNews');
		Route::match(['get','post'], '/latestNews/save/{id?}', 'Admin\Dashboard@saveLatestNews')->name('admin.latestNews.create');

		// Taluka
		Route::match(['get'], '/taluka', 'Admin\Dashboard@listTaluka')->name('admin.listTaluka');
		Route::match(['get','post'], '/taluka/save/{id?}', 'Admin\Dashboard@saveTaluka')->name('admin.taluka.save');

		Route::match(['get'], '/talukaJson', 'Admin\Dashboard@talukaJson')->name('admin.talukaJson');

		// District
		Route::match(['get'], '/district', 'Admin\Dashboard@listDistrict')->name('admin.listDistrict');
		Route::match(['get','post'], '/district/save/{id?}', 'Admin\Dashboard@saveDistrict')->name('admin.district.save');
		Route::match(['get'], '/districtJson', 'Admin\Dashboard@districtJson')->name('admin.districtJson');

		//Levels
		Route::match(['get','post'], '/levels', 'Admin\LevelsController@list')->name('admin.listLevels');
		Route::match(['get','post','put'], '/levels/save/{id?}', 'Admin\LevelsController@save')->name('admin.levels.save');
		Route::match(['get'], '/levels/json', 'Admin\LevelsController@json')->name('admin.levels.json');
		Route::match(['get','post'], '/levels/search', 'Admin\LevelsController@search')->name('admin.levels.search');

		//Users
		Route::match(['get'], '/users', 'Admin\UsersController@list')->name('admin.listUsers');
		Route::match(['get','post','put'], '/users/save/{id?}', 'Admin\UsersController@save')->name('admin.users.save');
		
		Route::match(['get','post','put'], '/users/staff/save/{id?}', 'Admin\UsersController@save')->name('admin.users.staff.save');

		Route::match(['get'], '/users/json', 'Admin\UsersController@json')->name('admin.users.json');

		//Recharge
		Route::match(['get'], '/recharge', 'RechargeController@index')->name('admin.rechargeIndex');
		Route::match(['get','put'], '/recharge/save/{id?}', 'RechargeController@save')->name('admin.recharge.save');

		//EtaxNotifications
		Route::match(['get'], '/notification', 'EtaxNotifyController@index')->name('admin.notify');
		Route::match(['get','post'], '/notification/save', 'EtaxNotifyController@save')->name('admin.notify.save');
		Route::match(['get'], '/notification/json', 'EtaxNotifyController@json')->name('admin.notify.json');

		//Downloads
		Route::match(['get'], '/downloads', 'DownloadsController@index')->name('admin.downloadsIndex');
		Route::match(['get','post'], '/downloads/centerSave', 'DownloadsController@centerSave')->name('admin.downloads.centerSave');
		Route::match(['get','post'], '/downloads/fileSave', 'DownloadsController@fileSave')->name('admin.downloads.fileSave');
		Route::match(['get'], '/downloads/fileJson', 'DownloadsController@fileJson')->name('admin.downloads.fileJson');

		Route::match(['get','post'], '/downloads-client', "DownloadsController@client")->name('admin.downloads.client');
		
		// SuggestUser
		Route::match(['get'], '/suggest', 'SuggestController@adminIndex')->name('admin.suggest.adminIndex');
		Route::match(['get','post','put'], '/suggest/save/{id?}', 'SuggestController@adminSave')->name('admin.suggest.save');
	});

	// BDM Group
	Route::group(['middleware' => ['role'],'prefix' => 'bdm'], function () {

		// BDM Dashboard
		Route::match(['get'], '/', 'Bdm\Dashboard@index')->name('bdm.dashboard');
		Route::group(['middleware' => 'account.status'],function () {
			//Levels
			Route::match(['get','post'], '/levels', 'Bdm\LevelsController@index')->name('bdm.sell.levels');
			Route::match(['get'], '/levels/archive', 'Bdm\LevelsController@archive')->name('bdm.levels.view');
			Route::match(['get'], '/levels/json', 'Bdm\LevelsController@json')->name('bdm.levels.json');
			Route::match(['get'], '/levels/myArchived', 'Bdm\LevelsController@myArchived')->name('bdm.levels.myArchived');
			Route::match(['get'], '/levels/view/{id?}', 'Bdm\LevelsController@view')->name('bdm.levels.view');
			
			//Users
			Route::match(['get'], '/users', 'Bdm\UsersController@list')->name('bdm.listUsers');
			Route::match(['get','post','put'], '/users/save/{id?}', 'Bdm\UsersController@save')->name('bdm.users.save');
			Route::match(['get'], '/users/json', 'Bdm\UsersController@json')->name('bdm.users.json');

			//Statements
			Route::match(['get'], '/statements', 'CommonController@statements')->name('bdm.statements');

			//Pan card
			Route::match(['get'], '/pancard', 'Services\PanCardController@index')->name('bdm.pancard.index');
			Route::match(['get','post','put'], '/pancard/save/{id?}', 'Services\PanCardController@save')->name('bdm.pancard.save');

		});
	});

	// User Group
	Route::group(['middleware' => ['role'],'prefix' => 'user'], function () {

		// user Dashboard
		Route::match(['get'], '/', 'User\Dashboard@index')->name('user.dashboard');

		Route::group(['middleware' => 'account.status'],function () {			

			//Levels
			Route::match(['get'], '/levels', 'User\LevelsController@index')->name('user.levels');
			Route::match(['get'], '/levels/json', 'User\LevelsController@json')->name('user.levels.json');
			Route::match(['get'], '/levels/view/{id?}', 'User\LevelsController@view')->name('user.levels.view');
			
			//Users
			Route::match(['get'], '/users', 'User\UsersController@list')->name('user.listUsers');
			Route::match(['get','post','put'], '/users/save/{id?}', 'User\UsersController@save')->name('user.users.save');
			Route::match(['get'], '/users/json', 'User\UsersController@json')->name('user.users.json');

			Route::match(['get'], '/recharge', 'RechargeController@userList')->name('user.rechargeIndex');
			Route::match(['get','post'], '/recharge/save/{id?}', 'RechargeController@save')->name('user.recharge.save');

			//Downloads
			Route::match(['get'], '/downloads', 'DownloadsController@listDownload')->name('user.listDownload');

			//EtaxNotifications
			Route::match(['get','post'], '/notification', 'EtaxNotifyController@viewAll')->name('user.notification.viewAll');

			//Statements
			Route::match(['get'], '/statements', 'CommonController@statements')->name('user.statements');

			//SuggestUser
			Route::match(['get'], '/suggest', 'SuggestController@userIndex')->name('user.suggest.userIndex');
			Route::match(['get','post','put'], '/suggest/save/{id?}', 'SuggestController@userSave')->name('user.suggest.save');

			//Redeem
			Route::match(['get'], '/redeem', 'RedeemController@userList')->name('user.redeem');
			Route::match(['get'], '/redeem/save', 'RedeemController@userSave')->name('user.redeem.save');

		});
	});


	Route::group(['prefix' => 'admin/services'],function () {
		Route::match(['get','post'], 'message/msgSave', 'Services\MessageLogs@msgSave')->name('admin.msgLog.msgSave');


		//PrintOut
		Route::match(['get'], 'pancard/printout/{id?}', 'Services\PanCardController@printout')->name('admin.pancard.printout');
		Route::match(['get'], 'et102/printout/{id?}', 'Services\ET102Controller@printout')->name('admin.et102.printout');
		Route::match(['get'], 'et103/printout/{id?}', 'Services\ET103Controller@printout')->name('admin.et103.printout');
		Route::match(['get'], 'et104/printout/{id?}', 'Services\ET104Controller@printout')->name('admin.et104.printout');
		Route::match(['get'], 'et105/printout/{id?}', 'Services\ET105Controller@printout')->name('admin.et105.printout');
		Route::match(['get'], 'et106/printout/{id?}', 'Services\ET106Controller@printout')->name('admin.et106.printout');
		Route::match(['get'], 'et107/printout/{id?}', 'Services\ET107Controller@printout')->name('admin.et107.printout');
		Route::match(['get'], 'et108/printout/{id?}', 'Services\ET108Controller@printout')->name('admin.et108.printout');
		Route::match(['get'], 'et109/printout/{id?}', 'Services\ET109Controller@printout')->name('admin.et109.printout');
		Route::match(['get'], 'et110/printout/{id?}', 'Services\ET110Controller@printout')->name('admin.et110.printout');
		Route::match(['get'], 'et111/printout/{id?}', 'Services\ET111Controller@printout')->name('admin.et111.printout');
		Route::match(['get'], 'et112/printout/{id?}', 'Services\ET112Controller@printout')->name('admin.et112.printout');
		Route::match(['get'], 'et113/printout/{id?}', 'Services\ET113Controller@printout')->name('admin.et113.printout');
		Route::match(['get'], 'et114/printout/{id?}', 'Services\ET114Controller@printout')->name('admin.et114.printout');


		//Pan card
		Route::match(['get'], 'pancard', 'Services\PanCardController@index')->name('admin.pancard.index');
		Route::match(['get','post','put'], 'pancard/save/{id?}', 'Services\PanCardController@save')->name('admin.pancard.save');
		Route::match(['get'], 'pancard/json', 'Services\PanCardController@json')->name('admin.pancard.json');
		Route::match(['get'], 'pancard/print/{id?}', 'Services\PanCardController@print')->name('admin.pancard.print');

		Route::match(['get'], 'et102', 'Services\ET102Controller@index')->name('admin.et102.index');
		Route::match(['get','post','put'], 'et102/save/{id?}', 'Services\ET102Controller@save')->name('admin.et102.save');
		Route::match(['get'], 'et102/json', 'Services\ET102Controller@json')->name('admin.et102.json');
		Route::match(['get'], 'et102/print/{id?}', 'Services\ET102Controller@print')->name('admin.et102.print');

		Route::match(['get'], 'et103', 'Services\ET103Controller@index')->name('admin.et103.index');
		Route::match(['get','post','put'], 'et103/save/{id?}', 'Services\ET103Controller@save')->name('admin.et103.save');
		Route::match(['get'], 'et103/json', 'Services\ET103Controller@json')->name('admin.et103.json');
		Route::match(['get'], 'et103/print/{id?}', 'Services\ET103Controller@print')->name('admin.et103.print');

		Route::match(['get'], 'et104', 'Services\ET104Controller@index')->name('admin.et104.index');
		Route::match(['get'], 'et104', 'Services\ET104Controller@index')->name('admin.et104.index');
		Route::match(['get','post','put'], 'et104/save/{id?}', 'Services\ET104Controller@save')->name('admin.et104.save');
		Route::match(['get'], 'et104/json', 'Services\ET104Controller@json')->name('admin.et104.json');
		Route::match(['get'], 'et104/print/{id?}', 'Services\ET104Controller@print')->name('admin.et104.print');

		Route::match(['get'], 'et105', 'Services\ET105Controller@index')->name('admin.et105.index');
		Route::match(['get','post','put'], 'et105/save/{id?}', 'Services\ET105Controller@save')->name('admin.et105.save');
		Route::match(['get'], 'et105/json', 'Services\ET105Controller@json')->name('admin.et105.json');
		Route::match(['get'], 'et105/print/{id?}', 'Services\ET105Controller@print')->name('admin.et105.print');

		Route::match(['get'], 'et106', 'Services\ET106Controller@index')->name('admin.et106.index');
		Route::match(['get','post','put'], 'et106/save/{id?}', 'Services\ET106Controller@save')->name('admin.et106.save');
		Route::match(['get'], 'et106/json', 'Services\ET106Controller@json')->name('admin.et106.json');
		Route::match(['get'], 'et106/print/{id?}', 'Services\ET106Controller@print')->name('admin.et106.print');

		Route::match(['get'], 'et107', 'Services\ET107Controller@index')->name('admin.et107.index');
		Route::match(['get','post','put'], 'et107/save/{id?}', 'Services\ET107Controller@save')->name('admin.et107.save');
		Route::match(['get'], 'et107/json', 'Services\ET107Controller@json')->name('admin.et107.json');
		Route::match(['get'], 'et107/print/{id?}', 'Services\ET107Controller@print')->name('admin.et107.print');

		Route::match(['get'], 'et108', 'Services\ET108Controller@index')->name('admin.et108.index');
		Route::match(['get','post','put'], 'et108/save/{id?}', 'Services\ET108Controller@save')->name('admin.et108.save');
		Route::match(['get'], 'et108/json', 'Services\ET108Controller@json')->name('admin.et108.json');
		Route::match(['get'], 'et108/print/{id?}', 'Services\ET108Controller@print')->name('admin.et108.print');

		Route::match(['get'], 'et109', 'Services\ET109Controller@index')->name('admin.et109.index');
		Route::match(['get','post','put'], 'et109/save/{id?}', 'Services\ET109Controller@save')->name('admin.et109.save');
		Route::match(['get'], 'et109/json', 'Services\ET109Controller@json')->name('admin.et109.json');
		Route::match(['get'], 'et109/print/{id?}', 'Services\ET109Controller@print')->name('admin.et109.print');

		Route::match(['get'], 'et110', 'Services\ET110Controller@index')->name('admin.et110.index');
		Route::match(['get','post','put'], 'et110/save/{id?}', 'Services\ET110Controller@save')->name('admin.et110.save');
		Route::match(['get'], 'et110/json', 'Services\ET110Controller@json')->name('admin.et110.json');
		Route::match(['get'], 'et110/print/{id?}', 'Services\ET110Controller@print')->name('admin.et110.print');

		Route::match(['get'], 'et111', 'Services\ET111Controller@index')->name('admin.et111.index');
		Route::match(['get','post','put'], 'et111/save/{id?}', 'Services\ET111Controller@save')->name('admin.et111.save');
		Route::match(['get'], 'et111/json', 'Services\ET111Controller@json')->name('admin.et111.json');
		Route::match(['get'], 'et111/print/{id?}', 'Services\ET111Controller@print')->name('admin.et111.print');

		Route::match(['get'], 'et112', 'Services\ET112Controller@index')->name('admin.et112.index');
		Route::match(['get','post','put'], 'et112/save/{id?}', 'Services\ET112Controller@save')->name('admin.et112.save');
		Route::match(['get'], 'et112/json', 'Services\ET112Controller@json')->name('admin.et112.json');
		Route::match(['get'], 'et112/print/{id?}', 'Services\ET112Controller@print')->name('admin.et112.print');

		Route::match(['get'], 'et113', 'Services\ET113Controller@index')->name('admin.et113.index');
		Route::match(['get','post','put'], 'et113/save/{id?}', 'Services\ET113Controller@save')->name('admin.et113.save');
		Route::match(['get'], 'et113/json', 'Services\ET113Controller@json')->name('admin.et113.json');
		Route::match(['get'], 'et113/print/{id?}', 'Services\ET113Controller@print')->name('admin.et113.print');

		Route::match(['get'], 'et114', 'Services\ET114Controller@index')->name('admin.et114.index');
		Route::match(['get','post','put'], 'et114/save/{id?}', 'Services\ET114Controller@save')->name('admin.et114.save');
		Route::match(['get'], 'et114/json', 'Services\ET114Controller@json')->name('admin.et114.json');
		Route::match(['get'], 'et114/print/{id?}', 'Services\ET114Controller@print')->name('admin.et114.print');
	});

	Route::group(['prefix' => 'user/services'],function () {
		// Message Logs
		Route::match(['get','post'], 'message/msgSave', 'Services\MessageLogs@msgSave')->name('user.msgLog.msgSave');

		//ET-101
		Route::match(['get'], 'pancard', 'Services\PanCardController@index')->name('user.pancard.index');
		Route::match(['get','post','put'], 'pancard/save/{id?}', 'Services\PanCardController@save')->name('user.pancard.save');
		Route::match(['get'], 'pancard/json', 'Services\PanCardController@json')->name('user.pancard.json');
		Route::match(['get'], 'pancard/print/{id?}', 'Services\PanCardController@print')->name('user.pancard.print');

		//ET-102
		Route::match(['get'], 'et102', 'Services\ET102Controller@index')->name('user.et102.index');
		Route::match(['get','post','put'], 'et102/save/{id?}', 'Services\ET102Controller@save')->name('user.et102.save');
		Route::match(['get'], 'et102/json', 'Services\ET102Controller@json')->name('user.et102.json');
		Route::match(['get'], 'et102/print/{id?}', 'Services\ET102Controller@print')->name('user.et102.print');

		//ET-103
		Route::match(['get'], 'et103', 'Services\ET103Controller@index')->name('user.et103.index');
		Route::match(['get','post','put'], 'et103/save/{id?}', 'Services\ET103Controller@save')->name('user.et103.save');
		Route::match(['get'], 'et103/json', 'Services\ET103Controller@json')->name('user.et103.json');
		Route::match(['get'], 'et103/print/{id?}', 'Services\ET103Controller@print')->name('user.et103.print');

		//ET-104
		Route::match(['get'], 'et104', 'Services\ET104Controller@index')->name('user.et104.index');
		Route::match(['get','post','put'], 'et104/save/{id?}', 'Services\ET104Controller@save')->name('user.et104.save');
		Route::match(['get'], 'et104/json', 'Services\ET104Controller@json')->name('user.et104.json');
		Route::match(['get'], 'et104/print/{id?}', 'Services\ET104Controller@print')->name('user.et104.print');

		//ET-105
		Route::match(['get'], 'et105', 'Services\ET105Controller@index')->name('user.et105.index');
		Route::match(['get','post','put'], 'et105/save/{id?}', 'Services\ET105Controller@save')->name('user.et105.save');
		Route::match(['get'], 'et105/json', 'Services\ET105Controller@json')->name('user.et105.json');
		Route::match(['get'], 'et105/print/{id?}', 'Services\ET105Controller@print')->name('user.et105.print');

		//ET-106
		Route::match(['get'], 'et106', 'Services\ET106Controller@index')->name('user.et106.index');
		Route::match(['get','post','put'], 'et106/save/{id?}', 'Services\ET106Controller@save')->name('user.et106.save');
		Route::match(['get'], 'et106/json', 'Services\ET106Controller@json')->name('user.et106.json');
		Route::match(['get'], 'et106/print/{id?}', 'Services\ET106Controller@print')->name('user.et106.print');

		//Et-107
		Route::match(['get'], 'et107', 'Services\ET107Controller@index')->name('user.et107.index');
		Route::match(['get','post','put'], 'et107/save/{id?}', 'Services\ET107Controller@save')->name('user.et107.save');
		Route::match(['get'], 'et107/json', 'Services\ET107Controller@json')->name('user.et107.json');
		Route::match(['get'], 'et107/print/{id?}', 'Services\ET107Controller@print')->name('user.et107.print');

		Route::match(['get'], 'et108', 'Services\ET108Controller@index')->name('user.et108.index');
		Route::match(['get','post','put'], 'et108/save/{id?}', 'Services\ET108Controller@save')->name('user.et108.save');
		Route::match(['get'], 'et108/json', 'Services\ET108Controller@json')->name('user.et108.json');
		Route::match(['get'], 'et108/print/{id?}', 'Services\ET108Controller@print')->name('user.et108.print');

		//Et-109
		Route::match(['get'], 'et109', 'Services\ET109Controller@index')->name('user.et109.index');
		Route::match(['get','post','put'], 'et109/save/{id?}', 'Services\ET109Controller@save')->name('user.et109.save');
		Route::match(['get'], 'et109/json', 'Services\ET109Controller@json')->name('user.et109.json');
		Route::match(['get'], 'et109/print/{id?}', 'Services\ET109Controller@print')->name('user.et109.print');

		//Et-110
		Route::match(['get'], 'et110', 'Services\ET110Controller@index')->name('user.et110.index');
		Route::match(['get','post','put'], 'et110/save/{id?}', 'Services\ET110Controller@save')->name('user.et110.save');
		Route::match(['get'], 'et110/json', 'Services\ET110Controller@json')->name('user.et110.json');
		Route::match(['get'], 'et110/print/{id?}', 'Services\ET110Controller@print')->name('user.et110.print');

		//Et-111
		Route::match(['get'], 'et111', 'Services\ET111Controller@index')->name('user.et111.index');
		Route::match(['get','post','put'], 'et111/save/{id?}', 'Services\ET111Controller@save')->name('user.et111.save');
		Route::match(['get'], 'et111/json', 'Services\ET111Controller@json')->name('user.et111.json');
		Route::match(['get'], 'et111/print/{id?}', 'Services\ET111Controller@print')->name('user.et111.print');

		//Et-112
		Route::match(['get'], 'et112', 'Services\ET112Controller@index')->name('user.et112.index');
		Route::match(['get','post','put'], 'et112/save/{id?}', 'Services\ET112Controller@save')->name('user.et112.save');
		Route::match(['get'], 'et112/json', 'Services\ET112Controller@json')->name('user.et112.json');
		Route::match(['get'], 'et112/print/{id?}', 'Services\ET112Controller@print')->name('user.et112.print');

		//Et-113
		Route::match(['get'], 'et113', 'Services\ET113Controller@index')->name('user.et113.index');
		Route::match(['get','post','put'], 'et113/save/{id?}', 'Services\ET113Controller@save')->name('user.et113.save');
		Route::match(['get'], 'et113/json', 'Services\ET113Controller@json')->name('user.et113.json');
		Route::match(['get'], 'et113/print/{id?}', 'Services\ET113Controller@print')->name('user.et113.print');

		//Et-114
		Route::match(['get'], 'et114', 'Services\ET114Controller@index')->name('user.et114.index');
		Route::match(['get','post','put'], 'et114/save/{id?}', 'Services\ET114Controller@save')->name('user.et114.save');
		Route::match(['get'], 'et114/json', 'Services\ET114Controller@json')->name('user.et114.json');
		Route::match(['get'], 'et114/print/{id?}', 'Services\ET114Controller@print')->name('user.et114.print');

	});

	Route::group(['prefix' => 'bdm/services'],function () {
		// Message Logs
		Route::match(['get','post'], 'message/msgSave', 'Services\MessageLogs@msgSave')->name('bdm.msgLog.msgSave');

				//ET-101
		Route::match(['get'], 'pancard', 'Services\PanCardController@index')->name('bdm.pancard.index');
		Route::match(['get','post','put'], 'pancard/save/{id?}', 'Services\PanCardController@save')->name('bdm.pancard.save');
		Route::match(['get'], 'pancard/json', 'Services\PanCardController@json')->name('bdm.pancard.json');
		Route::match(['get'], 'pancard/print/{id?}', 'Services\PanCardController@print')->name('bdm.pancard.print');

		//ET-102
		Route::match(['get'], 'et102', 'Services\ET102Controller@index')->name('bdm.et102.index');
		Route::match(['get','post','put'], 'et102/save/{id?}', 'Services\ET102Controller@save')->name('bdm.et102.save');
		Route::match(['get'], 'et102/json', 'Services\ET102Controller@json')->name('bdm.et102.json');
		Route::match(['get'], 'et102/print/{id?}', 'Services\ET102Controller@print')->name('bdm.et102.print');

		//ET-103
		Route::match(['get'], 'et103', 'Services\ET103Controller@index')->name('bdm.et103.index');
		Route::match(['get','post','put'], 'et103/save/{id?}', 'Services\ET103Controller@save')->name('bdm.et103.save');
		Route::match(['get'], 'et103/json', 'Services\ET103Controller@json')->name('bdm.et103.json');
		Route::match(['get'], 'et103/print/{id?}', 'Services\ET103Controller@print')->name('bdm.et103.print');

		//ET-104
		Route::match(['get'], 'et104', 'Services\ET104Controller@index')->name('bdm.et104.index');
		Route::match(['get','post','put'], 'et104/save/{id?}', 'Services\ET104Controller@save')->name('bdm.et104.save');
		Route::match(['get'], 'et104/json', 'Services\ET104Controller@json')->name('bdm.et104.json');
		Route::match(['get'], 'et104/print/{id?}', 'Services\ET104Controller@print')->name('bdm.et104.print');

		//ET-105
		Route::match(['get'], 'et105', 'Services\ET105Controller@index')->name('bdm.et105.index');
		Route::match(['get','post','put'], 'et105/save/{id?}', 'Services\ET105Controller@save')->name('bdm.et105.save');
		Route::match(['get'], 'et105/json', 'Services\ET105Controller@json')->name('bdm.et105.json');
		Route::match(['get'], 'et105/print/{id?}', 'Services\ET105Controller@print')->name('bdm.et105.print');

		//ET-106
		Route::match(['get'], 'et106', 'Services\ET106Controller@index')->name('bdm.et106.index');
		Route::match(['get','post','put'], 'et106/save/{id?}', 'Services\ET106Controller@save')->name('bdm.et106.save');
		Route::match(['get'], 'et106/json', 'Services\ET106Controller@json')->name('bdm.et106.json');
		Route::match(['get'], 'et106/print/{id?}', 'Services\ET106Controller@print')->name('bdm.et106.print');

		//Et-107
		Route::match(['get'], 'et107', 'Services\ET107Controller@index')->name('bdm.et107.index');
		Route::match(['get','post','put'], 'et107/save/{id?}', 'Services\ET107Controller@save')->name('bdm.et107.save');
		Route::match(['get'], 'et107/json', 'Services\ET107Controller@json')->name('bdm.et107.json');
		Route::match(['get'], 'et107/print/{id?}', 'Services\ET107Controller@print')->name('bdm.et107.print');

		Route::match(['get'], 'et108', 'Services\ET108Controller@index')->name('bdm.et108.index');
		Route::match(['get','post','put'], 'et108/save/{id?}', 'Services\ET108Controller@save')->name('bdm.et108.save');
		Route::match(['get'], 'et108/json', 'Services\ET108Controller@json')->name('bdm.et108.json');
		Route::match(['get'], 'et108/print/{id?}', 'Services\ET108Controller@print')->name('bdm.et108.print');

		//Et-109
		Route::match(['get'], 'et109', 'Services\ET109Controller@index')->name('bdm.et109.index');
		Route::match(['get','post','put'], 'et109/save/{id?}', 'Services\ET109Controller@save')->name('bdm.et109.save');
		Route::match(['get'], 'et109/json', 'Services\ET109Controller@json')->name('bdm.et109.json');
		Route::match(['get'], 'et109/print/{id?}', 'Services\ET109Controller@print')->name('bdm.et109.print');

		//Et-110
		Route::match(['get'], 'et110', 'Services\ET110Controller@index')->name('bdm.et110.index');
		Route::match(['get','post','put'], 'et110/save/{id?}', 'Services\ET110Controller@save')->name('bdm.et110.save');
		Route::match(['get'], 'et110/json', 'Services\ET110Controller@json')->name('bdm.et110.json');
		Route::match(['get'], 'et110/print/{id?}', 'Services\ET110Controller@print')->name('bdm.et110.print');

		//Et-111
		Route::match(['get'], 'et111', 'Services\ET111Controller@index')->name('bdm.et111.index');
		Route::match(['get','post','put'], 'et111/save/{id?}', 'Services\ET111Controller@save')->name('bdm.et111.save');
		Route::match(['get'], 'et111/json', 'Services\ET111Controller@json')->name('bdm.et111.json');
		Route::match(['get'], 'et111/print/{id?}', 'Services\ET111Controller@print')->name('bdm.et111.print');

		//Et-112
		Route::match(['get'], 'et112', 'Services\ET112Controller@index')->name('bdm.et112.index');
		Route::match(['get','post','put'], 'et112/save/{id?}', 'Services\ET112Controller@save')->name('bdm.et112.save');
		Route::match(['get'], 'et112/json', 'Services\ET112Controller@json')->name('bdm.et112.json');
		Route::match(['get'], 'et112/print/{id?}', 'Services\ET112Controller@print')->name('bdm.et112.print');

		//Et-113
		Route::match(['get'], 'et113', 'Services\ET113Controller@index')->name('bdm.et113.index');
		Route::match(['get','post','put'], 'et113/save/{id?}', 'Services\ET113Controller@save')->name('bdm.et113.save');
		Route::match(['get'], 'et113/json', 'Services\ET113Controller@json')->name('bdm.et113.json');
		Route::match(['get'], 'et113/print/{id?}', 'Services\ET113Controller@print')->name('bdm.et113.print');

		//Et-114
		Route::match(['get'], 'et114', 'Services\ET114Controller@index')->name('bdm.et114.index');
		Route::match(['get','post','put'], 'et114/save/{id?}', 'Services\ET114Controller@save')->name('bdm.et114.save');
		Route::match(['get'], 'et114/json', 'Services\ET114Controller@json')->name('bdm.et114.json');
		Route::match(['get'], 'et114/print/{id?}', 'Services\ET114Controller@print')->name('bdm.et114.print');

	});

});

Route::get('flush', function(){
    request()->session()->flush();
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});